APS Manual Reader


For using APS Online Manual :

-Install ADOBE Acrobat Reader by running "ar32e301.exe" file from "manual" directory present in the APS Designer 4.0 CD.

-Double Click the aps_manual.pdf file in Windows Explorer to view it.
or load "Acrobat Reader 3.01" from "Adobe Acrobat group" and open apsdes40_manual.pdf file in it.
