package com.example.StaticIndia;

//Added by me


import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener{
	
	private static Context context;
	private GestureDetector gestureDetector;
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_triangle=new int[1];
	private int[] vbo_triangle=new int[1];

	private int[] vao_rectangle=new int[1];
	private int[] vbo_rectangle=new int[1];
	
	
			private int[] vao_i1=new int[1];
			private int[] vao_i2=new int[1];
			private int[] vao_n=new int[1];
			private int[] vao_d=new int[1];
			private int[] vao_a=new int[1];
			private int[] vao_line1=new int[1];
			private int[] vao_line2=new int[1];
			private int[] vao_line3=new int[1];
			private int[] vbo_position_i1=new int[1];
			private int[] vbo_position_i2=new int[1];
			private int[] vbo_position_n=new int[1];
			private int[] vbo_position_d=new int[1];
			private int[] vbo_position_a=new int[1];
			private int[] vbo_position_line1=new int[1];
			private int[] vbo_position_line2=new int[1];
			private int[] vbo_position_line3=new int[1];
			private int[] vbo_color_i1=new int[1];
			private int[] vbo_color_i2=new int[1];
			private int[] vbo_color_n=new int[1];
			private int[] vbo_color_d=new int[1];
			private int[] vbo_color_a=new int[1];
			private int[] vbo_color_line1=new int[1];
			private int[] vbo_color_line2=new int[1];
			private int[] vbo_color_line3=new int[1];
	
	
	
	private int mvpUniform;
	private float[] perspectiveProjectionMatrix=new float[16];


	public GLESView(Context drawingContext){
		super(drawingContext);
		context=drawingContext;
		gestureDetector=new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityx,float velocityy)
	{
		
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distancex,float distancey)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR"+version);
		String version1=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR"+version1);
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	//Our custom methods
	
	private void initialize()
	{

		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"gl_Position=u_mvp_matrix * vPosition;"+
			"out_color=vColor;"+
			"}"
		);

		GLES32.glShaderSource(gVertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(gVertexShaderObject);
				


		//Error checking
		int[] iShaderCompileStatus=new int[1];
		int[] iInfoLogLength=new int[1];
		String szInfoLog=null;
		GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("iRTR:VertexShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		

		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor=out_color;"+
			"}"
		);

		GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(gFragmentShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("iRTR:FragmentShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject,gVertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gFragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");	
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR,"vColor");	
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);

	if (iShaderLinkStatus[0] == GLES32.GL_FALSE) {
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		if (iInfoLogLength[0] > 0) {
			szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);

			System.out.println("iRTR:shaderProgramObject"+szInfoLog);
			uninitialize();
			System.exit(0);	
			
		}
	}

		mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		final float[] i1=new float[]
		{
			-0.70f, 0.80f, 0.0f,

			- 0.72f, 0.80f, 0.0f,

			- 0.72f,-0.80f, 0.0f,

			- 0.70f, -0.80f, 0.0f,
		};
		
		final float[] i2=new float[]
		{
			0.20f, 0.80f, 0.0f,

			0.22f, 0.80f, 0.0f,

			0.22f, -0.80f, 0.0f,

			0.20f, -0.80f, 0.0f
		};	
		
		final float[] n=new float[]
		{
			-0.52f, 0.80f, 0.0f,

			- 0.50f, 0.80f, 0.0f,

			- 0.50f, -0.80f, 0.0f,

			- 0.52f, -0.80f, 0.0f,


			- 0.50f, 0.80f, 0.0f,

			- 0.48f, 0.80f, 0.0f,

			- 0.36f, -0.80f, 0.0f,
				
			- 0.38f, -0.80f, 0.0f,


			- 0.38f, 0.80f, 0.0f,

			- 0.36f, 0.80f, 0.0f,

			- 0.36f, -0.80f, 0.0f,

			- 0.38f, -0.80f, 0.0f,
		};	
		
		
		final float[] d=new float[]
		{
						-0.16f, 0.78f, 0.0f,

				- 0.14f, 0.78f, 0.0f,

				- 0.14f, -0.78f, 0.0f,

				- 0.16f, -0.78f, 0.0f,



				-0.16f, 0.78f, 0.0f,

				- 0.16f, 0.80f, 0.0f,

				0.0f, 0.80f, 0.0f,

				0.0f, 0.78f, 0.0f,


				- 0.16f, -0.80f, 0.0f,

				- 0.16f, -0.78f, 0.0f,

				0.0f, -0.78f, 0.0f,

				0.0f, -0.80f, 0.0f,



				-0.02f, 0.78f, 0.0f,

				0.0f, 0.78f, 0.0f,

				0.0f, -0.78f, 0.0f,

				- 0.02f, -0.78f, 0.0f
		};	
		
		final float[] a=new float[]
		{
						0.40f, -0.80f, 0.0f,

				0.42f, -0.80f, 0.0f,

				0.52f, 0.80f, 0.0f,

				0.50f, 0.80f, 0.0f,


				0.72f, -0.80f, 0.0f,

				0.74f, -0.80f, 0.0f,

				0.54f, 0.80f, 0.0f,

				0.52f, 0.80f, 0.0f
		};
		
		
		final float[] i1Color=new float[]
		{
						1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		final float[] i2Color=new float[]
		{
						1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		
		final float[] nColor=new float[]
		{
						1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f ,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		final float[] dColor=new float[]
		{
			1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		
		final float[] aColor=new float[]
		{
			0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f
		};
		
		final float[] line1Vertices = new float[]{
		0.50f,0.0f,0.0f,
		0.62f,0.0f,0.0f
	};

	final float[] line2Vertices = new float[]  {
		0.50f, -0.04f, 0.0f,
		0.63f, -0.04f, 0.0f
	};

	final float[] line3Vertices = new float[] {
		0.50f,-0.08f,0.0f,
		0.64f, -0.08f, 0.0f
	};

	final float[] line1Color = new float[]{
		1.0f, 0.647058823529f, 0.0f,
		1.0f, 0.647058823529f, 0.0f
	};
	final float[] line2Color = new float[] {
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};
	final float[] line3Color = new float[] {
		0.0705882352911f, 0.5333333f, 0.0f,
		0.0705882352911f, 0.5333333f, 0.0f,
	};
		
		//i***********************************************************************************************
		GLES32.glGenVertexArrays(1,vao_i1,0);
		GLES32.glBindVertexArray(vao_i1[0]);
		
		GLES32.glGenBuffers(1,vbo_position_i1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_i1[0]);
		
		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(i1.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer=byteBuffer.asFloatBuffer();
		positionBuffer.put(i1);
		positionBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i1.length*4,positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_i1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_i1[0]);
		
		ByteBuffer byteBuffer1=ByteBuffer.allocateDirect(i1Color.length*4);
		byteBuffer1.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer=byteBuffer1.asFloatBuffer();
		colorBuffer.put(i1Color);
		colorBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i1Color.length*4,colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//*****************************************************************************************************************
		GLES32.glGenVertexArrays(1,vao_i2,0);
		GLES32.glBindVertexArray(vao_i2[0]);
		GLES32.glGenBuffers(1,vbo_position_i2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_i2[0]);
		
		ByteBuffer byteBuffer2=ByteBuffer.allocateDirect(i2.length*4);
		byteBuffer2.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer1=byteBuffer2.asFloatBuffer();
		positionBuffer1.put(i2);
		positionBuffer1.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i2.length*4,positionBuffer1,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_i2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_i2[0]);
		
		ByteBuffer byteBuffer3=ByteBuffer.allocateDirect(i1Color.length*4);
		byteBuffer3.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer1=byteBuffer3.asFloatBuffer();
		colorBuffer1.put(i2Color);
		colorBuffer1.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i2Color.length*4,colorBuffer1,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//****************************************************************************************************************************
		
		GLES32.glGenVertexArrays(1,vao_n,0);
		GLES32.glBindVertexArray(vao_n[0]);
		GLES32.glGenBuffers(1,vbo_position_n,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_n[0]);
		
		ByteBuffer byteBuffer4=ByteBuffer.allocateDirect(n.length*4);
		byteBuffer4.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer2=byteBuffer4.asFloatBuffer();
		positionBuffer2.put(n);
		positionBuffer2.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,n.length*4,positionBuffer2,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_n,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_n[0]);
		
		ByteBuffer byteBuffer5=ByteBuffer.allocateDirect(nColor.length*4);
		byteBuffer5.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer2=byteBuffer5.asFloatBuffer();
		colorBuffer2.put(nColor);
		colorBuffer2.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,nColor.length*4,colorBuffer2,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//*****************************************************************************************************************************
		
		
		//****************************************************************************************************************************
		
		GLES32.glGenVertexArrays(1,vao_d,0);
		GLES32.glBindVertexArray(vao_d[0]);
		GLES32.glGenBuffers(1,vbo_position_d,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_d[0]);
		
		ByteBuffer byteBuffer6=ByteBuffer.allocateDirect(d.length*4);
		byteBuffer6.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer4=byteBuffer6.asFloatBuffer();
		positionBuffer4.put(d);
		positionBuffer4.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,d.length*4,positionBuffer4,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_d,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_d[0]);
		
		ByteBuffer byteBuffer7=ByteBuffer.allocateDirect(dColor.length*4);
		byteBuffer7.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer4=byteBuffer7.asFloatBuffer();
		colorBuffer4.put(dColor);
		colorBuffer4.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,dColor.length*4,colorBuffer4,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		//**************************************************************************************************************
		
		
		//****************************************************************************************************************************
		
		GLES32.glGenVertexArrays(1,vao_a,0);
		GLES32.glBindVertexArray(vao_a[0]); 
		GLES32.glGenBuffers(1,vbo_position_a,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_a[0]);
		
		ByteBuffer byteBuffer8=ByteBuffer.allocateDirect(a.length*4);
		byteBuffer8.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer5=byteBuffer8.asFloatBuffer();
		positionBuffer5.put(a);
		positionBuffer5.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,a.length*4,positionBuffer5,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_a,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_a[0]);
		
		ByteBuffer byteBuffer9=ByteBuffer.allocateDirect(aColor.length*4);
		byteBuffer9.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer5=byteBuffer9.asFloatBuffer();
		colorBuffer5.put(aColor);
		colorBuffer5.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,aColor.length*4,colorBuffer5,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		
		//***************************************************************************************************************************
		GLES32.glGenVertexArrays(1,vao_line1,0);
		GLES32.glBindVertexArray(vao_line1[0]); 
		GLES32.glGenBuffers(1,vbo_position_line1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line1[0]);
		
		ByteBuffer byteBuffer15=ByteBuffer.allocateDirect(line1Vertices.length*4);
		byteBuffer15.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer6=byteBuffer15.asFloatBuffer();
		positionBuffer6.put(line1Vertices);
		positionBuffer6.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line1Vertices.length*4,positionBuffer6,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line1[0]);
		
		ByteBuffer byteBuffer16=ByteBuffer.allocateDirect(line1Color.length*4);
		byteBuffer16.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer6=byteBuffer16.asFloatBuffer();
		colorBuffer6.put(line1Color);
		colorBuffer6.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line1Color.length*4,colorBuffer6,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//******************************************************************************************************************************************
		GLES32.glGenVertexArrays(1,vao_line2,0);
		GLES32.glBindVertexArray(vao_line2[0]); 
		GLES32.glGenBuffers(1,vbo_position_line2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line2[0]);
		
		ByteBuffer byteBuffer11=ByteBuffer.allocateDirect(line2Vertices.length*4);
		byteBuffer11.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer7=byteBuffer11.asFloatBuffer();
		positionBuffer7.put(line2Vertices);
		positionBuffer7.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line2Vertices.length*4,positionBuffer7,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line2[0]);
		
		ByteBuffer byteBuffer12=ByteBuffer.allocateDirect(line2Color.length*4);
		byteBuffer12.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer7=byteBuffer12.asFloatBuffer();
		colorBuffer7.put(line2Color);
		colorBuffer7.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line2Color.length*4,colorBuffer7,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		//*********************************************************************************************************************************
		
		
		GLES32.glGenVertexArrays(1,vao_line3,0);
		GLES32.glBindVertexArray(vao_line3[0]); 
		GLES32.glGenBuffers(1,vbo_position_line3,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line3[0]);
		
		ByteBuffer byteBuffer13=ByteBuffer.allocateDirect(line3Vertices.length*4);
		byteBuffer13.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer8=byteBuffer13.asFloatBuffer();
		positionBuffer8.put(line3Vertices);
		positionBuffer8.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line3Vertices.length*4,positionBuffer8,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line1[0]);
		
		ByteBuffer byteBuffer14=ByteBuffer.allocateDirect(line3Color.length*4);
		byteBuffer14.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer8=byteBuffer14.asFloatBuffer();
		colorBuffer8.put(line3Color);
		colorBuffer8.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line3Color.length*4,colorBuffer8,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		
		
		//**********************************************************************************************************************************
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	}
	private void resize(int width,int height)
	{
		if(height==0)
			height=1;
		GLES32.glViewport(0,0,width,height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	private void display()
	{	
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		float[] modelViewProjectionMatrix=new float[16];
		float[] modelViewMatrix=new float[16];
		
		//triangle
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,-0.6f, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_i1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);
		
		
		//rectangle
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,-0.3f, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_n[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glBindVertexArray(0);
		
		
		//d
				Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_d[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glBindVertexArray(0);
		
		
		//I
				Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.3f, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_i2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);
		
		
		//A
				Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.6f, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_a[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glBindVertexArray(0);
		
		//Line1
		
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.2f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
		//Line2
		
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.2f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
		//Line3
		
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.2f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line3[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
		
		GLES32.glUseProgram(0);
		requestRender();
	}
	private void uninitialize()
	{
		if (vbo_rectangle[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_rectangle,0);
			vbo_rectangle[0] = 0;
		}
		if (vao_rectangle[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_rectangle,0);
			vao_rectangle[0] = 0;
		}
		if (vbo_triangle[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_triangle,0);
			vbo_triangle[0] = 0;
		}
		if (vao_triangle[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_triangle,0);
			vao_triangle[0] = 0;
		}
		if (shaderProgramObject != 0) {
			int[] shaderCount=new int[1];

			GLES32.glUseProgram(shaderProgramObject);
			
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);	
	
			if (shaderCount[0]!=0) {
				
				int shaderNumber;
				int[] shaders = new int[shaderCount[0]];

				GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
	
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}	
	}
}
