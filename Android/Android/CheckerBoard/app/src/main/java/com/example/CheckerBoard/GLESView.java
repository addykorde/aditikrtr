package com.example.CheckerBoard;

//Added by me


import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener{
	
	private static Context context;
	private GestureDetector gestureDetector;
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int shaderProgramObject;
	final int checkImageWidth=64;
	final int checkImageHeight=64;
	byte[] checkImage= new byte[checkImageHeight*checkImageWidth*4];
	
	private int[] vao=new int[1];
	private int[] vbo_position=new int[1];
	private int[] texImage=new int[1];
	private int[] vbo_texcoord=new int[1];
	private int samplerUniform;
	
	
	private int mvpUniform;
	
	private float[] perspectiveProjectionMatrix=new float[16];


	public GLESView(Context drawingContext){
		super(drawingContext);
		context=drawingContext;
		gestureDetector=new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityx,float velocityy)
	{
		
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distancex,float distancey)
	{
		System.exit(0);
		return(true);
	}
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR"+version);
		String version1=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR"+version1);
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	//Our custom methods
	
	private void initialize()
	{

		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode=
		String.format
		(
			"#version 320 es" +
			"\n" +
			"in vec4 vPosition;" +
			"in vec2 vTexCoord;" +
			"uniform mat4 u_mvp_matrix;" +
			"out vec2 out_texcoord;" +
			"void main(void)" +
			"{" +
			"gl_Position=u_mvp_matrix*vPosition;" +
			"out_texcoord=vTexCoord;" +
			"}"
		);

		GLES32.glShaderSource(gVertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(gVertexShaderObject);
				


		//Error checking
		int[] iShaderCompileStatus=new int[1];
		int[] iInfoLogLength=new int[1];
		String szInfoLog=null;
		GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("iRTR:VertexShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		

		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode=
		String.format
		(
			"#version 320 es" +
			"\n" +
			"precision highp float;"+
			"in vec2 out_texcoord;" +
			"uniform highp sampler2D u_sampler;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"FragColor=texture(u_sampler,out_texcoord);" +
			"}"
		);

		GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(gFragmentShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("iRTR:FragmentShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject,gVertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gFragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");	
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,"vTexCoord");
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);

	if (iShaderLinkStatus[0] == GLES32.GL_FALSE) {
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		if (iInfoLogLength[0] > 0) {
			szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);

			System.out.println("iRTR:shaderProgramObject"+szInfoLog);
			uninitialize();
			System.exit(0);	
			
		}
	}

		mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		samplerUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_sampler");

		/*final float[] rectangleVertices=new float[]
		{
			1.0f, 1.0f, 0.0f,
			- 1.0f, 1.0f, 0.0f,
			- 1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f
		};*/

		final float[] rectangleTex = new float[]
		{
			1.0f, 1.0f,
			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f
		};
		
		GLES32.glGenVertexArrays(1,vao,0);
		GLES32.glBindVertexArray(vao[0]);
		
		GLES32.glGenBuffers(1,vbo_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);
		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(4*3*4);
		byteBuffer.order (ByteOrder.nativeOrder());
		FloatBuffer positionBuffer=byteBuffer.asFloatBuffer();
		positionBuffer.put(4*2*4);
		positionBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,4*3*4,positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_texcoord,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_texcoord[0]);
		ByteBuffer byteBuffer1=ByteBuffer.allocateDirect(rectangleTex.length*4);
		byteBuffer1.order (ByteOrder.nativeOrder());
		FloatBuffer positionBuffer1=byteBuffer1.asFloatBuffer();
		positionBuffer1.put(rectangleTex);
		positionBuffer1.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,rectangleTex.length*4,positionBuffer1,GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,2,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		
		GLES32.glBindVertexArray(0);

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		loadTexture();
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	}
	private void resize(int width,int height)
	{
		if(height==0)
			height=1;
		GLES32.glViewport(0,0,width,height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	private void display()
	{	float[] rectangleVertices1=new float[12];
		float[] rectangleVertices2=new float[12];
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		float[] modelViewProjectionMatrix=new float[16];
		float[] modelViewMatrix=new float[16];
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.0f,0.0f,-3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texImage[0]);
		GLES32.glUniform1i(samplerUniform, 0);
		
		rectangleVertices1[0] = -2.0f;
		rectangleVertices1[1] = -1.0f;
		rectangleVertices1[2] = 0.0f;
		rectangleVertices1[3] = -2.0f;
		rectangleVertices1[4] = 1.0f;
		rectangleVertices1[5] = 0.0f;
		rectangleVertices1[6] = 0.0f;
		rectangleVertices1[7] = 1.0f;
		rectangleVertices1[8] = 0.0f;
		rectangleVertices1[9] = 0.0f;
		rectangleVertices1[10] = -1.0f;
		rectangleVertices1[11] = 0.0f;
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);
		ByteBuffer byteBuffer3=ByteBuffer.allocateDirect(4*3*4);
		byteBuffer3.order (ByteOrder.nativeOrder());
		FloatBuffer positionBuffer3=byteBuffer3.asFloatBuffer();
		positionBuffer3.put(rectangleVertices1);
		positionBuffer3.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,4*3*4,positionBuffer3,GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,2,GLES32.GL_FLOAT,false,0,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);
	
	
		
		rectangleVertices2[0] = 1.0f;
		rectangleVertices2[1] = -1.0f;
		rectangleVertices2[2] = 0.0f;
		rectangleVertices2[3] = 1.0f;
		rectangleVertices2[4] = 1.0f;
		rectangleVertices2[5] = 0.0f;
		rectangleVertices2[6] = 2.41421f;
		rectangleVertices2[7] = 1.0f;
		rectangleVertices2[8] = -1.41421f;
		rectangleVertices2[9] = 2.41421f;
		rectangleVertices2[10] = -1.0f;
		rectangleVertices2[11] = -1.41421f;

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position[0]);
		ByteBuffer byteBuffer4=ByteBuffer.allocateDirect(4*3*4);
		byteBuffer4.order (ByteOrder.nativeOrder());
		FloatBuffer positionBuffer4=byteBuffer4.asFloatBuffer();
		positionBuffer4.put(rectangleVertices2);
		positionBuffer4.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,4*3*4,positionBuffer4,GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,2,GLES32.GL_FLOAT,false,0,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(vao[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);
		GLES32.glUseProgram(0);
		requestRender();
	}
	private void uninitialize()
	{
		if (vbo_position[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_position,0);
			vbo_position[0] = 0;
		}
		if (vbo_texcoord[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_texcoord,0);
			vbo_texcoord[0] = 0;
		}
		if (vao[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao,0);
			vao[0] = 0;
		}
		if (shaderProgramObject != 0) {
			int[] shaderCount=new int[1];

			GLES32.glUseProgram(shaderProgramObject);
			
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);	
	
			if (shaderCount[0]!=0) {
				
				int shaderNumber;
				int[] shaders = new int[shaderCount[0]];

				GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
	
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}	
	}
	
	private void loadTexture()
	{
		makeCheckImage();
		
		//Bitmap bitmap=BitmapFactory.decodeResource(context.getResources(),imgFileResourceId,options);
		//GLES32.glGenBuffers(1,vbo_texcoord,0);
		//GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_texcoord[0]);
		ByteBuffer byteBuffer8=ByteBuffer.allocateDirect(checkImageWidth*checkImageHeight*4);
		byteBuffer8.order (ByteOrder.nativeOrder());
		byteBuffer8.put(checkImage);
		byteBuffer8.position(0);
		Bitmap bitmap = Bitmap.createBitmap(checkImageWidth,checkImageHeight,Bitmap.Config.ARGB_8888);
		bitmap.copyPixelsFromBuffer(byteBuffer8);
		
		GLES32.glGenTextures(1, texImage,0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texImage[0]);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_REPEAT);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_REPEAT);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_NEAREST);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_NEAREST);

		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap,0);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,0);
	}
	
	private void makeCheckImage()
	{
		int i, j, c;


		for (i = 0; i < checkImageHeight; i++)
		{
			for (j = 0; j < checkImageWidth; j++)
			{
			c = ((i&8)^(j&8))*255;

			checkImage[(i*64+j)*4+0] = (byte)c;
			checkImage[(i*64+j)*4+1] = (byte)c;
			checkImage[(i*64+j)*4+2] = (byte)c;
			checkImage[(i*64+j)*4+3] = (byte)c;
			}
		}
		
	}
}
