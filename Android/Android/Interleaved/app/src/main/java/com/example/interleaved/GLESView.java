package com.example.interleaved;

//Added by me


import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;
import java.nio.ShortBuffer;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener{
	
	private static Context context;
	private GestureDetector gestureDetector;
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_pyramid=new int[1];
	private int[] vbo_pyramid_position=new int[1];
	private int[] vbo_pyramid_color=new int[1];

	private int[] vao_cube=new int[1];
	private int[] vbo_cube_position=new int[1];
	private int[] vbo_cube_color=new int[1];
	private int[] texture_stone=new int[1];
	private int[] vbo_cube_normal=new int[1];
	private int[] vbo_cube_tex=new int[1];



	private int[] vao_sphere=new int[1];
	private int[] vbo_sphere_position=new int[1];
	private int[] vbo_sphere_normal=new int[1];
	private int[] vbo_sphere_element=new int[1];
	private int samplerUniform;
	
	private float[] light_ambient=new float[4];
private float[] light_diffuse=new float[4];
private float[] light_specular=new float[4];
private float[] light_position=new float[4];

private float[] material_ambient=new float[4];
private float[] material_diffuse=new float[4];
private float[] material_specular=new float[4];
private float[] material_shininess=new float[1];
	FloatBuffer ambientBuffer;
	FloatBuffer diffuseBuffer;
	FloatBuffer specularBuffer;
	FloatBuffer positionBuffer;
	
	FloatBuffer mambientBuffer;
	FloatBuffer mdiffuseBuffer;
	FloatBuffer mspecularBuffer;
	FloatBuffer mshininessBuffer;
		private int mUniform;
		private int vUniform;
		private int pUniform;
		private int laUniform;
		private int kaUniform;
		private int lsUniform;
		private int ldUniform;
		private int kdUniform;
		private int ksUniform;
		private int lightPositionUniform;
		private int isLKeypressedUniform1;
		private int isLKeypressedUniform2;
		private int material_shininess_uniform;
	private int gbLight = 0;
	private float[] perspectiveProjectionMatrix=new float[16];
	float angle_pyramid=0.0f;
	float angle_cube=0.0f;
	float numVertices;
		float numElements;
	public GLESView(Context drawingContext){
		super(drawingContext);
		context=drawingContext;
		gestureDetector=new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		if(gbLight==0)
			gbLight++;
		else
			gbLight=0;
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		
		
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityx,float velocityy)
	{
		
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distancex,float distancey)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR"+version);
		String version1=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR"+version1);
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		
		display();
	}
	
	//Our custom methods
	
	private void initialize()
	{
		
		Sphere sphere=new Sphere();
		float sphere_vertices[]=new float[1146];
		float sphere_normals[]=new float[1146];
		float sphere_textures[]=new float[764];
		short sphere_elements[]=new short[2280];
		String szInfoLog1=null;
		sphere.getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);
		numVertices=sphere.getNumberOfSphereVertices();
		numElements=sphere.getNumberOfSphereElements();
		
		
		System.out.println("iRTR:Initialize:Before Vertex Shader"+szInfoLog1);
		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode=
		String.format
		(
				"#version 320 es"+
				"\n" +
				"in vec4 vPosition;" +
		"in vec3 vnormal;" +
		"in vec4 vColor;" +
		"out vec4 out_color;" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"in vec2 vTexCoord;" +
		"out vec2 out_texcoord;" +
		"uniform mediump int u_lkeyispressed2;" +
		"uniform vec4 u_light_position;" +
		"out vec4 eye_coordinates;" +
		"out vec3 light_direction;" +
		"out vec3 tnorm;" +
		"out vec3 viewer_vector;" +
		"void main(void)" +
		"{" +
		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" +
		"if(u_lkeyispressed2 == 1)" +
		"{" +
		"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" +
		"tnorm=mat3(u_v_matrix*u_m_matrix)*vnormal;" +
		"light_direction=vec3(u_light_position)-eye_coordinates.xyz;" +
		"viewer_vector=vec3(-eye_coordinates);" +
		"}" +
		"else" +
		"{" +
		"}" +
		"out_texcoord=vTexCoord;" +
		"out_color=vColor;"+
		"}"

		);

		GLES32.glShaderSource(gVertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(gVertexShaderObject);
				


		//Error checking
		int[] iShaderCompileStatus=new int[1];
		int[] iInfoLogLength=new int[1];
		String szInfoLog=null;
		GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("iRTR:VertexShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		

		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode=
		String.format
		(
				"#version 320 es"+
				"\n"+
				"precision highp float;"+
				"out vec4 FragColor;" +
				"uniform mediump int u_lkeyispressed1;" +
				"in vec2 out_texcoord;" +
				"in vec4 out_color;"+
				"uniform sampler2D u_sampler;" +
				"uniform vec3 u_ld;" +
				"uniform vec3 u_la;" +
				"uniform vec3 u_ls;" +
				"uniform vec3 u_ka;" +
				"uniform vec3 u_ks;" +
				"uniform vec3 u_kd;" +
				"uniform float m_shininess;"+
				"in vec3 light_direction;" +
				"in vec3 tnorm;"+
				"in vec3 viewer_vector;" +
				"vec3 phong_ads_light;"+
				"vec4 phong_ads_light1;"+
				"void main(void)" +
				"{" +
				"if(u_lkeyispressed1 == 1)"+
				"{" +
				"vec3 tdnorm=normalize(tnorm);" +
				"vec3 light_direction1=normalize(light_direction);" +
				"float tn_dot_ld=max(dot(light_direction1,tdnorm),0.0);" +
				"vec3 reflection_vector=reflect(-light_direction1,tdnorm);" +
				"vec3 viewer_vector1=normalize(viewer_vector);" +
				"vec3 ambient = u_la * u_ka;" +
				"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" +
				"vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,viewer_vector1),0.0),m_shininess);" +
				"phong_ads_light=ambient+diffuse+specular;" +
				"phong_ads_light1=vec4(phong_ads_light,1.0);"+
				"}" +
				"else"+
				"{"+
				"phong_ads_light1=vec4(1.0,1.0,1.0,1.0);" +
				"}"+
				"FragColor=vec4(texture(u_sampler,out_texcoord)*out_color*phong_ads_light1);"+
				"}"
		);

		GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(gFragmentShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("iRTR:FragmentShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject,gVertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gFragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");	
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vnormal");
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");	
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vnormal");
		
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);

	if (iShaderLinkStatus[0] == GLES32.GL_FALSE) {
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		if (iInfoLogLength[0] > 0) {
			szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);

			System.out.println("iRTR:shaderProgramObject"+szInfoLog);
			uninitialize();
			System.exit(0);	
			
		}
	}

		mUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_m_matrix");
		vUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_v_matrix");
		pUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");
		ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		material_shininess_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "m_shininess");
		lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
		isLKeypressedUniform1 = GLES32.glGetUniformLocation(shaderProgramObject, "u_lkeyispressed1");
		isLKeypressedUniform2 = GLES32.glGetUniformLocation(shaderProgramObject, "u_lkeyispressed2");
		samplerUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_sampler");
		final float[] cubeVCNT=new float[]
		{
	-1.0f, 1.0f, -1.0f,	 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,
1.0f, 1.0f, -1.0f,   0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,0.0f,
-1.0f, 1.0f, 1.0f,	 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,1.0f,
1.0f, 1.0f, 1.0f,	 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,1.0f,
                                                         
1.0f, -1.0f, -1.0f,	 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f,1.0f,0.0f,
-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f,1.0f,1.0f,
-1.0f, -1.0f, 1.0f,	 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f,0.0f,1.0f,
1.0f, -1.0f, 1.0f,	 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f,0.0f,0.0f,
                                                         
1.0f, 1.0f, 1.0f,	 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,0.0f,
-1.0f, 1.0f, 1.0f,	 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,1.0f,
-1.0f, -1.0f, 1.0f,	 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,1.0f,
1.0f, -1.0f, 1.0f,	 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,
                                                         
1.0f, 1.0f, -1.0f,	 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,-1.0f, 0.0f,0.0f,
-1.0f, 1.0f, -1.0f,	 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,-1.0f, 1.0f,0.0f,
-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,-1.0f, 1.0f,1.0f,
1.0f, -1.0f, -1.0f,	 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,-1.0f, 0.0f,1.0f,
                                                         
1.0f, 1.0f, -1.0f,	 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,1.0f,
1.0f, 1.0f, 1.0f,	 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,
1.0f, -1.0f, 1.0f,	 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,0.0f,
1.0f, -1.0f, -1.0f,	 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,1.0f,
                                                         
-1.0f, 1.0f, 1.0f,	 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f,1.0f,1.0f,
-1.0f, 1.0f, -1.0f,	 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f,0.0f,1.0f,
-1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f,0.0f,0.0f,
-1.0f, -1.0f, 1.0f , 1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f,0.0f
		};	
		
		GLES32.glGenVertexArrays(1,vao_cube,0);
		GLES32.glBindVertexArray(vao_cube[0]);
		
		
		GLES32.glGenBuffers(1,vbo_cube_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_cube_position[0]);
		ByteBuffer byteBuffer2=ByteBuffer.allocateDirect(24 * 11 * 4);
		byteBuffer2.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer1=byteBuffer2.asFloatBuffer();

		positionBuffer1.put(cubeVCNT);
		positionBuffer1.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,24 * 11 * 4,positionBuffer1,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,11 * 4,(0*4));
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,11 * 4,(3*4));
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);


		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,3,GLES32.GL_FLOAT,false,11 * 4,(6*4));
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);

		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,2,GLES32.GL_FLOAT,false,11 * 4,(9*4));
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
	light_ambient[0] = 0.0f;
	light_ambient[1] = 0.0f;
	light_ambient[2] = 0.0f;
	light_ambient[3] = 0.0f;
	
	ByteBuffer byteBuffer3=ByteBuffer.allocateDirect(light_ambient.length*4);
	byteBuffer3.order(ByteOrder.nativeOrder());
	ambientBuffer=byteBuffer3.asFloatBuffer();
	ambientBuffer.put(light_ambient);
	ambientBuffer.position(0);

	light_diffuse[0] = 1.0f;
	light_diffuse[1] = 1.0f;
	light_diffuse[2] = 1.0f;
	light_diffuse[3] = 1.0f;
	
	ByteBuffer byteBuffer4=ByteBuffer.allocateDirect(light_diffuse.length*4);
	byteBuffer4.order(ByteOrder.nativeOrder());
	diffuseBuffer=byteBuffer4.asFloatBuffer();
	diffuseBuffer.put(light_diffuse);
	diffuseBuffer.position(0);

	light_specular[0] = 1.0f;
	light_specular[1] = 1.0f;
	light_specular[2] = 1.0f;
	light_specular[3] = 1.0f;
	
	ByteBuffer byteBuffer5=ByteBuffer.allocateDirect(light_specular.length*4);
	byteBuffer5.order(ByteOrder.nativeOrder());
	specularBuffer=byteBuffer5.asFloatBuffer();
	specularBuffer.put(light_specular);
	specularBuffer.position(0);

	light_position[0] = 100.0f;
	light_position[1] = 100.0f;
	light_position[2] = 100.0f;
	light_position[3] = 1.0f;

	ByteBuffer byteBuffer6=ByteBuffer.allocateDirect(light_position.length*4);
	byteBuffer6.order(ByteOrder.nativeOrder());
	positionBuffer=byteBuffer6.asFloatBuffer();
	positionBuffer.put(light_position);
	positionBuffer.position(0);
	
	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 0.0f;
	
	ByteBuffer byteBuffer7=ByteBuffer.allocateDirect(material_ambient.length*4);
	byteBuffer7.order(ByteOrder.nativeOrder());
	mambientBuffer=byteBuffer7.asFloatBuffer();
	mambientBuffer.put(material_ambient);
	mambientBuffer.position(0);

	material_diffuse[0] = 1.0f;
	material_diffuse[1] = 1.0f;
	material_diffuse[2] = 1.0f;
	material_diffuse[3] = 1.0f;
	
	ByteBuffer byteBuffer8=ByteBuffer.allocateDirect(material_diffuse.length*4);
	byteBuffer8.order(ByteOrder.nativeOrder());
	mdiffuseBuffer=byteBuffer8.asFloatBuffer();
	mdiffuseBuffer.put(material_diffuse);
	mdiffuseBuffer.position(0);

	material_specular[0] = 1.0f;
	material_specular[1] = 1.0f;
	material_specular[2] = 1.0f;
	material_specular[3] = 1.0f;
	
	ByteBuffer byteBuffer9=ByteBuffer.allocateDirect(material_specular.length*4);
	byteBuffer9.order(ByteOrder.nativeOrder());
	mspecularBuffer=byteBuffer9.asFloatBuffer();
	mspecularBuffer.put(material_specular);
	mspecularBuffer.position(0);


	material_shininess[0] = 50.0f;
	ByteBuffer byteBuffer10=ByteBuffer.allocateDirect(material_shininess.length*4);
	byteBuffer10.order(ByteOrder.nativeOrder());
	mshininessBuffer=byteBuffer10.asFloatBuffer();
	mshininessBuffer.put(material_shininess);
	mshininessBuffer.position(0);

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		texture_stone[0]=loadTexture(R.raw.marble);
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	}
	private void resize(int width,int height)
	{
		if(height==0)
			height=1;
		GLES32.glViewport(0,0,width,height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	private void display()
	{	
		String szInfoLog1=null;
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		float[] projectionMatrix=new float[16];
		float[] modelMatrix=new float[16];
		float[] viewMatrix=new float[16];
		
		
		//triangle
		Matrix.setIdentityM(projectionMatrix,0);
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);
		//Matrix.setIdentityM(rotationMatrix,0);
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-5.0f);
		Matrix.rotateM(modelMatrix,0,angle_cube,1.0f,1.0f,1.0f);
		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		GLES32.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES32.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES32.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_stone[0]);
		GLES32.glUniform1i(samplerUniform, 0);
		if (gbLight == 1)
		{
		GLES32.glUniform1i(isLKeypressedUniform1, 1);
		GLES32.glUniform1i(isLKeypressedUniform2, 1);
		GLES32.glUniform3fv(laUniform, 1, ambientBuffer);
		GLES32.glUniform3fv(ldUniform, 1, diffuseBuffer);
		GLES32.glUniform3fv(lsUniform, 1, specularBuffer);
		GLES32.glUniform4fv(lightPositionUniform, 1, positionBuffer);

		GLES32.glUniform3fv(kaUniform, 1, mambientBuffer);
		GLES32.glUniform3fv(kdUniform, 1, mdiffuseBuffer);
		GLES32.glUniform3fv(ksUniform, 1, mspecularBuffer);
		GLES32.glUniform1fv(material_shininess_uniform, 1, mshininessBuffer);
		}
		else
		{
		GLES32.glUniform1i(isLKeypressedUniform1, 0);
		GLES32.glUniform1i(isLKeypressedUniform2, 0);
		}
		
		
		
		GLES32.glBindVertexArray(vao_cube[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 16, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 20, 4);
		GLES32.glBindVertexArray(0);
		
		
		
		GLES32.glUseProgram(0);
		Update();
		requestRender();
	}
	private void Update()
	{
		
		if(angle_cube<=360.0f)
		{
			angle_cube=angle_cube+2.0f;
		}
		else
		{
			angle_cube=0.0f;
		}
	}
	private void uninitialize()
	{
		if (vbo_sphere_position[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_sphere_position,0);
			vbo_sphere_position[0] = 0;
		}
		if (vbo_sphere_element[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_sphere_element,0);
			vbo_sphere_element[0] = 0;
		}
		if (vao_sphere[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_sphere,0);
			vao_sphere[0] = 0;
		}
		
		if (shaderProgramObject != 0) {
			int[] shaderCount=new int[1];

			GLES32.glUseProgram(shaderProgramObject);
			
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);	
	
			if (shaderCount[0]!=0) {
				
				int shaderNumber;
				int[] shaders = new int[shaderCount[0]];

				GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
	
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}	
	}


private int loadTexture(int imgFileResourceId)
	{
		int [] texture=new int[1];
		BitmapFactory.Options options= new BitmapFactory.Options();
		options.inScaled=false;
		Bitmap bitmap=BitmapFactory.decodeResource(context.getResources(),imgFileResourceId,options);
		
		GLES32.glGenTextures(1,texture,0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture[0]);
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT,4);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MAG_FILTER,GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,GLES32.GL_TEXTURE_MIN_FILTER,GLES32.GL_LINEAR_MIPMAP_LINEAR);
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D,0,bitmap,0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,0);
		return(texture[0]);
	}

}
