package com.example.twolightsonapyramid;

//Added by me


import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;
import java.nio.ShortBuffer;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener{
	
	private static Context context;
	private GestureDetector gestureDetector;
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_pyramid=new int[1];
	private int[] vbo_pyramid_position=new int[1];
	private int[] vbo_pyramid_normal=new int[1];

	
	private int[] vao_sphere=new int[1];
	private int[] vbo_sphere_position=new int[1];
	private int[] vbo_sphere_normal=new int[1];
	private int[] vbo_sphere_element=new int[1];
	
	
	private float[] light_ambient_red=new float[4];
private float[] light_diffuse_red=new float[4];
private float[] light_specular_red=new float[4];
private float[] light_position_red=new float[4];

private float[] light_ambient_blue=new float[4];
private float[] light_diffuse_blue=new float[4];
private float[] light_specular_blue=new float[4];
private float[] light_position_blue=new float[4];

private float[] material_ambient=new float[4];
private float[] material_diffuse=new float[4];
private float[] material_specular=new float[4];
private float[] material_shininess=new float[1];
	FloatBuffer ambientBuffer;
	FloatBuffer diffuseBuffer;
	FloatBuffer specularBuffer;
	FloatBuffer positionBuffer;
	
	FloatBuffer mambientBuffer;
	FloatBuffer mdiffuseBuffer;
	FloatBuffer mspecularBuffer;
	FloatBuffer mshininessBuffer;
		private int mUniform;
		private int vUniform;
		private int pUniform;
		private int laUniform_red;
		private int laUniform_blue;
		private int kaUniform;
		private int lsUniform_red;
		private int lsUniform_blue;
		private int ldUniform_red;
		private int ldUniform_blue;
		private int kdUniform;
		private int ksUniform;
		private int lightPositionUniform_red;
		private int lightPositionUniform_blue;
		private int isLKeypressedUniform;
		private int material_shininess_uniform;
	private int gbLight = 0;
	private float[] perspectiveProjectionMatrix=new float[16];
	float angle_pyramid=0.0f;
	float angle_cube=0.0f;
	float numVertices;
		float numElements;
	public GLESView(Context drawingContext){
		super(drawingContext);
		context=drawingContext;
		gestureDetector=new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		
		if(gbLight==0)
			gbLight++;
		else
			gbLight=0;
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityx,float velocityy)
	{
		
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distancex,float distancey)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR"+version);
		String version1=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR"+version1);
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		
		display();
	}
	
	//Our custom methods
	
	private void initialize()
	{
		
		String szInfoLog1=null;
		
		
		System.out.println("iRTR:Initialize:Before Vertex Shader"+szInfoLog1);
		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode=
		String.format
		(
				"#version 320 es"+
				"\n" +
				"in vec4 vPosition;" +
				"in vec3 vnormal;" +
				"uniform mat4 u_m_matrix;" +
				"uniform mat4 u_v_matrix;" +
				"uniform mat4 u_p_matrix;" +
				"uniform int u_lkeyispressed;" +
				"uniform vec3 u_ld_red;" +
				"uniform vec3 u_la_red;" +
				"uniform vec3 u_ls_red;" +
				"uniform vec3 u_ld_blue;" +
				"uniform vec3 u_la_blue;" +
				"uniform vec3 u_ls_blue;" +
				"uniform vec3 u_ka;" +
				"uniform vec3 u_ks;" +
				"uniform vec3 u_kd;" +
				"uniform float m_shininess;" +
				"uniform vec4 u_light_position_red;" +
				"uniform vec4 u_light_position_blue;" +
				"out vec3 finalColor;" +
				"vec3 phong_ads_light1;"+
				"vec3 phong_ads_light2;"+
				"void main(void)" +
				"{" +
				"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" +
				"if(u_lkeyispressed == 1)" +
				"{" +
				"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" +
				"vec3 tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" +
				"vec3 light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);" +
				"vec3 light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);" +
				"float tn_dot_ld_red=max(dot(light_direction_red,tnorm),0.0);" +
				"float tn_dot_ld_blue=max(dot(light_direction_blue,tnorm),0.0);" +
				"vec3 reflection_vector_red=reflect(-light_direction_red,tnorm);" +
				"vec3 reflection_vector_blue=reflect(-light_direction_blue,tnorm);" +
				"vec3 viewer_vector=normalize(vec3(-eye_coordinates));" +
				"vec3 ambient1 = u_la_red * u_ka;" +
				"vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld_red;" +
				"vec3 specular1=u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),m_shininess);" +
				"vec3 ambient2 = u_la_blue * u_ka;" +
				"vec3 diffuse2 = u_ld_blue * u_kd * tn_dot_ld_blue;" +
				"vec3 specular2=u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),m_shininess);" +
				"phong_ads_light1=ambient1+diffuse1+specular1;" +
				"phong_ads_light2=ambient2+diffuse2+specular2;" +
				"finalColor=phong_ads_light1+phong_ads_light2;"+
				"}" +
				"else" +
				"{" +
				"finalColor=vec3(1.0,1.0,1.0);" +
				"}" +

				"}"
		);

		GLES32.glShaderSource(gVertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(gVertexShaderObject);
				


		//Error checking
		int[] iShaderCompileStatus=new int[1];
		int[] iInfoLogLength=new int[1];
		String szInfoLog=null;
		GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("iRTR:VertexShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		

		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec3 finalColor;" +
			"out vec4 FragColor;" +
			"uniform int u_lkeyispressed;" +
			"void main(void)" +
			"{" +
			"FragColor=vec4(finalColor,1.0);" +
			"}"
		);

		GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(gFragmentShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("iRTR:FragmentShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject,gVertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gFragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");	
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vnormal");
		
		
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);

	if (iShaderLinkStatus[0] == GLES32.GL_FALSE) {
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		if (iInfoLogLength[0] > 0) {
			szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);

			System.out.println("iRTR:shaderProgramObject"+szInfoLog);
			uninitialize();
			System.exit(0);	
			
		}
	}

		mUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_m_matrix");
		vUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_v_matrix");
		pUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		laUniform_red = GLES32.glGetUniformLocation(shaderProgramObject, "u_la_red");
		lsUniform_red = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls_red");
		ldUniform_red = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld_red");
		laUniform_blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_la_blue");
		lsUniform_blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls_blue");
		ldUniform_blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld_blue");
		kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		material_shininess_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "m_shininess");
		lightPositionUniform_red = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position_red");
		lightPositionUniform_blue = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position_blue");
		isLKeypressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lkeyispressed");
		
		final float[] pyramidVertices=new float[]
		{
										0.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 1.0f ,
										1.0f, -1.0f, 1.0f ,
										0.0f, 1.0f, 0.0f ,
										1.0f, -1.0f, 1.0f ,
										1.0f, -1.0f, -1.0f ,
										0.0f, 1.0f, 0.0f ,
										-1.0f, -1.0f,-1.0f ,
										-1.0f, -1.0f, 1.0f ,
										0.0f, 1.0f, 0.0f ,
										1.0f, -1.0f, -1.0f ,
										-1.0f, -1.0f, -1.0f ,
		};
		final float[] pyramidNormals=new float[]
		{
			0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		


		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
	

		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		};
		GLES32.glGenVertexArrays(1,vao_pyramid,0);
		GLES32.glBindVertexArray(vao_pyramid[0]);
		
		GLES32.glGenBuffers(1,vbo_pyramid_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_pyramid_position[0]);
		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(pyramidVertices.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer=byteBuffer.asFloatBuffer();
		positionBuffer.put(pyramidVertices);
		positionBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,pyramidVertices.length*4,positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_pyramid_normal,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_pyramid_normal[0]);
		ByteBuffer byteBuffer1=ByteBuffer.allocateDirect(pyramidNormals.length*4);
		byteBuffer1.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer=byteBuffer1.asFloatBuffer();
		colorBuffer.put(pyramidNormals);
		colorBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,pyramidNormals.length*4,colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
	light_ambient_red[0] = 0.0f;
	light_ambient_red[1] = 0.0f;
	light_ambient_red[2] = 0.0f;
	light_ambient_red[3] = 1.0f;
	/*
	ByteBuffer byteBuffer3=ByteBuffer.allocateDirect(light_ambient.length*4);
	byteBuffer3.order(ByteOrder.nativeOrder());
	ambientBuffer=byteBuffer3.asFloatBuffer();
	ambientBuffer.put(light_ambient);
	ambientBuffer.position(0);*/

	light_diffuse_red[0] = 1.0f;
	light_diffuse_red[1] = 0.0f;
	light_diffuse_red[2] = 0.0f;
	light_diffuse_red[3] = 1.0f;
	/*
	ByteBuffer byteBuffer4=ByteBuffer.allocateDirect(light_diffuse.length*4);
	byteBuffer4.order(ByteOrder.nativeOrder());
	diffuseBuffer=byteBuffer4.asFloatBuffer();
	diffuseBuffer.put(light_diffuse);
	diffuseBuffer.position(0);*/

	light_specular_red[0] = 1.0f;
	light_specular_red[1] = 0.0f;
	light_specular_red[2] = 0.0f;
	light_specular_red[3] = 1.0f;
	
	/*ByteBuffer byteBuffer5=ByteBuffer.allocateDirect(light_specular.length*4);
	byteBuffer5.order(ByteOrder.nativeOrder());
	specularBuffer=byteBuffer5.asFloatBuffer();
	specularBuffer.put(light_specular);
	specularBuffer.position(0);*/

	light_position_red[0] = -2.0f;
	light_position_red[1] = 0.0f;
	light_position_red[2] = 0.0f;
	light_position_red[3] = 1.0f;
/*
	ByteBuffer byteBuffer6=ByteBuffer.allocateDirect(light_position.length*4);
	byteBuffer6.order(ByteOrder.nativeOrder());
	positionBuffer=byteBuffer6.asFloatBuffer();
	positionBuffer.put(light_position);
	positionBuffer.position(0);
*/
	
	
	
	light_ambient_blue[0] = 0.0f;
	light_ambient_blue[1] = 0.0f;
	light_ambient_blue[2] = 0.0f;
	light_ambient_blue[3] = 1.0f;
	/*
	ByteBuffer byteBuffer3=ByteBuffer.allocateDirect(light_ambient.length*4);
	byteBuffer3.order(ByteOrder.nativeOrder());
	ambientBuffer=byteBuffer3.asFloatBuffer();
	ambientBuffer.put(light_ambient);
	ambientBuffer.position(0);*/

	light_diffuse_blue[0] = 0.0f;
	light_diffuse_blue[1] = 0.0f;
	light_diffuse_blue[2] = 1.0f;
	light_diffuse_blue[3] = 1.0f;
	/*
	ByteBuffer byteBuffer4=ByteBuffer.allocateDirect(light_diffuse.length*4);
	byteBuffer4.order(ByteOrder.nativeOrder());
	diffuseBuffer=byteBuffer4.asFloatBuffer();
	diffuseBuffer.put(light_diffuse);
	diffuseBuffer.position(0);*/

	light_specular_blue[0] = 0.0f;
	light_specular_blue[1] = 0.0f;
	light_specular_blue[2] = 1.0f;
	light_specular_blue[3] = 1.0f;
	
	/*ByteBuffer byteBuffer5=ByteBuffer.allocateDirect(light_specular.length*4);
	byteBuffer5.order(ByteOrder.nativeOrder());
	specularBuffer=byteBuffer5.asFloatBuffer();
	specularBuffer.put(light_specular);
	specularBuffer.position(0);*/

	light_position_blue[0] = 2.0f;
	light_position_blue[1] = 0.0f;
	light_position_blue[2] = 0.0f;
	light_position_blue[3] = 1.0f;
/*
	ByteBuffer byteBuffer6=ByteBuffer.allocateDirect(light_position.length*4);
	byteBuffer6.order(ByteOrder.nativeOrder());
	positionBuffer=byteBuffer6.asFloatBuffer();
	positionBuffer.put(light_position);
	positionBuffer.position(0);
*/
	
	
	
	
	material_ambient[0] = 0.0f;
	material_ambient[1] = 0.0f;
	material_ambient[2] = 0.0f;
	material_ambient[3] = 0.0f;
	
	/*ByteBuffer byteBuffer7=ByteBuffer.allocateDirect(material_ambient.length*4);
	byteBuffer7.order(ByteOrder.nativeOrder());
	mambientBuffer=byteBuffer7.asFloatBuffer();
	mambientBuffer.put(material_ambient);
	mambientBuffer.position(0);*/

	material_diffuse[0] = 1.0f;
	material_diffuse[1] = 1.0f;
	material_diffuse[2] = 1.0f;
	material_diffuse[3] = 1.0f;
	
	/*ByteBuffer byteBuffer8=ByteBuffer.allocateDirect(material_diffuse.length*4);
	byteBuffer8.order(ByteOrder.nativeOrder());
	mdiffuseBuffer=byteBuffer8.asFloatBuffer();
	mdiffuseBuffer.put(material_diffuse);
	mdiffuseBuffer.position(0);*/

	material_specular[0] = 1.0f;
	material_specular[1] = 1.0f;
	material_specular[2] = 1.0f;
	material_specular[3] = 1.0f;
	
	/*ByteBuffer byteBuffer9=ByteBuffer.allocateDirect(material_specular.length*4);
	byteBuffer9.order(ByteOrder.nativeOrder());
	mspecularBuffer=byteBuffer9.asFloatBuffer();
	mspecularBuffer.put(material_specular);
	mspecularBuffer.position(0);*/


	material_shininess[0] = 50.0f;
	/*ByteBuffer byteBuffer10=ByteBuffer.allocateDirect(material_shininess.length*4);
	byteBuffer10.order(ByteOrder.nativeOrder());
	mshininessBuffer=byteBuffer10.asFloatBuffer();
	mshininessBuffer.put(material_shininess);
	mshininessBuffer.position(0);*/

		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	}
	private void resize(int width,int height)
	{
		if(height==0)
			height=1;
		GLES32.glViewport(0,0,width,height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	private void display()
	{	
		String szInfoLog1=null;
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		float[] projectionMatrix=new float[16];
		float[] modelMatrix=new float[16];
		float[] viewMatrix=new float[16];
		
		
		//triangle
		Matrix.setIdentityM(projectionMatrix,0);
		Matrix.setIdentityM(modelMatrix,0);
		Matrix.setIdentityM(viewMatrix,0);
		//Matrix.setIdentityM(rotationMatrix,0);
		Matrix.translateM(modelMatrix,0,0.0f,0.0f,-5.0f);
		Matrix.rotateM(modelMatrix,0,angle_pyramid,0.0f,1.0f,0.0f);
		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		GLES32.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES32.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES32.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
		
		if (gbLight == 1)
		{
		GLES32.glUniform1i(isLKeypressedUniform, 1);
		GLES32.glUniform3fv(laUniform_red, 1, light_ambient_red,0);
		GLES32.glUniform3fv(ldUniform_red, 1, light_diffuse_red,0);
		GLES32.glUniform3fv(lsUniform_red, 1, light_specular_red,0);
		GLES32.glUniform4fv(lightPositionUniform_red, 1, light_position_red,0);
		
		GLES32.glUniform3fv(laUniform_blue, 1, light_ambient_blue,0);
		GLES32.glUniform3fv(ldUniform_blue, 1, light_diffuse_blue,0);
		GLES32.glUniform3fv(lsUniform_blue, 1, light_specular_blue,0);
		GLES32.glUniform4fv(lightPositionUniform_blue, 1, light_position_blue,0);

		GLES32.glUniform3fv(kaUniform, 1, material_ambient,0);
		GLES32.glUniform3fv(kdUniform, 1, material_diffuse,0);
		GLES32.glUniform3fv(ksUniform, 1, material_specular,0);
		GLES32.glUniform1fv(material_shininess_uniform, 1, material_shininess,0);
		}
		else
		{
		GLES32.glUniform1i(isLKeypressedUniform, 0);
		}
		
		
		
		GLES32.glBindVertexArray(vao_pyramid[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		GLES32.glBindVertexArray(0);
		
		
		GLES32.glUseProgram(0);
		Update();
		requestRender();
	}
	private void Update()
	{
		if(angle_pyramid<=360.0f)
		{
			angle_pyramid=angle_pyramid+5.0f;
		}
		else
		{
			angle_pyramid=0.0f;
		}
		
	}
	private void uninitialize()
	{
		if (vbo_pyramid_position[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_pyramid_position,0);
			vbo_pyramid_position[0] = 0;
		}
		if (vao_pyramid[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_pyramid,0);
			vao_pyramid[0] = 0;
		}
		
		if (shaderProgramObject != 0) {
			int[] shaderCount=new int[1];

			GLES32.glUseProgram(shaderProgramObject);
			
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);	
	
			if (shaderCount[0]!=0) {
				
				int shaderNumber;
				int[] shaders = new int[shaderCount[0]];

				GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
	
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}	
	}
}
