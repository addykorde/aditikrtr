package com.example.DynamicIndia;

//Added by me


import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener{
	
	private static Context context;
	private GestureDetector gestureDetector;
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_triangle=new int[1];
	private int[] vbo_triangle=new int[1];

	private int[] vao_rectangle=new int[1];
	private int[] vbo_rectangle=new int[1];
	
	
			private int[] vao_i1=new int[1];
			private int[] vao_i2=new int[1];
			private int[] vao_n=new int[1];
			private int[] vao_d=new int[1];
			private int[] vao_a=new int[1];
			private int[] vao_line1=new int[1];
			private int[] vao_line2=new int[1];
			private int[] vao_line3=new int[1];
			private int[] vbo_position_i1=new int[1];
			private int[] vbo_position_i2=new int[1];
			private int[] vbo_position_n=new int[1];
			private int[] vbo_position_d=new int[1];
			private int[] vbo_position_a=new int[1];
			private int[] vbo_position_line1=new int[1];
			private int[] vbo_position_line2=new int[1];
			private int[] vbo_position_line3=new int[1];
			private int[] vbo_color_i1=new int[1];
			private int[] vbo_color_i2=new int[1];
			private int[] vbo_color_n=new int[1];
			private int[] vbo_color_d=new int[1];
			private int[] vbo_color_a=new int[1];
			private int[] vbo_color_line1=new int[1];
			private int[] vbo_color_line2=new int[1];
			private int[] vbo_color_line3=new int[1];
			private int[] vao_apex=new int[1];
			private int[] vbo_position_apex=new int[1];
			private int[] vbo_color_apex=new int[1];
			private int[] vao_iafquad=new int[1];
			private int[] vbo_position_iafquad=new int[1];
			private int[] vbo_color_iafquad=new int[1];
			private int[] vao_iafbquad=new int[1];
			private int[] vbo_position_iafbquad=new int[1];
			private int[] vbo_color_iafbquad=new int[1];
			private int[] vao_biggerwings1=new int[1];
			private int[] vbo_position_biggerwings1=new int[1];
			private int[] vbo_color_biggerwings1=new int[1];
			private int[] vao_biggerwings2=new int[1];
			private int[] vbo_position_biggerwings2=new int[1];
			private int[] vbo_color_biggerwings2=new int[1];
			private int[] vao_smallerwings1=new int[1];
			private int[] vbo_position_smallerwings1=new int[1];
			private int[] vbo_color_smallerwings1=new int[1];
			private int[] vao_smallerwings2=new int[1];
			private int[] vbo_position_smallerwings2=new int[1];
			private int[] vbo_color_smallerwings2=new int[1];
			private int[] voa_heatfurnace=new int[1];
			private int[] vbo_position_heatfurnace=new int[1];
			private int[] vbo_color_heatfurnace=new int[1];

			private int[] vao_pi=new int[1];
			private int[] vbo_position_pi=new int[1];
			private int[] vbo_color_pi=new int[1];

			private int[]vao_pa=new int[1];
			private int[]vbo_position_pa=new int[1];
			private int[]vbo_color_pa=new int[1];

			private int[]vao_pf=new int[1];
			private int[]vbo_position_pf=new int[1];
			private int[]vbo_color_pf=new int[1];

			private int[]vao_line4=new int[1];
			private int[]vbo_position_line4=new int[1];
			private int[]vbo_color_line4=new int[1];

			private int[]vao_line5=new int[1];
			private int[]vbo_position_line5=new int[1];
			private int[]vbo_color_line5=new int[1];

			private int[]vao_line6=new int[1];
			private int[]vbo_position_line6=new int[1];
			private int[]vbo_color_line6=new int[1];

			float translatei1=-2.0f;
			float translatea=3.2f;
			float translaten=5.0f;
			float translatei2=-7.5f;
			int flagd=0;
			int flagp=0;
			int flagp3=0;
			float linex1=-4.95f;
			float linex2=-5.25f;
			float linex3=-4.4f;

			float linez=-2.0f;

			int flag4=0;
			float planex=-4.0f;
			float planey=-4.0f;
			float planex1=-4.0f;
			float planey1=4.0f;
			float planex2=-4.0f;
			float planez=-3.0f;
			float planez1=-3.0f;
			float planez2=-3.0f;
			float planex3=0.9999999999999597f;
			float planey3=0.0f;
			float planex4=0.9999999999999597f;
			float planey4=0.0f;


	private int mvpUniform;
	private float[] perspectiveProjectionMatrix=new float[16];


	public GLESView(Context drawingContext){
		super(drawingContext);
		context=drawingContext;
		gestureDetector=new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityx,float velocityy)
	{
		
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distancex,float distancey)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR"+version);
		String version1=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR"+version1);
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	//Our custom methods
	
	private void initialize()
	{

		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"gl_Position=u_mvp_matrix * vPosition;"+
			"out_color=vColor;"+
			"}"
		);

		GLES32.glShaderSource(gVertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(gVertexShaderObject);
				


		//Error checking
		int[] iShaderCompileStatus=new int[1];
		int[] iInfoLogLength=new int[1];
		String szInfoLog=null;
		GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("iRTR:VertexShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		

		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor=out_color;"+
			"}"
		);

		GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(gFragmentShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("iRTR:FragmentShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject,gVertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gFragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");	
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR,"vColor");	
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);

	if (iShaderLinkStatus[0] == GLES32.GL_FALSE) {
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		if (iInfoLogLength[0] > 0) {
			szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);

			System.out.println("iRTR:shaderProgramObject"+szInfoLog);
			uninitialize();
			System.exit(0);	
			
		}
	}

		mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		final float[] i1=new float[]
		{
			-0.70f, 0.80f, 0.0f,

			- 0.72f, 0.80f, 0.0f,

			- 0.72f,-0.80f, 0.0f,

			- 0.70f, -0.80f, 0.0f,
		};
		
		final float[] i2=new float[]
		{
			0.20f, 0.80f, 0.0f,

			0.22f, 0.80f, 0.0f,

			0.22f, -0.80f, 0.0f,

			0.20f, -0.80f, 0.0f
		};	
		
		final float[] n=new float[]
		{
			-0.52f, 0.80f, 0.0f,

			- 0.50f, 0.80f, 0.0f,

			- 0.50f, -0.80f, 0.0f,

			- 0.52f, -0.80f, 0.0f,


			- 0.50f, 0.80f, 0.0f,

			- 0.48f, 0.80f, 0.0f,

			- 0.36f, -0.80f, 0.0f,
				
			- 0.38f, -0.80f, 0.0f,


			- 0.38f, 0.80f, 0.0f,

			- 0.36f, 0.80f, 0.0f,

			- 0.36f, -0.80f, 0.0f,

			- 0.38f, -0.80f, 0.0f,
		};	
		
		
		final float[] d=new float[]
		{
						-0.16f, 0.78f, 0.0f,

				- 0.14f, 0.78f, 0.0f,

				- 0.14f, -0.78f, 0.0f,

				- 0.16f, -0.78f, 0.0f,



				-0.16f, 0.78f, 0.0f,

				- 0.16f, 0.80f, 0.0f,

				0.0f, 0.80f, 0.0f,

				0.0f, 0.78f, 0.0f,


				- 0.16f, -0.80f, 0.0f,

				- 0.16f, -0.78f, 0.0f,

				0.0f, -0.78f, 0.0f,

				0.0f, -0.80f, 0.0f,



				-0.02f, 0.78f, 0.0f,

				0.0f, 0.78f, 0.0f,

				0.0f, -0.78f, 0.0f,

				- 0.02f, -0.78f, 0.0f
		};	
		
		final float[] a=new float[]
		{
						0.40f, -0.80f, 0.0f,

				0.42f, -0.80f, 0.0f,

				0.52f, 0.80f, 0.0f,

				0.50f, 0.80f, 0.0f,


				0.72f, -0.80f, 0.0f,

				0.74f, -0.80f, 0.0f,

				0.54f, 0.80f, 0.0f,

				0.52f, 0.80f, 0.0f
		};
		
		
		final float[] i1Color=new float[]
		{
						1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		final float[] i2Color=new float[]
		{
						1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		
		final float[] nColor=new float[]
		{
						1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f ,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		final float[] dColor=new float[]
		{
			1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
		};
		
		
		final float[] aColor=new float[]
		{
			0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f
		};
		
		final float[] line1Vertices = new float[]{
		0.50f,0.0f,0.0f,
		0.62f,0.0f,0.0f
	};

	final float[] line2Vertices = new float[]  {
		0.50f, -0.04f, 0.0f,
		0.63f, -0.04f, 0.0f
	};

	final float[] line3Vertices = new float[] {
		0.50f,-0.08f,0.0f,
		0.64f, -0.08f, 0.0f
	};

	final float[] line1Color = new float[]{
		1.0f, 0.647058823529f, 0.0f,
		1.0f, 0.647058823529f, 0.0f
	};
	final float[] line2Color = new float[] {
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};
	final float[] line3Color = new float[] {
		0.0705882352911f, 0.5333333f, 0.0f,
		0.0705882352911f, 0.5333333f, 0.0f,
	};
	
	final float[] apexVertices=new float[] {
	0.60f,0.0f,0.0f,
	0.50f,-0.20f,0.0f,
	0.50f,0.20f,0.0f};

	final float[] apexColor=new float[]{
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f};

	final float[] biafVertices=new float[]{
	0.50f,-0.20f,0.0f,
	-0.10f,-0.20f,0.0f,
	-0.10f,0.20f,0.0f,
	0.50f,0.20f,0.0f,
	};
	
	final float[] biafColor=new float[]{
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	};


	final float[] iafquad=new float[]{
	-0.10f,0.15f,0.0f,
	-0.10f,-0.15f,0.0f,
	-1.25f,-0.15f,0.0f,
	-1.25f,0.15f,0.0f,
	};	

	final float[] iafColor=new float[]{
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	};

	
	final float[] bigVertices1=new float[]{
	-0.25f,0.15f,0.0f,
	-0.35f,0.65f,0.0f,
	-0.55f,0.65f,0.0f,
	-0.55f,0.15f,0.0f
	};

	final float[] bigColor1=new float[]{
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	};

	final float[] bigVertices2=new float[]{
	-0.25f,-0.15f,0.0f,
	-0.35f,-0.65f,0.0f,
	-0.55f,-0.65f,0.0f,
	-0.55f,-0.15f,0.0f
	};

	final float[] bigColor2=new float[]{
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	};


	final float[] smallVertices1=new float[]{
	-1.0f,0.15f,0.0f,
	-1.05f,0.45f,0.0f,
	-1.10f,0.45f,0.0f,
	-1.10f,0.15f,0.0f
	};

	final float[] smallColor1=new float[]{
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	};
	
	final float[] smallVertices2=new float[]{
	-1.0f,-0.15f,0.0f,
	-1.05f,-0.45f,0.0f,
	-1.10f,-0.45f,0.0f,
	-1.10f,-0.15f,0.0f
	};
	
	final float[] smallColor2=new float[]{
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	0.729411f, 0.8627f, 0.933f,
	};

	//Heat Furnace


	
	final float[] piPosition=new float[]{
		-0.65f, 0.12f, 0.0f,
		-0.65f, -0.12f, 0.0f};
	final float[] piColor=new float[]{
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f};

	
	final float[] paPosition=new float[]{
		-0.50f, -0.12f, 0.0f,
		-0.40f, 0.12f, 0.0f,
		-0.40f, 0.12f, 0.0f,
		-0.30f, -0.12f, 0.0f,
		-0.45f, 0.02f, 0.0f,
		-0.35f, 0.02f, 0.0f};
	final float[] paColor=new float[]{
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f};

	final float[] pfPosition=new float[]{
		-0.15f, 0.12f, 0.0f,
		-0.15f, -0.12f, 0.0f,
		-0.15f, 0.12f, 0.0f,
		-0.04f, 0.12f, 0.0f,
		-0.15f, 0.04f, 0.0f,
		-0.06f, 0.04f, 0.0f};
	final float[] pfColor=new float[]{
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f};
	final float[] line4Vertices=new float[]{2.0f,0.03f,0.0f,
					     1.0f,0.03f,0.0f}
					   ;

	final float[] line5Vertices=new float[]{-2.0f,-0.01f,0.0f,
					    1.0f,-0.01f,0.0f};

	final float[] line6Vertices=new float[]{-2.0f,-0.05f,0.0f,
					    1.0f,-0.05f,0.0f
						};
	final float[] line4Color=new float[]{
		1.0f, 0.647058823529f, 0.0f,
		1.0f, 0.647058823529f, 0.0f
								};
	
	final float[] line5Color=new float[]{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
								};
								
	final float[] line6Color=new float[]{
		0.0705882352911f, 0.5333333f, 0.0f,
		0.0705882352911f, 0.5333333f, 0.0f
								};

	//===================================================================================================

		GLES32.glGenVertexArrays(1,vao_apex,0);
		GLES32.glBindVertexArray(vao_apex[0]);
		
		GLES32.glGenBuffers(1,vbo_position_apex,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_apex[0]);
		
		ByteBuffer byteBuffer18=ByteBuffer.allocateDirect(apexVertices.length*4);
		byteBuffer18.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer10=byteBuffer18.asFloatBuffer();
		positionBuffer10.put(apexVertices);
		positionBuffer10.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,apexVertices.length*4,positionBuffer10,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_apex,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_apex[0]);
		
		ByteBuffer byteBuffer19=ByteBuffer.allocateDirect(apexColor.length*4);
		byteBuffer19.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer10=byteBuffer19.asFloatBuffer();
		colorBuffer10.put(apexColor);
		colorBuffer10.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,apexColor.length*4,colorBuffer10,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//==============================================================================================
		GLES32.glGenVertexArrays(1,vao_iafquad,0); 
		GLES32.glBindVertexArray(vao_iafquad[0]);
		
		GLES32.glGenBuffers(1,vbo_position_iafquad,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_iafquad[0]);
		
		ByteBuffer byteBuffer20=ByteBuffer.allocateDirect(iafquad.length*4);
		byteBuffer20.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer11=byteBuffer20.asFloatBuffer();
		positionBuffer11.put(iafquad);
		positionBuffer11.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,iafquad.length*4,positionBuffer11,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_iafquad,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_iafquad[0]);
		
		ByteBuffer byteBuffer21=ByteBuffer.allocateDirect(iafColor.length*4);
		byteBuffer21.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer11=byteBuffer21.asFloatBuffer();
		colorBuffer11.put(iafColor);
		colorBuffer11.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,iafColor.length*4,colorBuffer11,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		GLES32.glGenVertexArrays(1,vao_iafbquad,0); 
		GLES32.glBindVertexArray(vao_iafbquad[0]);
		
		GLES32.glGenBuffers(1,vbo_position_iafbquad,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_iafbquad[0]);
		
		ByteBuffer byteBuffer22=ByteBuffer.allocateDirect(biafVertices.length*4);
		byteBuffer22.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer12=byteBuffer22.asFloatBuffer();
		positionBuffer12.put(biafVertices);
		positionBuffer12.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,biafVertices.length*4,positionBuffer12,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_iafbquad,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_iafbquad[0]);
		
		ByteBuffer byteBuffer23=ByteBuffer.allocateDirect(biafColor.length*4);
		byteBuffer23.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer12=byteBuffer23.asFloatBuffer();
		colorBuffer12.put(biafColor);
		colorBuffer12.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,biafColor.length*4,colorBuffer12,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);


		//================================================================================================
		GLES32.glGenVertexArrays(1,vao_biggerwings1,0); 
		GLES32.glBindVertexArray(vao_biggerwings1[0]);
		
		GLES32.glGenBuffers(1,vbo_position_biggerwings1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_biggerwings1[0]);
		
		ByteBuffer byteBuffer24=ByteBuffer.allocateDirect(bigVertices1.length*4);
		byteBuffer24.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer14=byteBuffer24.asFloatBuffer();
		positionBuffer14.put(bigVertices1);
		positionBuffer14.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,bigVertices1.length*4,positionBuffer14,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_biggerwings1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_biggerwings1[0]);
		
		ByteBuffer byteBuffer25=ByteBuffer.allocateDirect(bigColor1.length*4);
		byteBuffer25.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer13=byteBuffer25.asFloatBuffer();
		colorBuffer13.put(bigColor1);
		colorBuffer13.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,bigColor1.length*4,colorBuffer13,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);


		//===========================================================================================
		GLES32.glGenVertexArrays(1,vao_biggerwings2,0); 
		GLES32.glBindVertexArray(vao_biggerwings2[0]);
		
		GLES32.glGenBuffers(1,vbo_position_biggerwings2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_biggerwings2[0]);
		
		ByteBuffer byteBuffer26=ByteBuffer.allocateDirect(bigVertices2.length*4);
		byteBuffer26.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer15=byteBuffer26.asFloatBuffer();
		positionBuffer15.put(bigVertices2);
		positionBuffer15.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,bigVertices2.length*4,positionBuffer15,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_biggerwings2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_biggerwings2[0]);
		
		ByteBuffer byteBuffer27=ByteBuffer.allocateDirect(bigColor2.length*4);
		byteBuffer27.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer15=byteBuffer27.asFloatBuffer();
		colorBuffer15.put(bigColor2);
		colorBuffer15.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,bigColor2.length*4,colorBuffer15,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//====================================================================================================
		GLES32.glGenVertexArrays(1,vao_smallerwings1,0); 
		GLES32.glBindVertexArray(vao_smallerwings1[0]);
		
		GLES32.glGenBuffers(1,vbo_position_smallerwings1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_smallerwings1[0]);
		
		ByteBuffer byteBuffer28=ByteBuffer.allocateDirect(smallVertices1.length*4);
		byteBuffer28.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer16=byteBuffer28.asFloatBuffer();
		positionBuffer16.put(smallVertices1);
		positionBuffer16.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,smallVertices1.length*4,positionBuffer16,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_smallerwings1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_smallerwings1[0]);
		
		ByteBuffer byteBuffer29=ByteBuffer.allocateDirect(smallColor1.length*4);
		byteBuffer29.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer16=byteBuffer29.asFloatBuffer();
		colorBuffer16.put(smallColor1);
		colorBuffer16.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,smallColor1.length*4,colorBuffer16,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//===================================================================================================

		GLES32.glGenVertexArrays(1,vao_smallerwings2,0); 
		GLES32.glBindVertexArray(vao_smallerwings2[0]);
		
		GLES32.glGenBuffers(1,vbo_position_smallerwings2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_smallerwings2[0]);
		
		ByteBuffer byteBuffer30=ByteBuffer.allocateDirect(smallVertices2.length*4);
		byteBuffer30.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer17=byteBuffer30.asFloatBuffer();
		positionBuffer17.put(smallVertices2);
		positionBuffer17.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,smallVertices2.length*4,positionBuffer17,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_smallerwings2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_smallerwings2[0]);
		
		ByteBuffer byteBuffer42=ByteBuffer.allocateDirect(smallColor2.length*4);
		byteBuffer42.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer17=byteBuffer42.asFloatBuffer();
		colorBuffer17.put(smallColor2);
		colorBuffer17.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,smallColor2.length*4,colorBuffer17,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//=================================================================================================
		GLES32.glGenVertexArrays(1,vao_pi,0); 
		GLES32.glBindVertexArray(vao_pi[0]);
		
		GLES32.glGenBuffers(1,vbo_position_pi,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_pi[0]);
		
		ByteBuffer byteBuffer31=ByteBuffer.allocateDirect(piPosition.length*4);
		byteBuffer31.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer18=byteBuffer31.asFloatBuffer();
		positionBuffer18.put(piPosition);
		positionBuffer18.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,piPosition.length*4,positionBuffer18,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_pi,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_pi[0]);
		
		ByteBuffer byteBuffer32=ByteBuffer.allocateDirect(piColor.length*4);
		byteBuffer32.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer18=byteBuffer32.asFloatBuffer();
		colorBuffer18.put(piColor);
		colorBuffer18.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,piColor.length*4,colorBuffer18,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//===============================================================================================

		GLES32.glGenVertexArrays(1,vao_pa,0); 
		GLES32.glBindVertexArray(vao_pa[0]);
		
		GLES32.glGenBuffers(1,vbo_position_pa,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_pa[0]);
		
		ByteBuffer byteBuffer33=ByteBuffer.allocateDirect(paPosition.length*4);
		byteBuffer33.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer19=byteBuffer33.asFloatBuffer();
		positionBuffer19.put(paPosition);
		positionBuffer19.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,paPosition.length*4,positionBuffer19,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_pa,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_pa[0]);
		
		ByteBuffer byteBuffer34=ByteBuffer.allocateDirect(paColor.length*4);
		byteBuffer34.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer19=byteBuffer34.asFloatBuffer();
		colorBuffer19.put(paColor);
		colorBuffer19.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,paColor.length*4,colorBuffer19,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//===============================================================================================
		GLES32.glGenVertexArrays(1,vao_pf,0); 
		GLES32.glBindVertexArray(vao_pf[0]);
		
		GLES32.glGenBuffers(1,vbo_position_pf,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_pf[0]);
		
		ByteBuffer byteBuffer43=ByteBuffer.allocateDirect(pfPosition.length*4);
		byteBuffer43.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer20=byteBuffer43.asFloatBuffer();
		positionBuffer20.put(pfPosition);
		positionBuffer20.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,pfPosition.length*4,positionBuffer20,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_pf,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_pf[0]);
		
		ByteBuffer byteBuffer35=ByteBuffer.allocateDirect(pfColor.length*4);
		byteBuffer35.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer20=byteBuffer35.asFloatBuffer();
		colorBuffer20.put(pfColor);
		colorBuffer20.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,pfColor.length*4,colorBuffer20,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//===================================================================================================

		GLES32.glGenVertexArrays(1,vao_line4,0); 
		GLES32.glBindVertexArray(vao_line4[0]);
		
		GLES32.glGenBuffers(1,vbo_position_line4,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line4[0]);
		
		ByteBuffer byteBuffer36=ByteBuffer.allocateDirect(line4Vertices.length*4);
		byteBuffer36.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer21=byteBuffer36.asFloatBuffer();
		positionBuffer21.put(line4Vertices);
		positionBuffer21.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line4Vertices.length*4,positionBuffer21,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line4,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line4[0]);
		
		ByteBuffer byteBuffer37=ByteBuffer.allocateDirect(line4Color.length*4);
		byteBuffer37.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer21=byteBuffer37.asFloatBuffer();
		colorBuffer21.put(line4Color);
		colorBuffer21.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line4Color.length*4,colorBuffer21,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//==============================================================================================

		GLES32.glGenVertexArrays(1,vao_line5,0); 
		GLES32.glBindVertexArray(vao_line5[0]);
		
		GLES32.glGenBuffers(1,vbo_position_line5,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line5[0]);
		
		ByteBuffer byteBuffer38=ByteBuffer.allocateDirect(line5Vertices.length*4);
		byteBuffer38.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer22=byteBuffer38.asFloatBuffer();
		positionBuffer22.put(line5Vertices);
		positionBuffer22.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line5Vertices.length*4,positionBuffer22,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line5,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line5[0]);
		
		ByteBuffer byteBuffer39=ByteBuffer.allocateDirect(line5Color.length*4);
		byteBuffer39.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer22=byteBuffer39.asFloatBuffer();
		colorBuffer22.put(line5Color);
		colorBuffer22.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line5Color.length*4,colorBuffer22,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//======================================================================================================
		GLES32.glGenVertexArrays(1,vao_line6,0); 
		GLES32.glBindVertexArray(vao_line6[0]);
		
		GLES32.glGenBuffers(1,vbo_position_line6,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line6[0]);
		
		ByteBuffer byteBuffer40=ByteBuffer.allocateDirect(line6Vertices.length*4);
		byteBuffer40.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer23=byteBuffer40.asFloatBuffer();
		positionBuffer23.put(line6Vertices);
		positionBuffer23.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line6Vertices.length*4,positionBuffer23,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line6,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line6[0]);
		
		ByteBuffer byteBuffer41=ByteBuffer.allocateDirect(line6Color.length*4);
		byteBuffer41.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer23=byteBuffer41.asFloatBuffer();
		colorBuffer23.put(line6Color);
		colorBuffer23.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line6Color.length*4,colorBuffer23,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//i***********************************************************************************************
		GLES32.glGenVertexArrays(1,vao_i1,0);
		GLES32.glBindVertexArray(vao_i1[0]);
		
		GLES32.glGenBuffers(1,vbo_position_i1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_i1[0]);
		
		ByteBuffer byteBuffer=ByteBuffer.allocateDirect(i1.length*4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer=byteBuffer.asFloatBuffer();
		positionBuffer.put(i1);
		positionBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i1.length*4,positionBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_i1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_i1[0]);
		
		ByteBuffer byteBuffer1=ByteBuffer.allocateDirect(i1Color.length*4);
		byteBuffer1.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer=byteBuffer1.asFloatBuffer();
		colorBuffer.put(i1Color);
		colorBuffer.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i1Color.length*4,colorBuffer,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);

		//*****************************************************************************************************************
		GLES32.glGenVertexArrays(1,vao_i2,0);
		GLES32.glBindVertexArray(vao_i2[0]);
		GLES32.glGenBuffers(1,vbo_position_i2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_i2[0]);
		
		ByteBuffer byteBuffer2=ByteBuffer.allocateDirect(i2.length*4);
		byteBuffer2.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer1=byteBuffer2.asFloatBuffer();
		positionBuffer1.put(i2);
		positionBuffer1.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i2.length*4,positionBuffer1,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_i2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_i2[0]);
		
		ByteBuffer byteBuffer3=ByteBuffer.allocateDirect(i1Color.length*4);
		byteBuffer3.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer1=byteBuffer3.asFloatBuffer();
		colorBuffer1.put(i2Color);
		colorBuffer1.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,i2Color.length*4,colorBuffer1,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//****************************************************************************************************************************
		
		GLES32.glGenVertexArrays(1,vao_n,0);
		GLES32.glBindVertexArray(vao_n[0]);
		GLES32.glGenBuffers(1,vbo_position_n,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_n[0]);
		
		ByteBuffer byteBuffer4=ByteBuffer.allocateDirect(n.length*4);
		byteBuffer4.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer2=byteBuffer4.asFloatBuffer();
		positionBuffer2.put(n);
		positionBuffer2.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,n.length*4,positionBuffer2,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_n,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_n[0]);
		
		ByteBuffer byteBuffer5=ByteBuffer.allocateDirect(nColor.length*4);
		byteBuffer5.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer2=byteBuffer5.asFloatBuffer();
		colorBuffer2.put(nColor);
		colorBuffer2.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,nColor.length*4,colorBuffer2,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//*****************************************************************************************************************************
		
		
		//****************************************************************************************************************************
		
		GLES32.glGenVertexArrays(1,vao_d,0);
		GLES32.glBindVertexArray(vao_d[0]);
		GLES32.glGenBuffers(1,vbo_position_d,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_d[0]);
		
		ByteBuffer byteBuffer6=ByteBuffer.allocateDirect(d.length*4);
		byteBuffer6.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer4=byteBuffer6.asFloatBuffer();
		positionBuffer4.put(d);
		positionBuffer4.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,d.length*4,positionBuffer4,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_d,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_d[0]);
		
		ByteBuffer byteBuffer7=ByteBuffer.allocateDirect(dColor.length*4);
		byteBuffer7.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer4=byteBuffer7.asFloatBuffer();
		colorBuffer4.put(dColor);
		colorBuffer4.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,dColor.length*4,colorBuffer4,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		//**************************************************************************************************************
		
		
		//****************************************************************************************************************************
		
		GLES32.glGenVertexArrays(1,vao_a,0);
		GLES32.glBindVertexArray(vao_a[0]); 
		GLES32.glGenBuffers(1,vbo_position_a,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_a[0]);
		
		ByteBuffer byteBuffer8=ByteBuffer.allocateDirect(a.length*4);
		byteBuffer8.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer5=byteBuffer8.asFloatBuffer();
		positionBuffer5.put(a);
		positionBuffer5.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,a.length*4,positionBuffer5,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_a,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_a[0]);
		
		ByteBuffer byteBuffer9=ByteBuffer.allocateDirect(aColor.length*4);
		byteBuffer9.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer5=byteBuffer9.asFloatBuffer();
		colorBuffer5.put(aColor);
		colorBuffer5.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,aColor.length*4,colorBuffer5,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		
		//***************************************************************************************************************************
		GLES32.glGenVertexArrays(1,vao_line1,0);
		GLES32.glBindVertexArray(vao_line1[0]); 
		GLES32.glGenBuffers(1,vbo_position_line1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line1[0]);
		
		ByteBuffer byteBuffer15=ByteBuffer.allocateDirect(line1Vertices.length*4);
		byteBuffer15.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer6=byteBuffer15.asFloatBuffer();
		positionBuffer6.put(line1Vertices);
		positionBuffer6.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line1Vertices.length*4,positionBuffer6,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line1[0]);
		
		ByteBuffer byteBuffer16=ByteBuffer.allocateDirect(line1Color.length*4);
		byteBuffer16.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer6=byteBuffer16.asFloatBuffer();
		colorBuffer6.put(line1Color);
		colorBuffer6.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line1Color.length*4,colorBuffer6,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//******************************************************************************************************************************************
		GLES32.glGenVertexArrays(1,vao_line2,0);
		GLES32.glBindVertexArray(vao_line2[0]); 
		GLES32.glGenBuffers(1,vbo_position_line2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line2[0]);
		
		ByteBuffer byteBuffer11=ByteBuffer.allocateDirect(line2Vertices.length*4);
		byteBuffer11.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer7=byteBuffer11.asFloatBuffer();
		positionBuffer7.put(line2Vertices);
		positionBuffer7.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line2Vertices.length*4,positionBuffer7,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line2[0]);
		
		ByteBuffer byteBuffer12=ByteBuffer.allocateDirect(line2Color.length*4);
		byteBuffer12.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer7=byteBuffer12.asFloatBuffer();
		colorBuffer7.put(line2Color);
		colorBuffer7.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line2Color.length*4,colorBuffer7,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		//*********************************************************************************************************************************
		
		
		GLES32.glGenVertexArrays(1,vao_line3,0);
		GLES32.glBindVertexArray(vao_line3[0]); 
		GLES32.glGenBuffers(1,vbo_position_line3,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_position_line3[0]);
		
		ByteBuffer byteBuffer13=ByteBuffer.allocateDirect(line3Vertices.length*4);
		byteBuffer13.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer8=byteBuffer13.asFloatBuffer();
		positionBuffer8.put(line3Vertices);
		positionBuffer8.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line3Vertices.length*4,positionBuffer8,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_color_line1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_color_line1[0]);
		
		ByteBuffer byteBuffer14=ByteBuffer.allocateDirect(line3Color.length*4);
		byteBuffer14.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer8=byteBuffer14.asFloatBuffer();
		colorBuffer8.put(line3Color);
		colorBuffer8.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line3Color.length*4,colorBuffer8,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		//===================================================================================
		
		//**********************************************************************************************************************************
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	}
	private void resize(int width,int height)
	{
		if(height==0)
			height=1;
		GLES32.glViewport(0,0,width,height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	private void display()
	{	
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		float[] modelViewProjectionMatrix=new float[16];
		float[] modelViewMatrix=new float[16];
		float[] scaleMatrix=new float[16];

		//triangle
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,translatei1, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_i1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);
		
		
		//rectangle
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,-0.3f, translaten, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_n[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glBindVertexArray(0);
		
		
		//d
		if(flagd==1)
		{
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_d[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 8, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 12, 4);
		GLES32.glBindVertexArray(0);
		flagp=1;
		}
		
		//I
				Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.3f, translatei2, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_i2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);
		
		
		//A
				Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,translatea, 0.0f, -3.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_a[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4);
		GLES32.glBindVertexArray(0);
		
		//Line1
		if(flag4==1)
		{
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.2f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
		//Line2
		
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.2f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
		//Line3
		
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.2f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line3[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		}

		if(flagp==1){


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_apex[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafbquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pi[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pa[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex,planey,planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pf[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey+0.05f, planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey+0.02f, planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey-0.01f, planez);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line3[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		//=======================================================================================
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_apex[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafbquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pi[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pa[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex1, planey1, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pf[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey1+0.05f, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey1+0.02f, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey1-0.01f, planez1);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line3[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		//=====================================================================================
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_apex[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafbquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pi[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pa[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex2, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pf[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, 0.07f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, 0.03f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, 0.0f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line3[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		//======================================================================================

		if(flagp3==1)
		{
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_apex[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafbquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pi[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pa[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex3, planey3, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pf[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey3+0.05f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey3+0.02f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey3-0.01f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line3[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		//===============================================================================================
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_apex[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_iafbquad[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_biggerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings1[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_smallerwings2[0]);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pi[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pa[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);


		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,planex4, planey4, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_pf[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 2, 2);
		GLES32.glDrawArrays(GLES32.GL_LINES, 4, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey4+0.05f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey4+0.02f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);
		Matrix.translateM(modelViewMatrix,0,linex1, planey4-0.01f, -3.0f);
		Matrix.scaleM(scaleMatrix,0,0.5f,0.5f,0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_line3[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);

		//============================================================================================
		}
		}
		
		GLES32.glUseProgram(0);
		Update();
		requestRender();
	}
	private void Update(){

		if(translatei1<=-0.6f)
	{
		translatei1=translatei1+0.02f;
	}
	if(translatea>=0.6f)
	{
		translatea=translatea-0.02f;
	}
	if(translaten>=0.0f)
	{
		translaten=translaten-0.02f;
	}
	if(translatei2<=0.0f)
	{
		translatei2=translatei2+0.02f;
		//System.out.println("iRTR:Transalte i2"+translatei2);
		if(translatei2==0.019993413f)
		flagd=1;

	}
	if(flagp==1)
	{
	
	if(planex<=0.0f)
	{
		planex=planex+0.01f;
		//System.out.println("iRTR:Transalte planex"+planex);
		if(planex==0.009996468f)
		{
		planez=5.0f;
		}
	}
	if(planey<=0.0f)
	{
		planey=planey+0.01f;
	}

	if(planex1<=0.0f)
	{
		planex1=planex1+0.01f;

		
		if(planex1==0.009996468f)
		planez1=5.0f;
		
	}
	if(planey1>=0.0f)
	{
		planey1=planey1-0.01f;
	}

		
	if(planex2<=3.0f)
	{
		
		planex2=planex2+0.01f;
		System.out.println("iRTR:Transalte planex2"+planex2);
		linex1=linex1+0.01f;
		
		if(planex2==0.9999958f)
		flagp3=1;
		if(planex2==2.049995f)
		flag4=1;
		
	} 
	
	if(flagp3==1)
	{
		if(planex3<=3.0f)
			planex3=planex3+0.01f;
		if(planey3<=2.0f)
			planey3=planey3+0.01f;
		if(planex4<=3.0f)
			planex4=planex4+0.01f;
		if(planey4<=2.0f)
			planey4=planey4-0.01f;
	}
		
   	}

	}
	private void uninitialize()
	{
		if (vbo_rectangle[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_rectangle,0);
			vbo_rectangle[0] = 0;
		}
		if (vao_rectangle[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_rectangle,0);
			vao_rectangle[0] = 0;
		}
		if (vbo_triangle[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_triangle,0);
			vbo_triangle[0] = 0;
		}
		if (vao_triangle[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_triangle,0);
			vao_triangle[0] = 0;
		}
		if (shaderProgramObject != 0) {
			int[] shaderCount=new int[1];

			GLES32.glUseProgram(shaderProgramObject);
			
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);	
	
			if (shaderCount[0]!=0) {
				
				int shaderNumber;
				int[] shaders = new int[shaderCount[0]];

				GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
	
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}	
	}
}
