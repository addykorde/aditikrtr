package com.example.TessellationShader;

//Added by me


import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;
import java.lang.Math;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener{
	
	private static Context context;
	private GestureDetector gestureDetector;
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int gTessellationControlShaderObject;
	private int gTessellationEvaluationShaderObject;
	private int shaderProgramObject;
	
	
	
	private int[] vao_line=new int[1];
	private int[] vbo_line_position=new int[1];
	private int[] vbo_line_color=new int[1];
	
	private int mvpUniform;
	private int gNumberOfSegmentUniform;
	private int gNumberOfStripsUniform;
	private int gLineColorUniform;

	private int gNumberOfLineSegments=1;
	private float[] perspectiveProjectionMatrix=new float[16];
	private float[] gColorLine=new float[16];
	float angle_triangle=0.0f;
	float angle_rectangle=0.0f;
	FloatBuffer positionBuffer1;
	public GLESView(Context drawingContext){
		super(drawingContext);
		context=drawingContext;
		gestureDetector=new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		gNumberOfLineSegments++;
			if (gNumberOfLineSegments >= 50)
				gNumberOfLineSegments = 50;
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityx,float velocityy)
	{
		
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
		gNumberOfLineSegments--;
			if (gNumberOfLineSegments <= 0)
				gNumberOfLineSegments = 1;
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distancex,float distancey)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR"+version);
		String version1=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR"+version1);
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		
		display();
	}
	
	//Our custom methods
	
	private void initialize()
	{

		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec2 vPosition;"+
			"void main(void)"+
			"{"+
			"gl_Position=vec4(vPosition,0.0,1.0);"+
			"}"
		);

		GLES32.glShaderSource(gVertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(gVertexShaderObject);
				


		//Error checking
		int[] iShaderCompileStatus=new int[1];
		int[] iInfoLogLength=new int[1];
		String szInfoLog=null;
		GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("iRTR:VertexShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
//================================================================================================================

		gTessellationControlShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);
		final String gTessellationControlShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"layout(vertices=4)out;"+
			"uniform int numberOfSegments;"+
			"uniform int numberOfStrips;"+
			"void main(void)"+
			"{"+
			"gl_out[gl_InvocationID].gl_Position=gl_in[gl_InvocationID].gl_Position;"+
			"gl_TessLevelOuter[0]=float(numberOfStrips);"+
			"gl_TessLevelOuter[1]=float(numberOfSegments);"+
			"}"
		);

		GLES32.glShaderSource(gTessellationControlShaderObject,gTessellationControlShaderSourceCode);
		GLES32.glCompileShader(gTessellationControlShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gTessellationControlShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gTessellationControlShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gTessellationControlShaderObject);
				System.out.println("iRTR:TessellationControlShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}


//===================================================================================================================

gTessellationEvaluationShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);
		final String gTessellationEvaluationShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"layout(isolines)in;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"float u=gl_TessCoord.x;"+
			"vec3 p0=gl_in[0].gl_Position.xyz;"+
			"vec3 p1=gl_in[1].gl_Position.xyz;"+
			"vec3 p2=gl_in[2].gl_Position.xyz;"+
			"vec3 p3=gl_in[3].gl_Position.xyz;"+
			"float u1=(1.0-u);"+
			"float u2=u*u;"+
			"float b3=u2*u;"+
			"float b2=3.0*u2*u1;"+
			"float b1=3.0*u*u1*u1;"+
			"float b0=u1*u1*u1;"+
			"vec3 p=p0*b0+p1*b1+p2*b2+p3*b3;"+
			"gl_Position=u_mvp_matrix*vec4(p,1.0);"+
			"}"
		);

		GLES32.glShaderSource(gTessellationEvaluationShaderObject,gTessellationEvaluationShaderSourceCode);
		GLES32.glCompileShader(gTessellationEvaluationShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gTessellationEvaluationShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gTessellationEvaluationShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gTessellationEvaluationShaderObject);
				System.out.println("iRTR:TessellationEvaluationShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

//====================================================================================================================		
		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"out vec4 FragColor;"+
			"uniform vec4 lineColor;"+
			"void main(void)"+
			"{"+
			"FragColor=lineColor;"+
			"}"	
		);

		GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(gFragmentShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("iRTR:FragmentShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject,gVertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gTessellationControlShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gTessellationEvaluationShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gFragmentShaderObject);
		

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");	
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);

	if (iShaderLinkStatus[0] == GLES32.GL_FALSE) {
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		if (iInfoLogLength[0] > 0) {
			szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);

			System.out.println("iRTR:shaderProgramObject"+szInfoLog);
			uninitialize();
			System.exit(0);	
			
		}
	}

		mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		gNumberOfSegmentUniform = GLES32.glGetUniformLocation(shaderProgramObject,"numberOfSegments");
		gNumberOfStripsUniform = GLES32.glGetUniformLocation(shaderProgramObject,"numberOfStrips");
		gLineColorUniform = GLES32.glGetUniformLocation(shaderProgramObject, "lineColor");

		
		
		final float[] lineVertices=new float[]{
			 -1.0f,-1.0f,-0.5f,1.0f,0.5f,-1.0f,1.0f,1.0f
		};
		
	
		GLES32.glBindVertexArray(0);
		
		
		GLES32.glGenVertexArrays(1,vao_line,0);
		GLES32.glBindVertexArray(vao_line[0]);
		
		
		
		GLES32.glGenBuffers(1,vbo_line_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_line_position[0]);
		ByteBuffer byteBuffer5=ByteBuffer.allocateDirect(lineVertices.length*4);
		byteBuffer5.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer5=byteBuffer5.asFloatBuffer();
		positionBuffer5.put(lineVertices);
		positionBuffer5.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,8*4,positionBuffer5,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,2,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		
		GLES32.glBindVertexArray(0);
		
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	}
	private void resize(int width,int height)
	{
		if(height==0)
			height=1;
		GLES32.glViewport(0,0,width,height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	private void display()
	{	
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		float[] modelViewProjectionMatrix=new float[16];
		float[] modelViewMatrix=new float[16];
		
		
		//triangle
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		//Matrix.setIdentityM(gColorLine,0);
	
		
		Matrix.translateM(modelViewMatrix,0,0.0f,0.0f,-4.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glUniform1i(gNumberOfSegmentUniform,gNumberOfLineSegments);
		GLES32.glUniform1i(gNumberOfStripsUniform, 1);
		GLES32.glUniform4f(gLineColorUniform, 1.0f, 1.0f, 0.0f, 1.0f);
		GLES32.glBindVertexArray(vao_line[0]);
		GLES32.glPatchParameteri(GLES32.GL_PATCH_VERTICES, 4);
		GLES32.glDrawArrays(GLES32.GL_PATCHES, 0, 4);
		GLES32.glBindVertexArray(0);
		
		
		
		GLES32.glUseProgram(0);

		requestRender();
	}
	private void Update()
	{
		
	}
	private void uninitialize()
	{
		if (vbo_line_position[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_line_position,0);
			vbo_line_position[0] = 0;
		}
		if (vao_line[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_line,0);
			vao_line[0] = 0;
		}
		if (shaderProgramObject != 0) {
			int[] shaderCount=new int[1];

			GLES32.glUseProgram(shaderProgramObject);
			
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);	
	
			if (shaderCount[0]!=0) {
				
				int shaderNumber;
				int[] shaders = new int[shaderCount[0]];

				GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
	
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}	
	}
}
