package com.example.graph;

//Added by me


import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import android.opengl.Matrix;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener{
	
	private static Context context;
	private GestureDetector gestureDetector;
	private int gVertexShaderObject;
	private int gFragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_lines1=new int[1];
	private int[] vbo_lines1=new int[1];
	private int[] vbo_line1_color=new int[1];

	private int[] vao_lines2=new int[1];
	private int[] vbo_lines2=new int[1];
	private int[] vbo_line2_color=new int[1];
	
	private int[] vao_lines3=new int[1];
	private int[] vbo_lines3=new int[1];
	private int[] vbo_line3_color=new int[1];
	
	private int[] vao_lines4=new int[1];
	private int[] vbo_lines4=new int[1];
	private int[] vbo_line4_color=new int[1];	
	
	private int[] vao_circle=new int[1];
	private int[] vbo_circle_color=new int[1];
	private int[] vbo_circle_position=new int[1];
	
	private int[] vao_glines=new int[1];
	private int[] vbo_glines=new int[1];
	private int[] vbo_glines_color=new int[1];

	private int[] vao_glines1=new int[1];
	private int[] vbo_glines1=new int[1];
	private int[] vbo_glines1_color=new int[1];
	
	private int[] vao_square1=new int[1];
	private int[] vbo_position_square1=new int[1];
	private int[] vbo_color_square1=new int[1];
	
	private int[] vao_square2=new int[1];
	private int[] vbo_position_square2=new int[1];
	private int[] vbo_color_square2=new int[1];	
	
	private int[] vao_square3=new int[1];
	private int[] vbo_position_square3=new int[1];
	private int[] vbo_color_square3=new int[1];	
	
	private int[] vao_square4=new int[1];
	private int[] vbo_position_square4=new int[1];
	private int[] vbo_color_square4=new int[1];
	
	private int[] vao_circle2=new int[1];
	private int[] vbo_circle2_position=new int[1];
	private int[] vbo_cicle2_color=new int[1];	
	
	
	private int mvpUniform;
	private float[] perspectiveProjectionMatrix=new float[16];


	public GLESView(Context drawingContext){
		super(drawingContext);
		context=drawingContext;
		gestureDetector=new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventaction=event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}
	
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityx,float velocityy)
	{
		
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distancex,float distancey)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	@Override
	public void onShowPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	@Override
	public void onSurfaceCreated(GL10 gl,EGLConfig config)
	{
		String version=gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR"+version);
		String version1=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR"+version1);
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused,int width,int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	//Our custom methods
	
	private void initialize()
	{

		gVertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"gl_Position=u_mvp_matrix * vPosition;"+
			"out_color=vColor;"+
			"}"
		);

		GLES32.glShaderSource(gVertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(gVertexShaderObject);
				


		//Error checking
		int[] iShaderCompileStatus=new int[1];
		int[] iInfoLogLength=new int[1];
		String szInfoLog=null;
		GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gVertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gVertexShaderObject);
				System.out.println("iRTR:VertexShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		

		gFragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode=
		String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor=out_color;"+
			"}"	
		);

		GLES32.glShaderSource(gFragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(gFragmentShaderObject);
				


		//Error checking
		iShaderCompileStatus[0]=0;
		iInfoLogLength[0]=0;
		szInfoLog=null;
		GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

		if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(gFragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0]>0)
			{
				szInfoLog=GLES32.glGetShaderInfoLog(gFragmentShaderObject);
				System.out.println("iRTR:FragmentShaderObject"+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		shaderProgramObject=GLES32.glCreateProgram();
		GLES32.glAttachShader(shaderProgramObject,gVertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,gFragmentShaderObject);

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");	
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR,"vColor");
		GLES32.glLinkProgram(shaderProgramObject);
		
		int[] iShaderLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);

	if (iShaderLinkStatus[0] == GLES32.GL_FALSE) {
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);

		if (iInfoLogLength[0] > 0) {
			szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);

			System.out.println("iRTR:shaderProgramObject"+szInfoLog);
			uninitialize();
			System.exit(0);	
			
		}
	}

		mvpUniform=GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		final float[] lineVertices=new float[]
		{
			-1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f
		};
		final float[] line1Vertices=new float[]
		{
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};
		
		
		
		final float[] glineColor=new float[]
		{
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f
		};
		
			final float[] glineColor1=new float[]
		{
			0.0f,1.0f,0.0f,
			0.0f,1.0f,0.0f
		};
		
			final float[] lineColor=new float[]
		{
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f,
		};
			final float[] glineVertices=new float[]
		{
			-1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f
		};
		
			final float[] gline1Vertices=new float[]
		{
			0.0f,1.0f,0.0f,
			0.0f,-1.0f,0.0f
		};
		
			
	
		//=============================================================================================================
		
		
		
		GLES32.glGenVertexArrays(1,vao_lines1,0);
		GLES32.glBindVertexArray(vao_lines1[0]);
		
		GLES32.glGenBuffers(1,vbo_lines1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_lines1[0]);
		ByteBuffer byteBuffer9=ByteBuffer.allocateDirect(lineVertices.length*4);
		byteBuffer9.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer9=byteBuffer9.asFloatBuffer();
		positionBuffer9.put(lineVertices);
		positionBuffer9.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,lineVertices.length*4,positionBuffer9,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_line1_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_line1_color[0]);
		ByteBuffer byteBuffer14=ByteBuffer.allocateDirect(lineColor.length*4);
		byteBuffer14.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer14=byteBuffer14.asFloatBuffer();
		colorBuffer14.put(lineColor);
		colorBuffer14.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,lineColor.length*4,colorBuffer14,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//==================================================================================================================
		
			
		GLES32.glGenVertexArrays(1,vao_lines2,0);
		GLES32.glBindVertexArray(vao_lines2[0]);
		
		GLES32.glGenBuffers(1,vbo_lines2,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_lines2[0]);
		ByteBuffer byteBuffer10=ByteBuffer.allocateDirect(line1Vertices.length*4);
		byteBuffer10.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer10=byteBuffer10.asFloatBuffer();
		positionBuffer10.put(line1Vertices);
		positionBuffer10.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,line1Vertices.length*4,positionBuffer10,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_line2_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_line2_color[0]);
		ByteBuffer byteBuffer11=ByteBuffer.allocateDirect(lineColor.length*4);
		byteBuffer11.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer11=byteBuffer11.asFloatBuffer();
		colorBuffer11.put(lineColor);
		colorBuffer11.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,lineColor.length*4,colorBuffer11,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//=============================================================================================================
		 	
		
		//==========================================================================================================
		GLES32.glGenVertexArrays(1,vao_glines,0);
		GLES32.glBindVertexArray(vao_glines[0]);
		
		GLES32.glGenBuffers(1,vbo_glines,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_glines[0]);
		ByteBuffer byteBuffer17=ByteBuffer.allocateDirect(glineVertices.length*4);
		byteBuffer17.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer17=byteBuffer17.asFloatBuffer();
		positionBuffer17.put(glineVertices);
		positionBuffer17.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,glineVertices.length*4,positionBuffer17,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_glines_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_glines_color[0]);
		ByteBuffer byteBuffer18=ByteBuffer.allocateDirect(glineColor.length*4);
		byteBuffer18.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer18=byteBuffer18.asFloatBuffer();
		colorBuffer18.put(glineColor);
		colorBuffer18.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,glineColor.length*4,colorBuffer18,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		//=======================================================================================================
		GLES32.glGenVertexArrays(1,vao_glines1,0);
		GLES32.glBindVertexArray(vao_glines1[0]);
		
		GLES32.glGenBuffers(1,vbo_glines1,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_glines1[0]);
		ByteBuffer byteBuffer19=ByteBuffer.allocateDirect(gline1Vertices.length*4);
		byteBuffer19.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer19=byteBuffer19.asFloatBuffer();
		positionBuffer19.put(gline1Vertices);
		positionBuffer19.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,gline1Vertices.length*4,positionBuffer19,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glGenBuffers(1,vbo_glines1_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_glines1_color[0]);
		ByteBuffer byteBuffer20=ByteBuffer.allocateDirect(glineColor.length*4);
		byteBuffer20.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer20=byteBuffer20.asFloatBuffer();
		colorBuffer20.put(glineColor);
		colorBuffer20.position(0);
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,glineColor.length*4,colorBuffer20,GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
		
		GLES32.glBindVertexArray(0);
		
		//=========================================================================================================
			
		
		
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);
	}
	private void resize(int width,int height)
	{
		if(height==0)
			height=1;
		GLES32.glViewport(0,0,width,height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}
	private void display()
	{	
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		float[] modelViewProjectionMatrix=new float[16];
		float[] modelViewMatrix=new float[16];
		
		//graphs
		
		for(float x = -1.0f; x <= 1.05f; x = x + 0.05f)
		{
			Matrix.setIdentityM(modelViewProjectionMatrix,0);
			Matrix.setIdentityM(modelViewMatrix,0);
			Matrix.translateM(modelViewMatrix,0,0.0f, x, -2.0f);
			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
			GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
			GLES32.glBindVertexArray(vao_glines[0]);
			GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
			GLES32.glBindVertexArray(0);
		}
		
		for(float y = -1.0f; y <= 1.05f; y = y + 0.05f)
		{
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,y, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_glines1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
		}
		
		
		
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_lines1[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
		
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f, -2.0f);
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glBindVertexArray(vao_lines2[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);
		GLES32.glBindVertexArray(0);
		
	
		
		GLES32.glUseProgram(0);
		requestRender();
	}
	private void uninitialize()
	{
	/*	if (vbo_rectangle_position[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_rectangle_position,0);
			vbo_rectangle_position[0] = 0;
		}
		if (vbo_rectangle_color[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_rectangle_color,0);
			vbo_rectangle_color[0] = 0;
		}
		if (vao_rectangle[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_rectangle,0);
			vao_rectangle[0] = 0;
		}
		if (vbo_triangle_color[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_triangle_color,0);
			vbo_triangle_color[0] = 0;
		}
		if (vbo_triangle_position[0]!=0) {
			GLES32.glDeleteBuffers(1,vbo_triangle_position,0);
			vbo_triangle_position[0] = 0;
		}
		if (vao_triangle[0]!=0) {
			GLES32.glDeleteVertexArrays(1,vao_triangle,0);
			vao_triangle[0] = 0;
		}*/
		if (shaderProgramObject != 0) {
			int[] shaderCount=new int[1];

			GLES32.glUseProgram(shaderProgramObject);
			
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);	
	
			if (shaderCount[0]!=0) {
				
				int shaderNumber;
				int[] shaders = new int[shaderCount[0]];

				GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
	
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}	
	}
}
