//
//  AppDelegate.h
//  Dynamic India
//
//  Created by SHARVARI KORDE on 10/04/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


