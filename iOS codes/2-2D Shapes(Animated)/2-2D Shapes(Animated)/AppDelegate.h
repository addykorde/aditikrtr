//
//  AppDelegate.h
//  2-2D Shapes(Animated)
//
//  Created by SHARVARI KORDE on 18/01/20.
//  Copyright © 2020 SHARVARI KORDE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

