//
//  main.m
//  2-2D Shapes(Animated)
//
//  Created by SHARVARI KORDE on 18/01/20.
//  Copyright © 2020 SHARVARI KORDE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    appDelegateClassName = NSStringFromClass([AppDelegate class]);
    
    int res = UIApplicationMain(argc, argv, nil, appDelegateClassName);
    
    [pool release];
    
    return res;
}
