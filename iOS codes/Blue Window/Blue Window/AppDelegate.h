//
//  AppDelegate.h
//  Blue Window
//
//  Created by SHARVARI KORDE on 05/01/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


