//
//  GLESView.m
//  Toggle Shaders
//
//  Created by SHARVARI KORDE on 28/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//

#import<OpenGLES/ES3/gl.h>

#import<OpenGLES/Es3/glext.h>



#import "vmath.h"
#import "Sphere.h"
#import "GLESView.h"
#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64


enum {
    
    AMC_ATTRIBUTE_POSITION = 0,
    
    AMC_ATTRIBUTE_COLOR,
    
    AMC_ATTRIBUTE_NORMAL,
    
    AMC_ATTRIBUTE_TEXCOORD0
    
};

Sphere *sphere=[Sphere alloc];
int vkey=0;
int gbLight=0;
@implementation GLESView

{
    
    EAGLContext *eaglContext;
    
    
    
    GLuint defaultFramebuffer;
    
    GLuint colorRenderbuffer;
    
    GLuint depthRenderbuffer;
    
    
    
    id displayLink;
    
    NSInteger animationFrameInterval;
    
    BOOL isAnimating;
    
    GLuint texImage;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    GLuint gVertexShaderObject1;
    GLuint gFragmentShaderObject1;
    GLuint gShaderProgramObject1;
    
    
    int numVertices;
    int numElements;
    
    GLuint vao_sphere;
    
    GLuint vbo_sphere_position;
    
    GLuint vbo_sphere_normal;
    
    GLuint vbo_sphere_element;
    GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_position_pyramid;
    GLuint vbo_position_cube;
    GLuint vbo_color_pyramid;
    GLuint vbo_color_cube;
    GLuint mUniform;
    GLuint vUniform;
    GLuint pUniform;
    GLuint laUniform;
    GLuint kaUniform;
    GLuint lsUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint lightPositionUniform;
    GLuint isLKeypressedUniform;
    GLuint isLKeypressedUniform1;
    GLuint material_shininess_uniform;
    
    
    GLuint mUniform1;
    GLuint vUniform1;
    GLuint pUniform1;
    GLuint laUniform1;
    GLuint kaUniform1;
    GLuint lsUniform1;
    GLuint ldUniform1;
    GLuint kdUniform1;
    GLuint ksUniform1;
    GLuint lightPositionUniform1;
    GLuint isLKeypressedUniform2;
    GLuint isLKeypressedUniform3;
    GLuint material_shininess_uniform1;
    vmath::mat4 perspectiveProjectionMatrix;
    
    
    float light_ambient[4];
    float light_diffuse[4];
    float light_specular[4];
    float light_position[4];
    
    float material_ambient[4];
    float material_diffuse[4];
    float material_specular[4];
    float material_shininess[1];
    
    
    GLuint texture_sampler_uniform;
    GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
    
    
}



-(id)initWithFrame:(CGRect)frame;

{
    
    self=[super initWithFrame:frame];
    
    
    
    if(self)
        
    {
        
        
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        
        
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        
        
        if(eaglContext==nil)
            
        {
            
            [self release];
            
            return(nil);
            
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        
        
        glGenFramebuffers(1,&defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        
        
        GLint backingWidth;
        
        GLint backingHeight;
        
        
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
            
        {
            
            printf("Failed to create complete  framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            
            glDeleteFramebuffers(1, &defaultFramebuffer);
            
            glDeleteFramebuffers(1,&colorRenderbuffer);
            
            glDeleteFramebuffers(1, &depthRenderbuffer);
            
            
            
            return(nil);
            
        }
        
        
        
        printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        
        
        
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        float sphere_vertices[1146];
        
        float sphere_normals[1146];
        
        float sphere_textures[764];
        
        short sphere_elemets[2280];
        
        
        [sphere getSphereVertexData:sphere_vertices andArray1:sphere_normals andArray2:sphere_textures andArray3:sphere_elemets];
        
        
        
        
        
        numVertices=[sphere getNumberOfSphereVertices];
        
        numElements=[sphere getNumberOfSphereElements];
        
        
        gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode =
        
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vnormal;" \
        "uniform mat4 u_m_matrix;" \
        "uniform mat4 u_v_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform int u_lkeyispressed;" \
        "uniform vec4 u_light_position;" \
        "out vec4 eye_coordinates;" \
        "out vec3 light_direction;" \
        "out vec3 tnorm;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
        "if(u_lkeyispressed == 1)" \
        "{" \
        "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
        "tnorm=mat3(u_v_matrix*u_m_matrix)*vnormal;" \
        "light_direction=vec3(u_light_position)-eye_coordinates.xyz;" \
        "viewer_vector=vec3(-eye_coordinates);" \
        "}" \
        "else" \
        "{" \
        "}" \
        
        "}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        glCompileShader(gVertexShaderObject);

        GLint iInfoLogLength=0;
        
        GLint iShaderCompileStatus=0;
        
        char *szInfolog=NULL;
        
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Vertex Shader 1 %s", szInfolog);
                    
                    free(szInfolog);
                    
                    [self release];
                    
                }
                
            }
            
        }
        
        iInfoLogLength=0;
        
        iShaderCompileStatus=0;
        
        szInfolog=NULL;
        
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "out vec4 FragColor;" \
        "uniform int u_lkeyispressed1;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ls;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_ks;" \
        "uniform vec3 u_kd;" \
        "uniform float m_shininess;"
        "in vec3 light_direction;" \
        "in vec3 tnorm;"
        "in vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if(u_lkeyispressed1 == 1)" \
        "{" \
        "vec3 tdnorm=normalize(tnorm);" \
        "vec3 light_direction1=normalize(light_direction);" \
        "float tn_dot_ld=max(dot(light_direction1,tdnorm),0.0);" \
        "vec3 reflection_vector=reflect(-light_direction1,tdnorm);" \
        "vec3 viewer_vector1=normalize(viewer_vector);" \
        "vec3 ambient = u_la * u_ka;" \
        "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
        "vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,viewer_vector1),0.0),m_shininess);" \
        "vec3 phong_ads_light=ambient+diffuse+specular;" \
        "FragColor=vec4(phong_ads_light,1.0);" \
        "}" \
        "else"
        "{"
        "FragColor=vec4(1.0,1.0,1.0,1.0);" \
        "}"
        "}";
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        glCompileShader(gFragmentShaderObject);
        
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Fragment Shader 1%s", szInfolog);
                    
                    free(szInfolog);
            
                    [self release];
                    
                }
                
            }
            
        }
        
        gShaderProgramObject = glCreateProgram();
        
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vnormal");
        glLinkProgram(gShaderProgramObject);
        
        GLint iProgramLinkStatus=0;
        
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        
        if (iProgramLinkStatus == GL_FALSE)
            
        {
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
                    
                    printf( "Inside Linking of program 1%s", szInfolog);
                    
                    free(szInfolog);
                    
                    [self release];
                    
                    
                }
                
            }
        }
        
        
        
        //===============================================================================================================
        
        
        gVertexShaderObject1=glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vertexShaderSourceCode1 =
        
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vnormal;" \
        "uniform mat4 u_m_matrix;" \
        "uniform mat4 u_v_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform int u_lkeyispressed;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ls;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_ks;" \
        "uniform vec3 u_kd;" \
        "uniform float m_shininess;"
        "uniform vec4 u_light_position;" \
        "out vec3 phong_ads_light;" \
        "void main(void)" \
        "{" \
        "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
        "if(u_lkeyispressed == 1)" \
        "{" \
        "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
        "vec3 tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \
        "vec3 light_direction=normalize(vec3(u_light_position)-eye_coordinates.xyz);" \
        "float tn_dot_ld=max(dot(light_direction,tnorm),0.0);" \
        "vec3 reflection_vector=reflect(-light_direction,tnorm);" \
        "vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \
        "vec3 ambient = u_la * u_ka;" \
        "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
        "vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,viewer_vector),0.0),m_shininess);" \
        "phong_ads_light=ambient+diffuse+specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_light=vec3(1.0,1.0,1.0);" \
        "}" \
        
        "}";
        
        
        glShaderSource(gVertexShaderObject1, 1, (const GLchar **)&vertexShaderSourceCode1, NULL);
        
        glCompileShader(gVertexShaderObject1);
        
       iInfoLogLength=0;
        
        iShaderCompileStatus=0;
        
        szInfolog=NULL;
        
        glGetShaderiv(gVertexShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gVertexShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Vertex Shader 2%s", szInfolog);
                    
                    free(szInfolog);
                    
                    [self release];
                    
                }
                
            }
            
        }
        
        iInfoLogLength=0;
        
        iShaderCompileStatus=0;
        
        szInfolog=NULL;
        
        gFragmentShaderObject1 = glCreateShader(GL_FRAGMENT_SHADER);
        
        const GLchar* fragmentShaderSourceCode1 =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "\n" \
        "in vec3 phong_ads_light;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor=vec4(phong_ads_light,1.0);" \
        "}";
        
        glShaderSource(gFragmentShaderObject1, 1, (const GLchar **)&fragmentShaderSourceCode1, NULL);
        
        glCompileShader(gFragmentShaderObject1);
        
        glGetShaderiv(gFragmentShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gFragmentShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    glGetShaderInfoLog(gFragmentShaderObject1, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Fragment Shader 2%s", szInfolog);
                    
                    free(szInfolog);
                    
                    [self release];
                    
                }
                
            }
            
        }
        
        gShaderProgramObject1 = glCreateProgram();
        
        glAttachShader(gShaderProgramObject1, gVertexShaderObject1);
        
        glAttachShader(gShaderProgramObject1, gFragmentShaderObject1);
        
        glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_NORMAL, "vnormal");
        glLinkProgram(gShaderProgramObject1);
        
         iProgramLinkStatus=0;
        
        glGetProgramiv(gShaderProgramObject1, GL_LINK_STATUS, &iProgramLinkStatus);
        
        if (iProgramLinkStatus == GL_FALSE)
            
        {
            glGetProgramiv(gShaderProgramObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    glGetProgramInfoLog(gShaderProgramObject1, iInfoLogLength, &written, szInfolog);
                    
                    printf( "Inside Linking of program 2%s", szInfolog);
                    
                    free(szInfolog);
                    
                    [self release];
                    
                    
                }
                
            }
        }
        
        
        
        
        mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
        vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
        pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
        laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
        lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
        ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
        kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
        kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
        ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "m_shininess");
        lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
        isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");
        isLKeypressedUniform1=glGetUniformLocation(gShaderProgramObject,"u_lkeyispressed1");
        
        
        mUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_m_matrix");
        vUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_v_matrix");
        pUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_p_matrix");
        laUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_la");
        lsUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_ls");
        ldUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_ld");
        kdUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_kd");
        kaUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_ka");
        ksUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_ks");
        material_shininess_uniform1 = glGetUniformLocation(gShaderProgramObject1, "m_shininess");
        lightPositionUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_light_position");
        isLKeypressedUniform2 = glGetUniformLocation(gShaderProgramObject1, "u_lkeyispressed");
        isLKeypressedUniform3=glGetUniformLocation(gShaderProgramObject1,"u_lkeyispressed1");
        
        glGenVertexArrays(1, &vao_sphere);
        
        glBindVertexArray(vao_sphere);
        
        glGenBuffers(1, &vbo_sphere_position);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        
        
        
        glGenBuffers(1, &vbo_sphere_normal);
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        
        glGenBuffers(1, &vbo_sphere_element);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
        
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elemets), sphere_elemets, GL_STATIC_DRAW);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        
        
        
        glBindVertexArray(0);
        
        
        
        
        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        
        
        
        light_ambient[0] = 0.0f;
        light_ambient[1] = 0.0f;
        light_ambient[2] = 0.0f;
        light_ambient[3] = 0.0f;
        
        light_diffuse[0] = 1.0f;
        light_diffuse[1] = 1.0f;
        light_diffuse[2] = 1.0f;
        light_diffuse[3] = 1.0f;
        
        light_specular[0] = 1.0f;
        light_specular[1] = 1.0f;
        light_specular[2] = 1.0f;
        light_specular[3] = 1.0f;
        
        light_position[0] = 100.0f;
        light_position[1] = 100.0f;
        light_position[2] = 100.0f;
        light_position[3] = 1.0f;
        
        
        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 0.0f;
        
        material_diffuse[0] = 1.0f;
        material_diffuse[1] = 1.0f;
        material_diffuse[2] = 1.0f;
        material_diffuse[3] = 1.0f;
        
        material_specular[0] = 1.0f;
        material_specular[1] = 1.0f;
        material_specular[2] = 1.0f;
        material_specular[3] = 1.0f;
        
        
        material_shininess[0] = 50.0f;
        
        
        
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        
        
        
        
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        
        
        
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        
        
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        
        
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
        
    }
    
    return(self);
    
}


+(Class)layerClass

{
    
    //code
    
    return([CAEAGLLayer class]);
    
}



-(void)drawView:(id)sender

{
    //    float rectangleTex[8];
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
   if(vkey==0)
   glUseProgram(gShaderProgramObject);
   else
   glUseProgram(gShaderProgramObject1);
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath:: mat4 projectionMatrix;
    vmath::mat4 rotationMatrix;
    
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    //rotationMatrix = rotate(angle_cube, 0.0f, 1.0f, 0.0f);
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    if (gbLight == 1)
    {
        glUniform1i(isLKeypressedUniform, 1);
        glUniform1i(isLKeypressedUniform1,1);
        glUniform3fv(laUniform, 1, light_ambient);
        glUniform3fv(ldUniform, 1, light_diffuse);
        glUniform3fv(lsUniform, 1, light_specular);
        glUniform4fv(lightPositionUniform, 1, light_position);
        
        glUniform3fv(kaUniform, 1, material_ambient);
        glUniform3fv(kdUniform, 1, material_diffuse);
        glUniform3fv(ksUniform, 1, material_specular);
        glUniform1fv(material_shininess_uniform, 1, material_shininess);
        
        
    }
    else
    {
        glUniform1i(isLKeypressedUniform, 0);
        glUniform1i(isLKeypressedUniform1, 0);
    }
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    //=============================================================================
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    //rotationMatrix = rotate(angle_cube, 0.0f, 1.0f, 0.0f);
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform1, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform1, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform1, 1, GL_FALSE, projectionMatrix);
    if (gbLight == 1)
    {
        glUniform1i(isLKeypressedUniform2, 1);
        glUniform1i(isLKeypressedUniform3,1);
        glUniform3fv(laUniform1, 1, light_ambient);
        glUniform3fv(ldUniform1, 1, light_diffuse);
        glUniform3fv(lsUniform1, 1, light_specular);
        glUniform4fv(lightPositionUniform1, 1, light_position);
        
        glUniform3fv(kaUniform1, 1, material_ambient);
        glUniform3fv(kdUniform1, 1, material_diffuse);
        glUniform3fv(ksUniform1, 1, material_specular);
        glUniform1fv(material_shininess_uniform1, 1, material_shininess);
        
        
    }
    else
    {
        glUniform1i(isLKeypressedUniform2, 0);
        glUniform1i(isLKeypressedUniform3, 0);
    }
    
    
    //========================================================
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    glUseProgram(0);
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}



-(void)layoutSubviews

{
    
    GLint width;
    
    GLint height;
    
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    
    
    glViewport(0, 0, width, height);
    
    
    
    GLfloat fwidth=(GLfloat)width;
    
    GLfloat fheight=(GLfloat)height;
    
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);
    
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        
    {
        
        printf("Failed To Create Complete Framebuffer Object in layout subviews%x",
               
               glCheckFramebufferStatus(GL_FRAMEBUFFER));
        
    }
    
    
    
    [self drawView:nil];
    
}





-(void)startAnimation

{
    
    if(!isAnimating)
        
    {
        
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        
        
        isAnimating=YES;
        
        
        
    }
    
}



-(void)stopAnimation

{
    
    if(isAnimating)
        
    {
        
        [displayLink invalidate];
        
        displayLink=nil;
        
        
        
        isAnimating=NO;
        
    }
    
}

-(BOOL)acceptsFirstResponder

{
    
    return(YES);
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    
    
}



-(void)onSingleTap:(UITapGestureRecognizer *)gr

{
    
    // centralText=@"'onSingleTap' Event Occured";
    gbLight++;
    if(gbLight>1)
        gbLight=0;
    if(vkey==0)
    {
        gbLight=1;
        vkey=1;
    }
    else
    {
        gbLight=1;
        vkey=0;
    }
    [self setNeedsDisplay];
    
}



-(void)onDoubleTap:(UITapGestureRecognizer *)gr

{
    
    //  centralText=@"'onDoubleTap' Event Occured";
    
    [self setNeedsDisplay];
    
}



-(void)onSwipe:(UISwipeGestureRecognizer *)gr

{
    
    [self release];
    
    exit(0);
    
}



-(void)onLongPress:(UILongPressGestureRecognizer *)gr

{
    
    
    
}



-(void)dealloc

{
    
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    
    
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    if(depthRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        
        depthRenderbuffer=0;
        
    }
    
    if(colorRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        
        colorRenderbuffer=0;
        
    }
    
    if(defaultFramebuffer)
        
    {
        
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        
        defaultFramebuffer=0;
        
    }
    
    if([EAGLContext currentContext]==eaglContext)
        
    {
        
        [EAGLContext setCurrentContext:nil];
        
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
    
}



@end




