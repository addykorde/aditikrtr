//
//  GLESView.m
//  Two Lights On A Pyramid
//
//  Created by SHARVARI KORDE on 28/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//


#import<OpenGLES/ES3/gl.h>

#import<OpenGLES/Es3/glext.h>



#import "vmath.h"

#import "GLESView.h"
#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64


enum {
    
    AMC_ATTRIBUTE_POSITION = 0,
    
    AMC_ATTRIBUTE_COLOR,
    
    AMC_ATTRIBUTE_NORMAL,
    
    AMC_ATTRIBUTE_TEXCOORD0
    
};


struct Lights {
    float Ambient[4];
    float Diffuse[4];
    float Specular[4];
    float Position[4];
};

float MaterialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
float MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
float MaterialShininess[] = { 50.0f };

Lights lights[2];

int gbLight=0;
@implementation GLESView



{
    
    
    
    EAGLContext *eaglContext;
    
    
    
    GLuint defaultFramebuffer;
    
    GLuint colorRenderbuffer;
    
    GLuint depthRenderbuffer;
    
    
    
    id displayLink;
    
    NSInteger animationFrameInterval;
    
    BOOL isAnimating;
    
    GLuint texImage;
    
    GLint gVertexShaderObject;
    
    GLint gFragmentShaderObject;
    
    GLint gShaderProgramObject;
    
    
    
    int numVertices;
    int numElements;
    
    GLuint vao_pyramid;
    GLuint vbo_position_pyramid;
    GLuint vbo_normal_pyramid;
    GLuint vbo_normal1_pyramid;
    GLuint mUniform;
    GLuint vUniform;
    GLuint pUniform;
    GLuint laUniform_red;
    GLuint kaUniform_red;
    GLuint lsUniform_red;
    GLuint ldUniform_red;
    GLuint kdUniform_red;
    GLuint ksUniform_red;
    GLuint lightPositionUniform_red;
    GLuint laUniform_blue;
    GLuint kaUniform_blue;
    GLuint lsUniform_blue;
    GLuint ldUniform_blue;
    GLuint kdUniform_blue;
    GLuint ksUniform_blue;
    GLuint lightPositionUniform_blue;
    GLuint isLKeypressedUniform;
    GLuint material_shininess_uniform;
    vmath::mat4 perspectiveProjectionMatrix;
    
    
    float light_ambient[4];
    float light_diffuse[4];
    float light_specular[4];
    float light_position[4];
    
    float material_ambient[4];
    float material_diffuse[4];
    float material_specular[4];
    float material_shininess[1];
    
    
    GLuint texture_sampler_uniform;
    GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
    
    
}



-(id)initWithFrame:(CGRect)frame;

{
    
    self=[super initWithFrame:frame];
    
    
    
    if(self)
        
    {
        
        
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        
        
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        
        
        if(eaglContext==nil)
            
        {
            
            [self release];
            
            return(nil);
            
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        
        
        glGenFramebuffers(1,&defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        
        
        GLint backingWidth;
        
        GLint backingHeight;
        
        
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
            
        {
            
            printf("Failed to create complete  framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            
            glDeleteFramebuffers(1, &defaultFramebuffer);
            
            glDeleteFramebuffers(1,&colorRenderbuffer);
            
            glDeleteFramebuffers(1, &depthRenderbuffer);
            
            
            
            return(nil);
            
        }
        
        
        
        printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        
        
        
        
        isAnimating=NO;
        
        animationFrameInterval=60;
      
        
        gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        
        
        
        const GLchar* vertexShaderSourceCode =
        
        "#version 300 es" \
        "\n"\
        "in vec4 vPosition;" \
        "in vec3 vnormal;" \
        "uniform mat4 u_m_matrix;" \
        "uniform mat4 u_v_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform int u_lkeyispressed;" \
        "uniform vec3 u_ld_blue;" \
        "uniform vec3 u_la_blue;" \
        "uniform vec3 u_ls_blue;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_ks;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ld_red;" \
        "uniform vec3 u_la_red;" \
        "uniform vec3 u_ls_red;" \
        "uniform float m_shininess;"
        "uniform vec4 u_light_position_red;" \
        "uniform vec4 u_light_position_blue;" \
        "out vec3 finalColor;" \
        "vec3 light_direction_red;"\
        "vec3 light_direction_blue;"\
        "out vec3 phong_ads_light1;" \
        "out vec3 phong_ads_light2;" \
        "vec3 reflection_vector_blue;"\
        "vec3 reflection_vector_red;"\
        "float tn_dot_ld_red;" \
        "float tn_dot_ld_blue;" \
        "vec3 ambient1;"\
        "vec3 ambient2;"\
        "vec3 diffuse1;"\
        "vec3 diffuse2;"\
        "vec3 specular1;"\
        "vec3 specular2;"\
        "vec3 tnorm;" \
        "void main(void)" \
        "{" \
        "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
        "if(u_lkeyispressed == 1)" \
        "{" \
        "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
        "tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \
        
        "light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);" \
        
        "light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);" \
        
        "tn_dot_ld_red=max(dot(light_direction_red,tnorm),0.0);" \
        
        "tn_dot_ld_blue=max(dot(light_direction_blue,tnorm),0.0);" \
        
        "reflection_vector_red=reflect(-light_direction_red,tnorm);" \
        "reflection_vector_blue=reflect(-light_direction_blue,tnorm);" \
        
        "vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \
        "ambient1  = u_la_red* u_ka;" \
        "ambient2= u_la_blue* u_ka;" \
        
        "diffuse1 = u_ld_red * u_kd * tn_dot_ld_red;" \
        "diffuse2 = u_ld_blue * u_kd * tn_dot_ld_blue;" \
        
        "specular1 = u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),m_shininess);" \
        "specular2 = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),m_shininess);" \
        
        "phong_ads_light1  =ambient1+diffuse1+specular1;" \
        "phong_ads_light2  =ambient2+diffuse2+specular2;" \
        "finalColor=phong_ads_light2+phong_ads_light1;" \
        "}" \
        "else" \
        "{" \
        "finalColor=vec3(1.0,1.0,1.0);"  \
        "}" \
        
        "}";
        
        
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        glCompileShader(gVertexShaderObject);
        
        
        
        GLint iInfoLogLength=0;
        
        GLint iShaderCompileStatus=0;
        
        char *szInfolog=NULL;
        
        
        
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Vertex Shader %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        
        
        iInfoLogLength=0;
        
        iShaderCompileStatus=0;
        
        szInfolog=NULL;
        
        
        
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        
        
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec3 phong_ads_light1;" \
        "in vec3 phong_ads_light2;" \
        "in vec3 finalColor;" \
        "out vec4 FragColor1;" \
        "uniform int u_lkeyispressed;" \
        "void main(void)" \
        "{" \
        "FragColor1=vec4(finalColor,1.0);" \
        "}";
        
        
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        glCompileShader(gFragmentShaderObject);
        
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Fragment Shader %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        gShaderProgramObject = glCreateProgram();
        
        
        
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        
        
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vnormal");
        glLinkProgram(gShaderProgramObject);
        
        
        
        GLint iProgramLinkStatus=0;
        
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        
        
        
        if (iProgramLinkStatus == GL_FALSE)
            
        {
            
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
                    
                    printf( "Inside Linking of program %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
        vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
        pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
        
        laUniform_red = glGetUniformLocation(gShaderProgramObject, "u_la_red");
        lsUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
        ldUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
        
        kdUniform_red = glGetUniformLocation(gShaderProgramObject, "u_kd");
        kaUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ka");
        ksUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ks");
        
        lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");
        
        laUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
        lsUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
        ldUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
        
        lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");
        
        isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");
        
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "m_shininess");
        
        const GLfloat pyramidVertices[] = { 0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f ,
            1.0f, -1.0f, 1.0f ,
            
            0.0f, 1.0f, 0.0f ,
            1.0f, -1.0f, 1.0f ,
            1.0f, -1.0f, -1.0f ,
            
            0.0f, 1.0f, 0.0f ,
            -1.0f, -1.0f,-1.0f ,
            -1.0f, -1.0f, 1.0f ,
            
            0.0f, 1.0f, 0.0f ,
            1.0f, -1.0f, -1.0f ,
            -1.0f, -1.0f, -1.0f ,
        };
        
        const GLfloat pyramidNormals[] = {
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            
            
            
            0.894427f, 0.447214f, 0.0f,
            0.894427f, 0.447214f, 0.0f,
            0.894427f, 0.447214f, 0.0f,
            
            
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f,
            
            
            0.0f, 0.447214f, -0.89442f,
            0.0f, 0.447214f, -0.89442f,
            0.0f, 0.447214f, -0.89442f,
            
            
        };
        
        
        
        //Pyramid
        glGenVertexArrays(1, &vao_pyramid);
        glBindVertexArray(vao_pyramid);
        glGenBuffers(1, &vbo_position_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1, &vbo_normal_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glBindVertexArray(0);
        
        lights[0].Ambient[0] = 0.0f;
        lights[0].Ambient[1] = 0.0f;
        lights[0].Ambient[2] = 0.0f;
        lights[0].Ambient[3] = 1.0f;
        
        lights[0].Diffuse[0] = 1.0f;
        lights[0].Diffuse[1] = 0.0f;
        lights[0].Diffuse[2] = 0.0f;
        lights[0].Diffuse[3] = 1.0f;
        
        
        lights[0].Specular[0] = 1.0f;
        lights[0].Specular[1] = 0.0f;
        lights[0].Specular[2] = 0.0f;
        lights[0].Specular[3] = 1.0f;
        
        
        lights[0].Position[0] = -2.0f;
        lights[0].Position[1] = 0.0f;
        lights[0].Position[2] = 0.0f;
        lights[0].Position[3] = 1.0f;
        
        lights[1].Ambient[0] = 0.0f;
        lights[1].Ambient[1] = 0.0f;
        lights[1].Ambient[2] = 0.0f;
        lights[1].Ambient[3] = 1.0f;
        
        lights[1].Diffuse[0] = 0.0f;
        lights[1].Diffuse[1] = 0.0f;
        lights[1].Diffuse[2] = 1.0f;
        lights[1].Diffuse[3] = 1.0f;
        
        lights[1].Specular[0] = 0.0f;
        lights[1].Specular[1] = 0.0f;
        lights[1].Specular[2] = 1.0f;
        lights[1].Specular[3] = 1.0f;
        
        lights[1].Position[0] = 2.0f;
        lights[1].Position[1] = 0.0f;
        lights[1].Position[2] = 0.0f;
        lights[1].Position[3] = 1.0f;
        
        
        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        
        
        
        light_ambient[0] = 0.0f;
        light_ambient[1] = 0.0f;
        light_ambient[2] = 0.0f;
        light_ambient[3] = 0.0f;
        
        light_diffuse[0] = 1.0f;
        light_diffuse[1] = 1.0f;
        light_diffuse[2] = 1.0f;
        light_diffuse[3] = 1.0f;
        
        light_specular[0] = 1.0f;
        light_specular[1] = 1.0f;
        light_specular[2] = 1.0f;
        light_specular[3] = 1.0f;
        
        light_position[0] = 100.0f;
        light_position[1] = 100.0f;
        light_position[2] = 100.0f;
        light_position[3] = 1.0f;
        
        
        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 0.0f;
        
        material_diffuse[0] = 1.0f;
        material_diffuse[1] = 1.0f;
        material_diffuse[2] = 1.0f;
        material_diffuse[3] = 1.0f;
        
        material_specular[0] = 1.0f;
        material_specular[1] = 1.0f;
        material_specular[2] = 1.0f;
        material_specular[3] = 1.0f;
        
        
        material_shininess[0] = 50.0f;
        
        
        
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        
        
        
        
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        
        
        
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        
        
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        
        
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
        
    }
    
    
    
    
    
    return(self);
    
    
    
    
    
}


+(Class)layerClass

{
    
    //code
    
    return([CAEAGLLayer class]);
    
}



-(void)drawView:(id)sender

{
    static float angle_pyramid=0.0f;
    //    float rectangleTex[8];
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    glUseProgram(gShaderProgramObject);
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;
    vmath::mat4 rotationMatrix;
    
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);
    modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    modelMatrix = modelMatrix * rotationMatrix;
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    if (gbLight == 1)
    {
        glUniform1i(isLKeypressedUniform, 1);
        
        glUniform3fv(laUniform_blue, 1, lights[1].Ambient);
        glUniform3fv(ldUniform_blue, 1, lights[1].Diffuse);
        glUniform3fv(lsUniform_blue, 1, lights[1].Specular);
        
        glUniform4fv(lightPositionUniform_blue, 1, lights[1].Position);
        
        
        glUniform3fv(laUniform_red, 1, lights[0].Ambient);
        glUniform3fv(ldUniform_red, 1, lights[0].Diffuse);
        glUniform3fv(lsUniform_red, 1, lights[0].Specular);
        glUniform4fv(lightPositionUniform_red, 1, lights[0].Position);
        
        
        glUniform3fv(kaUniform_red, 1, MaterialAmbient);
        glUniform3fv(kdUniform_red, 1, MaterialDiffuse);
        glUniform3fv(ksUniform_red, 1, MaterialSpecular);
        glUniform1fv(material_shininess_uniform, 1, MaterialShininess);
        
        
    }
    else
    {
        glUniform1i(isLKeypressedUniform, 0);
    }
    
    glBindVertexArray(vao_pyramid);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);
    glUseProgram(0);
    
    if (angle_pyramid <= 360.0f)
    {
        angle_pyramid = angle_pyramid + 1.0f;
    }
    else
    {
        angle_pyramid = 0.0f;
    }
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}



-(void)layoutSubviews

{
    
    GLint width;
    
    GLint height;
    
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    
    
    glViewport(0, 0, width, height);
    
    
    
    GLfloat fwidth=(GLfloat)width;
    
    GLfloat fheight=(GLfloat)height;
    
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);
    
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        
    {
        
        printf("Failed To Create Complete Framebuffer Object in layout subviews%x",
               
               glCheckFramebufferStatus(GL_FRAMEBUFFER));
        
    }
    
    
    
    [self drawView:nil];
    
}





-(void)startAnimation

{
    
    if(!isAnimating)
        
    {
        
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        
        
        isAnimating=YES;
        
        
        
    }
    
}



-(void)stopAnimation

{
    
    if(isAnimating)
        
    {
        
        [displayLink invalidate];
        
        displayLink=nil;
        
        
        
        isAnimating=NO;
        
    }
    
}

-(BOOL)acceptsFirstResponder

{
    
    return(YES);
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    
    
}



-(void)onSingleTap:(UITapGestureRecognizer *)gr

{
    
    // centralText=@"'onSingleTap' Event Occured";
    gbLight++;
    if(gbLight>1)
        gbLight=0;
    
    [self setNeedsDisplay];
    
}



-(void)onDoubleTap:(UITapGestureRecognizer *)gr

{
    
    //  centralText=@"'onDoubleTap' Event Occured";
    
    [self setNeedsDisplay];
    
}



-(void)onSwipe:(UISwipeGestureRecognizer *)gr

{
    
    [self release];
    
    exit(0);
    
}



-(void)onLongPress:(UILongPressGestureRecognizer *)gr

{
    
    
    
}



-(void)dealloc

{
    
    if (vbo_position_pyramid)
    {
        glDeleteBuffers(1, &vbo_position_pyramid);
        vbo_position_pyramid = 0;
    }
    if (vao_pyramid)
    {
        glDeleteVertexArrays(1, &vao_pyramid);
        vao_pyramid = 0;
    }
    if(depthRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        
        depthRenderbuffer=0;
        
    }
    
    if(colorRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        
        colorRenderbuffer=0;
        
    }
    
    if(defaultFramebuffer)
        
    {
        
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        
        defaultFramebuffer=0;
        
    }
    
    if([EAGLContext currentContext]==eaglContext)
        
    {
        
        [EAGLContext setCurrentContext:nil];
        
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
    
}



@end


