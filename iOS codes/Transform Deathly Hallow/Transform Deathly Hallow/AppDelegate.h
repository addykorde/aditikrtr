//
//  AppDelegate.h
//  Transform Deathly Hallow
//
//  Created by SHARVARI KORDE on 29/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
