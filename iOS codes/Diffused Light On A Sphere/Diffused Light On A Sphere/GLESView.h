//
//  GLESView.h
//  Diffused Light On A Sphere
//
//  Created by SHARVARI KORDE on 28/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

@end
