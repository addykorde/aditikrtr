//
//  AppDelegate.h
//  Viewport
//
//  Created by SHARVARI KORDE on 28/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
