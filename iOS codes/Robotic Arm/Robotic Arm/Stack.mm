//
//  Stack.cpp
//  
//
//  Created by SHARVARI KORDE on 28/01/20.
//
#import<OpenGLES/ES3/gl.h>
#import<stdio.h>
#include<stdlib.h>
#import<OpenGLES/Es3/glext.h>

#include "Stack.h"


Stack::Stack()
{
    top = NULL;
}


void Stack::push(mat4 val)
{
    struct Node *temp;
    temp = (struct Node*)malloc(sizeof(struct Node));
    temp->data = val;
    temp->next = top;
    top = temp;
}


mat4 Stack::pop()
{
    struct Node *temp;
    mat4 val;
    val.identity();
    if (top == NULL)
    {
        
    }
    else
    {
        temp = top;
        top = top->next;
        val = temp->data;
        free(temp);
    }
    return val;
}

