//
//  GLESView.m
//  3D-Rotation
//
//  Created by SHARVARI KORDE on 27/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//

#import<OpenGLES/ES3/gl.h>

#import<OpenGLES/Es3/glext.h>



#import "vmath.h"

#import "GLESView.h"
#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64


enum {
    
    AMC_ATTRIBUTE_POSITION = 0,
    
    AMC_ATTRIBUTE_COLOR,
    
    AMC_ATTRIBUTE_NORMAL,
    
    AMC_ATTRIBUTE_TEXCOORD0
    
};


static int key1=0;




@implementation GLESView



{
    
    
    
    EAGLContext *eaglContext;
    
    
    
    GLuint defaultFramebuffer;
    
    GLuint colorRenderbuffer;
    
    GLuint depthRenderbuffer;
    
    
    
    id displayLink;
    
    NSInteger animationFrameInterval;
    
    BOOL isAnimating;
    
    GLuint texImage;
    
    GLint gVertexShaderObject;
    
    GLint gFragmentShaderObject;
    
    GLint gShaderProgramObject;
    GLuint vao_i1;
    GLuint vao_i2;
    GLuint vao_n;
    GLuint vao_d;
    GLuint vao_a;
    GLuint vao_line1;
    GLuint vao_line2;
    GLuint vao_line3;
    GLuint vbo_position_i1;
    GLuint vbo_position_i2;
    GLuint vbo_position_n;
    GLuint vbo_position_d;
    GLuint vbo_position_a;
    GLuint vbo_position_line1;
    GLuint vbo_position_line2;
    GLuint vbo_position_line3;
    GLuint vbo_color_i1;
    GLuint vbo_color_i2;
    GLuint vbo_color_n;
    GLuint vbo_color_d;
    GLuint vbo_color_a;
    GLuint vbo_color_line1;
    GLuint vbo_color_line2;
    GLuint vbo_color_line3;
    GLuint mvpUniform;
    
    vmath::mat4 perspectiveProjectionMatrix;
    
    GLuint texture_sampler_uniform;
    GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
    
    
}



-(id)initWithFrame:(CGRect)frame;

{
    
    self=[super initWithFrame:frame];
    
    
    
    if(self)
        
    {
        
        
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        
        
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        
        
        if(eaglContext==nil)
            
        {
            
            [self release];
            
            return(nil);
            
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        
        
        glGenFramebuffers(1,&defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        
        
        GLint backingWidth;
        
        GLint backingHeight;
        
        
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
            
        {
            
            printf("Failed to create complete  framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            
            glDeleteFramebuffers(1, &defaultFramebuffer);
            
            glDeleteFramebuffers(1,&colorRenderbuffer);
            
            glDeleteFramebuffers(1, &depthRenderbuffer);
            
            
            
            return(nil);
            
        }
        
        
        
        printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        
        
        
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        
        
        gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        
        
        
        const GLchar* vertexShaderSourceCode =
        
        "#version 300 es" \
        
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position=u_mvp_matrix*vPosition;" \
        "out_color=vColor;"
        "}";
        
        
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        glCompileShader(gVertexShaderObject);
        
        
        
        GLint iInfoLogLength=0;
        
        GLint iShaderCompileStatus=0;
        
        char *szInfolog=NULL;
        
        
        
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Vertex Shader %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        
        
        iInfoLogLength=0;
        
        iShaderCompileStatus=0;
        
        szInfolog=NULL;
        
        
        
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        
        
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor=out_color;" \
        "}";
        
        
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        glCompileShader(gFragmentShaderObject);
        
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Fragment Shader %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        gShaderProgramObject = glCreateProgram();
        
        
        
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        
        
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
        glLinkProgram(gShaderProgramObject);
        
        
        
        GLint iProgramLinkStatus=0;
        
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        
        
        
        if (iProgramLinkStatus == GL_FALSE)
            
        {
            
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
                    
                    printf( "Inside Linking of program %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");
        const GLfloat i1[] =
        { -0.70f, 0.80f, 0.0f,
            
            - 0.72f, 0.80f, 0.0f,
            
            - 0.72f,-0.80f, 0.0f,
            
            - 0.70f, -0.80f, 0.0f,
        };
        const GLfloat i2[] = {
            0.20f, 0.80f, 0.0f,
            
            0.22f, 0.80f, 0.0f,
            
            0.22f, -0.80f, 0.0f,
            
            0.20f, -0.80f, 0.0f
            
        };
        const GLfloat n[] = {
            -0.52f, 0.80f, 0.0f,
            
            - 0.50f, 0.80f, 0.0f,
            
            - 0.50f, -0.80f, 0.0f,
            
            - 0.52f, -0.80f, 0.0f,
            
            
            - 0.50f, 0.80f, 0.0f,
            
            - 0.48f, 0.80f, 0.0f,
            
            - 0.36f, -0.80f, 0.0f,
            
            - 0.38f, -0.80f, 0.0f,
            
            
            - 0.38f, 0.80f, 0.0f,
            
            - 0.36f, 0.80f, 0.0f,
            
            - 0.36f, -0.80f, 0.0f,
            
            - 0.38f, -0.80f, 0.0f,
            
        };
        const GLfloat d[] = { -0.16f, 0.78f, 0.0f,
            
            - 0.14f, 0.78f, 0.0f,
            
            - 0.14f, -0.78f, 0.0f,
            
            - 0.16f, -0.78f, 0.0f,
            
            
            
            -0.16f, 0.78f, 0.0f,
            
            - 0.16f, 0.80f, 0.0f,
            
            0.0f, 0.80f, 0.0f,
            
            0.0f, 0.78f, 0.0f,
            
            
            - 0.16f, -0.80f, 0.0f,
            
            - 0.16f, -0.78f, 0.0f,
            
            0.0f, -0.78f, 0.0f,
            
            0.0f, -0.80f, 0.0f,
            
            
            
            -0.02f, 0.78f, 0.0f,
            
            0.0f, 0.78f, 0.0f,
            
            0.0f, -0.78f, 0.0f,
            
            - 0.02f, -0.78f, 0.0f
        };
        const GLfloat a[] = {
            0.40f, -0.80f, 0.0f,
            
            0.42f, -0.80f, 0.0f,
            
            0.52f, 0.80f, 0.0f,
            
            0.50f, 0.80f, 0.0f,
            
            
            0.72f, -0.80f, 0.0f,
            
            0.74f, -0.80f, 0.0f,
            
            0.54f, 0.80f, 0.0f,
            
            0.52f, 0.80f, 0.0f
        };
        const GLfloat i1Color[] = {
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f };
        const GLfloat i2Color[] = {
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f };
        const GLfloat nColor[] = {
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f ,
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f
        };
        const GLfloat dColor[] = {
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            
            
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            
            
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f
        };
        const GLfloat aColor[] = { 0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            0.0705882352911f, 0.5333333f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f,
            
            1.0f, 0.647058823529f, 0.0f };
        
        const GLfloat line1Vertices[] = {
            0.50f,0.0f,0.0f,
            0.62,0.0f,0.0f
        };
        
        const GLfloat line2Vertices[] = {
            0.50f, -0.04f, 0.0f,
            0.63f, -0.04f, 0.0f
        };
        
        const GLfloat line3Vertices[] = {
            0.50f,-0.08f,0.0f,
            0.64f, -0.08f, 0.0f
        };
        
        const GLfloat line1Color[]={
            1.0f, 0.647058823529f, 0.0f,
            1.0f, 0.647058823529f, 0.0f
        };
        const GLfloat line2Color[] = {
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
        };
        const GLfloat line3Color[] = {
            0.0705882352911f, 0.5333333f, 0.0f,
            0.0705882352911f, 0.5333333f, 0.0f,
        };
        //i1
        glGenVertexArrays(1, &vao_i1);
        glBindVertexArray(vao_i1);
        glGenBuffers(1, &vbo_position_i1);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i1);
        glBufferData(GL_ARRAY_BUFFER, sizeof(i1), i1, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_i1);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i1);
        glBufferData(GL_ARRAY_BUFFER, sizeof(i1Color), i1Color, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        //n
        glGenVertexArrays(1, &vao_n);
        glBindVertexArray(vao_n);
        glGenBuffers(1, &vbo_position_n);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
        glBufferData(GL_ARRAY_BUFFER, sizeof(n), n, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_n);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_n);
        glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        //d
        glGenVertexArrays(1, &vao_d);
        glBindVertexArray(vao_d);
        glGenBuffers(1, &vbo_position_d);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_d);
        glBufferData(GL_ARRAY_BUFFER, sizeof(d), d, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_d);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_d);
        glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        //i2
        glGenVertexArrays(1, &vao_i2);
        glBindVertexArray(vao_i2);
        glGenBuffers(1, &vbo_position_i2);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i2);
        glBufferData(GL_ARRAY_BUFFER, sizeof(i2), i2, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_i2);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i2);
        glBufferData(GL_ARRAY_BUFFER, sizeof(i2Color), i2Color, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        //a
        glGenVertexArrays(1, &vao_a);
        glBindVertexArray(vao_a);
        glGenBuffers(1, &vbo_position_a);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_a);
        glBufferData(GL_ARRAY_BUFFER, sizeof(a), a, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_a);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_a);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        
        //line1
        glGenVertexArrays(1, &vao_line1);
        glBindVertexArray(vao_line1);
        glGenBuffers(1, &vbo_position_line1);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line1);
        glBufferData(GL_ARRAY_BUFFER, sizeof(line1Vertices), line1Vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_line1);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line1);
        glBufferData(GL_ARRAY_BUFFER, sizeof(line1Color), line1Color, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        //line2
        glGenVertexArrays(1, &vao_line2);
        glBindVertexArray(vao_line2);
        glGenBuffers(1, &vbo_position_line2);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line2);
        glBufferData(GL_ARRAY_BUFFER, sizeof(line2Vertices), line2Vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_line2);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line2);
        glBufferData(GL_ARRAY_BUFFER, sizeof(line2Color), line2Color, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        //line3
        
        glGenVertexArrays(1, &vao_line3);
        glBindVertexArray(vao_line3);
        glGenBuffers(1, &vbo_position_line3);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line3);
        glBufferData(GL_ARRAY_BUFFER, sizeof(line3Vertices), line3Vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_color_line3);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line3);
        glBufferData(GL_ARRAY_BUFFER, sizeof(line3Color), line3Color, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        
        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        
        
        
        
        
        
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        
        
        
        
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        
        
        
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        
        
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        
        
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
        
    }
    
    
    
    
    
    return(self);
    
    
    
    
    
}


+(Class)layerClass

{
    
    //code
    
    return([CAEAGLLayer class]);
    
}



-(void)drawView:(id)sender

{
    //    float rectangleTex[8];
    [EAGLContext setCurrentContext:eaglContext];
    
    
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    glUseProgram(gShaderProgramObject);
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(-0.6f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_i1);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);
    
    //N
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(-0.3f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_n);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glBindVertexArray(0);
    
    //D
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_d);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glBindVertexArray(0);
    
    //I
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.3f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_i2);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);
    
    //A
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.6f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_a);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glBindVertexArray(0);
    
    //Line1
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.2f, 0.0f, -2.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_line1);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    //Line2
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.2f, 0.0f, -2.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_line2);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    //Line3
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.2f, 0.0f, -2.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_line3);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    
    
    glUseProgram(0);
    
   
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}



-(void)layoutSubviews

{
    
    GLint width;
    
    GLint height;
    
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    
    
    glViewport(0, 0, width, height);
    
    
    
    GLfloat fwidth=(GLfloat)width;
    
    GLfloat fheight=(GLfloat)height;
    
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);
    
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        
    {
        
        printf("Failed To Create Complete Framebuffer Object in layout subviews%x",
               
               glCheckFramebufferStatus(GL_FRAMEBUFFER));
        
    }
    
    
    
    [self drawView:nil];
    
}





-(void)startAnimation

{
    
    if(!isAnimating)
        
    {
        
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        
        
        isAnimating=YES;
        
        
        
    }
    
}



-(void)stopAnimation

{
    
    if(isAnimating)
        
    {
        
        [displayLink invalidate];
        
        displayLink=nil;
        
        
        
        isAnimating=NO;
        
    }
    
}

-(BOOL)acceptsFirstResponder

{
    
    return(YES);
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    
    
}



-(void)onSingleTap:(UITapGestureRecognizer *)gr

{
    
    // centralText=@"'onSingleTap' Event Occured";
    key1++;
    if(key1>=5)
        key1=0;
    [self setNeedsDisplay];
    
}



-(void)onDoubleTap:(UITapGestureRecognizer *)gr

{
    
    //  centralText=@"'onDoubleTap' Event Occured";
    
    [self setNeedsDisplay];
    
}



-(void)onSwipe:(UISwipeGestureRecognizer *)gr

{
    
    [self release];
    
    exit(0);
    
}



-(void)onLongPress:(UILongPressGestureRecognizer *)gr

{
    
    
    
}



-(void)dealloc

{
    
    if (vbo_position_i1)
    {
        glDeleteBuffers(1, &vbo_position_i1);
        vbo_position_i1 = 0;
    }
    if (vao_i1)
    {
        glDeleteVertexArrays(1, &vao_i1);
        vao_i1 = 0;
    }
    if (vbo_position_i2)
    {
        glDeleteBuffers(1, &vbo_position_i2);
        vbo_position_i2= 0;
    }
    if (vao_i2)
    {
        glDeleteVertexArrays(1, &vao_i2);
        vao_i2 = 0;
    }

    
    
    
    
    
    if(depthRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        
        depthRenderbuffer=0;
        
    }
    
    if(colorRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        
        colorRenderbuffer=0;
        
    }
    
    if(defaultFramebuffer)
        
    {
        
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        
        defaultFramebuffer=0;
        
    }
    
    if([EAGLContext currentContext]==eaglContext)
        
    {
        
        [EAGLContext setCurrentContext:nil];
        
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
    
}



@end


