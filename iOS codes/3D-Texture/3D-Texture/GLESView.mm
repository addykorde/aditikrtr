//
//  GLESView.m
//  Tweaked Smiley
//
//  Created by SHARVARI KORDE on 25/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//

#import<OpenGLES/ES3/gl.h>

#import<OpenGLES/Es3/glext.h>



#import "vmath.h"

#import "GLESView.h"



enum {
    
    AMC_ATTRIBUTE_POSITION = 0,
    
    AMC_ATTRIBUTE_COLOR,
    
    AMC_ATTRIBUTE_NORMAL,
    
    AMC_ATTRIBUTE_TEXCOORD0
    
};


static int key1=0;




@implementation GLESView



{
    
    
    
    EAGLContext *eaglContext;
    
    
    
    GLuint defaultFramebuffer;
    
    GLuint colorRenderbuffer;
    
    GLuint depthRenderbuffer;
    
    
    
    id displayLink;
    
    NSInteger animationFrameInterval;
    
    BOOL isAnimating;
    
    
    
    GLint gVertexShaderObject;
    
    GLint gFragmentShaderObject;
    
    GLint gShaderProgramObject;
    
    
    
    
    GLuint vao_rectangle;
    GLuint vbo_rectangle_position;
    GLuint vbo_rectangle_texture;
    GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_position_pyramid;
    GLuint vbo_position_cube;
    GLuint vbo_texture_pyramid;
    GLuint vbo_texture_cube;
    GLuint static_texture;
    GLuint mvpUniform;
    GLuint pyramid_texture;
    GLuint cube_texture;
    vmath::mat4 perspectiveProjectionMatrix;
    
    GLuint texture_sampler_uniform;
    
    
    
}



-(id)initWithFrame:(CGRect)frame;

{
    
    self=[super initWithFrame:frame];
    
    
    
    if(self)
        
    {
        
        
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        
        
        
        eaglLayer.opaque=YES;
        
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                      
                                      kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        
        
        
        if(eaglContext==nil)
            
        {
            
            [self release];
            
            return(nil);
            
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        
        
        glGenFramebuffers(1,&defaultFramebuffer);
        
        glGenRenderbuffers(1,&colorRenderbuffer);
        
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        
        
        GLint backingWidth;
        
        GLint backingHeight;
        
        
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        
        glBindRenderbuffer(GL_RENDERBUFFER,depthRenderbuffer);
        
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
            
        {
            
            printf("Failed to create complete  framebuffer object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            
            glDeleteFramebuffers(1, &defaultFramebuffer);
            
            glDeleteFramebuffers(1,&colorRenderbuffer);
            
            glDeleteFramebuffers(1, &depthRenderbuffer);
            
            
            
            return(nil);
            
        }
        
        
        
        printf("Renderer : %s | GL Version : %s | GLSL Version :%s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        
        
        
        
        isAnimating=NO;
        
        animationFrameInterval=60;
        
        
        
        gVertexShaderObject=glCreateShader(GL_VERTEX_SHADER);
        
        
        
        const GLchar* vertexShaderSourceCode =
        
        "#version 300 es" \
        
        "\n" \
        
        "in vec4 vPosition;" \
        "in vec2 vTex_Coord;" \
        "out vec2 out_texcoord;"\
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position=u_mvp_matrix*vPosition;" \
        "out_texcoord=vTex_Coord;"\
        
        "}";
        
        
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        glCompileShader(gVertexShaderObject);
        
        
        
        GLint iInfoLogLength=0;
        
        GLint iShaderCompileStatus=0;
        
        char *szInfolog=NULL;
        
        
        
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Vertex Shader %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        
        
        iInfoLogLength=0;
        
        iShaderCompileStatus=0;
        
        szInfolog=NULL;
        
        
        
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        
        
        const GLchar* fragmentShaderSourceCode =
        
        "#version 300 es" \
        
        "\n" \
        
        "precision highp float;"\
        "in vec2 out_texcoord;"\
        "uniform sampler2D u_texture_sampler;"\
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "vec3 tex=vec3(texture(u_texture_sampler,out_texcoord));"\
        "FragColor=vec4(tex,1.0);" \
        
        "}" ;
        
        
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        
        
        
        
        glCompileShader(gFragmentShaderObject);
        
        
        
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
        
        
        
        if (iShaderCompileStatus == GL_FALSE)
            
        {
            
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
                    
                    printf("Inside Fragment Shader %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        gShaderProgramObject = glCreateProgram();
        
        
        
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        
        
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTex_Coord");
        
        glLinkProgram(gShaderProgramObject);
        
        
        
        GLint iProgramLinkStatus=0;
        
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
        
        
        
        if (iProgramLinkStatus == GL_FALSE)
            
        {
            
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            
            
            
            if (iInfoLogLength > 0)
                
            {
                
                szInfolog = (GLchar *)malloc(iInfoLogLength);
                
                if (szInfolog != NULL)
                    
                {
                    
                    GLsizei written;
                    
                    
                    
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
                    
                    printf( "Inside Linking of program %s", szInfolog);
                    
                    free(szInfolog);
                    
                    
                    
                    [self release];
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");
        
        
        texture_sampler_uniform=glGetUniformLocation(gShaderProgramObject,"u_texture_sampler");
        
        
        pyramid_texture=[self loadTextureFromBMPFile:@"Stone" :@"bmp"];
        
        cube_texture=[self loadTextureFromBMPFile:@"Vijay_Kundali" :@"bmp"];
        const GLfloat pyramidVertices[] = { 0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f ,
            1.0f, -1.0f, 1.0f ,
            0.0f, 1.0f, 0.0f ,
            1.0f, -1.0f, 1.0f ,
            1.0f, -1.0f, -1.0f ,
            0.0f, 1.0f, 0.0f ,
            -1.0f, -1.0f,-1.0f ,
            -1.0f, -1.0f, 1.0f ,
            0.0f, 1.0f, 0.0f ,
            1.0f, -1.0f, -1.0f ,
            -1.0f, -1.0f, -1.0f ,
        };
        const GLfloat pyramidTex[] = { 0.5f, 1.0f,
            
            0.0f, 0.0f,
            
            1.0f, 0.0f,
            0.5, 1.0f,
            
            1.0f, 0.0f,
            
            0.0f, 0.0f,
            0.5f, 1.0f,
            
            0.0f, 0.0f,
            
            1.0f, 0.0f,
            0.5f, 1.0f,
            
            1.0f, 0.0f,
        };
        
        
        const GLfloat cubeVertices[] = {
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            
            1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            
            -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f};
        const GLfloat cubeTex[] = {     0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            0.0f,1.0f,
            
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            1.0f,1.0f,
            
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f
        };
        
        
        glGenVertexArrays(1, &vao_pyramid);
        glBindVertexArray(vao_pyramid);
        glGenBuffers(1, &vbo_position_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_texture_pyramid);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_pyramid);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTex), pyramidTex, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        
        glGenVertexArrays(1, &vao_cube);
        glBindVertexArray(vao_cube);
        glGenBuffers(1, &vbo_position_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        glGenBuffers(1, &vbo_texture_cube);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_cube);
        glBufferData(GL_ARRAY_BUFFER, sizeof(cubeTex), cubeTex, GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glBindVertexArray(0);
        
        
        glEnable(GL_DEPTH_TEST);
        
        glDepthFunc(GL_LEQUAL);
        
        
        
        
        
        
        
        glClearColor(0.0f,0.0f,0.0f,0.0f);
        
        perspectiveProjectionMatrix=vmath::mat4::identity();
        
        
        
        
        
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        
        
        
        
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        
        
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        
        
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
        
    }
    
    
    
    
    
    return(self);
    
    
    
    
    
}

-(GLuint)loadTextureFromBMPFile:(NSString *)texFileName :(NSString *)extension
{
    NSString *textureFileNameWithPath=[[NSBundle mainBundle]pathForResource:texFileName ofType:extension];
    
    UIImage *bmpImage=[[UIImage alloc]
                       initWithContentsOfFile:textureFileNameWithPath];
    if(!bmpImage)
    {
        NSLog(@"can't find %@",textureFileNameWithPath);
        return (0);
    }
    
    CGImageRef cgImage=bmpImage.CGImage;
    
    int w=(int)CGImageGetWidth(cgImage);
    int h=(int)CGImageGetHeight(cgImage);
    CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    void* pixels =(void *)CFDataGetBytePtr(imageData);
    GLuint bmpTexture;
    
    glGenTextures(1, &bmpTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    
    glBindTexture(GL_TEXTURE_2D, bmpTexture);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    
    
    glGenerateMipmap(GL_TEXTURE_2D);
    return(bmpTexture);
    
}

+(Class)layerClass

{
    
    //code
    
    return([CAEAGLLayer class]);
    
}



-(void)drawView:(id)sender

{
   static float angle_cube=0.0f,angle_pyramid=0.0f;
    [EAGLContext setCurrentContext:eaglContext];
    
    
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    
    
    glUseProgram(gShaderProgramObject);
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    vmath::mat4 rotationMatrix;
    
    
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindTexture(GL_TEXTURE_2D,pyramid_texture);
    glBindVertexArray(vao_pyramid);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);
    
    
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(angle_cube, angle_cube, angle_cube);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindTexture(GL_TEXTURE_2D,cube_texture);
    glBindVertexArray(vao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);

    
    glUseProgram(0);
    if (angle_pyramid <= 360.0f)
    {
        angle_pyramid = angle_pyramid + 1.0f;
    }
    else
    {
        angle_pyramid = 0.0f;
    }
    if (angle_cube <= 360.0f)
    {
        angle_cube = angle_cube + 1.0f;
    }
    else
    {
        angle_cube = 0.0f;
    }

    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}



-(void)layoutSubviews

{
    
    GLint width;
    
    GLint height;
    
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    
    
    glViewport(0, 0, width, height);
    
    
    
    GLfloat fwidth=(GLfloat)width;
    
    GLfloat fheight=(GLfloat)height;
    
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,fwidth/fheight,0.1f,100.0f);
    
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        
    {
        
        printf("Failed To Create Complete Framebuffer Object in layout subviews%x",
               
               glCheckFramebufferStatus(GL_FRAMEBUFFER));
        
    }
    
    
    
    [self drawView:nil];
    
}





-(void)startAnimation

{
    
    if(!isAnimating)
        
    {
        
        displayLink=[NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop]forMode:NSDefaultRunLoopMode];
        
        
        
        isAnimating=YES;
        
        
        
    }
    
}



-(void)stopAnimation

{
    
    if(isAnimating)
        
    {
        
        [displayLink invalidate];
        
        displayLink=nil;
        
        
        
        isAnimating=NO;
        
    }
    
}

-(BOOL)acceptsFirstResponder

{
    
    return(YES);
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    
    
}



-(void)onSingleTap:(UITapGestureRecognizer *)gr

{
    
    // centralText=@"'onSingleTap' Event Occured";
    key1++;
    if(key1>=5)
        key1=0;
    [self setNeedsDisplay];
    
}



-(void)onDoubleTap:(UITapGestureRecognizer *)gr

{
    
    //  centralText=@"'onDoubleTap' Event Occured";
    
    [self setNeedsDisplay];
    
}



-(void)onSwipe:(UISwipeGestureRecognizer *)gr

{
    
    [self release];
    
    exit(0);
    
}



-(void)onLongPress:(UILongPressGestureRecognizer *)gr

{
    
    
    
}



-(void)dealloc

{
    
    if (vbo_rectangle_position)
    {
        glDeleteBuffers(1, &vbo_rectangle_position);
        vbo_rectangle_position = 0;
    }
    if (vao_rectangle)
    {
        glDeleteVertexArrays(1, &vao_rectangle);
        vao_rectangle = 0;
    }
    
    
    
    
    
    
    if(depthRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        
        depthRenderbuffer=0;
        
    }
    
    if(colorRenderbuffer)
        
    {
        
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        
        colorRenderbuffer=0;
        
    }
    
    if(defaultFramebuffer)
        
    {
        
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        
        defaultFramebuffer=0;
        
    }
    
    if([EAGLContext currentContext]==eaglContext)
        
    {
        
        [EAGLContext setCurrentContext:nil];
        
    }
    
    [eaglContext release];
    
    eaglContext=nil;
    
    [super dealloc];
    
}



@end


