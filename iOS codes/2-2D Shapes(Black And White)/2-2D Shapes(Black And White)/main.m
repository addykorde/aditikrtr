//
//  main.m
//  2-2D Shapes(Black And White)
//
//  Created by SHARVARI KORDE on 18/01/20.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
