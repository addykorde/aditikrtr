//
//  AppDelegate.h
//  Twenty Four Spheres
//
//  Created by SHARVARI KORDE on 31/01/20.
//  Copyright © 2020 ADITI KORDE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

