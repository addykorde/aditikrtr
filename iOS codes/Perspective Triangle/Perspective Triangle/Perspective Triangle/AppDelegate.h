//
//  AppDelegate.h
//  Perspective Triangle
//
//  Created by SHARVARI KORDE on 18/01/20.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

