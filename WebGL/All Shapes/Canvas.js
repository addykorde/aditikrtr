var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros=
{
AMC_ATTRIBUTE_POSITION:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXCOORD0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_circle;
var vbo_circle_position;
var vbo_circle_color;

var vao_circle1;
var vbo_circle_position1;
var vbo_circle_color1;

var vao_line1;
var vbo_line1_position;
var vbo_line1_color;

var vao_line2;
var vbo_line2_position;
var vbo_line2_color;

var vao_line3;
var vbo_line3_position;
var vbo_line3_color;

var vao_line4;
var vbo_line4_position;
var vbo_line4_color;


var vao_glines;
var vbo_glines;
var vbo_glines_color;
var vao_glines1;
var vbo_glines1;
var vbo_glines1_color;
var vao_square1;
var vao_square2;
var vao_square3;
var vao_square4;
var vbo_position_square1;
var vbo_position_square2;
var vbo_position_square3;
var vbo_position_square4;
var vbo_color_square1;
var vbo_color_square2;
var vbo_color_square3;
var vbo_color_square4;

var vao_rectangle;
var vbo_rectangle_position;
var mvpUniform;
 
var perspectiveProjectionMatrix;


var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;


var angleTriangle=0.0;
var angleRectangle=0.0;

function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	
	init();
	resize();
	draw();
	
}


function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}


function init()
{
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 out_color;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_mvp_matrix*vPosition;"+
	"gl_PointSize=2.0;"+
	"out_color=vColor;"+
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"out vec4 FragColor;"+
	"in vec4 out_color;"+
	"void main(void)"+
	"{"+
	"FragColor=out_color;"+
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	
	var line1Vertices=new Float32Array([
						0.0,0.5,0.0,
						0.5,-0.5,0.0]);
	var line2Vertices=new Float32Array([
			0.5,-0.5,0.0,
			-0.5,-0.5,0.0]);
			
	var line3Vertices=new Float32Array([
			0.0,0.5,0.0,
			0.0,-0.5,0.0]);
						
	var lineVertices=new Float32Array([
						-0.5,-0.5,0.0,
			0.0,0.5,0.0]);
	
	var lineColor=new Float32Array([1.0,1.0,0.0,
	1.0,1.0,0.0]);
	

	var glineColor=new Float32Array([
		1.0,0.0,1.0,
		1.0,0.0,1.0
	]);

	var glineColor1=new Float32Array([
		1.0,0.0,1.0,
		1.0,0.0,1.0
	]);



	var glineVertices=new Float32Array([
	-1.0,0.0,0.0,
	1.0,0.0,0.0 ]);

	var gline1Vertices=new Float32Array([
		0.0,1.0,0.0,
		0.0,-1.0,0.0
	]);

	var square1Vertices=new Float32Array([
		-0.5,-0.5,0.0,
		-0.5,0.5,0.0
	]);
	var square2Vertices=new Float32Array([
		-0.5,0.5,0.0,
		0.5,0.5,0.0
	]);
	var square3Vertices=new Float32Array([
		0.5,0.5,0.0,
		0.5,-0.5,0.0
	]);
	var square4Vertices=new Float32Array([
		0.5,-0.5,0.0,
		-0.5,-0.5,0.0
	]);

	var square1Color=new Float32Array([
	
		1.0,1.0,0.0,
		1.0,1.0,0.0
	]);

	var square2Color=new Float32Array([
	
		1.0,1.0,0.0,
		1.0,1.0,0.0
	]);

	var square3Color=new Float32Array([
	
		1.0,1.0,0.0,
		1.0,1.0,0.0
	]);

	var square4Color=new Float32Array([
	
		1.0,1.0,0.0,
		1.0,1.0,0.0
]);


	var circlePos=new Float32Array(18852);
	var circleColor=new Float32Array(18852);
	var angle = 0.0; 
	var angle1 = 0.0;
	var radius=0.301;
	
		var index = 0;
		var index1 = 0;
		

		for (angle = 0.0; angle < 6.2831853; angle = angle + 0.001)
		{
		circlePos[index++] = (Math.cos(angle))*radius;
		circlePos[index++] = (Math.sin(angle))*radius-0.20;
		circlePos[index++] = 0.0;
	

		}
		
		for (angle1 = 0.0; angle1 < 6.2831853; angle1 = angle1 + 0.001)
		{
		circleColor[index1++] = 1.0;
		circleColor[index1++] = 1.0;
		circleColor[index1++] = 0.0;
	

		}
		
	
	vao_circle=gl.createVertexArray();
	gl.bindVertexArray(vao_circle);
	vbo_circle_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_position);
	gl.bufferData(gl.ARRAY_BUFFER,circlePos,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_circle_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_color);
	gl.bufferData(gl.ARRAY_BUFFER,circleColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	vao_line1=gl.createVertexArray();
	gl.bindVertexArray(vao_line1);
	vbo_line1_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line1_position);
	gl.bufferData(gl.ARRAY_BUFFER,line1Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_line1_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line1_color);
	gl.bufferData(gl.ARRAY_BUFFER,lineColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	vao_line2=gl.createVertexArray();
	gl.bindVertexArray(vao_line2);
	vbo_line2_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line2_position);
	gl.bufferData(gl.ARRAY_BUFFER,line2Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_line2_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line2_color);
	gl.bufferData(gl.ARRAY_BUFFER,lineColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	vao_line3=gl.createVertexArray();
	gl.bindVertexArray(vao_line3);
	vbo_line3_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line3_position);
	gl.bufferData(gl.ARRAY_BUFFER,line3Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_line3_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line3_color);
	gl.bufferData(gl.ARRAY_BUFFER,lineColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	vao_line4=gl.createVertexArray();
	gl.bindVertexArray(vao_line4);
	vbo_line4_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line4_position);
	gl.bufferData(gl.ARRAY_BUFFER,lineVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_line4_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_line4_color);
	gl.bufferData(gl.ARRAY_BUFFER,lineColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_glines=gl.createVertexArray();
	gl.bindVertexArray(vao_glines);
	vbo_glines=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_glines);
	gl.bufferData(gl.ARRAY_BUFFER,glineVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_glines_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_glines_color);
	gl.bufferData(gl.ARRAY_BUFFER,glineColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_glines1=gl.createVertexArray();
	gl.bindVertexArray(vao_glines1);
	vbo_glines1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_glines1);
	gl.bufferData(gl.ARRAY_BUFFER,gline1Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_glines1_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_glines1_color);
	gl.bufferData(gl.ARRAY_BUFFER,glineColor1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	vao_square1=gl.createVertexArray();
	gl.bindVertexArray(vao_square1);
	vbo_position_square1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_square1);
	gl.bufferData(gl.ARRAY_BUFFER,square1Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_square1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_square1);
	gl.bufferData(gl.ARRAY_BUFFER,square1Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	vao_square2=gl.createVertexArray();
	gl.bindVertexArray(vao_square2);
	vbo_position_square2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_square2);
	gl.bufferData(gl.ARRAY_BUFFER,square2Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_square2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_square2);
	gl.bufferData(gl.ARRAY_BUFFER,square2Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_square3=gl.createVertexArray();
	gl.bindVertexArray(vao_square3);
	vbo_position_square3=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_square3);
	gl.bufferData(gl.ARRAY_BUFFER,square3Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_square3=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_square3);
	gl.bufferData(gl.ARRAY_BUFFER,square3Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	vao_square4=gl.createVertexArray();
	gl.bindVertexArray(vao_square4);
	vbo_position_square4=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_square4);
	gl.bufferData(gl.ARRAY_BUFFER,square4Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_square4=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_square4);
	gl.bufferData(gl.ARRAY_BUFFER,square4Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	var circlePos1=new Float32Array(18852);
	var circleColor1=new Float32Array(18852);
	var angle2 = 0.0; 
	var angle3 = 0.0;
	var radius1=0.7;
	
		var index2 = 0;
		var index3 = 0;
		

		for (angle2 = 0.0; angle2 < 6.2831853; angle2 = angle2 + 0.001)
		{
		circlePos1[index2++] = (Math.cos(angle2))*radius1;
		circlePos1[index2++] = (Math.sin(angle2))*radius1-0.40;
		circlePos1[index2++] = 0.0;
		

		}
		
		for (angle1 = 0.0; angle1 < 6.2831853; angle1 = angle1 + 0.001)
		{
		circleColor1[index3++] = 1.0;
		circleColor1[index3++] = 1.0;
		circleColor1[index3++] = 0.0;
	

		}
		
	
	vao_circle1=gl.createVertexArray();
	gl.bindVertexArray(vao_circle1);
	vbo_circle_position1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_position1);
	gl.bufferData(gl.ARRAY_BUFFER,circlePos1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_circle_color1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_circle_color1);
	gl.bufferData(gl.ARRAY_BUFFER,circleColor1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);




	perspectiveProjectionMatrix=mat4.create();
	
	gl.clearColor(0.0,0.0,0.0,1.0);
}


function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);



	for(var x = -1.0; x <= 1.05; x = x + 0.05)
	{
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,x,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_glines);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	}


	for(var y = -1.0; y <= 1.05; y = y + 0.05)
	{
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[y,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_glines1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	}

	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(angleTriangle));
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_circle);
	gl.drawArrays(gl.POINTS,0,((2 * 3.1415) / 0.001));
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(angleTriangle));
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line4);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_square1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_square2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_square3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_square4);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.4,-3.0]);
	mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(angleTriangle));
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_circle1);
	gl.drawArrays(gl.POINTS,0,((2 * 3.1415) / 0.001));
	gl.bindVertexArray(null);


	
	
	gl.useProgram(null);
	
	
	requestAnimationFrame(draw,canvas);
	
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}
		
	if(vbo)
	{	
		gl.deleteBuffer(vbo);
		vbo=null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{
	
} 

function degToRad(degrees)
{
	return(degrees*Math.PI/180);
}
