var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros=
{
AMC_ATTRIBUTE_POSITION:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXCOORD0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_rectangle;
var vbo_rectangle_position;
var vbo_rectangle_texture;

var texImage;
var mvpUniform;

var perspectiveProjectionMatrix;

var checkImageWidth=64;
var checkImageHeight=64;

var checkImage=new Uint8Array(checkImageWidth*checkImageHeight*4);

var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame= 
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

var static_texture=0;
var uniform_texture0_sampler;


function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	
	init();
	resize();
	draw();
	
}


function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}


function init()
{
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec2 vTexture0_coord;"+
	"out vec2 out_texture0_coord;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_mvp_matrix*vPosition;"+
	"out_texture0_coord=vTexture0_coord;"+
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec2 out_texture0_coord;"+
	"uniform highp sampler2D u_texture0_sampler;" +
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor=texture(u_texture0_sampler,out_texture0_coord);"+
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0,"vTexture0_coord");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	MakeCheckImage();
	static_texture=gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D,static_texture);
	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.REPEAT);
	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.REPEAT);
	gl.pixelStorei(gl.UNPACK_ALIGNMENT,1);
	gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,checkImageWidth,checkImageHeight,0,gl.RGBA,gl.UNSIGNED_BYTE,checkImage);
	gl.bindTexture(gl.TEXTURE_2D,null);

	
	
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	uniform_texture0_sampler=gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");
	

						
	/*var rectangleVertices=new Float32Array([
						1.0,1.0,0.0,
						-1.0,1.0,0.0,
						-1.0,-1.0,0.0,
						1.0,-1.0,0.0]);*/
						
						
	var rectangleTex=new Float32Array([
												1.0, 1.0,
												0.0, 1.0,
												0.0, 0.0,
												1.0, 0.0]);
	
	

	vao_rectangle=gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle);
	
	vbo_rectangle_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_rectangle_position);
	gl.bufferData(gl.ARRAY_BUFFER,0,gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_rectangle_texture=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_rectangle_texture);
	gl.bufferData(gl.ARRAY_BUFFER,rectangleTex,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0,2,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	

	perspectiveProjectionMatrix=mat4.create();
	
	gl.clearColor(0.0,0.0,0.0,1.0);
}


function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}


function MakeCheckImage()
{
	var i, j, c;

for (i = 0; i < checkImageHeight; i++)
		{
			for (j = 0; j < checkImageWidth; j++)
			{
			c = (((i&0x8)==0)^((j&0x8)==0))*255;

			checkImage[(i*64+j)*4+0] = c;
			checkImage[(i*64+j)*4+1] = c;
			checkImage[(i*64+j)*4+2] = c;
			checkImage[(i*64+j)*4+3] = 255;
			}
		}
		
		
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var rectangleVertices1=[];
	var rectangleVertices2=[];
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-4.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindTexture(gl.TEXTURE_2D,static_texture);
	gl.uniform1i(uniform_texture0_sampler,0);
	rectangleVertices1[0] = -2.0;
	rectangleVertices1[1] = -1.0;
	rectangleVertices1[2] = 0.0;
	rectangleVertices1[3] = -2.0;
	rectangleVertices1[4] = 1.0;
	rectangleVertices1[5] = 0.0;
	rectangleVertices1[6] = 0.0;
	rectangleVertices1[7] = 1.0;
	rectangleVertices1[8] = 0.0;
	rectangleVertices1[9] = 0.0;
	rectangleVertices1[10] = -1.0;
	rectangleVertices1[11] = 0.0;
	
	gl.bindVertexArray(vao_rectangle);
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_rectangle_position);
	gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(rectangleVertices1),gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-4.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindTexture(gl.TEXTURE_2D,static_texture);
	gl.uniform1i(uniform_texture0_sampler,0);
	rectangleVertices2[0] = 1.0;
	rectangleVertices2[1] = -1.0;
	rectangleVertices2[2] = 0.0;
	rectangleVertices2[3] = 1.0;
	rectangleVertices2[4] = 1.0;
	rectangleVertices2[5] = 0.0;
	rectangleVertices2[6] = 2.41421;
	rectangleVertices2[7] = 1.0;
	rectangleVertices2[8] = -1.41421;
	rectangleVertices2[9] = 2.41421;
	rectangleVertices2[10] = -1.0;
	rectangleVertices2[11] = -1.41421;
	
	gl.bindVertexArray(vao_rectangle);
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_rectangle_position);
	gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(rectangleVertices2),gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	
	gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
	
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}
		
	if(vbo)
	{	
		gl.deleteBuffer(vbo);
		vbo=null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{
	
} 