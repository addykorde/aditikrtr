var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[100.0,100.0,100.0,1.0];

var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var material_shininess=128.0;

var sphere=null;

var angleCube=0.0;

var uniform_texture0_sampler;

var cube_texture=0;
var perspectiveProjectionMatrix;



var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform,ldUniform,lsUniform,lightPositionUniform;
var kaUniform,kdUniform,ksUniform,materialShininessUniform;
var LKeyPressedUniform;

var bLKeyPressed=false;

var vao_cube;
var vbo_cube_position;
var vbo_cube_normal;
var vbo_cube_color;
var vbo_cube_texture;

var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;


function main()
{
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas failed");
	else
		console.log("Obtaining Canvas Succeeded");
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	init();
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}

function init()
{
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision mediump int;"+
	"in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"in vec4 vColor;" +
	"out vec4 out_color;"+
	"uniform mat4 u_model_matrix;"+
	"uniform mat4 u_view_matrix;"+
	"uniform mat4 u_projection_matrix;"+
	"in vec2 vTexture0_coord;"+
	"out vec2 out_texcoord;"+
	"uniform mediump int u_LKeyPressed;"+
	"uniform vec4 u_light_position;"+
	"out vec3 transformed_normals;"+
	"out vec3 light_direction;"+
	"out vec3 viewerVector;"+
	"out vec3 reflectionVector;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;"+
	"if(u_LKeyPressed==1)"+
	"{"+
	"vec4 eye_coordinates=u_view_matrix*u_model_matrix*vPosition;"+
	"transformed_normals=mat3(u_view_matrix*u_model_matrix)*vNormal;"+
	"light_direction=vec3(u_light_position)-eye_coordinates.xyz;"+
	"reflectionVector=reflect(-light_direction,transformed_normals);"+
	"viewerVector=vec3(-eye_coordinates);"+
	"}"+
	"out_texcoord=vTexture0_coord;"+
	"out_color=vColor;"+
	"}";
	
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error+"In Vertex Shader");
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+	
	"in vec3 transformed_normals;"+
	"in vec3 light_direction;"+
	"in vec2 out_texcoord;" +
	"in vec4 out_color;"+
	"uniform sampler2D u_sampler;"+
	"in vec3 viewerVector;"+
	"out vec4 FragColor;"+
	"uniform vec3 u_La;"+
	"uniform vec3 u_Ld;"+
	"uniform vec3 u_Ls;"+
	"uniform vec3 u_Ka;"+
	"uniform vec3 u_Kd;"+
	"uniform vec3 u_Ks;"+
	"uniform float u_material_shininess;"+
	"in vec3 reflectionVector;"+
	"uniform int u_LKeyPressed;"+
	"vec3 phong_ads_color;"+
	"void main(void)"+
	"{"+
	"if(u_LKeyPressed==1)"+
	"{"+
	"vec3 normalized_transformed_normals=normalize(transformed_normals);"+
	"vec3 normalized_light_direction=normalize(light_direction);"+
	"vec3 normalized_viewer_vector=normalize(viewerVector);"+
	"vec3 reflectionVector_normalize=normalize(reflectionVector);"+
	"vec3 ambient=u_La*u_Ka;"+
	"float tn_dot_ld=max(dot(normalized_transformed_normals,normalized_light_direction),0.0);"+
	"vec3 diffuse=u_Ld*u_Kd*tn_dot_ld;"+
	"vec3 specular=u_Ls*u_Ks*pow(max(dot(reflectionVector_normalize,normalized_viewer_vector),0.0),u_material_shininess);"+
	"phong_ads_color=ambient+diffuse+specular;"+
	"}"+
	"else"+
	"{"+
	"phong_ads_color=vec3(1.0,1.0,1.0);"+
	"}"+
	"FragColor=vec4(texture(u_sampler,out_texcoord)*out_color*(phong_ads_color,1.0));"+
	"}"
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error+"In Fragment Shader");
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,"vTexture0_coord");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_COLOR,"vColor");
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	cube_texture=gl.createTexture();
	cube_texture.image=new Image();
	cube_texture.image.src="marble.png";
	cube_texture.image.onload=function()
	{
		gl.bindTexture(gl.TEXTURE_2D,cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,cube_texture.image);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	}

	modelMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
	viewMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
	projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
	laUniform=gl.getUniformLocation(shaderProgramObject,"u_La");
	ldUniform=gl.getUniformLocation(shaderProgramObject,"u_Ld");
	lsUniform=gl.getUniformLocation(shaderProgramObject,"u_Ls");
	lightPositionUniform=gl.getUniformLocation(shaderProgramObject,"u_light_position");
	kaUniform=gl.getUniformLocation(shaderProgramObject,"u_Ka");
	kdUniform=gl.getUniformLocation(shaderProgramObject,"u_Kd");
	ksUniform=gl.getUniformLocation(shaderProgramObject,"u_Ks");
	materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
	uniform_texture0_sampler=gl.getUniformLocation(shaderProgramObject,"u_texture0_sampler");

	

var cubeVCNT=new Float32Array([
1.0, 1.0, -1.0,       0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0,0.0,
-1.0, 1.0, -1.0,     0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0,0.0,
-1.0, 1.0, 1.0,	    0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0,1.0,
1.0, 1.0, 1.0,	   0.0, 1.0, 0.0,0.0, 0.0, 1.0, 0.0,1.0,

1.0, -1.0, -1.0,	 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0,0.0,
-1.0, -1.0, -1.0,     1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0,1.0,
-1.0, -1.0, 1.0,	 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0,1.0,
1.0, -1.0, 1.0,	 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0,0.0,

1.0, 1.0, 1.0,	 1.0, 0.0, 0.0, 0.0, 0.0, -1.0,1.0,0.0,
-1.0, 1.0, 1.0,	 1.0, 0.0, 0.0, 0.0, 0.0, -1.0,1.0,1.0,
-1.0, -1.0, 1.0,	 1.0, 0.0, 0.0, 0.0, 0.0, -1.0,0.0,1.0,
1.0, -1.0, 1.0,	 1.0, 0.0, 0.0, 0.0, 0.0, -1.0,0.0,0.0,

1.0, 1.0, -1.0,	 1.0, 1.0, 0.0, -1.0, 0.0, 0.0,0.0,0.0,
-1.0, 1.0, -1.0,	 1.0, 1.0, 0.0, -1.0, 0.0, 0.0,1.0,0.0,
-1.0, -1.0, -1.0,     1.0, 1.0, 0.0, -1.0, 0.0, 0.0,1.0,1.0,
1.0, -1.0, -1.0,	 1.0, 1.0, 0.0, -1.0, 0.0, 0.0,0.0,1.0,

1.0, 1.0, -1.0,	 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0,1.0,
1.0, 1.0, 1.0,	 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0,0.0,
1.0, -1.0, 1.0,	 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0,0.0,
1.0, -1.0, -1.0,	 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0,1.0,

-1.0, 1.0, 1.0,	 1.0, 0.0, 1.0, 0.0, -1.0, 0.0,1.0,1.0,
-1.0, 1.0, -1.0,	 1.0, 0.0, 1.0, 0.0, -1.0, 0.0,0.0,1.0,
-1.0, -1.0, -1.0,     1.0, 0.0, 1.0, 0.0, -1.0, 0.0,0.0,0.0,
-1.0, -1.0, 1.0 ,     1.0, 0.0, 1.0, 0.0, -1.0, 0.0,1.0,0.0
]);
	vao_cube=gl.createVertexArray();
	gl.bindVertexArray(vao_cube);
	vbo_cube_position_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_cube_position_position);
	gl.bufferData(gl.ARRAY_BUFFER,cubeVCNT,gl.STATIC_DRAW);

	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,11*4,0*4);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_COLOR,3,gl.FLOAT,false,11*4,3*4);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_COLOR);
	
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL,3,gl.FLOAT,false,11*4,6*4);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL);
	
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,false,11*4,9*4);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);

	
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindVertexArray(null);
	
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	gl.clearColor(0.0,0.0,0.0,1.0);
	perspectiveProjectionMatrix=mat4.create();
	
}

function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);
	
	if(bLKeyPressed==true)
	{
		gl.uniform1i(LKeyPressedUniform,1);
		
		gl.uniform3fv(laUniform,light_ambient);
		gl.uniform3fv(ldUniform,light_diffuse);
		gl.uniform3fv(lsUniform,light_specular);
		gl.uniform4fv(lightPositionUniform,light_position);
		
		gl.uniform3fv(kaUniform,material_ambient);
		gl.uniform3fv(kdUniform,material_diffuse);
		gl.uniform3fv(ksUniform,material_specular);
		gl.uniform1f(materialShininessUniform,material_shininess);
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform,0);
	}
	
	var modelMatrix=mat4.create();
	var viewMatrix=mat4.create();
	
	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);
	mat4.rotateY(modelMatrix,modelMatrix,degToRad(angleCube));
	mat4.rotateX(modelMatrix,modelMatrix,degToRad(angleCube));
	mat4.rotateZ(modelMatrix,modelMatrix,degToRad(angleCube));
	gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);
	gl.bindTexture(gl.TEXTURE_2D,cube_texture);
	gl.uniform1i(uniform_texture0_sampler,0);
	gl.bindVertexArray(vao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	gl.drawArrays(gl.TRIANGLE_FAN,20,4);
	gl.bindVertexArray(null)
	gl.useProgram(null);

	
	angleCube=angleCube+2.0;
	if(angleCube>=360.0)
		angleCube=angleCube-360.0;
	requestAnimationFrame(draw,canvas);
	
}


function uninitialize()
{
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 76:
		if(bLKeyPressed==false)
			bLKeyPressed=true;
		else
			bLKeyPressed=false;
		break;
		
		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{
	//code
} 

function degToRad(degrees)
{
	return(degrees*Math.PI/180);
}








