var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros=
{
AMC_ATTRIBUTE_POSITION:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXCOORD0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vbo_pyramid_position;
var vbo_pyramid_normal;
var vao_cube;
var vbo_cube_position;
var vbo_cube_color;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform_red,ldUniform_red,lsUniform_red,lightPositionUniform,laUniform_blue,ldUniform_blue,lsUniform_blue;
var kaUniform,kdUniform,ksUniform,materialShininessUniform;
var LKeyPressedUniform;


var ambient_red=[];
var diffuse_red=[];
var specular_red=[];
var position_red=[];

var ambient_blue=[];
var diffuse_blue=[];
var specular_blue=[];
var position_blue=[];

var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var material_shininess=50.0;


var bLKeyPressed=false;

var perspectiveProjectionMatrix;


var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;


var anglePyramid=0.0;
var angleCube=0.0;

function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	
	init();
	resize();
	draw();
	
}


function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozrequestFullScreen)
			canvas.mozrequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}


function init()
{
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"uniform mat4 u_model_matrix;"+
	"uniform mat4 u_view_matrix;"+
	"uniform mat4 u_projection_matrix;"+
	"uniform mediump int u_LKeyPressed;"+
	"uniform vec3 u_La_Blue;"+
	"uniform vec3 u_Ld_Blue;"+
	"uniform vec3 u_Ls_Blue;"+
	"uniform vec3 u_La_Red;"+
	"uniform vec3 u_Ld_Red;"+
	"uniform vec3 u_Ls_Red;"+
	"uniform vec4 u_light_position_Red;"+
	"uniform vec4 u_light_position_Blue;"+
	"uniform vec3 u_Ka;"+
	"uniform vec3 u_Kd;"+
	"uniform vec3 u_Ks;"+
	"uniform float u_material_shininess;"+
	"vec3 phong_ads_color1;"+
	"vec3 phong_ads_color2;"+
	"out vec3 FinalColor;"+
	"void main(void)"+
	"{"+
	"if(u_LKeyPressed==1)"+
	"{"+
	"vec4 eye_coordinates=u_view_matrix*u_model_matrix*vPosition;"+
	"vec3 transformed_normals=normalize(mat3(u_view_matrix*u_model_matrix)*vNormal);"+
	"vec3 light_direction_Red=normalize(vec3(u_light_position_Red)-eye_coordinates.xyz);"+
	"vec3 light_direction_Blue=normalize(vec3(u_light_position_Blue)-eye_coordinates.xyz);"+
	"float tn_dot_ld_Red=max(dot(transformed_normals,light_direction_Red),0.0);"+
	"float tn_dot_ld_Blue=max(dot(transformed_normals,light_direction_Blue),0.0);"+
	"vec3 ambient1=u_La_Red*u_Ka;"+
	"vec3 diffuse1=u_Ld_Red*u_Kd*tn_dot_ld_Red;"+
	"vec3 reflection_vector1=reflect(-light_direction_Red,transformed_normals);"+
	"vec3 viewer_vector=normalize(-eye_coordinates.xyz);"+
	"vec3 specular1=u_Ls_Red*u_Ks*pow(max(dot(reflection_vector1,viewer_vector),0.0),u_material_shininess);"+
	"phong_ads_color1=ambient1+diffuse1+specular1;"+
	"vec3 ambient2=u_La_Blue*u_Ka;"+
	"vec3 diffuse2=u_Ld_Blue*u_Kd*tn_dot_ld_Blue;"+
	"vec3 reflection_vector2=reflect(-light_direction_Blue,transformed_normals);"+
	"vec3 specular2=u_Ls_Blue*u_Ks*pow(max(dot(reflection_vector2,viewer_vector),0.0),u_material_shininess);"+
	"phong_ads_color2=ambient2+diffuse2+specular2;"+
	"FinalColor=phong_ads_color1+phong_ads_color2;"+
	"}"+
	"else"+
	"{"+
	"FinalColor=vec3(1.0,1.0,1.0);"+
	"}"+
	"gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;"+
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 FinalColor;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor=vec4(FinalColor,1.0);"+
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	modelMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
	viewMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
	projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
	laUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_La_Blue");
	ldUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_Ld_Blue");
	lsUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_Ls_Blue");
	laUniform_red=gl.getUniformLocation(shaderProgramObject,"u_La_Red");
	ldUniform_red=gl.getUniformLocation(shaderProgramObject,"u_Ld_Red");
	lsUniform_red=gl.getUniformLocation(shaderProgramObject,"u_Ls_Red");
	lightPositionUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_light_position_Blue");
	lightPositionUniform_red=gl.getUniformLocation(shaderProgramObject,"u_light_position_Red");
	kaUniform=gl.getUniformLocation(shaderProgramObject,"u_Ka");
	kdUniform=gl.getUniformLocation(shaderProgramObject,"u_Kd");
	ksUniform=gl.getUniformLocation(shaderProgramObject,"u_Ks");
	materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
	
	
	var pyramidVertices=new Float32Array([
										0.0, 1.0, 0.0,
										-1.0, -1.0, 1.0,
										1.0, -1.0, 1.0,
										0.0, 1.0, 0.0,
										1.0, -1.0, 1.0,
										1.0, -1.0, -1.0,
										0.0, 1.0, 0.0,
										-1.0, -1.0,-1.0,
										-1.0, -1.0, 1.0,
										0.0, 1.0, 0.0,
										1.0, -1.0, -1.0,
										-1.0, -1.0, -1.0]);
	var pyramidNormal=new Float32Array([
								0.0, 0.447214, 0.894427,
								0.0, 0.447214, 0.894427,
								0.0, 0.447214, 0.894427,
								


								0.894427, 0.447214, 0.0,
								0.894427, 0.447214, 0.0,
								0.894427, 0.447214, 0.0,
								

								-0.894427, 0.447214, 0.0,
								-0.894427, 0.447214, 0.0,
								-0.894427, 0.447214, 0.0,
							

								0.0, 0.447214, -0.89442,
								0.0, 0.447214, -0.89442,
								0.0, 0.447214, -0.89442,]);
						
	
	vao_pyramid=gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
	
	vbo_pyramid_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_pyramid_position);
	gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_pyramid_normal=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_pyramid_normal);
	gl.bufferData(gl.ARRAY_BUFFER,pyramidNormal,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	
	ambient_red[0]=0.0;
	ambient_red[1]=0.0;
	ambient_red[2]=0.0;
	ambient_red[3]=1.0;
	
	diffuse_red[0]=1.0;
	diffuse_red[1]=0.0;
	diffuse_red[2]=0.0;
	diffuse_red[3]=1.0;
	
	specular_red[0]=1.0;
	specular_red[1]=0.0;
	specular_red[2]=0.0;
	specular_red[3]=1.0;
	
	position_red[0]=-2.0;
	position_red[1]=0.0;
	position_red[2]=0.0;
	position_red[3]=1.0;
	
	
	ambient_blue[0]=0.0;
	ambient_blue[1]=0.0;
	ambient_blue[2]=0.0;
	ambient_blue[3]=1.0;
	
	diffuse_blue[0]=0.0;
	diffuse_blue[1]=0.0;
	diffuse_blue[2]=1.0;
	diffuse_blue[3]=1.0;
	
	specular_blue[0]=0.0;
	specular_blue[1]=0.0;
	specular_blue[2]=1.0;
	specular_blue[3]=1.0;
	
	position_blue[0]=2.0;
	position_blue[1]=0.0;
	position_blue[2]=0.0;
	position_blue[3]=1.0;
	
	
	

	perspectiveProjectionMatrix=mat4.create();
	
	gl.clearColor(0.0,0.0,0.0,1.0);
	
	gl.enable(gl.DEPTH_TEST);
	//gl.enable(gl.CULL_FACE);
	
	
}


function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	
	if(bLKeyPressed==true)
	{
		gl.uniform1i(LKeyPressedUniform,1);
		
		gl.uniform3f(laUniform_red,0.0,0.0,0.0);
		gl.uniform3f(ldUniform_red,1.0,0.0,0.0);
		gl.uniform3f(lsUniform_red,1.0,0.0,0.0);
		gl.uniform4fv(lightPositionUniform_red,position_red);
		
		gl.uniform3f(laUniform_blue,0.0,0.0,0.0);
		gl.uniform3f(ldUniform_blue,0.0,0.0,1.0);
		gl.uniform3f(lsUniform_blue,0.0,0.0,1.0);
		gl.uniform4fv(lightPositionUniform_blue,position_blue);
		
		gl.uniform3fv(kaUniform,material_ambient);
		gl.uniform3fv(kdUniform,material_diffuse);
		gl.uniform3fv(ksUniform,material_specular);
		gl.uniform1f(materialShininessUniform,material_shininess);
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform,0);
	}
	
	
	
	var modelMatrix=mat4.create();
	var viewMatrix=mat4.create();
	
	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-4.0]);
	
	gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);
	mat4.rotateY(modelMatrix,modelMatrix,degToRad(anglePyramid));
	gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);

	gl.bindVertexArray(vao_pyramid);
	gl.drawArrays(gl.TRIANGLES,0,12);
	gl.bindVertexArray(null);
	

	
	gl.useProgram(null);
	angleCube=angleCube+2.0;
	if(angleCube>=360.0)
		angleCube=angleCube-360.0;
	
	anglePyramid=anglePyramid+2.0;
	if(anglePyramid>=360.0)
		anglePyramid=anglePyramid-360.0;
	
	requestAnimationFrame(draw,canvas);
	
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}
		
	if(vbo)
	{	
		gl.deleteBuffer(vbo);
		vbo=null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 76:
		if(bLKeyPressed==false)
			bLKeyPressed=true;
		else
			bLKeyPressed=false;
		break;
		
		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{
	
} 

function degToRad(degrees)
{
	return(degrees*Math.PI/180);
}
