var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros=
{
AMC_ATTRIBUTE_POSITION:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXCOORD0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_triangle;
var vbo_triangle_position;
var vbo_triangle_color;
var vao_rectangle;
var vbo_rectangle_position;
var mvpUniform;

var perspectiveProjectionMatrix;


var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;




function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	
	init();
	resize();
	draw();
	
}


function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozrequestFullScreen)
			canvas.mozrequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}


function init()
{
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 out_color;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_mvp_matrix*vPosition;"+
	"out_color=vColor;"+
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"out vec4 FragColor;"+
	"in vec4 out_color;"+
	"void main(void)"+
	"{"+
	"FragColor=out_color;"+
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	
	var triangleVertices=new Float32Array([
						0.0,1.0,0.0,
						-1.0,-1.0,0.0,
						1.0,-1.0,0.0]);
	var triangleColor=new Float32Array([
								1.0,0.0,0.0,
               					0.0,1.0,0.0,
								0.0,0.0,1.0]);
						
	var rectangleVertices=new Float32Array([
						-1.0,-1.0,0.0,
						-1.0,1.0,0.0,
						1.0,1.0,0.0,
						1.0,-1.0,0.0]);
	
	vao_triangle=gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);
	
	vbo_triangle_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_triangle_position);
	gl.bufferData(gl.ARRAY_BUFFER,triangleVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_triangle_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_triangle_color);
	gl.bufferData(gl.ARRAY_BUFFER,triangleColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_rectangle=gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle);
	vbo_rectangle_position_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_rectangle_position_position);
	gl.bufferData(gl.ARRAY_BUFFER,rectangleVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR,0.0,0.0,1.0);
	
	gl.bindVertexArray(null);

	perspectiveProjectionMatrix=mat4.create();
	
	gl.clearColor(0.0,0.0,0.0,1.0);
}


function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,0.0,-6.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_triangle);
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-6.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_rectangle);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
	
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}
		
	if(vbo)
	{	
		gl.deleteBuffer(vbo);
		vbo=null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{
	
} 