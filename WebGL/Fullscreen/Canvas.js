var canvas=null;
var context=null;

function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	console.log("Canvas Width :"+canvas.width+" And Canvas Height:"+canvas.height);
	
	var context=canvas.getContext("2d");
	
	if(!context)
		console.log("Obtaining context Failed");
	else
		console.log("Obtaining context Succeeded");
	
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	drawText("Hello World !!!");
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	
}


function drawText(text)
{
	context.textAlign="center";
	context.textBaseline="middle";
	
	context.font="48px sans-serif";
	context.fillStyle="white";
	context.fillText(text,canvas.width/2,canvas.height/2);
	
}


function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozrequestFullScreen)
			canvas.mozrequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		
	}
	
	else
	{
		if(document.exitFullScreen)
			document.exitFullScreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullScreen)
			document.webkitExitFullScreen();
		else if(document.msExitFullScreen)
			document.msExitFullScreen();
		
	}
}


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:toggleFullScreen();
		
		drawText("Hello World !!!");
		break;
	}
}

function mouseDown(event)
{
	alert("A mouse is pressed");
}