var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[100.0,100.0,100.0,1.0];

var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var material_shininess=50.0;

var sphere=null;
var shoulder =0.0;
var elbow=0.0;
var perspectiveProjectionMatrix;

var modelViewMatrixUniform,projectionMatrixUniform;
var ldUniform,kdUniform,lightPositionUniform;
var LKeyPressedUniform;


var mvpUniform;

var bLKeyPressed=false;

var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;


var stack=new Array();	

function main()
{
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas failed");
	else
		console.log("Obtaining Canvas Succeeded");
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	init();
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}

function init()
{
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 out_color;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_mvp_matrix*vPosition;"+
	"out_color=vColor;"+
	"}";
	
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error+"In Vertex Shader");
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"out vec4 FragColor;"+
	"in vec4 out_color;"+
	"void main(void)"+
	"{"+
	"FragColor=vec4(0.5, 0.35, 0.05,1.0);"+
	"}";
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error+"In Fragment Shader");
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	//gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	
	sphere1=new Mesh();
	makeSphere(sphere1,2.0,30,30);
	
	sphere2=new Mesh();
	makeSphere(sphere2,2.0,30,30);
	
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	gl.clearColor(0.0,0.0,0.0,1.0);
	perspectiveProjectionMatrix=mat4.create();
	
}

function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);
	//var  sp=new Stack();
	
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	var translationMatrix=mat4.create();
	var rotationMatrix=mat4.create();
	var scaleMatrix=mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -20.0]);
	
	//mat4.multiply(modelViewMatrix,modelViewMatrix,translationMatrix);
	stack.push(Object.assign({}, modelViewMatrix));
	mat4.rotateZ(modelViewMatrix,modelViewMatrix,degToRad(shoulder));
	mat4.translate(modelViewMatrix,modelViewMatrix,[1.0, 0.0, 0.0]);
	stack.push(Object.assign({}, modelViewMatrix));
	mat4.scale(modelViewMatrix,modelViewMatrix,[2.0, 0.5, 1.0]);
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	
	
	sphere1.draw();

	

	 mat4.identity(modelViewMatrix);
   mat4.identity(modelViewProjectionMatrix);

   modelViewMatrix=stack.pop();

   mat4.translate(modelViewMatrix, modelViewMatrix, [6.0, 0.0, 0.0]);

   mat4.rotateZ(modelViewMatrix, modelViewMatrix,degToRad(elbow));

   mat4.translate(modelViewMatrix, modelViewMatrix, [1.0, 0.0, 0.0]);
 

   stack.push(Object.assign({}, modelViewMatrix));

    mat4.scale(modelViewMatrix, modelViewMatrix, [2.0, 0.5, 1.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	sphere2.draw();

	stack.pop();
	stack.pop();


	gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
	
}


function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}
		
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 76:
		if(bLKeyPressed==false)
			bLKeyPressed=true;
		else
			bLKeyPressed=false;
		break;
		

		case 83:
		case 115:
		shoulder = (shoulder + 3) % 360;
		break;

		case 69:
		case 101:
		elbow = (elbow + 3) % 360;
		break;

		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{
	//code
} 

function degToRad(degrees)
{
	return(degrees*Math.PI/180);
}








