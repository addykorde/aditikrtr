var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros=
{
AMC_ATTRIBUTE_POSITION:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXCOORD0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_i1;
var vao_i2;
var vao_n;
var vao_d;
var vao_a;
var vao_line1;
var vao_line2;
var vao_line3;
var vao_line4;
var vao_line5;
var vao_line6;

var vbo_position_i1;
var vbo_position_i2;
var vbo_position_n;
var vbo_position_d;
var vbo_position_a;
var vbo_position_line1;
var vbo_position_line2;
var vbo_position_line3;
var vbo_position_line4;
var vbo_position_line5;
var vbo_position_line6;

var planex=-4.0;
var planey=-4.0;
var planex1=-4.0;
var planey1=4.0;
var planex2=-4.0;
var planez=-3.0;
var planez1=-3.0;
var planez2=-3.0;
var planex3=0.9999999999999597;
var planey3=0.0;
var planex4=0.9999999999999597;
var planey4=0.0;
var vbo_color_i1;
var vbo_color_i2;
var vbo_color_n;
var vbo_color_d;
var vbo_color_a;
var vbo_color_line1;
var vbo_color_line2;
var vbo_color_line3;
var vbo_color_line4;
var vbo_color_line5;
var vbo_color_line6;
var vao_apex;
var vbo_position_apex;
var vbo_color_apex;
var vao_iafquad;
var vbo_position_iafquad;
var vbo_color_iafquad;
var vao_iafbquad;
var vbo_position_iafbquad;
var vbo_color_iafbquad;
var vao_biggerwings1;
var vbo_position_biggerwings1;
var vbo_color_biggerwings1;
var vao_biggerwings2;
var vbo_position_biggerwings2;
var vbo_color_biggerwings2;
var vao_smallerwings1;
var vbo_position_smallerwings1;
var vbo_color_smallerwings1;
var vao_smallerwings2;
var vbo_position_smallerwings2;
var vbo_color_smallerwings2;
var voa_heatfurnace;
var vbo_position_heatfurnace;
var vbo_color_heatfurnace;
var mvpUniform;
var translatei1=-2.0;
var translatea=3.2;
var translaten=5.0;
var translatei2=-7.5;
var flagd=0;
var perspectiveProjectionMatrix;

var flagp=0;
var flagp3=0;

var linex1=-5.25;
var linex2=-5.25;
var linex3=-4.4;

var linez=-2.0;

var flag4=0;

var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;




function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	
	init();
	resize();
	draw();
	
}


function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}


function init()
{
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 out_color;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_mvp_matrix*vPosition;"+
	"out_color=vColor;"+
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"out vec4 FragColor;"+
	"in vec4 out_color;"+
	"void main(void)"+
	"{"+
	"FragColor=out_color;"+
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	
	var i1=new Float32Array([
						-0.70, 0.80, 0.0,

						- 0.72, 0.80, 0.0,

						- 0.72,-0.80, 0.0,

						- 0.70, -0.80, 0.0,]);
	var i2=new Float32Array([
						0.20, 0.80, 0.0,

						0.22, 0.80, 0.0,

						0.22, -0.80, 0.0,

						0.20, -0.80, 0.0]);	
	var n=new Float32Array([
						-0.52, 0.80, 0.0,

	- 0.50, 0.80, 0.0,

	- 0.50, -0.80, 0.0,

	- 0.52, -0.80, 0.0,


	- 0.50, 0.80, 0.0,

	- 0.48, 0.80, 0.0,

	- 0.36, -0.80, 0.0,
		
	- 0.38, -0.80, 0.0,
 

	- 0.38, 0.80, 0.0,

	- 0.36, 0.80, 0.0,

	- 0.36, -0.80, 0.0,

	- 0.38, -0.80, 0.0]);
	var d=new Float32Array([
						-0.16, 0.78, 0.0,

	- 0.14, 0.78, 0.0,

	- 0.14, -0.78, 0.0,

	- 0.16, -0.78, 0.0,



	-0.16, 0.78, 0.0,

	- 0.16, 0.80, 0.0,

	0.0, 0.80, 0.0,

	0.0, 0.78, 0.0,


	- 0.16, -0.80, 0.0,

	- 0.16, -0.78, 0.0,

	0.0, -0.78, 0.0,

	0.0, -0.80, 0.0,



	-0.02, 0.78, 0.0,

	0.0, 0.78, 0.0,

	0.0, -0.78, 0.0,

	- 0.02, -0.78, 0.0]);
	var a=new Float32Array([
	0.40, -0.80, 0.0,

	0.42, -0.80, 0.0,

	0.52, 0.80, 0.0,

	0.50, 0.80, 0.0,


	0.72, -0.80, 0.0,

	0.74, -0.80, 0.0,

	0.54, 0.80, 0.0,

	0.52, 0.80, 0.0]);
						
	var i1Color=new Float32Array([
								1.0, 0.647058823529, 0.0,

								1.0, 0.647058823529, 0.0,

								0.0705882352911, 0.5333333, 0.0,

								0.0705882352911, 0.5333333, 0.0 ]);
	var i2Color=new Float32Array([
								1.0, 0.647058823529, 0.0,

								1.0, 0.647058823529, 0.0,

								0.0705882352911, 0.5333333, 0.0,

								0.0705882352911, 0.5333333, 0.0]);
	var nColor=new Float32Array([
								1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,
	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0 ,
	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0]);

	var dColor=new Float32Array([
	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,



	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,


	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,



	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0]);
	var aColor=new Float32Array([
								0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,


	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0]);
	
	
	
	
	var line1Vertices=new Float32Array([
								0.50,0.0,0.0,
								0.62,0.0,0.0]);
	var line2Vertices=new Float32Array([
								0.50, -0.04, 0.0,
								0.63, -0.04, 0.0]);
	var line3Vertices=new Float32Array([
								0.50,-0.08,0.0,
								0.64, -0.08, 0.0]);

	var line4Vertices=new Float32Array([-2.0,0.03,0.0,
					     1.0,0.03,0.0
					   ]);

	var line5Vertices=new Float32Array([-2.0,-0.01,0.0,
					    1.0,-0.01,0.0]);

	var line6Vertices=new Float32Array([-2.0,-0.05,0.0,
					    1.0,-0.05,0.0
						]);
								
	var line1Color=new Float32Array([
		1.0, 0.647058823529, 0.0,
		1.0, 0.647058823529, 0.0
								]);
	
	var line2Color=new Float32Array([
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
								]);
								
	var line3Color=new Float32Array([
		0.0705882352911, 0.5333333, 0.0,
		0.0705882352911, 0.5333333, 0.0
								]);

	var line4Color=new Float32Array([
		1.0, 0.647058823529, 0.0,
		1.0, 0.647058823529, 0.0
								]);
	
	var line5Color=new Float32Array([
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
								]);
								
	var line6Color=new Float32Array([
		0.0705882352911, 0.5333333, 0.0,
		0.0705882352911, 0.5333333, 0.0
								]);
	
		var apexVertices=new Float32Array([
	0.60,0.0,0.0,
	0.50,-0.20,0.0,
	0.50,0.20,0.0]);

	var apexColor=new Float32Array([
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933]);

	var biafVertices=new Float32Array([
	0.50,-0.20,0.0,
	-0.10,-0.20,0.0,
	-0.10,0.20,0.0,
	0.50,0.20,0.0,
	]);
	
	var biafColor=new Float32Array([
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	]);


	var iafquad=new Float32Array([
	-0.10,0.15,0.0,
	-0.10,-0.15,0.0,
	-1.25,-0.15,0.0,
	-1.25,0.15,0.0,
	]);	

	var iafColor=new Float32Array([
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	]);

	
	var bigVertices1=new Float32Array([
	-0.25,0.15,0.0,
	-0.35,0.65,0.0,
	-0.55,0.65,0.0,
	-0.55,0.15,0.0
	]);

	var bigColor1=new Float32Array([
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	]);

	var bigVertices2=new Float32Array([
	-0.25,-0.15,0.0,
	-0.35,-0.65,0.0,
	-0.55,-0.65,0.0,
	-0.55,-0.15,0.0
	]);

	var bigColor2=new Float32Array([
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	]);


	var smallVertices1=new Float32Array([
	-1.0,0.15,0.0,
	-1.05,0.45,0.0,
	-1.10,0.45,0.0,
	-1.10,0.15,0.0
	]);

	var smallColor1=new Float32Array([
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	]);
	
	var smallVertices2=new Float32Array([
	-1.0,-0.15,0.0,
	-1.05,-0.45,0.0,
	-1.10,-0.45,0.0,
	-1.10,-0.15,0.0
	]);
	
	var smallColor2=new Float32Array([
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	0.729411, 0.8627, 0.933,
	]);

	//Heat Furnace


	
	var piPosition=new Float32Array([
		-0.65, 0.12, 0.0,
		-0.65, -0.12, 0.0]);
	var piColor=new Float32Array([
		0.0,0.0,1.0,
		0.0,0.0,1.0]);

	
	var paPosition=new Float32Array([
		-0.50, -0.12, 0.0,
		-0.40, 0.12, 0.0,
		-0.40, 0.12, 0.0,
		-0.30, -0.12, 0.0,
		-0.45, 0.02, 0.0,
		-0.35, 0.02, 0.0]);
	var paColor=new Float32Array([
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0]);

	var pfPosition=new Float32Array([
		-0.15, 0.12, 0.0,
		-0.15, -0.12, 0.0,
		-0.15, 0.12, 0.0,
		-0.04, 0.12, 0.0,
		-0.15, 0.04, 0.0,
		-0.06, 0.04, 0.0]);
	var pfColor=new Float32Array([
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0]);





	vao_pi=gl.createVertexArray();
	gl.bindVertexArray(vao_pi);
	
	vbo_position_pi=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_pi);
	gl.bufferData(gl.ARRAY_BUFFER,piPosition,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_pi=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_pi);
	gl.bufferData(gl.ARRAY_BUFFER,piColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

vao_pa=gl.createVertexArray();
	gl.bindVertexArray(vao_pa);
	
	vbo_position_pa=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_pa);
	gl.bufferData(gl.ARRAY_BUFFER,paPosition,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_pa=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_pa);
	gl.bufferData(gl.ARRAY_BUFFER,paColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_pf=gl.createVertexArray();
	gl.bindVertexArray(vao_pf);
	
	vbo_position_pf=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_pf);
	gl.bufferData(gl.ARRAY_BUFFER,pfPosition,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_pf=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_pf);
	gl.bufferData(gl.ARRAY_BUFFER,pfColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	

	vao_apex=gl.createVertexArray();
	gl.bindVertexArray(vao_apex);
	
	vbo_position_apex=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_apex);
	gl.bufferData(gl.ARRAY_BUFFER,apexVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_apex=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_apex);
	gl.bufferData(gl.ARRAY_BUFFER,apexColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	vao_iafquad=gl.createVertexArray();
	gl.bindVertexArray(vao_iafquad);
	
	vbo_position_iafquad=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_iafquad);
	gl.bufferData(gl.ARRAY_BUFFER,iafquad,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_iafquad=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_iafquad);
	gl.bufferData(gl.ARRAY_BUFFER,iafColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_iafbquad=gl.createVertexArray();
	gl.bindVertexArray(vao_iafbquad);
	
	vbo_position_iafbquad=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_iafbquad);
	gl.bufferData(gl.ARRAY_BUFFER,biafVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_iafbquad=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_iafbquad);
	gl.bufferData(gl.ARRAY_BUFFER,biafColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);



	vao_biggerwings1=gl.createVertexArray();
	gl.bindVertexArray(vao_biggerwings1);
	
	vbo_position_biggerwings1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_biggerwings1);
	gl.bufferData(gl.ARRAY_BUFFER,bigVertices1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_biggerwings1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_biggerwings1);
	gl.bufferData(gl.ARRAY_BUFFER,bigColor1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	vao_biggerwings2=gl.createVertexArray();
	gl.bindVertexArray(vao_biggerwings2);
	
	vbo_position_biggerwings2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_biggerwings2);
	gl.bufferData(gl.ARRAY_BUFFER,bigVertices2,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_biggerwings2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_biggerwings2);
	gl.bufferData(gl.ARRAY_BUFFER,bigColor2,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_smallerwings1=gl.createVertexArray();
	gl.bindVertexArray(vao_smallerwings1);
	
	vbo_position_smallerwings1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_smallerwings1);
	gl.bufferData(gl.ARRAY_BUFFER,smallVertices1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_smallerwings1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_smallerwings1);
	gl.bufferData(gl.ARRAY_BUFFER,smallColor1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	

	vao_smallerwings2=gl.createVertexArray();
	gl.bindVertexArray(vao_smallerwings2);
	
	vbo_position_smallerwings2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_smallerwings2);
	gl.bufferData(gl.ARRAY_BUFFER,smallVertices2,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_smallerwings2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_smallerwings2);
	gl.bufferData(gl.ARRAY_BUFFER,smallColor2,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	vao_i1=gl.createVertexArray();
	gl.bindVertexArray(vao_i1);
	
	vbo_i1_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i1_position);
	gl.bufferData(gl.ARRAY_BUFFER,i1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_i1_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i1_color);
	gl.bufferData(gl.ARRAY_BUFFER,i1Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	vao_i2=gl.createVertexArray();
	gl.bindVertexArray(vao_i2);
	
	vbo_i2_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i2_position);
	gl.bufferData(gl.ARRAY_BUFFER,i2,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_i2_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i2_color);
	gl.bufferData(gl.ARRAY_BUFFER,i2Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	
	vao_n=gl.createVertexArray();
	gl.bindVertexArray(vao_n);
	
	vbo_n_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_n_position);
	gl.bufferData(gl.ARRAY_BUFFER,n,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_n_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_n_color);
	gl.bufferData(gl.ARRAY_BUFFER,nColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	vao_d=gl.createVertexArray();
	gl.bindVertexArray(vao_d);
	
	vbo_d_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_d_position);
	gl.bufferData(gl.ARRAY_BUFFER,d,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_d_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_d_color);
	gl.bufferData(gl.ARRAY_BUFFER,dColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	vao_a=gl.createVertexArray();
	gl.bindVertexArray(vao_a);
	
	vbo_a_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_a_position);
	gl.bufferData(gl.ARRAY_BUFFER,a,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_a_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_a_color);
	gl.bufferData(gl.ARRAY_BUFFER,aColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	
	
	vao_line1=gl.createVertexArray();
	gl.bindVertexArray(vao_line1);
	
	vbo_position_line1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line1);
	gl.bufferData(gl.ARRAY_BUFFER,line1Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_line1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line1);
	gl.bufferData(gl.ARRAY_BUFFER,line1Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	vao_line2=gl.createVertexArray();
	gl.bindVertexArray(vao_line2);
	vbo_position_line2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line2);
	gl.bufferData(gl.ARRAY_BUFFER,line2Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_line2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line2);
	gl.bufferData(gl.ARRAY_BUFFER,line2Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	
	vao_line3=gl.createVertexArray();
	gl.bindVertexArray(vao_line3);
	vbo_position_line3=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line3);
	gl.bufferData(gl.ARRAY_BUFFER,line3Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_line3=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line3);
	gl.bufferData(gl.ARRAY_BUFFER,line3Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	vao_line4=gl.createVertexArray();
	gl.bindVertexArray(vao_line4);
	vbo_position_line4=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line4);
	gl.bufferData(gl.ARRAY_BUFFER,line4Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_line4=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line4);
	gl.bufferData(gl.ARRAY_BUFFER,line4Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_line5=gl.createVertexArray();
	gl.bindVertexArray(vao_line5);
	vbo_position_line5=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line5);
	gl.bufferData(gl.ARRAY_BUFFER,line5Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_line5=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line5);
	gl.bufferData(gl.ARRAY_BUFFER,line5Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	vao_line6=gl.createVertexArray();
	gl.bindVertexArray(vao_line6);
	vbo_position_line6=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line6);
	gl.bufferData(gl.ARRAY_BUFFER,line6Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_line6=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line6);
	gl.bufferData(gl.ARRAY_BUFFER,line6Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	perspectiveProjectionMatrix=mat4.create();
	
	gl.clearColor(0.0,0.0,0.0,1.0);
}


function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[translatei1, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_i1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.3, translatei2, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_i2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[-0.3, translaten, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_n);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.bindVertexArray(null);
	
	if(flagd==1)
	{
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_d);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.bindVertexArray(null);
	flagp=1;
	}
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[translatea, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_a);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.bindVertexArray(null);
	if(flag4==1)
	{
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.2, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.2, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.2, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	}
	
	if(flagp==1)
	{
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	var scaleMatrix=mat4.create();
	//scaleMatrix=mat4.scale(2.0,2.0,2.0);
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	//mat4.multiply(modelViewMatrix,scaleMatrix,modelViewMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_apex);
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafbquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pi);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey, planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pa);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex, planey,planez]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pf);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey, planez]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey, planez]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey, planez]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

//================================================================================================================

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	var scaleMatrix=mat4.create();
	//scaleMatrix=mat4.scale(2.0,2.0,2.0);
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	//mat4.multiply(modelViewMatrix,scaleMatrix,modelViewMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_apex);
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafbquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pi);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pa);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex1, planey1, planez1]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pf);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);


var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey1, planez]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey1, planez]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey1, planez]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

//===============================================================================================


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	var scaleMatrix=mat4.create();
	//scaleMatrix=mat4.scale(2.0,2.0,2.0);
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	//mat4.multiply(modelViewMatrix,scaleMatrix,modelViewMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_apex);
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafbquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2,0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pi);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pa);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex2, 0.0, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pf);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, 0.05, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, 0.05, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, 0.05, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);


//================================================================================================================
	
	if(flagp3==1)
	{
	//planex3=1.0;
	//planey3=0.0;
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	var scaleMatrix=mat4.create();
	//scaleMatrix=mat4.scale(2.0,2.0,2.0);
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	//mat4.multiply(modelViewMatrix,scaleMatrix,modelViewMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_apex);
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafbquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3,planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pi);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pa);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex3, planey3, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pf);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey3, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey3, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey3, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

	//================================================================================================

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	var scaleMatrix=mat4.create();
	//scaleMatrix=mat4.scale(2.0,2.0,2.0);
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	//mat4.multiply(modelViewMatrix,scaleMatrix,modelViewMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_apex);
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);


	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_iafbquad);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4,planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_biggerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_smallerwings2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pi);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pa);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[planex4, planey4, -3.0]);
	mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,0.5,0.5]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_pf);
	gl.drawArrays(gl.LINES,0,2);
	gl.drawArrays(gl.LINES,2,2);
	gl.drawArrays(gl.LINES,4,2);
	gl.bindVertexArray(null);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey4, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey4, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[linex1, planey4, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);


	}
	}
	

	if(translatei1<=-0.6)
	{
		translatei1=translatei1+0.02;
	}

	if(translatea>=0.6)
	{
		translatea=translatea-0.02;
	}
	if(translaten>=0.0)
	{
		translaten=translaten-0.02;
	}
	if(translatei2<=0.0)
	{
		translatei2=translatei2+0.02;
		//console.log(translatei2);
		if(translatei2==0.019999999999928676)
		flagd=1;

	}
	
	if(flagp==1)
	{
	
	if(planex<=0.0)
	{
		planex=planex+0.01;
		if(planex==0.009999999999959009)
		{
		planez=5.0;
		}
	}
	if(planey<=0.0)
	{
		planey=planey+0.01;
	}

	if(planex1<=0.0)
	{
		planex1=planex1+0.01;

		//console.log(planex1);
		if(planex1==0.009999999999959009)
		planez1=5.0;
		
	}
	if(planey1>=0.0)
	{
		planey1=planey1-0.01;
	}

		
	if(planex2<=3.0)
	{
		var fs;
		planex2=planex2+0.01;
		linex1=linex1+0.01;
		console.log(planex2);
		if(planex2==0.9999999999999597)
		flagp3=1;
		if(planex2==2.0499999999999594)
		flag4=1;
		
	}
	
	if(flagp3==1)
	{
		if(planex3<=3.0)
			planex3=planex3+0.01;
		if(planey3<=2.0)
			planey3=planey3+0.01;
		if(planex4<=3.0)
			planex4=planex4+0.01;
		if(planey4<=2.0)
			planey4=planey4-0.01;
	}
		
   	}
	
	
	gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
	
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}
		
	if(vbo)
	{	
		gl.deleteBuffer(vbo);
		vbo=null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{ 
	
} 