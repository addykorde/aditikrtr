var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var light_ambient_red=[0.0,0.0,0.0];
var light_diffuse_red=[1.0,0.0,0.0];
var light_specular_red=[1.0,0.0,0.0];
//var light_position_red=[0.0,0.0,0.0,1.0];


var light_position_red=[];
light_position_red[0]=0.0;
light_position_red[1]=0.0;
light_position_red[2]=0.0;
light_position_red[3]=0.0;

var light_ambient_green=[0.0,0.0,0.0];
var light_diffuse_green=[0.0,1.0,0.0];
var light_specular_green=[0.0,1.0,0.0];
var light_position_green=[0.0,0.0,0.0,1.0];

var light_ambient_blue=[0.0,0.0,0.0];
var light_diffuse_blue=[0.0,0.0,1.0];
var light_specular_blue=[0.0,0.0,1.0];
var light_position_blue=[0.0,0.0,0.0,1.0];


var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var material_shininess=50.0;
var sphere=null;

var perspectiveProjectionMatrix;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform_red,ldUniform_red,lsUniform_red,lightPositionUniform_red;
var laUniform_blue,ldUniform_blue,lsUniform_blue,lightPositionUniform_blue;
var laUniform_green,ldUniform_green,lsUniform_green,lightPositionUniform_green;
var modelMatrixUniform1, viewMatrixUniform1, projectionMatrixUniform1;
var laUniform1_red,ldUniform1_red,lsUniform1_red,lightPositionUniform1_red;
var laUniform1_blue,ldUniform1_blue,lsUniform1_blue,lightPositionUniform1_blue;
var laUniform1_green,ldUniform1_green,lsUniform1_green,lightPositionUniform1_green;
var kaUniform,kdUniform,ksUniform,materialShininessUniform;
var LKeyPressedUniform;
var LKeyPressedUniform1;
var LightAnglezero=0.0,LightAngleone=0.0,LightAngletwo=0.0;
var bLKeyPressed=false;

var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;

var vkey=0;
function main()
{
	canvas=document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas failed");
	else
		console.log("Obtaining Canvas Succeeded");
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	init();
	resize();
	draw();
}

function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}

function init()
{
	gl=canvas.getContext("webgl2");
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision mediump int;"+
	"in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"uniform mat4 u_model_matrix;"+
	"uniform mat4 u_view_matrix;"+
	"uniform mat4 u_projection_matrix;"+
	"uniform mediump int u_LKeyPressed;"+
	"uniform vec4 u_light_position_red;"+
	"uniform vec4 u_light_position_blue;"+
	"uniform vec4 u_light_position_green;"+
	"out vec3 transformed_normals;"+
	"out vec3 light_direction_red;"+
	"out vec3 light_direction_blue;"+
	"out vec3 light_direction_green;"+
	"out vec3 viewerVector;"+
	"out vec3 reflectionVector_red;"+
	"out vec3 reflectionVector_blue;"+
	"out vec3 reflectionVector_green;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;"+
	"if(u_LKeyPressed==1)"+
	"{"+
	"vec4 eye_coordinates=u_view_matrix*u_model_matrix*vPosition;"+
	"transformed_normals=mat3(u_view_matrix*u_model_matrix)*vNormal;"+
	"light_direction_red=vec3(u_light_position_red)-eye_coordinates.xyz;"+
	"light_direction_green=vec3(u_light_position_green)-eye_coordinates.xyz;"+
	"light_direction_blue=vec3(u_light_position_blue)-eye_coordinates.xyz;"+
	"reflectionVector_red=reflect(-light_direction_red,transformed_normals);"+
	"reflectionVector_blue=reflect(-light_direction_blue,transformed_normals);"+
	"reflectionVector_green=reflect(-light_direction_green,transformed_normals);"+
	"viewerVector=vec3(-eye_coordinates);"+
	"}"+
	
	"}";
	
	
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error+"In Vertex Shader");
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+	
	"in vec3 transformed_normals;"+
	"in vec3 light_direction_red;"+
	"in vec3 light_direction_green;"+
	"in vec3 light_direction_blue;"+
	"in vec3 viewerVector;"+
	"out vec4 FragColor;"+
	"uniform vec3 u_La_red;"+
	"uniform vec3 u_La_green;"+
	"uniform vec3 u_La_blue;"+
	"uniform vec3 u_Ld_red;"+
	"uniform vec3 u_Ld_green;"+
	"uniform vec3 u_Ld_blue;"+
	"uniform vec3 u_Ls_red;"+
	"uniform vec3 u_Ls_blue;"+
	"uniform vec3 u_Ls_green;"+
	"uniform vec3 u_Ka;"+
	"uniform vec3 u_Kd;"+
	"uniform vec3 u_Ks;"+
	"uniform float u_material_shininess;"+
	"in vec3 reflectionVector_red;"+
	"in vec3 reflectionVector_blue;"+
	"in vec3 reflectionVector_green;"+
	"uniform int u_LKeyPressed;"+
	"vec3 phong_ads_color1;"+
	"vec3 phong_ads_color2;"+
	"vec3 phong_ads_color3;"+
	"vec3 phong_ads_color;"+
	"void main(void)"+
	"{"+
	"if(u_LKeyPressed==1)"+
	"{"+
	"vec3 normalized_transformed_normals=normalize(transformed_normals);"+
	"vec3 normalized_light_direction_red=normalize(light_direction_red);"+
	"vec3 normalized_light_direction_green=normalize(light_direction_green);"+
	"vec3 normalized_light_direction_blue=normalize(light_direction_blue);"+
	"vec3 normalized_viewer_vector=normalize(viewerVector);"+
	"vec3 reflectionVector_normalize_red=normalize(reflectionVector_red);"+
	"vec3 reflectionVector_normalize_green=normalize(reflectionVector_green);"+
	"vec3 reflectionVector_normalize_blue=normalize(reflectionVector_blue);"+
	"vec3 ambient1=u_La_red*u_Ka;"+
	"vec3 ambient2=u_La_green*u_Ka;"+
	"vec3 ambient3=u_La_blue*u_Ka;"+
	"float tn_dot_ld_red=max(dot(normalized_transformed_normals,normalized_light_direction_red),0.0);"+
	"float tn_dot_ld_green=max(dot(normalized_transformed_normals,normalized_light_direction_green),0.0);"+
	"float tn_dot_ld_blue=max(dot(normalized_transformed_normals,normalized_light_direction_blue),0.0);"+
	"vec3 diffuse1=u_Ld_red*u_Kd*tn_dot_ld_red;"+
	"vec3 diffuse2=u_Ld_green*u_Kd*tn_dot_ld_green;"+
	"vec3 diffuse3=u_Ld_blue*u_Kd*tn_dot_ld_blue;"+
	"vec3 specular1=u_Ls_red*u_Ks*pow(max(dot(reflectionVector_normalize_red,normalized_viewer_vector),0.0),u_material_shininess);"+
	"vec3 specular2=u_Ls_green*u_Ks*pow(max(dot(reflectionVector_normalize_green,normalized_viewer_vector),0.0),u_material_shininess);"+
	"vec3 specular3=u_Ls_blue*u_Ks*pow(max(dot(reflectionVector_normalize_blue,normalized_viewer_vector),0.0),u_material_shininess);"+
	"phong_ads_color1=ambient1+diffuse1+specular1;"+
	"phong_ads_color2=ambient2+diffuse2+specular2;"+
	"phong_ads_color3=ambient3+diffuse3+specular3;"+
	"phong_ads_color=phong_ads_color1+phong_ads_color2+phong_ads_color3;"+
	"}"+
	"else"+
	"{"+
	"phong_ads_color=vec3(1.0,1.0,1.0);"+
	"}"+
	"FragColor=vec4(phong_ads_color,1.0);"+
	"}"
	
	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error+"In Fragment Shader");
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	modelMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
	viewMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
	projectionMatrixUniform=gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LKeyPressedUniform=gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
	laUniform_red=gl.getUniformLocation(shaderProgramObject,"u_La_red");
	ldUniform_red=gl.getUniformLocation(shaderProgramObject,"u_Ld_red");
	lsUniform_red=gl.getUniformLocation(shaderProgramObject,"u_Ls_red");
	laUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_La_blue");
	ldUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_Ld_blue");
	lsUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_Ls_blue");
	laUniform_green=gl.getUniformLocation(shaderProgramObject,"u_La_green");
	ldUniform_green=gl.getUniformLocation(shaderProgramObject,"u_Ld_green");
	lsUniform_green=gl.getUniformLocation(shaderProgramObject,"u_Ls_green");
	lightPositionUniform_red=gl.getUniformLocation(shaderProgramObject,"u_light_position_red");
	lightPositionUniform_blue=gl.getUniformLocation(shaderProgramObject,"u_light_position_blue");
	lightPositionUniform_green=gl.getUniformLocation(shaderProgramObject,"u_light_position_green");
	kaUniform=gl.getUniformLocation(shaderProgramObject,"u_Ka");
	kdUniform=gl.getUniformLocation(shaderProgramObject,"u_Kd");
	ksUniform=gl.getUniformLocation(shaderProgramObject,"u_Ks");
	materialShininessUniform=gl.getUniformLocation(shaderProgramObject,"u_material_shininess");
	
	//
	var vertexShaderSourceCode1=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec3 vNormal;"+
	"uniform mat4 u_model_matrix;"+
	"uniform mat4 u_view_matrix;"+
	"uniform mat4 u_projection_matrix;"+
	"uniform mediump int u_LKeyPressed;"+
	"uniform vec3 u_La_red;"+
	"uniform vec3 u_Ld_red;"+
	"uniform vec3 u_Ls_red;"+
	"uniform vec3 u_La_blue;"+
	"uniform vec3 u_Ld_blue;"+
	"uniform vec3 u_Ls_blue;"+
	"uniform vec3 u_La_green;"+
	"uniform vec3 u_Ld_green;"+
	"uniform vec3 u_Ls_green;"+
	"uniform vec4 u_light_position_red;"+
	"uniform vec4 u_light_position_blue;"+
	"uniform vec4 u_light_position_green;"+
	"uniform vec3 u_Ka;"+
	"uniform vec3 u_Kd;"+
	"uniform vec3 u_Ks;"+
	"uniform float u_material_shininess;"+
	"out vec3 phong_ads_color1;"+
	"out vec3 phong_ads_color2;"+
	"out vec3 phong_ads_color3;"+
	"out vec3 finalColor;"+
	"void main(void)"+
	"{"+
	"if(u_LKeyPressed==1)"+
	"{"+
	"vec4 eye_coordinates=u_view_matrix*u_model_matrix*vPosition;"+
	"vec3 transformed_normals=normalize(mat3(u_view_matrix*u_model_matrix)*vNormal);"+
	"vec3 light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);"+
	"vec3 light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);"+
	"vec3 light_direction_green=normalize(vec3(u_light_position_green)-eye_coordinates.xyz);"+
	"float tn_dot_ld_red=max(dot(transformed_normals,light_direction_red),0.0);"+
	"float tn_dot_ld_blue=max(dot(transformed_normals,light_direction_blue),0.0);"+
	"float tn_dot_ld_green=max(dot(transformed_normals,light_direction_green),0.0);"+
	"vec3 ambient_red=u_La_red*u_Ka;"+
	"vec3 ambient_blue=u_La_blue*u_Ka;"+
	"vec3 ambient_green=u_La_green*u_Ka;"+
	"vec3 diffuse_red=u_Ld_red*u_Kd*tn_dot_ld_red;"+
	"vec3 diffuse_blue=u_Ld_blue*u_Kd*tn_dot_ld_blue;"+
	"vec3 diffuse_green=u_Ld_green*u_Kd*tn_dot_ld_green;"+
	"vec3 reflection_vector_red=reflect(-light_direction_red,transformed_normals);"+
	"vec3 reflection_vector_blue=reflect(-light_direction_blue,transformed_normals);"+
	"vec3 reflection_vector_green=reflect(-light_direction_green,transformed_normals);"+
	"vec3 viewer_vector=normalize(-eye_coordinates.xyz);"+
	"vec3 specular_red=u_Ls_red*u_Ks*pow(max(dot(reflection_vector_red,viewer_vector),0.0),u_material_shininess);"+
	"vec3 specular_blue=u_Ls_blue*u_Ks*pow(max(dot(reflection_vector_blue,viewer_vector),0.0),u_material_shininess);"+
	"vec3 specular_green=u_Ls_green*u_Ks*pow(max(dot(reflection_vector_green,viewer_vector),0.0),u_material_shininess);"+
	"phong_ads_color1=ambient_red+diffuse_red+specular_red;"+
	"phong_ads_color2=ambient_green+diffuse_green+specular_green;"+
	"phong_ads_color3=ambient_blue+diffuse_blue+specular_blue;"+
	"finalColor=phong_ads_color1+phong_ads_color2+phong_ads_color3;"+
	"}"+
	"else"+
	"{"+
	"finalColor=vec3(1.0,1.0,1.0);"+
	"}"+
	"gl_Position=u_projection_matrix*u_view_matrix*u_model_matrix*vPosition;"+
	"}";
	
	
	
	vertexShaderObject1=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject1,vertexShaderSourceCode1);
	gl.compileShader(vertexShaderObject1);
	if(gl.getShaderParameter(vertexShaderObject1,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject1);
		if(error.length > 0)
		{
			alert(error+"In Vertex Shader(PV)");
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode1=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec3 finalColor;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor=vec4(finalColor,1.0);"+
	"}";
	
	fragmentShaderObject1=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject1,fragmentShaderSourceCode1);
	gl.compileShader(fragmentShaderObject1);
	if(gl.getShaderParameter(fragmentShaderObject1,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject1);
		if(error.length > 0)
		{
			alert(error+"In Fragment Shader(PV)");
			uninitialize();
		}
	}
	
	shaderProgramObject1=gl.createProgram();
	gl.attachShader(shaderProgramObject1,vertexShaderObject1);
	gl.attachShader(shaderProgramObject1,fragmentShaderObject1);

	gl.bindAttribLocation(shaderProgramObject1,WebGLMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");
	gl.bindAttribLocation(shaderProgramObject1,WebGLMacros.VDG_ATTRIBUTE_NORMAL,"vNormal");
	
	gl.linkProgram(shaderProgramObject1);
	if(!gl.getProgramParameter(shaderProgramObject1,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject1);
		if(error.length>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	

	
	
	modelMatrixUniform1=gl.getUniformLocation(shaderProgramObject1,"u_model_matrix");
	viewMatrixUniform1=gl.getUniformLocation(shaderProgramObject1,"u_view_matrix");
	projectionMatrixUniform1=gl.getUniformLocation(shaderProgramObject1,"u_projection_matrix");
	LKeyPressedUniform1=gl.getUniformLocation(shaderProgramObject1,"u_LKeyPressed");
	laUniform1_red=gl.getUniformLocation(shaderProgramObject1,"u_La_red");
	laUniform1_blue=gl.getUniformLocation(shaderProgramObject1,"u_La_blue");
	laUniform1_green=gl.getUniformLocation(shaderProgramObject1,"u_La_green");
	ldUniform1_red=gl.getUniformLocation(shaderProgramObject1,"u_Ld_red");
	ldUniform1_blue=gl.getUniformLocation(shaderProgramObject1,"u_Ld_blue");
	ldUniform1_green=gl.getUniformLocation(shaderProgramObject1,"u_Ld_green");
	lsUniform1_red=gl.getUniformLocation(shaderProgramObject1,"u_Ls_red");
	lsUniform1_blue=gl.getUniformLocation(shaderProgramObject1,"u_Ls_blue");
	lsUniform1_green=gl.getUniformLocation(shaderProgramObject1,"u_Ls_green");
	lightPositionUniform1_red=gl.getUniformLocation(shaderProgramObject1,"u_light_position_red");
	lightPositionUniform1_blue=gl.getUniformLocation(shaderProgramObject1,"u_light_position_blue");
	lightPositionUniform1_green=gl.getUniformLocation(shaderProgramObject1,"u_light_position_green");
	kaUniform1=gl.getUniformLocation(shaderProgramObject1,"u_Ka");
	kdUniform1=gl.getUniformLocation(shaderProgramObject1,"u_Kd");
	ksUniform1=gl.getUniformLocation(shaderProgramObject1,"u_Ks");
	materialShininessUniform1=gl.getUniformLocation(shaderProgramObject1,"u_material_shininess");



	//
	
	sphere=new Mesh();
	makeSphere(sphere,2.0,30,30);
	
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	gl.enable(gl.CULL_FACE);
	gl.clearColor(0.0,0.0,0.0,1.0);
	perspectiveProjectionMatrix=mat4.create();
	
}

function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
	if(vkey==1)
	gl.useProgram(shaderProgramObject);
	else
	gl.useProgram(shaderProgramObject1);
	
	if(bLKeyPressed==true)
	{
		gl.uniform1i(LKeyPressedUniform,1);
		
		gl.uniform3fv(laUniform_red,light_ambient_red);
		gl.uniform3fv(ldUniform_red,light_diffuse_red);
		gl.uniform3fv(lsUniform_red,light_specular_red);
		gl.uniform4fv(lightPositionUniform_red,light_position_red);
		
		gl.uniform3fv(laUniform_blue,light_ambient_blue);
		gl.uniform3fv(ldUniform_blue,light_diffuse_blue);
		gl.uniform3fv(lsUniform_blue,light_specular_blue);
		gl.uniform4fv(lightPositionUniform_blue,light_position_blue);
		
		gl.uniform3fv(laUniform_green,light_ambient_green);
		gl.uniform3fv(ldUniform_green,light_diffuse_green);
		gl.uniform3fv(lsUniform_green,light_specular_green);
		gl.uniform4fv(lightPositionUniform_green,light_position_green);
		
		gl.uniform3fv(kaUniform,material_ambient);
		gl.uniform3fv(kdUniform,material_diffuse);
		gl.uniform3fv(ksUniform,material_specular);
		gl.uniform1f(materialShininessUniform,material_shininess);
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform,0);
	}
	
	var modelMatrix=mat4.create();
	var viewMatrix=mat4.create();
	
	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);
	gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
	gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
	gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);
	



		if(bLKeyPressed==true)
	{
		gl.uniform1i(LKeyPressedUniform1,1);
		
		gl.uniform3fv(laUniform1_red,light_ambient_red);
		gl.uniform3fv(ldUniform1_red,light_diffuse_red);
		gl.uniform3fv(lsUniform1_red,light_specular_red);
		//light_position_red[1]={LightAnglezero};
		//console.log("L Key is pressed");
		//mat4.rotateY(modelMatrix1,modelMatrix1,degToRad(LightAnglezero));
		gl.uniform4fv(lightPositionUniform1_red,light_position_red);
		
		gl.uniform3fv(laUniform1_blue,light_ambient_blue);
		gl.uniform3fv(ldUniform1_blue,light_diffuse_blue);
		gl.uniform3fv(lsUniform1_blue,light_specular_blue);
		gl.uniform4fv(lightPositionUniform1_blue,light_position_blue);
		
		gl.uniform3fv(laUniform1_green,light_ambient_green);
		gl.uniform3fv(ldUniform1_green,light_diffuse_green);
		gl.uniform3fv(lsUniform1_green,light_specular_green);
		gl.uniform4fv(lightPositionUniform1_green,light_position_green);
		
		
		gl.uniform3fv(kaUniform1,material_ambient);
		gl.uniform3fv(kdUniform1,material_diffuse);
		gl.uniform3fv(ksUniform1,material_specular);
		gl.uniform1f(materialShininessUniform1,material_shininess);
		
		gl.uniform1i(LKeyPressedUniform1,1);
		
		
		
	}
	else
	{
		gl.uniform1i(LKeyPressedUniform1,0);
		
	}
	

	var modelMatrix1=mat4.create();
	var viewMatrix1=mat4.create();
	mat4.translate(modelMatrix1,modelMatrix1,[0.0,0.0,-6.0]);
	gl.uniformMatrix4fv(modelMatrixUniform1,false,modelMatrix1);
	gl.uniformMatrix4fv(viewMatrixUniform1,false,viewMatrix1);
	gl.uniformMatrix4fv(projectionMatrixUniform1,false,perspectiveProjectionMatrix);
	






	sphere.draw();
	gl.useProgram(null);


	LightAnglezero = LightAnglezero + 0.01;
	LightAngleone = LightAngleone + 0.01;
	LightAngletwo = LightAngletwo + 0.01;
	if (LightAnglezero >= 2 * Math.PI)
	{
		LightAnglezero = 0.0;

	}
	light_position_red[1] = 100.0*Math.sin(LightAnglezero);
	light_position_red[2] = 100.0*Math.cos(LightAnglezero);

	if (LightAngleone >= 2 * Math.PI)
	{
		LightAngleone = 0.0;
	}
	light_position_blue[0] = 100.0*Math.sin(LightAngleone);
	light_position_blue[2] = 100.0*Math.cos(LightAngleone);
	if (LightAngletwo >= 2 * Math.PI)
	{
		LightAngletwo = 0.0;
	}
	light_position_green[0] = 100.0*Math.sin(LightAngleone);
	light_position_green[1] = 100.0*Math.cos(LightAngleone);

	requestAnimationFrame(draw,canvas);
	
}


function uninitialize()
{
	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}
		
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 76:
		if(bLKeyPressed==false)
			bLKeyPressed=true;
		else
			bLKeyPressed=false;
		break;
		case 118:
		case 86:
		vkey=0;
		break;
		
		case 127:
		toggleFullScreen();
		break;
		
		case 102:
		case 70:
		vkey=1;
		break;
	}
}

function mouseDown(event)
{
	//code
} 

function degToRad(degrees)
{
	return(degrees*Math.PI/180);
}

