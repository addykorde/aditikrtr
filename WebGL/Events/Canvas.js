var canvas=null;
var context=null;

function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	console.log("Canvas Width :"+canvas.width+" And Canvas Height:"+canvas.height);
	
	var context=canvas.getContext("2d");
	
	if(!context)
		console.log("Obtaining context Failed");
	else
		console.log("Obtaining context Succeeded");
	
	context.fillStyle="black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	context.textAlign="center";
	context.textBaseline="middle";
	
	var str="Hello World !!!";

	context.font="48px sans-serif";
	
	context.fillStyle="white";
	
	context.fillText(str,canvas.width/2,canvas.height/2);
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	
}

function keyDown(event)
{
	alert("A key is pressed");
}

function mouseDown(event)
{
	alert("A mouse is pressed");
}