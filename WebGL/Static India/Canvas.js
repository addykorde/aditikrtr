var canvas=null;
var gl=null;
var bFullScreen=false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros=
{
AMC_ATTRIBUTE_POSITION:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXCOORD0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_i1;
var vao_i2;
var vao_n;
var vao_d;
var vao_a;
var vao_line1;
var vao_line2;
var vao_line3;
var vbo_position_i1;
var vbo_position_i2;
var vbo_position_n;
var vbo_position_d;
var vbo_position_a;
var vbo_position_line1;
var vbo_position_line2;
var vbo_position_line3;
var vbo_color_i1;
var vbo_color_i2;
var vbo_color_n;
var vbo_color_d;
var vbo_color_a;
var vbo_color_line1;
var vbo_color_line2;
var vbo_color_line3;
var mvpUniform;

var perspectiveProjectionMatrix;


var requestAnimationFrame=
window.requestAnimationFrame||
window.webkitRequestAnimationFrame||
window.mozRequestAnimationFrame||
window.oRequestAnimationFrame||
window.msRequestAnimationFrame;

var cancelAnimationFrame=
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame||window.webkitCancelAnimationFrame||
window.mozCancelRequestAnimationFrame||window.mozCancelAnimationFrame||
window.oCancelRequestAnimationFrame||window.oCancelAnimationFrame||
window.msCancelRequestAnimationFrame||window.msCancelAnimationFrame;




function main()
{
	canvas =document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed");
	else
		console.log("Obtaining Canvas Succeeded");
	
	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;
	
	
	
	window.addEventListener("keydown",keyDown,false);
	window.addEventListener("click",mouseDown,false);
	window.addEventListener("resize",resize,false);
	
	
	init();
	resize();
	draw();
	
}


function toggleFullScreen()
{
	var fullscreen_element=
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullscreenElement ||
	null;
	
	if(fullscreen_element==null)
	{
		if(canvas.requestFullScreen)
			canvas.requestFullScreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullScreen)
			canvas.webkitRequestFullScreen();
		else if(canvas.msrequestFullScreen)
			canvas.msrequestFullScreen();
		bFullScreen=true;
	}
	
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancleFullScreen)
			document.mozCancleFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen=false;
		
	}
}


function init()
{
	gl=canvas.getContext("webgl2");
	
	if(gl==null)
	{
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.viewportWidth=canvas.width;
	gl.viewportHeight=canvas.height;
	
	var vertexShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec4 vColor;"+
	"out vec4 out_color;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position=u_mvp_matrix*vPosition;"+
	"out_color=vColor;"+
	"}";
	vertexShaderObject=gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	var fragmentShaderSourceCode=
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"out vec4 FragColor;"+
	"in vec4 out_color;"+
	"void main(void)"+
	"{"+
	"FragColor=out_color;"+
	"}";

	fragmentShaderObject=gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error=gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	shaderProgramObject=gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");
	
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error=gl.getProgramInfoLog(shaderProgramObject);
		if(error.lenght>0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	mvpUniform=gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	
	
	var i1=new Float32Array([
						-0.70, 0.80, 0.0,

						- 0.72, 0.80, 0.0,

						- 0.72,-0.80, 0.0,

						- 0.70, -0.80, 0.0,]);
	var i2=new Float32Array([
						0.20, 0.80, 0.0,

						0.22, 0.80, 0.0,

						0.22, -0.80, 0.0,

						0.20, -0.80, 0.0]);	
	var n=new Float32Array([
						-0.52, 0.80, 0.0,

	- 0.50, 0.80, 0.0,

	- 0.50, -0.80, 0.0,

	- 0.52, -0.80, 0.0,


	- 0.50, 0.80, 0.0,

	- 0.48, 0.80, 0.0,

	- 0.36, -0.80, 0.0,
		
	- 0.38, -0.80, 0.0,
 

	- 0.38, 0.80, 0.0,

	- 0.36, 0.80, 0.0,

	- 0.36, -0.80, 0.0,

	- 0.38, -0.80, 0.0]);
	var d=new Float32Array([
						-0.16, 0.78, 0.0,

	- 0.14, 0.78, 0.0,

	- 0.14, -0.78, 0.0,

	- 0.16, -0.78, 0.0,



	-0.16, 0.78, 0.0,

	- 0.16, 0.80, 0.0,

	0.0, 0.80, 0.0,

	0.0, 0.78, 0.0,


	- 0.16, -0.80, 0.0,

	- 0.16, -0.78, 0.0,

	0.0, -0.78, 0.0,

	0.0, -0.80, 0.0,



	-0.02, 0.78, 0.0,

	0.0, 0.78, 0.0,

	0.0, -0.78, 0.0,

	- 0.02, -0.78, 0.0]);
	var a=new Float32Array([
	0.40, -0.80, 0.0,

	0.42, -0.80, 0.0,

	0.52, 0.80, 0.0,

	0.50, 0.80, 0.0,


	0.72, -0.80, 0.0,

	0.74, -0.80, 0.0,

	0.54, 0.80, 0.0,

	0.52, 0.80, 0.0]);
						
	var i1Color=new Float32Array([
								1.0, 0.647058823529, 0.0,

								1.0, 0.647058823529, 0.0,

								0.0705882352911, 0.5333333, 0.0,

								0.0705882352911, 0.5333333, 0.0 ]);
	var i2Color=new Float32Array([
								1.0, 0.647058823529, 0.0,

								1.0, 0.647058823529, 0.0,

								0.0705882352911, 0.5333333, 0.0,

								0.0705882352911, 0.5333333, 0.0]);
	var nColor=new Float32Array([
								1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,
	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0 ,
	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0]);

	var dColor=new Float32Array([
								1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,



	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,


	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,



	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0]);
	var aColor=new Float32Array([
								0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0,


	0.0705882352911, 0.5333333, 0.0,

	0.0705882352911, 0.5333333, 0.0,

	1.0, 0.647058823529, 0.0,

	1.0, 0.647058823529, 0.0]);
	
	
	
	
	var line1Vertices=new Float32Array([
								0.50,0.0,0.0,
								0.62,0.0,0.0]);
	var line2Vertices=new Float32Array([
								0.50, -0.04, 0.0,
								0.63, -0.04, 0.0]);
	var line3Vertices=new Float32Array([
								0.50,-0.08,0.0,
								0.64, -0.08, 0.0]);
								
	var line1Color=new Float32Array([
		1.0, 0.647058823529, 0.0,
		1.0, 0.647058823529, 0.0
								]);
	
	var line2Color=new Float32Array([
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
								]);
								
	var line3Color=new Float32Array([
		0.0705882352911, 0.5333333, 0.0,
		0.0705882352911, 0.5333333, 0.0
								]);
	
	
	vao_i1=gl.createVertexArray();
	gl.bindVertexArray(vao_i1);
	
	vbo_i1_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i1_position);
	gl.bufferData(gl.ARRAY_BUFFER,i1,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_i1_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i1_color);
	gl.bufferData(gl.ARRAY_BUFFER,i1Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	vao_i2=gl.createVertexArray();
	gl.bindVertexArray(vao_i2);
	
	vbo_i2_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i2_position);
	gl.bufferData(gl.ARRAY_BUFFER,i2,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_i2_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_i2_color);
	gl.bufferData(gl.ARRAY_BUFFER,i2Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	
	vao_n=gl.createVertexArray();
	gl.bindVertexArray(vao_n);
	
	vbo_n_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_n_position);
	gl.bufferData(gl.ARRAY_BUFFER,n,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_n_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_n_color);
	gl.bufferData(gl.ARRAY_BUFFER,nColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	vao_d=gl.createVertexArray();
	gl.bindVertexArray(vao_d);
	
	vbo_d_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_d_position);
	gl.bufferData(gl.ARRAY_BUFFER,d,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_d_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_d_color);
	gl.bufferData(gl.ARRAY_BUFFER,dColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	vao_a=gl.createVertexArray();
	gl.bindVertexArray(vao_a);
	
	vbo_a_position=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_a_position);
	gl.bufferData(gl.ARRAY_BUFFER,a,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_a_color=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_a_color);
	gl.bufferData(gl.ARRAY_BUFFER,aColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	
	
	vao_line1=gl.createVertexArray();
	gl.bindVertexArray(vao_line1);
	
	vbo_position_line1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line1);
	gl.bufferData(gl.ARRAY_BUFFER,line1Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_line1=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line1);
	gl.bufferData(gl.ARRAY_BUFFER,line1Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	vao_line2=gl.createVertexArray();
	gl.bindVertexArray(vao_line2);
	vbo_position_line2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line2);
	gl.bufferData(gl.ARRAY_BUFFER,line2Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_line2=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line2);
	gl.bufferData(gl.ARRAY_BUFFER,line2Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	
	
	
	vao_line3=gl.createVertexArray();
	gl.bindVertexArray(vao_line3);
	vbo_position_line3=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_line3);
	gl.bufferData(gl.ARRAY_BUFFER,line3Vertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	vbo_color_line3=gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_line3);
	gl.bufferData(gl.ARRAY_BUFFER,line3Color,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	perspectiveProjectionMatrix=mat4.create();
	
	gl.clearColor(0.0,0.0,0.0,1.0);
}


function resize()
{
	if(bFullScreen==true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}
	
	gl.viewport(0,0,canvas.width,canvas.height);
	
	
		mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);
	
	
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[-0.6, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_i1);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.3, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_i2);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.bindVertexArray(null);
	
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[-0.3, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_n);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_d);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.6, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_a);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.bindVertexArray(null);
	
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.2, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line1);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.2, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line2);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	var modelViewMatrix=mat4.create();
	var modelViewProjectionMatrix=mat4.create();
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.2, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);
	gl.bindVertexArray(vao_line3);
	gl.drawArrays(gl.LINES,0,2);
	gl.bindVertexArray(null);
	
	
	gl.useProgram(null);
	requestAnimationFrame(draw,canvas);
	
}

function uninitialize()
{
	if(vao)
	{
		gl.deleteVertexArray(vao);
		vao=null;
	}
		
	if(vbo)
	{	
		gl.deleteBuffer(vbo);
		vbo=null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject=null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject=null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject=null;
	}

}	


function keyDown(event)
{
	switch(event.keyCode)
	{
		case 27:uninitialize();
		window.close();
		break;
		
		case 70:
		toggleFullScreen();
		break;
	}
}

function mouseDown(event)
{
	
} 