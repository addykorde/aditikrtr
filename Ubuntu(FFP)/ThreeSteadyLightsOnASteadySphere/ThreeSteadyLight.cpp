#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glx.h>
#include<GL/gl.h>
#include<GL/glu.h>



using namespace std;


//Global variables.

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGLXContext;

bool blight = false;

struct Lights{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
};

Lights lights[3];

GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
GLfloat MaterialShininess[] = { 128.0f };

GLUquadric *quadric = NULL;
GLfloat LightAnglezero = 0.0f, LightAngleone = 0.0f, LightAngletwo = 0.0f;
int main(void)
{
	
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();
	
	//Opengl functions prototype declarations
	void initialize(void);
	void resize(int,int);
	void display(void);
	void Update(void);
	//Local variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	bool bdone=false;
	
	
	
	
	CreateWindow();
	initialize();
	
	XEvent event;
	KeySym keysym;
	
	
		while(bdone==false)
		{
		while(XPending(gpDisplay))
		{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
			break;
			case KeyPress:
			keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
			switch(keysym)
			{
				case XK_Escape:
				bdone=true;
				break;
				
				case XK_F:
				case XK_f:
				if(bFullscreen==false)
				{
					ToggleFullscreen();
					bFullscreen=true;
				}
				else{
					ToggleFullscreen();
					bFullscreen=false;
				}
				break;
				case XK_l:
				case XK_L:
				if (blight == false)
				{
				blight = true;
				glEnable(GL_LIGHTING);
				}
				else
				{
				blight = false;
				glDisable(GL_LIGHTING);
				}
				break;
				default:
				break;
			}
			break;
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
				default:
				break;
			}
			break;
			case MotionNotify:
			break;
			case ConfigureNotify:
			winWidth=event.xconfigure.width;
			winHeight=event.xconfigure.height;
			resize(winWidth,winHeight);
			break;
			case Expose:
			break;
			case DestroyNotify:
			break;
			case 33:
			bdone=true;
			break;
			default:
			break;
		}
		}
			Update();
			display();
		}
		uninitialize();
		return(0);
}

void CreateWindow()
{
	
	void uninitialize(void);
	
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[]={GLX_RGBA,
					GLX_DOUBLEBUFFER,True,
					//GLX_WINDOW,
					GLX_RED_SIZE,8,
					GLX_GREEN_SIZE,8,
					GLX_BLUE_SIZE,8,
					GLX_ALPHA_SIZE,8,
					GLX_DEPTH_SIZE,24,
					None}; //None=0
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	defaultDepth=DefaultDepth(gpDisplay,defaultScreen);
	
	
	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	
	if(gpXVisualInfo==NULL)
	{
		printf("ERROR : Unable to Open gpXVisualInfo.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
						RootWindow(gpDisplay,gpXVisualInfo->screen),
						gpXVisualInfo->visual,
						AllocNone);
	gColormap=winAttribs.colormap;
	
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|
		StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
						  RootWindow(gpDisplay,gpXVisualInfo->screen),
						  0,
						  0,
						  giWindowWidth,
						  giWindowHeight,
						  0,
						  gpXVisualInfo->depth,
						  InputOutput,
						  gpXVisualInfo->visual,
						  styleMask,
						  &winAttribs);
	if(!gWindow)
	{
		printf("ERROR :Failed to create main window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"Sphere with Single WHite Light");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void initialize(void)
{
	void resize(int, int);
	
	lights[0].Ambient[0] = { 0.0f };
	lights[0].Ambient[1] = { 0.0f };
	lights[0].Ambient[2] = { 0.0f };
	lights[0].Ambient[3] = { 1.0f };

	lights[0].Diffuse[0] = { 1.0f };
	lights[0].Diffuse[1] = { 0.0f };
	lights[0].Diffuse[2] = { 0.0f };
	lights[0].Diffuse[3] = { 1.0f };

	lights[0].Specular[0] = { 1.0f };
	lights[0].Specular[1] = { 0.0f };
	lights[0].Specular[2] = { 0.0f };
	lights[0].Specular[3] = { 0.0f };

	lights[0].Position[0] = { 0.0f };
	lights[0].Position[1] = { 0.0f };
	lights[0].Position[2] = { 0.0f };
	lights[0].Position[3] = { 0.0f };


	lights[1].Ambient[0] = { 0.0f };
	lights[1].Ambient[1] = { 0.0f };
	lights[1].Ambient[2] = { 0.0f };
	lights[1].Ambient[3] = { 1.0f };

	lights[1].Diffuse[0] = { 0.0f };
	lights[1].Diffuse[1] = { 1.0f };
	lights[1].Diffuse[2] = { 0.0f };
	lights[1].Diffuse[3] = { 1.0f };

	lights[1].Specular[0] = { 0.0f };
	lights[1].Specular[1] = { 1.0f };
	lights[1].Specular[2] = { 0.0f };
	lights[1].Specular[3] = { 1.0f };

	lights[1].Position[0] = { 0.0f };
	lights[1].Position[1] = { 0.0f };
	lights[1].Position[2] = { 0.0f };
	lights[1].Position[3] = { 1.0f };


	lights[2].Ambient[0] = { 0.0f };
	lights[2].Ambient[1] = { 0.0f };
	lights[2].Ambient[2] = { 0.0f };
	lights[2].Ambient[3] = { 1.0f };

	lights[2].Diffuse[0] = { 0.0f };
	lights[2].Diffuse[1] = { 0.0f };
	lights[2].Diffuse[2] = { 1.0f };
	lights[3].Diffuse[3] = { 1.0f };

	lights[2].Specular[0] = { 0.0f };
	lights[2].Specular[1] = { 0.0f };
	lights[2].Specular[2] = { 1.0f };
	lights[2].Specular[3] = { 1.0f };

	lights[2].Position[0] = { 0.0f };
	lights[2].Position[1] = { 0.0f };
	lights[2].Position[2] = { 0.0f };
	lights[2].Position[2] = { 1.0f };

	
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
	//Usual Opengl initialization code
	glShadeModel(GL_SMOOTH);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lights[0].Ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lights[0].Diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lights[0].Specular);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, lights[1].Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lights[1].Diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lights[1].Specular);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2, GL_AMBIENT, lights[2].Ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, lights[2].Diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, lights[2].Specular);
	glEnable(GL_LIGHT2);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
    resize(giWindowWidth,giWindowHeight);

	
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 3.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	glPushMatrix();
	glRotatef(LightAnglezero, 1.0f, 0.0f, 0.0f);
	lights[0].Position[1] = { LightAnglezero };
	glLightfv(GL_LIGHT0, GL_POSITION, lights[0].Position);
	glPopMatrix();

	glPushMatrix();
	glRotatef(LightAngleone, 0.0f, 1.0f, 0.0f);
	lights[1].Position[0] = { LightAngleone };
	glLightfv(GL_LIGHT1, GL_POSITION, lights[1].Position);
	glPopMatrix();

	glPushMatrix();
	glRotatef(LightAngletwo, 0.0f, 0.0f, 1.0f);
	lights[1].Position[0] = { LightAngletwo };
	glLightfv(GL_LIGHT2, GL_POSITION, lights[2].Position);
	glPopMatrix();


	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75f, 30.0f, 30.0f);

	glPopMatrix();
	
	glXSwapBuffers(gpDisplay,gWindow);
}


void Update()
{
	LightAnglezero = LightAnglezero + 0.05f;
	LightAngleone = LightAngleone + 0.05f;
	LightAngletwo = LightAngletwo + 0.05f;
	if (LightAnglezero >= 360.0f)
	{
		LightAnglezero = 0.0f;
	}
	if (LightAngleone >= 360.0f)
	{
		LightAngleone = 0.0f;
	}
	if (LightAngletwo >= 360.0f)
	{
		LightAngletwo = 0.0f;
	}
}


void uninitialize(void)
{
	GLXContext CurrentglxContext=glXGetCurrentContext();
	
	if(CurrentglxContext!=NULL&& CurrentglxContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
		
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}



