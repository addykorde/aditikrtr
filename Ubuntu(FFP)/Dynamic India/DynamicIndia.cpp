#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/glx.h>
#include<GL/gl.h>
#include<GL/glu.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#define RADIUS 1.0f
#define RADIUS2 1.08f

using namespace std;


//Global variables.

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGLXContext;
int flagd = 1;

GLfloat iOffseti1 = 0.0f;
GLfloat iOffseta = 0.0f;
GLfloat iOffsetn = 0.0f;
GLfloat iOffseti = 0.0f;
GLfloat iColoror = 0.0f, iColorog = 0.0f, iColorob = 0.0f;
GLfloat iColorgr = 0.0f, iColorgg = 0.0f, iColorgb = 0.0f;


int flagi = 0;
int flagpp = 0, flagf = 0;
bool gbActiveWindow = false;
bool bDone = false;
bool bIsFullScreen = false;
int iRet;


GLfloat iOffsetx = 0.0f;

GLfloat originX = -0.5f, originY = 1.0f;
GLfloat angle = 0.0f, angley = 0.0f, anglez = 0.0f, anglez1 = 0.0f;

GLfloat anglel1 = 0.0f, anglel2 = 0.0f;

GLfloat iOffsetxl1 = 0.0f;
GLfloat iOffsetxl2 = 0.0f;

GLfloat iOffsetx1 = 0.0f, iOffsety1 = 0.0f;
GLfloat iOffsetx2 = 0.0f, iOffsety2 = 0.0f, iOffsetx3 = 0.0f, iOffsetx4 = 0.0f;


int flag = 1;
int flag1 = 1, flag3 = 1, flagp = 1, flagp1 = 1, flag4 = 1;


int main(void)
{
	
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();
	
	//Opengl functions prototype declarations
	void initialize(void);
	void resize(int,int);
	void display(void);
	void Update(void);
	//Local variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	bool bdone=false;
	
	
	
	
	CreateWindow();
	initialize();
	
	XEvent event;
	KeySym keysym;
	
	
		while(bdone==false)
		{
		while(XPending(gpDisplay))
		{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
			break;
			case KeyPress:
			keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
			switch(keysym)
			{
				case XK_Escape:
				bdone=true;
				break;
				
				/*case XK_F:
				case XK_f:
				if(bFullscreen==false)
				{
					ToggleFullscreen();
					bFullscreen=true;
				}
				else{
					ToggleFullscreen();
					bFullscreen=false;
				}
				break;*/
				
				default:
				break;
			}
			break;
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
				default:
				break;
			}
			break;
			case MotionNotify:
			break;
			case ConfigureNotify:
			winWidth=event.xconfigure.width;
			winHeight=event.xconfigure.height;
			resize(winWidth,winHeight);
			break;
			case Expose:
			break;
			case DestroyNotify:
			break;
			case 33:
			bdone=true;
			break;
			default:
			break;
		}
		}
			//Update();
			display();
		}
		uninitialize();
		return(0);
}

void CreateWindow()
{
	
	void uninitialize(void);
	
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[]={GLX_RGBA,
					GLX_DOUBLEBUFFER,True,
					GLX_WINDOW,
					GLX_RED_SIZE,8,
					GLX_BLUE_SIZE,8,
					GLX_GREEN_SIZE,8,
					GLX_ALPHA_SIZE,8,
					None}; //None=0
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	defaultDepth=DefaultDepth(gpDisplay,defaultScreen);
	
	
	
	
	
	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
	
	if(gpXVisualInfo==NULL)
	{
		printf("ERROR : Unable to Open gpXVisualInfo.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
						RootWindow(gpDisplay,gpXVisualInfo->screen),
						gpXVisualInfo->visual,
						AllocNone);
	gColormap=winAttribs.colormap;
	
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|
		StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
						  RootWindow(gpDisplay,gpXVisualInfo->screen),
						  0,
						  0,
						  giWindowWidth,
						  giWindowHeight,
						  0,
						  gpXVisualInfo->depth,
						  InputOutput,
						  gpXVisualInfo->visual,
						  styleMask,
						  &winAttribs);
	if(!gWindow)
	{
		printf("ERROR :Failed to create main window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"2D-Shapes Rotations");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void initialize(void)
{
	void uninitailize();
	void ToggleFullscreen();
	void resize(int, int);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
	ToggleFullscreen();
	
	//Usual Opengl initialization code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(giWindowWidth,giWindowHeight);

	
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display()
{
	static GLfloat angle1 = 270.0f;
	static GLfloat angle2 = 270.0f;
	static GLfloat angle3 = 360.0f;
	static GLfloat angle4 = 0.0f;

	static GLfloat anglel5 = 270.0f;
	static GLfloat anglel6 = 0.0f;
	static GLfloat angle6 = 270.0f;
	static GLfloat anglel7 = 360.0f;
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();




	glLineWidth(3.0f);



	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.80f + iOffseti1, 0.0f, -3.0f);



	if (iOffseti1 <= 1.2f)
	{
		iOffseti1 = iOffseti1 + 0.0025f;


	}
	//I
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.70f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.72f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.72f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.70f, -0.80f, 0.0f);

	glEnd();





	//N

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.38f, 5.6f + iOffsetn, -3.0f);

	if (iOffsetn >= -5.6f)
	{
		iOffsetn = iOffsetn - 0.0025f;

	}


	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.52f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.50f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.50f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.52f, -0.80f, 0.0f);

	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.50f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.48f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.26f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.28f, -0.80f, 0.0f);

	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.28f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.26f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.26f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.28f, -0.80f, 0.0f);

	glEnd();



	//D

	if (flagd == 2)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.17f, 0.0f, -3.0f);

		if (iColoror <= 1.0f&&iColorog <= 0.647058823529f)
		{
			iColoror = iColoror + 0.0005;
			iColorog = iColorog + 0.0003;
		}
		else
		{
			flagpp = 2;
		}
		if (iColorgr <= 0.0705882352911f&&iColorgg <= 0.5333333f)
		{
			iColorgr = iColorgr + 0.0001;
			iColorgg = iColorgg + 0.0015;
		}
		


		glBegin(GL_QUADS);
		glColor3f(0.0f + iColoror, 0.0f + iColorog, 0.0f);
		glVertex3f(-0.16f, 0.78f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0f + iColorog, 0.0f);
		glVertex3f(-0.14f, 0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.14f, -0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.16f, -0.78f, 0.0f);




		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(-0.16f, 0.78f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(-0.16f, 0.80f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(0.14f, 0.80f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(0.14f, 0.78f, 0.0f);

		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.16f, -0.80f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.16f, -0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(0.14f, -0.78f, 0.0f);
		glColor3f(0.10f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(0.14f, -0.80f, 0.0f);


		glColor3f(0.0 + iColoror, -0.0 + iColorog, 0.0f);
		glVertex3f(0.14f, 0.78f, 0.0f);
		glColor3f(0.0f + iColoror, -0.0 + iColorog, 0.0f);
		glVertex3f(0.12f, 0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, -0.0f + iColorgg, 0.0f);
		glVertex3f(0.12f, -0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, -0.0f + iColorgg, 0.0f);
		glVertex3f(0.14f, -0.78f, 0.0f);


		glEnd();



	}


	//I

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.002f, -7.7f + iOffseti, -3.0f);

	if (iOffseti <= 7.7f)
	{
		iOffseti = iOffseti + 0.0025f;

	}
	else
	{
		flagd = 2;
	}


	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.20f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.22f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.22f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.20f, -0.80f, 0.0f);

	glEnd();



	//A

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.8f - iOffseta, 0.0f, -3.0f);

	if (iOffseta <= 3.6f)
	{
		iOffseta = iOffseta + 0.0025f;

	}

	glBegin(GL_QUADS);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.30f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.32f, -0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.52f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.50f, 0.80f, 0.0f);

	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.82f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.84f, -0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.54f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.52f, 0.80f, 0.0f);

	glEnd();

	//Three Lines for A
	if (flagf == 3)
	{
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.647058823529f, 0.0f);
		glVertex3f(0.42f, -0.02f, 0.0f);
		glVertex3f(0.68, -0.02f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.42f, 0.0f, 0.0f);
		glVertex3f(0.68f, 0.0f, 0.0f);


		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(0.42f, 0.02f, 0.0f);
		glVertex3f(0.68f, 0.02f, 0.0f);
		glEnd();
	}

	//Plane
	//-----------------------------------------------------------------------------------------------------------------------


	if (flagpp == 2)
	{


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();


		glTranslatef(-0.5f - cos(angle), 1.0f - sin(angle), -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (angle <= M_PI_2 * RADIUS)
		{
			glColor3f(0.729411f, 0.8627f, 0.933f);
			angle = angle + 0.009f;
		}
		else
		{
			glColor3f(0.0f, 0.0f, 0.0f);
			glTranslatef(0.0f, 0.0f, 10.0f);

		}

		glRotatef(angle1, 0.0f, 0.0f, 1.0f);

		if (angle1 <= 360.0f)
		{
			angle1 = angle1 + 0.7f;

			if (angle1 == 360.0f)
			{
				angle1 = 361.0f;

			}

		}






		//Apex

		glBegin(GL_TRIANGLES);
		glVertex3f(0.60f + iOffsetx, 0.0f, 0.0f);
		glVertex3f(0.50f + iOffsetx, -0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx, 0.20f, 0.0f);
		glEnd();

		//Before IAF

		glBegin(GL_QUADS);
		glVertex3f(0.50f + iOffsetx, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx, 0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx, 0.20f, 0.0f);
		glEnd();

		//One where I write IAF
		glBegin(GL_QUADS);
		glVertex3f(-0.10f + iOffsetx, 0.15f, 0.0f);
		glVertex3f(-0.10f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.65f, 0.12f, 0.0f);
		glVertex3f(-0.65f, -0.12f, 0.0f);

		glVertex3f(-0.50f, -0.12f, 0.0f);
		glVertex3f(-0.40f, 0.12f, 0.0f);

		glVertex3f(-0.40f, 0.12f, 0.0f);
		glVertex3f(-0.30f, -0.12f, 0.0f);

		glVertex3f(-0.45f, 0.02f, 0.0f);
		glVertex3f(-0.35f, 0.02f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.15f, -0.12f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.04f, 0.12f, 0.0f);

		glVertex3f(-0.15f, 0.04f, 0.0f);
		glVertex3f(-0.06f, 0.04f, 0.0f);

		glEnd();

		//BiggerWings
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx, 0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, 0.15f, 0.0f);
		glEnd();


		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, -0.15f, 0.0f);
		glEnd();


		//SmallerWings

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx, 0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, -0.15f, 0.0f);
		glEnd();


		//Heat Furnace

		glBegin(GL_LINES);
		glVertex3f(-1.25f + iOffsetx, 0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx, 0.09f, 0.0f);

		glVertex3f(-1.25f + iOffsetx, -0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx, -0.09f, 0.0f);


		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(-1.25f, 0.06f, 0.0f);
		glVertex3f(-1.50f, 0.06f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.25f, 0.0f, 0.0f);
		glVertex3f(-1.50f, 0.0f, 0.0f);



		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(-1.25f, -0.06f, 0.0f);
		glVertex3f(-1.50f, -0.06f, 0.0f);

		glEnd();



		//-------------------------------------------------------------------------------------------------------

		//ThreeLines
		/*glLineWidth(3.0f);
		glTranslatef(-0.5f - cos(anglel6), 1.0f - sin(anglel6), -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (angle <= M_PI_2 * RADIUS)
		{
			glColor3f(0.729411f, 0.8627f, 0.933f);
			anglel6 = anglel6 + 0.0007f;
		}
		else
		{
			glColor3f(0.0f, 0.0f, 0.0f);
			glTranslatef(0.0f, 0.0f, 10.0f);

		}

		glRotatef(angle6, 0.0f, 0.0f, 1.0f);

		if (angle6 <= 360.0f)
		{
			angle6 = angle6 + 0.05f;

			if (angle6 == 360.0f)
			{
				angle6 = 361.0f;

			}

		}*/

			

		
		//----------------------------------------------------------------------------------------------------------------------


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.5f - cos(angley), -1.0f + sin(angley), -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (angley <= M_PI_2 * RADIUS)
		{
			glColor3f(0.729411f, 0.8627f, 0.933f);
			angley = angley + 0.009f;
		}
		else
		{
			glColor3f(0.0f, 0.0f, 0.0f);
			glTranslatef(0.0f, 0.0f, 10.0f);
		}



		glRotatef(angle2, 0.0f, 0.0f, -1.0f);

		if (angle2 <= 360.0f)
		{
			angle2 = angle2 + 0.7f;

			if (angle2 == 360.0f)
			{
				angle2 = 360.1f;
			}
		}

		//Apex
		glBegin(GL_TRIANGLES);
		glVertex3f(0.60f + iOffsetx2, 0.0f, 0.0f);
		glVertex3f(0.50f + iOffsetx2, -0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx2, 0.20f, 0.0f);
		glEnd();

		//Before IAF

		glBegin(GL_QUADS);
		glVertex3f(0.50f + iOffsetx2, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx2, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx2, 0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx2, 0.20f, 0.0f);
		glEnd();

		//One where I write IAF
		glBegin(GL_QUADS);
		glVertex3f(-0.10f + iOffsetx2, 0.15f, 0.0f);
		glVertex3f(-0.10f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx2, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.65f, 0.12f, 0.0f);
		glVertex3f(-0.65f, -0.12f, 0.0f);

		glVertex3f(-0.50f, -0.12f, 0.0f);
		glVertex3f(-0.40f, 0.12f, 0.0f);

		glVertex3f(-0.40f, 0.12f, 0.0f);
		glVertex3f(-0.30f, -0.12f, 0.0f);

		glVertex3f(-0.45f, 0.02f, 0.0f);
		glVertex3f(-0.35f, 0.02f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.15f, -0.12f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.04f, 0.12f, 0.0f);

		glVertex3f(-0.15f, 0.04f, 0.0f);
		glVertex3f(-0.06f, 0.04f, 0.0f);

		glEnd();

		//BiggerWings
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx2, 0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx2, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, 0.15f, 0.0f);
		glEnd();


		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx2, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, -0.15f, 0.0f);
		glEnd();


		//SmallerWings

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx2, 0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx2, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx2, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, -0.15f, 0.0f);
		glEnd();


		//Heat Furnace

		glBegin(GL_LINES);
		glVertex3f(-1.25f + iOffsetx2, 0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx2, 0.09f, 0.0f);

		glVertex3f(-1.25f + iOffsetx2, -0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx2, -0.09f, 0.0f);

		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(-1.25f, 0.06f, 0.0f);
		glVertex3f(-1.50f, 0.06f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.25f, 0.0f, 0.0f);
		glVertex3f(-1.50f, 0.0f, 0.0f);



		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(-1.25f, -0.06f, 0.0f);
		glVertex3f(-1.50f, -0.06f, 0.0f);
		glEnd();


		//---------------------------------------------------------------------------------------------------------------------------------------


		//Straight Plane
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-1.5f - iOffsetx1, 0.0f, -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (iOffsetx1 >= -2.25f)
		{
			iOffsetx1 = iOffsetx1 - 0.0055;

		}
		else
		{

			iOffsetx1 = iOffsetx1 - 0.0058;
			flagp = 3;
			flagp1 = 3;

		}
		//Apex
		glBegin(GL_TRIANGLES);
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glVertex3f(0.60f, 0.0f, 0.0f);
		glVertex3f(0.50f, -0.20f, 0.0f);
		glVertex3f(0.50f, 0.20f, 0.0f);
		glEnd();

		//Before IAF

		glBegin(GL_QUADS);
		glVertex3f(0.50f, -0.20f, 0.0f);
		glVertex3f(-0.10f, -0.20f, 0.0f);
		glVertex3f(-0.10f, 0.20f, 0.0f);
		glVertex3f(0.50f, 0.20f, 0.0f);
		glEnd();

		//One where I write IAF
		glBegin(GL_QUADS);
		glVertex3f(-0.10f, 0.15f, 0.0f);
		glVertex3f(-0.10f, -0.15f, 0.0f);
		glVertex3f(-1.25f, -0.15f, 0.0f);
		glVertex3f(-1.25f, 0.15f, 0.0f);
		glEnd();
		
		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.65f, 0.12f, 0.0f);
		glVertex3f(-0.65f, -0.12f, 0.0f);

		glVertex3f(-0.50f, -0.12f, 0.0f);
		glVertex3f(-0.40f, 0.12f, 0.0f);

		glVertex3f(-0.40f, 0.12f, 0.0f);
		glVertex3f(-0.30f, -0.12f, 0.0f);

		glVertex3f(-0.45f, 0.02f, 0.0f);
		glVertex3f(-0.35f, 0.02f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.15f, -0.12f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.04f, 0.12f, 0.0f);

		glVertex3f(-0.15f, 0.04f, 0.0f);
		glVertex3f(-0.06f, 0.04f, 0.0f);

		glEnd();
		//BiggerWings
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glBegin(GL_QUADS);
		glVertex3f(-0.25f, 0.15f, 0.0f);
		glVertex3f(-0.35f, 0.65f, 0.0f);
		glVertex3f(-0.55f, 0.65f, 0.0f);
		glVertex3f(-0.55f, 0.15f, 0.0f);
		glEnd();


		glBegin(GL_QUADS);
		glVertex3f(-0.25f, -0.15f, 0.0f);
		glVertex3f(-0.35f, -0.65f, 0.0f);
		glVertex3f(-0.55f, -0.65f, 0.0f);
		glVertex3f(-0.55f, -0.15f, 0.0f);
		glEnd();


		//SmallerWings

		glBegin(GL_QUADS);
		glVertex3f(-1.0f, 0.15f, 0.0f);
		glVertex3f(-1.05f, 0.45f, 0.0f);
		glVertex3f(-1.10f, 0.45f, 0.0f);
		glVertex3f(-1.10f, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3f(-1.0f, -0.15f, 0.0f);
		glVertex3f(-1.05f, -0.45f, 0.0f);
		glVertex3f(-1.10f, -0.45f, 0.0f);
		glVertex3f(-1.10f, -0.15f, 0.0f);
		glEnd();


		//Heat Furnace

		glBegin(GL_LINES);
		glVertex3f(-1.25f, 0.05f, 0.0f);
		glVertex3f(-1.40f, 0.09f, 0.0f);

		glVertex3f(-1.25f, -0.05f, 0.0f);
		glVertex3f(-1.40f, -0.09f, 0.0f);
		glEnd();
		
		//ThreeLines

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		if (iOffsetxl1 >= -20.5f)
		{
			glTranslatef(-1.5f - iOffsetx1, 0.0f, -2.0f);
		}

		else
		{
			glTranslatef(0.0f, 0.0f, 5.0f);
			flagf = 3;
		}



		glScalef(0.2f, 0.2f, 0.2f);

		iOffsetxl1 = iOffsetxl1 - 0.03f;
		glBegin(GL_LINES);
		glLineWidth(50.0f);

		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(-1.25f, 0.06f, 0.0f);
		glVertex3f(-1.90f + iOffsetxl1, 0.06f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.25f, 0.0f, 0.0f);
		glVertex3f(-1.90f + iOffsetxl1, 0.0f, 0.0f);



		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(-1.25f, -0.05f, 0.0f);
		glVertex3f(-1.90f + iOffsetxl1, -0.05f, 0.0f);

		glEnd();

		//-------------------------------------------------------------------------------------------------------------------------


		if (flagp1 == 3)
		{
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();


			glTranslatef(1.8f - cos(anglez1), 0.0f + sin(anglez1), -2.0f);
			glScalef(0.2f, 0.2f, 0.2f);
			glColor3f(0.729411f, 0.8627f, 0.933f);
			if (anglez1 <= M_PI_2 * RADIUS2)
			{
				anglez1 = anglez1 + 0.0005f;
			}


			glRotatef(angle4, 0.0f, 0.0f, 1.0f);

			if (angle4 <= 180.0f)
			{
				angle4 = angle4 + 0.09f;

				if (angle4 == 180.0f)
				{
					angle4 = 181.0f;
					flag4 = 2;
				}
			}


			if (flag4 = 2)
			{
				iOffsetx4 = iOffsetx4 + 0.0098f;
			}
			else
			{
				iOffsetx4 = 0.0f;
			}


			//Apex
			glBegin(GL_TRIANGLES);
			glVertex3f(0.60f + iOffsetx4, 0.0f, 0.0f);
			glVertex3f(0.50f + iOffsetx4, -0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx4, 0.20f, 0.0f);
			glEnd();

			//Before IAF

			glBegin(GL_QUADS);
			glVertex3f(0.50f + iOffsetx4, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx4, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx4, 0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx4, 0.20f, 0.0f);
			glEnd();

			//One where I write IAF
			glBegin(GL_QUADS);
			glVertex3f(-0.10f + iOffsetx4, 0.15f, 0.0f);
			glVertex3f(-0.10f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx4, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_LINES);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-0.65f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.65f+iOffsetx4, -0.12f, 0.0f);

			glVertex3f(-0.50f+iOffsetx4, -0.12f, 0.0f);
			glVertex3f(-0.40f+iOffsetx4, 0.12f, 0.0f);

			glVertex3f(-0.40f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.30f+iOffsetx4, -0.12f, 0.0f);

			glVertex3f(-0.45f+iOffsetx4, 0.02f, 0.0f);
			glVertex3f(-0.35f+iOffsetx4, 0.02f, 0.0f);

			glVertex3f(-0.15f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.15f+iOffsetx4, -0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.04f+iOffsetx4, 0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx4, 0.04f, 0.0f);
			glVertex3f(-0.06f+iOffsetx4, 0.04f, 0.0f);

			glEnd();

			//BiggerWings
			glColor3f(0.729411f, 0.8627f, 0.933f);
			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx4, 0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx4, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, 0.15f, 0.0f);
			glEnd();


			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx4, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, -0.15f, 0.0f);
			glEnd();


			//SmallerWings

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx4, 0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx4, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx4, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, -0.15f, 0.0f);
			glEnd();


			//Heat Furnace

			glBegin(GL_LINES);
			glVertex3f(-1.25f + iOffsetx4, 0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx4, 0.09f, 0.0f);

			glVertex3f(-1.25f + iOffsetx4, -0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx4, -0.09f, 0.0f);
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(-1.25f+iOffsetx4, 0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx4, 0.06f, 0.0f);


			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-1.25f+iOffsetx4, 0.0f, 0.0f);
			glVertex3f(-1.50f+iOffsetx4, 0.0f, 0.0f);



			glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
			glVertex3f(-1.25f+iOffsetx4, -0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx4, -0.06f, 0.0f);
			glEnd();

		}

		//------------------------------------------------------------------------------------------------------------


		if (flagp == 3)
		{
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();


			glTranslatef(1.8f - cos(anglez), 0.0f - sin(anglez), -2.0f);
			glScalef(0.2f, 0.2f, 0.2f);
			glColor3f(0.729411f, 0.8627f, 0.933f);

			if (anglez <= M_PI_2 * RADIUS2)
			{
				anglez = anglez + 0.005f;
			}


			glRotatef(angle3, 0.0f, 0.0f, 1.0f);

			if (angle3 >= 90.0f)
			{
				angle3 = angle3 - 0.009f;

				if (angle3 == 90.0f)
				{
					angle3 = 91.0f;
					flag3 = 2;
				}
			}


			if (flag3 = 2)
			{
				iOffsetx3 = iOffsetx3 + 0.0098f;
			}
			else
			{
				iOffsetx3 = 0.0f;
			}


			//Apex
			glBegin(GL_TRIANGLES);
			glVertex3f(0.60f + iOffsetx3, 0.0f, 0.0f);
			glVertex3f(0.50f + iOffsetx3, -0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx3, 0.20f, 0.0f);
			glEnd();

			//Before IAF

			glBegin(GL_QUADS);
			glVertex3f(0.50f + iOffsetx3, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx3, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx3, 0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx3, 0.20f, 0.0f);
			glEnd();

			//One where I write IAF
			glBegin(GL_QUADS);
			glVertex3f(-0.10f + iOffsetx3, 0.15f, 0.0f);
			glVertex3f(-0.10f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx3, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_LINES);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-0.65f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.65f+iOffsetx3, -0.12f, 0.0f);

			glVertex3f(-0.50f+iOffsetx3, -0.12f, 0.0f);
			glVertex3f(-0.40f+iOffsetx3, 0.12f, 0.0f);

			glVertex3f(-0.40f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.30f+iOffsetx3, -0.12f, 0.0f);

			glVertex3f(-0.45f+iOffsetx3, 0.02f, 0.0f);
			glVertex3f(-0.35f+iOffsetx3, 0.02f, 0.0f);

			glVertex3f(-0.15f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.15f+iOffsetx3, -0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.04f+iOffsetx3, 0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx3, 0.04f, 0.0f);
			glVertex3f(-0.06f+iOffsetx3, 0.04f, 0.0f);

			glEnd();

			//BiggerWings
			glColor3f(0.729411f, 0.8627f, 0.933f);
			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx3, 0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx3, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, 0.15f, 0.0f);
			glEnd();


			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx3, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, -0.15f, 0.0f);
			glEnd();


			//SmallerWings

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx3, 0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx3, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx3, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, -0.15f, 0.0f);
			glEnd();


			//Heat Furnace

			glBegin(GL_LINES);
			glVertex3f(-1.25f + iOffsetx3, 0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx3, 0.09f, 0.0f);

			glVertex3f(-1.25f + iOffsetx3, -0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx3, -0.09f, 0.0f);
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(-1.25f+iOffsetx3, 0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx3, 0.06f, 0.0f);


			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-1.25f+iOffsetx3, 0.0f, 0.0f);
			glVertex3f(-1.50f+iOffsetx3, 0.0f, 0.0f);



			glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
			glVertex3f(-1.25f+iOffsetx3, -0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx3, -0.06f, 0.0f);
			glEnd();
		}
	}


	glXSwapBuffers(gpDisplay,gWindow);
}




void uninitialize(void)
{
	GLXContext CurrentglxContext=glXGetCurrentContext();
	
	if(CurrentglxContext!=NULL&& CurrentglxContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
		
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}



