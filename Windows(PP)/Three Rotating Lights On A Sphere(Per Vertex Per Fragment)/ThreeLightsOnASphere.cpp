#include<Windows.h>
#include<stdio.h>
#include<GL/glew.h>
#include<gl/GL.h>
#include"vmath.h"
#include"Sphere-dot.h"
#include"Stack.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere-dot.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC gHdc = NULL;
HGLRC ghrc = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool bIsFullScreen = false;
bool bDone = false;
int iRet;

int vkey = 0;

//Shaders variable

GLint gVertexShaderObject1;
GLint gFragmentShaderObject1;
GLint gShaderProgramObject1;
GLint gVertexShaderObject2;
GLint gFragmentShaderObject2;
GLint gShaderProgramObject2;
GLfloat LightAnglezero = 0.0f, LightAngleone = 0.0f, LightAngletwo = 0.0f;

//Global Variables

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;
GLuint mUniform;
GLuint vUniform;
GLuint pUniform;
GLuint laUniform_red;
GLuint laUniform_blue;
GLuint laUniform_green;
GLuint kaUniform;
GLuint lsUniform_red;
GLuint lsUniform_green;
GLuint lsUniform_blue;
GLuint ldUniform_red;
GLuint ldUniform_green;
GLuint ldUniform_blue;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform_red;
GLuint lightPositionUniform_blue;
GLuint lightPositionUniform_green;
GLuint isLKeypressedUniform;
mat4 perspectiveProjectionMatrix;
GLuint material_shininess_uniform;

static GLfloat angle_pyramid = 0.0f, angle_cube = 0.0f;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];
int gNumVertices, gNumElements;
int gbLight = 0;

struct Lights {
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float position[4];
};

float MaterialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
float MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
float MaterialShininess[] = { 50.0f };


Lights light[3];



enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

FILE * gpFile = NULL;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	int initialize(void);

	void display();
	void Update();
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("3 Rotating Lights On A Sphere");
	WNDCLASSEX wndclass;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("3 Rotating Lights On A Sphere Per Vertex Per Fragment"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixel Format failed");
		DestroyWindow(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext failed");
		DestroyWindow(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent failed");
		DestroyWindow(0);
	}
	else
	{
		fprintf(gpFile, "Initialize Function succeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update();
			}

			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen();
	void uninitialize();

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
			vkey = 1;
			break;
		case 'V':
			vkey = 0;
			break;
		case VK_ESCAPE:
			if (bIsFullScreen == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;
			
		case 'L':
			if (gbLight == 0)
			{
				gbLight = 1;
			}
			else
			{
				gbLight = 0;
			}
			break;
		case 'Q':
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}

	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);

		ShowCursor(true);
		bIsFullScreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatDescriptor;
	GLenum result;
	void uninitialize();
	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfolog = NULL;


	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;

	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatDescriptor = ChoosePixelFormat(gHdc, &pfd);

	if (iPixelFormatDescriptor == 0)
	{
		return -1;
	}
	if (SetPixelFormat(gHdc, iPixelFormatDescriptor, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(gHdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return -4;
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		uninitialize();
		fprintf(gpFile, "Inside GLEW\n");
		DestroyWindow(gHwnd);
	}


	gVertexShaderObject1 = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code.
	const GLchar* vertexShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vnormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ks;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ls_green;" \

		"uniform float m_shininess;"
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec4 u_light_position_green;" \
		"out vec3 finalColor;" \
		"vec3 light_direction_red;"\
		"vec3 light_direction_blue;"\
		"vec3 light_direction_green;"\
		"out vec3 phong_ads_light1;" \
		"out vec3 phong_ads_light2;" \
		"out vec3 phong_ads_light3;" \
		"vec3 reflection_vector_blue;"\
		"vec3 reflection_vector_green;"\
		"vec3 reflection_vector_red;"\
		"float tn_dot_ld_red;" \
		"float tn_dot_ld_blue;" \
		"float tn_dot_ld_green;" \
		"vec3 ambient1;"\
		"vec3 ambient2;"\
		"vec3 ambient3;"\
		"vec3 diffuse1;"\
		"vec3 diffuse2;"\
		"vec3 diffuse3;"\
		"vec3 specular1;"\
		"vec3 specular2;"\
		"vec3 specular3;"\
		"vec3 tnorm;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
		"tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \

		"light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);" \

		"light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);" \

		"light_direction_green=normalize(vec3(u_light_position_green)-eye_coordinates.xyz);" \

		"tn_dot_ld_red=max(dot(light_direction_red,tnorm),0.0);" \

		"tn_dot_ld_blue=max(dot(light_direction_blue,tnorm),0.0);" \

		"tn_dot_ld_green=max(dot(light_direction_green,tnorm),0.0);" \

		"reflection_vector_red=reflect(-light_direction_red,tnorm);" \
		"reflection_vector_blue=reflect(-light_direction_blue,tnorm);" \
		"reflection_vector_green=reflect(-light_direction_green,tnorm);" \

		"vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \


		"ambient1  = u_la_red* u_ka;" \
		"ambient2= u_la_green* u_ka;" \
		"ambient3= u_la_blue* u_ka;" \

		"diffuse1 = u_ld_red * u_kd * tn_dot_ld_red;" \
		"diffuse2 = u_ld_green * u_kd * tn_dot_ld_green;" \
		"diffuse3 = u_ld_blue * u_kd * tn_dot_ld_blue;" \

		"specular1 = u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),m_shininess);" \
		"specular2 = u_ls_green * u_ks * pow(max(dot(reflection_vector_green,viewer_vector),0.0),m_shininess);" \
		"specular3 = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),m_shininess);" \

		"phong_ads_light1  =ambient1+diffuse1+specular1;" \
		"phong_ads_light2  =ambient2+diffuse2+specular2;" \
		"phong_ads_light3  =ambient3+diffuse3+specular3;" \

		"finalColor= phong_ads_light3+phong_ads_light1+phong_ads_light2;" \
		"}" \
		"else" \
		"{" \
		"finalColor=vec3(1.0,1.0,1.0);"  \
		"}" \

		"}";


	glShaderSource(gVertexShaderObject1, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject1);

	glGetShaderiv(gVertexShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}


	//For Fragment Shader.

	gFragmentShaderObject1 = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec3 phong_ads_light1;" \
		"in vec3 phong_ads_light2;" \
		"in vec3 finalColor;" \
		"out vec4 FragColor1;" \
		"uniform int u_lkeyispressed;" \
		"void main(void)" \
		"{" \

		"FragColor1=vec4(finalColor,1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject1, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(gFragmentShaderObject1);

	glGetShaderiv(gFragmentShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}


	//Create shader program object

	gShaderProgramObject1 = glCreateProgram();

	glAttachShader(gShaderProgramObject1, gVertexShaderObject1);

	glAttachShader(gShaderProgramObject1, gFragmentShaderObject1);

	glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject1, AMC_ATTIBUTE_NORMAL, "vnormal");

	glLinkProgram(gShaderProgramObject1);

	glGetProgramiv(gShaderProgramObject1, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//
	mUniform = glGetUniformLocation(gShaderProgramObject1, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject1, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject1, "u_p_matrix");
	laUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_la_red");
	laUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_la_green");
	laUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_la_blue");
	lsUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_ls_red");
	lsUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_ls_green");
	lsUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_ls_blue");
	ldUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_ld_red");
	ldUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_ld_green");
	ldUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_ld_blue");
	kdUniform = glGetUniformLocation(gShaderProgramObject1, "u_kd");
	kaUniform = glGetUniformLocation(gShaderProgramObject1, "u_ka");
	ksUniform = glGetUniformLocation(gShaderProgramObject1, "u_ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject1, "m_shininess");
	lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_light_position_red");
	lightPositionUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_light_position_green");
	lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_light_position_blue");
	isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject1, "u_lkeyispressed");



	//============================================================================================================

	gVertexShaderObject2 = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code.
	const GLchar* vertexShaderSourceCode2 =
		"#version 440" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vnormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec4 u_light_position_green;" \
		"out vec4 eye_coordinates;" \
		"out vec3 light_direction_red;" \
		"out vec3 light_direction_blue;" \
		"out vec3 light_direction_green;" \
		"out vec3 tnorm;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
		"tnorm=mat3(u_v_matrix*u_m_matrix)*vnormal;" \
		"light_direction_red=vec3(u_light_position_red)-eye_coordinates.xyz;" \
		"light_direction_blue=vec3(u_light_position_blue)-eye_coordinates.xyz;" \
		"light_direction_green=vec3(u_light_position_green)-eye_coordinates.xyz;" \
		"viewer_vector=vec3(-eye_coordinates);" \
		"}" \
		"else" \
		"{" \
		"}" \

		"}";

	glShaderSource(gVertexShaderObject2, 1, (const GLchar **)&vertexShaderSourceCode2, NULL);
	glCompileShader(gVertexShaderObject2);

	glGetShaderiv(gVertexShaderObject2, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject2, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader 2%s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}


	//For Fragment Shader.

	gFragmentShaderObject2 = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode2 =
		"#version 440" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ks;" \
		"uniform vec3 u_kd;" \
		"uniform float m_shininess;"
		"in vec3 light_direction_red;" \
		"in vec3 light_direction_blue;" \
		"in vec3 light_direction_green;" \
		"in vec3 tnorm;"
		"in vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec3 tdnorm=normalize(tnorm);" \
		"vec3 light_direction1_red=normalize(light_direction_red);" \
		"vec3 light_direction1_blue=normalize(light_direction_blue);" \
		"vec3 light_direction1_green=normalize(light_direction_green);" \
		"float tn_dot_ld1=max(dot(light_direction1_red,tdnorm),0.0);" \
		"float tn_dot_ld2=max(dot(light_direction1_green,tdnorm),0.0);" \
		"float tn_dot_ld3=max(dot(light_direction1_blue,tdnorm),0.0);" \
		"vec3 reflection_vector_red=reflect(-light_direction1_red,tdnorm);" \
		"vec3 reflection_vector_blue=reflect(-light_direction1_blue,tdnorm);" \
		"vec3 reflection_vector_green=reflect(-light_direction1_green,tdnorm);" \
		"vec3 viewer_vector1=normalize(viewer_vector);" \
		"vec3 ambient1 = u_la_red * u_ka;" \
		"vec3 ambient2 = u_la_green * u_ka;" \
		"vec3 ambient3 = u_la_blue * u_ka;" \
		"vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld1;" \
		"vec3 diffuse2 = u_ld_green * u_kd * tn_dot_ld2;" \
		"vec3 diffuse3 = u_ld_blue * u_kd * tn_dot_ld3;" \
		"vec3 specular1=u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector1),0.0),m_shininess);" \
		"vec3 specular2=u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector1),0.0),m_shininess);" \
		"vec3 specular3=u_ls_green * u_ks * pow(max(dot(reflection_vector_green,viewer_vector1),0.0),m_shininess);" \
		"vec3 phong_ads_ligh1=ambient1+diffuse1+specular1;" \
		"vec3 phong_ads_ligh2=ambient2+diffuse2+specular2;" \
		"vec3 phong_ads_ligh3=ambient3+diffuse3+specular3;" \
		"vec3 finalColor=phong_ads_ligh1+phong_ads_ligh2+phong_ads_ligh3;" \
		"FragColor=vec4(finalColor,1.0);" \
		"}" \
		"else"
		"{"
		"FragColor=vec4(1.0,1.0,1.0,1.0);" \
		"}"
		"}";

	glShaderSource(gFragmentShaderObject2, 1, (const GLchar **)&fragmentShaderSourceCode2, NULL);
	glCompileShader(gFragmentShaderObject2);

	glGetShaderiv(gFragmentShaderObject2, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject2, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}


	//Create shader program object

	gShaderProgramObject2 = glCreateProgram();

	glAttachShader(gShaderProgramObject2, gVertexShaderObject2);

	glAttachShader(gShaderProgramObject2, gFragmentShaderObject2);

	glBindAttribLocation(gShaderProgramObject2, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject2, AMC_ATTIBUTE_NORMAL, "vnormal");

	glLinkProgram(gShaderProgramObject2);

	glGetProgramiv(gShaderProgramObject2, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject2, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//
	mUniform = glGetUniformLocation(gShaderProgramObject2, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject2, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject2, "u_p_matrix");
	laUniform_red = glGetUniformLocation(gShaderProgramObject2, "u_la_red");
	laUniform_green = glGetUniformLocation(gShaderProgramObject2, "u_la_green");
	laUniform_blue = glGetUniformLocation(gShaderProgramObject2, "u_la_blue");
	lsUniform_red = glGetUniformLocation(gShaderProgramObject2, "u_ls_red");
	lsUniform_green = glGetUniformLocation(gShaderProgramObject2, "u_ls_green");
	lsUniform_blue = glGetUniformLocation(gShaderProgramObject2, "u_ls_blue");
	ldUniform_red = glGetUniformLocation(gShaderProgramObject2, "u_ld_red");
	ldUniform_green = glGetUniformLocation(gShaderProgramObject2, "u_ld_green");
	ldUniform_blue = glGetUniformLocation(gShaderProgramObject2, "u_ld_blue");
	kdUniform = glGetUniformLocation(gShaderProgramObject2, "u_kd");
	kaUniform = glGetUniformLocation(gShaderProgramObject2, "u_ka");
	ksUniform = glGetUniformLocation(gShaderProgramObject2, "u_ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject2, "m_shininess");
	lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject2, "u_light_position_red");
	lightPositionUniform_green = glGetUniformLocation(gShaderProgramObject2, "u_light_position_green");
	lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject2, "u_light_position_blue");
	isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject2, "u_lkeyispressed");




	//========================================================================================================================
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	//Sphere
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Cube


	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	light[0].ambient[0] = 0.0f;
	light[0].ambient[1] = 0.0f;
	light[0].ambient[2] = 0.0f;
	light[0].ambient[3] = 1.0f;

	light[0].diffuse[0] = 1.0f;
	light[0].diffuse[1] = 0.0f;
	light[0].diffuse[2] = 0.0f;
	light[0].diffuse[3] = 1.0f;

	light[0].specular[0] = 1.0f;
	light[0].specular[1] = 0.0f;
	light[0].specular[2] = 0.0f;
	light[0].specular[3] = 1.0f;

	light[0].position[0] = 0.0f;
	light[0].position[1] = 0.0f;
	light[0].position[2] = 0.0f;
	light[0].position[3] = 1.0f;


	light[1].ambient[0] = 0.0f;
	light[1].ambient[1] = 0.0f;
	light[1].ambient[2] = 0.0f;
	light[1].ambient[3] = 1.0f;

	light[1].diffuse[0] = 0.0f;
	light[1].diffuse[1] = 1.0f;
	light[1].diffuse[2] = 0.0f;
	light[1].diffuse[3] = 1.0f;

	light[1].specular[0] = 0.0f;
	light[1].specular[1] = 1.0f;
	light[1].specular[2] = 0.0f;
	light[1].specular[3] = 1.0f;

	light[1].position[0] = 0.0f;
	light[1].position[1] = 0.0f;
	light[1].position[2] = 0.0f;
	light[1].position[3] = 1.0f;

	light[2].ambient[0] = 0.0f;
	light[2].ambient[1] = 0.0f;
	light[2].ambient[2] = 0.0f;
	light[2].ambient[3] = 1.0f;

	light[2].diffuse[0] = 0.0f;
	light[2].diffuse[1] = 0.0f;
	light[2].diffuse[2] = 1.0f;
	light[2].diffuse[3] = 1.0f;

	light[2].specular[0] = 0.0f;
	light[2].specular[1] = 0.0f;
	light[2].specular[2] = 1.0f;
	light[2].specular[3] = 1.0f;

	light[2].position[0] = 0.0f;
	light[2].position[1] = 0.0f;
	light[2].position[2] = 0.0f;
	light[2].position[3] = 1.0f;



	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	perspectiveProjectionMatrix = mat4::identity();
	//glEnable(GL_CULL_FACE);
	//glDisable(GL_CULL_FACE);
	//glEnable(GL_FRONT_FACE);
	//glEnable(GL_BACK);
	resize(WIN_WIDTH, WIN_HEIGHT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	return 0;
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if(vkey==1)
	glUseProgram(gShaderProgramObject2);
	else
	glUseProgram(gShaderProgramObject1);
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 rotationMatrix;
	mat4 modelViewMatrix;

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	modelViewMatrix = mat4::identity();


	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	if (gbLight == 1)
	{
		glUniform1i(isLKeypressedUniform, 1);




		glUniform3fv(laUniform_red, 1, light[0].ambient);
		glUniform3fv(ldUniform_red, 1, light[0].diffuse);
		glUniform3fv(lsUniform_red, 1, light[0].specular);

		glUniform4fv(lightPositionUniform_red, 1, light[0].position);



		glUniform3fv(laUniform_green, 1, light[1].ambient);
		glUniform3fv(ldUniform_green, 1, light[1].diffuse);
		glUniform3fv(lsUniform_green, 1, light[1].specular);
		//rotationMatrix = mat4::identity();
		//rotationMatrix = rotate(LightAngleone, 0.0f, 1.0f, 0.0f);
		//light[1].position[0] = LightAngleone;
		glUniform4fv(lightPositionUniform_green, 1, light[1].position);



		glUniform3fv(laUniform_blue, 1, light[2].ambient);
		glUniform3fv(ldUniform_blue, 1, light[2].diffuse);
		glUniform3fv(lsUniform_blue, 1, light[2].specular);

		
		glUniform4fv(lightPositionUniform_blue, 1, light[2].position);


		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1fv(material_shininess_uniform, 1, MaterialShininess);


	}
	else
	{
		glUniform1i(isLKeypressedUniform, 0);
	}

	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//Cube


	glUseProgram(0);
	SwapBuffers(gHdc);
}

void Update()
{
	//LightAnglezero = LightAnglezero + 0.00000000005f;

	LightAnglezero = LightAnglezero + 0.001f;
	LightAngleone = LightAngleone + 0.001f;
	LightAngletwo = LightAngletwo + 0.001f;
	if (LightAnglezero >= 2 * M_PI)
	{
		LightAnglezero = 0.0f;

	}
	light[0].position[1] = 100.0f*sin(LightAnglezero);
	light[0].position[2] = 100.0f*cos(LightAnglezero);

	if (LightAngleone >= 2 * M_PI)
	{
		LightAngleone = 0.0f;
	}
	light[1].position[0] = 100.0f*sin(LightAngleone);
	light[1].position[2] = 100.0f*cos(LightAngleone);
	if (LightAngletwo >= 2 * M_PI)
	{
		LightAngletwo = 0.0f;
	}
	light[2].position[0] = 100.0f*sin(LightAngleone);
	light[2].position[1] = 100.0f*cos(LightAngleone);
}

void uninitialize()
{
	if (bIsFullScreen == true) {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Log File closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}


	glUseProgram(gShaderProgramObject1);
	glDetachShader(gShaderProgramObject1, gFragmentShaderObject1);
	glDetachShader(gShaderProgramObject1, gVertexShaderObject1);
	glDeleteShader(gFragmentShaderObject1);
	gFragmentShaderObject1 = 0;
	glDeleteShader(gVertexShaderObject1);
	gVertexShaderObject1 = 0;
	glDeleteProgram(gShaderProgramObject1);
	gShaderProgramObject1 = 0;
	glUseProgram(0);
}


