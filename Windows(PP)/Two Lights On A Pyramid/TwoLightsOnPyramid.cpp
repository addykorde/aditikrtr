#include<Windows.h>
#include<stdio.h>
#include<GL/glew.h>
#include<gl/GL.h>
#include"vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC gHdc = NULL;
HGLRC ghrc = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool bIsFullScreen = false;
bool bDone = false;
int iRet;

//Shaders variable

GLint gVertexShaderObject;
GLint gFragmentShaderObject;
GLint gShaderProgramObject;
GLint gFragmentShaderObject1;

//Global Variables

GLuint vao_pyramid;
GLuint vao_cube;
GLuint vbo_position_pyramid;
GLuint vbo_normal_pyramid;
GLuint vbo_normal1_pyramid;
GLuint mUniform;
GLuint vUniform;
GLuint pUniform;
GLuint laUniform_red;
GLuint kaUniform_red;
GLuint lsUniform_red;
GLuint ldUniform_red;
GLuint kdUniform_red;
GLuint ksUniform_red;
GLuint lightPositionUniform_red;
GLuint laUniform_blue;
GLuint kaUniform_blue;
GLuint lsUniform_blue;
GLuint ldUniform_blue;
GLuint kdUniform_blue;
GLuint ksUniform_blue;
GLuint lightPositionUniform_blue;
GLuint isLKeypressedUniform;
mat4 perspectiveProjectionMatrix;
GLuint material_shininess_uniform;
static GLfloat angle_pyramid = 0.0f, angle_cube = 0.0f;

struct Lights {
	float Ambient[4];
	float Diffuse[4];
	float Specular[4];
	float Position[4];
};

float MaterialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
float MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
float MaterialShininess[] = { 50.0f };

Lights lights[2];
int gbLight = 0;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTIBUTE_NORMAL,
	AMC_ATTRIBUTE_NORMAL1,
	AMC_ATTRIBUTE_TEXCOORD0
};

FILE * gpFile = NULL;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	int initialize(void);

	void display();
	void Update();
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("3D Rotation");
	WNDCLASSEX wndclass;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Two Lights On Pyramid"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixel Format failed");
		DestroyWindow(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext failed");
		DestroyWindow(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent failed");
		DestroyWindow(0);
	}
	else
	{
		fprintf(gpFile, "Initialize Function succeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update();
			}

			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen();
	void uninitialize();

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
			if (bIsFullScreen == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;

		case'L':
			if (gbLight == 0)
			{
				gbLight = 1;
			}
			else
			{
				gbLight = 0;
			}
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}

	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);

		ShowCursor(true);
		bIsFullScreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatDescriptor;
	GLenum result;
	void uninitialize();
	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfolog = NULL;


	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;

	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatDescriptor = ChoosePixelFormat(gHdc, &pfd);

	if (iPixelFormatDescriptor == 0)
	{
		return -1;
	}
	if (SetPixelFormat(gHdc, iPixelFormatDescriptor, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(gHdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return -4;
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		uninitialize();
		fprintf(gpFile, "Inside GLEW\n");
		DestroyWindow(gHwnd);
	}


	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code.
	const GLchar* vertexShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vnormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ks;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform float m_shininess;"
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_blue;" \
		"out vec3 finalColor;" \
		"vec3 light_direction_red;"\
		"vec3 light_direction_blue;"\
		"out vec3 phong_ads_light1;" \
		"out vec3 phong_ads_light2;" \
		"vec3 reflection_vector_blue;"\
		"vec3 reflection_vector_red;"\
		"float tn_dot_ld_red;" \
		"float tn_dot_ld_blue;" \
		"vec3 ambient1;"\
		"vec3 ambient2;"\
		"vec3 diffuse1;"\
		"vec3 diffuse2;"\
		"vec3 specular1;"\
		"vec3 specular2;"\
		"vec3 tnorm;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
		"tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \

		"light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);" \

		"light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);" \

		"tn_dot_ld_red=max(dot(light_direction_red,tnorm),0.0);" \

		"tn_dot_ld_blue=max(dot(light_direction_blue,tnorm),0.0);" \

		"reflection_vector_red=reflect(-light_direction_red,tnorm);" \
		"reflection_vector_blue=reflect(-light_direction_blue,tnorm);" \

		"vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \


		"ambient1  = u_la_red* u_ka;" \
		"ambient2= u_la_blue* u_ka;" \

		"diffuse1 = u_ld_red * u_kd * tn_dot_ld_red;" \
		"diffuse2 = u_ld_blue * u_kd * tn_dot_ld_blue;" \

		"specular1 = u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),m_shininess);" \
		"specular2 = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),m_shininess);" \

		"phong_ads_light1  =ambient1+diffuse1+specular1;" \
		"phong_ads_light2  =ambient2+diffuse2+specular2;" \
		"finalColor=phong_ads_light2+phong_ads_light1;" \
		"}" \
		"else" \
		"{" \
		"finalColor=vec3(1.0,1.0,1.0);"  \
		"}" \

		"}";


	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}


	//For Fragment Shader.

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec3 phong_ads_light1;" \
		"in vec3 phong_ads_light2;" \
		"in vec3 finalColor;" \
		"out vec4 FragColor1;" \
		"uniform int u_lkeyispressed;" \
		"void main(void)" \
		"{" \
	
		"FragColor1=vec4(finalColor,1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//

	
	//Create shader program object

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);


	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTIBUTE_NORMAL, "vnormal");



	glLinkProgram(gShaderProgramObject);

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");

	laUniform_red = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	lsUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	ldUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ld_red");

	kdUniform_red = glGetUniformLocation(gShaderProgramObject, "u_kd");
	kaUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ksUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ks");

	lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");

	laUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	lsUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	ldUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");

	lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");

	isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");

	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "m_shininess");


	const GLfloat pyramidVertices[] = { 0.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 1.0f ,
										1.0f, -1.0f, 1.0f ,

										0.0f, 1.0f, 0.0f ,
										1.0f, -1.0f, 1.0f ,
										1.0f, -1.0f, -1.0f ,

										0.0f, 1.0f, 0.0f ,
										-1.0f, -1.0f,-1.0f ,
										-1.0f, -1.0f, 1.0f ,

										0.0f, 1.0f, 0.0f ,
										1.0f, -1.0f, -1.0f ,
										-1.0f, -1.0f, -1.0f ,
	};
	
	const GLfloat pyramidNormals[] = {
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		


		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
	

		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		

	};

	
	
	//Pyramid
	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);
	glGenBuffers(1, &vbo_position_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_normal_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


	//Values
	
	lights[0].Ambient[0] = 0.0f;
	lights[0].Ambient[1] = 0.0f;
	lights[0].Ambient[2] = 0.0f;
	lights[0].Ambient[3] = 1.0f;

	lights[0].Diffuse[0] = 1.0f;
	lights[0].Diffuse[1] = 0.0f;
	lights[0].Diffuse[2] = 0.0f;
	lights[0].Diffuse[3] = 1.0f;


	lights[0].Specular[0] = 1.0f;
	lights[0].Specular[1] = 0.0f;
	lights[0].Specular[2] = 0.0f;
	lights[0].Specular[3] = 1.0f;


	lights[0].Position[0] = -2.0f;
	lights[0].Position[1] = 0.0f;
	lights[0].Position[2] = 0.0f;
	lights[0].Position[3] = 1.0f;

	lights[1].Ambient[0] = 0.0f;
	lights[1].Ambient[1] = 0.0f;
	lights[1].Ambient[2] = 0.0f;
	lights[1].Ambient[3] = 1.0f;

	lights[1].Diffuse[0] = 0.0f;
	lights[1].Diffuse[1] = 0.0f;
	lights[1].Diffuse[2] = 1.0f;
	lights[1].Diffuse[3] = 1.0f;

	lights[1].Specular[0] = 0.0f;
	lights[1].Specular[1] = 0.0f;
	lights[1].Specular[2] = 1.0f;
	lights[1].Specular[3] = 1.0f;

	lights[1].Position[0] = 2.0f;
	lights[1].Position[1] = 0.0f;
	lights[1].Position[2] = 0.0f;
	lights[1].Position[3] = 1.0f;


	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	perspectiveProjectionMatrix = mat4::identity();
	//glEnable(GL_CULL_FACE);
	//glDisable(GL_CULL_FACE);
	//glEnable(GL_FRONT_FACE);
	//glEnable(GL_BACK);
	resize(WIN_WIDTH, WIN_HEIGHT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	return 0;
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	glUseProgram(gShaderProgramObject);
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 rotationMatrix;


	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	rotationMatrix = rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);
	modelMatrix = translate(0.0f, 0.0f, -6.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	if (gbLight == 1)
	{
		glUniform1i(isLKeypressedUniform, 1);

		glUniform3fv(laUniform_blue, 1, lights[1].Ambient);
		glUniform3fv(ldUniform_blue, 1, lights[1].Diffuse);
		glUniform3fv(lsUniform_blue, 1, lights[1].Specular);

		glUniform4fv(lightPositionUniform_blue, 1, lights[1].Position);


		glUniform3fv(laUniform_red, 1, lights[0].Ambient);
		glUniform3fv(ldUniform_red, 1, lights[0].Diffuse);
		glUniform3fv(lsUniform_red, 1, lights[0].Specular);
		glUniform4fv(lightPositionUniform_red, 1, lights[0].Position);

		
		glUniform3fv(kaUniform_red, 1, MaterialAmbient);
		glUniform3fv(kdUniform_red, 1, MaterialDiffuse);
		glUniform3fv(ksUniform_red, 1, MaterialSpecular);
		glUniform1fv(material_shininess_uniform, 1, MaterialShininess);


	}
	else
	{
		glUniform1i(isLKeypressedUniform, 0);
	}

	glBindVertexArray(vao_pyramid);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);

	//Cube
	

	glUseProgram(0);
	SwapBuffers(gHdc);
}

void Update()
{
	if (angle_pyramid <= 360.0f)
	{
		angle_pyramid = angle_pyramid + 0.05f;
	}
	else
	{
		angle_pyramid = 0.0f;
	}
}

void uninitialize()
{
	if (bIsFullScreen == true) {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Log File closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}


	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
}


