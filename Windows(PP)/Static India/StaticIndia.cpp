#include<Windows.h>
#include<stdio.h>
#include<GL/glew.h>
#include<gl/GL.h>
#include"vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

HDC gHdc = NULL;
HGLRC ghrc = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool bIsFullScreen = false;
bool bDone = false;
int iRet;

//Shaders variable

GLint gVertexShaderObject;
GLint gFragmentShaderObject;
GLint gShaderProgramObject;


//Global Variables

GLuint vao_i1;
GLuint vao_i2;
GLuint vao_n;
GLuint vao_d;
GLuint vao_a;
GLuint vao_line1;
GLuint vao_line2;
GLuint vao_line3;
GLuint vbo_position_i1;
GLuint vbo_position_i2;
GLuint vbo_position_n;
GLuint vbo_position_d;
GLuint vbo_position_a;
GLuint vbo_position_line1;
GLuint vbo_position_line2;
GLuint vbo_position_line3;
GLuint vbo_color_i1;
GLuint vbo_color_i2;
GLuint vbo_color_n;
GLuint vbo_color_d;
GLuint vbo_color_a;
GLuint vbo_color_line1;
GLuint vbo_color_line2;
GLuint vbo_color_line3;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

FILE * gpFile = NULL;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	int initialize(void);

	void display();
	void Update();
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("Static India");
	WNDCLASSEX wndclass;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Static India"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixel Format failed");
		DestroyWindow(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext failed");
		DestroyWindow(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent failed");
		DestroyWindow(0);
	}
	else
	{
		fprintf(gpFile, "Initialize Function succeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{


			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen();
	void uninitialize();

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
			if (bIsFullScreen == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}

	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);

		ShowCursor(true);
		bIsFullScreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatDescriptor;
	GLenum result;
	void uninitialize();
	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfolog = NULL;


	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;

	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatDescriptor = ChoosePixelFormat(gHdc, &pfd);

	if (iPixelFormatDescriptor == 0)
	{
		return -1;
	}
	if (SetPixelFormat(gHdc, iPixelFormatDescriptor, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(gHdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return -4;
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		uninitialize();
		fprintf(gpFile, "Inside GLEW\n");
		DestroyWindow(gHwnd);
	}


	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code.
	const GLchar* vertexShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_color=vColor;"
		"}";


	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}


	//For Fragment Shader.

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor=out_color;" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}


	//Create shader program object

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	glLinkProgram(gShaderProgramObject);

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat i1[] = 
	{ -0.70f, 0.80f, 0.0f,

	- 0.72f, 0.80f, 0.0f,

	- 0.72f,-0.80f, 0.0f,

	- 0.70f, -0.80f, 0.0f,
	};
	const GLfloat i2[] = {
	0.20f, 0.80f, 0.0f,

	0.22f, 0.80f, 0.0f,

	0.22f, -0.80f, 0.0f,

	0.20f, -0.80f, 0.0f

	};
	const GLfloat n[] = {
	-0.52f, 0.80f, 0.0f,

	- 0.50f, 0.80f, 0.0f,

	- 0.50f, -0.80f, 0.0f,

	- 0.52f, -0.80f, 0.0f,


	- 0.50f, 0.80f, 0.0f,

	- 0.48f, 0.80f, 0.0f,

	- 0.36f, -0.80f, 0.0f,
		
	- 0.38f, -0.80f, 0.0f,


	- 0.38f, 0.80f, 0.0f,

	- 0.36f, 0.80f, 0.0f,

	- 0.36f, -0.80f, 0.0f,

	- 0.38f, -0.80f, 0.0f,

	};
	const GLfloat d[] = { -0.16f, 0.78f, 0.0f,

	- 0.14f, 0.78f, 0.0f,

	- 0.14f, -0.78f, 0.0f,

	- 0.16f, -0.78f, 0.0f,



	-0.16f, 0.78f, 0.0f,

	- 0.16f, 0.80f, 0.0f,

	0.0f, 0.80f, 0.0f,

	0.0f, 0.78f, 0.0f,


	- 0.16f, -0.80f, 0.0f,

	- 0.16f, -0.78f, 0.0f,

	0.0f, -0.78f, 0.0f,

	0.0f, -0.80f, 0.0f,



	-0.02f, 0.78f, 0.0f,

	0.0f, 0.78f, 0.0f,

	0.0f, -0.78f, 0.0f,

	- 0.02f, -0.78f, 0.0f
	};
	const GLfloat a[] = { 
	0.40f, -0.80f, 0.0f,

	0.42f, -0.80f, 0.0f,

	0.52f, 0.80f, 0.0f,

	0.50f, 0.80f, 0.0f,


	0.72f, -0.80f, 0.0f,

	0.74f, -0.80f, 0.0f,

	0.54f, 0.80f, 0.0f,

	0.52f, 0.80f, 0.0f
	};
	const GLfloat i1Color[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f };
	const GLfloat i2Color[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f };
	const GLfloat nColor[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f ,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
	};
	const GLfloat dColor[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
	};
	const GLfloat aColor[] = { 0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f };
	
	const GLfloat line1Vertices[] = {
		0.50f,0.0f,0.0f,
		0.62,0.0f,0.0f
	};

	const GLfloat line2Vertices[] = {
		0.50f, -0.04f, 0.0f,
		0.63f, -0.04f, 0.0f
	};

	const GLfloat line3Vertices[] = {
		0.50f,-0.08f,0.0f,
		0.64f, -0.08f, 0.0f
	};

	const GLfloat line1Color[]={
		1.0f, 0.647058823529f, 0.0f,
		1.0f, 0.647058823529f, 0.0f
	};
	const GLfloat line2Color[] = {
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};
	const GLfloat line3Color[] = {
		0.0705882352911f, 0.5333333f, 0.0f,
		0.0705882352911f, 0.5333333f, 0.0f,
	};
	//i1
	glGenVertexArrays(1, &vao_i1);
	glBindVertexArray(vao_i1);
	glGenBuffers(1, &vbo_position_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1), i1, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1Color), i1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//n
	glGenVertexArrays(1, &vao_n);
	glBindVertexArray(vao_n);
	glGenBuffers(1, &vbo_position_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(n), n, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//d
	glGenVertexArrays(1, &vao_d);
	glBindVertexArray(vao_d);
	glGenBuffers(1, &vbo_position_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(d), d, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//i2
	glGenVertexArrays(1, &vao_i2);
	glBindVertexArray(vao_i2);
	glGenBuffers(1, &vbo_position_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2), i2, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2Color), i2Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//a
	glGenVertexArrays(1, &vao_a);
	glBindVertexArray(vao_a);
	glGenBuffers(1, &vbo_position_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(a), a, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	//line1
	glGenVertexArrays(1, &vao_line1);
	glBindVertexArray(vao_line1);
	glGenBuffers(1, &vbo_position_line1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line1Vertices), line1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_line1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line1Color), line1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//line2
	glGenVertexArrays(1, &vao_line2);
	glBindVertexArray(vao_line2);
	glGenBuffers(1, &vbo_position_line2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line2Vertices), line2Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_line2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line2Color), line2Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//line3

	glGenVertexArrays(1, &vao_line3);
	glBindVertexArray(vao_line3);
	glGenBuffers(1, &vbo_position_line3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line3Vertices), line3Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_line3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line3Color), line3Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	perspectiveProjectionMatrix = mat4::identity();
	resize(WIN_WIDTH, WIN_HEIGHT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	return 0;
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	//I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(-0.6f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_i1);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//N
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(-0.3f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_n);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glBindVertexArray(0);

	//D
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_d);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glBindVertexArray(0);

	//I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.3f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_i2);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glBindVertexArray(0);

	//A
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.6f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_a);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glBindVertexArray(0);

	//Line1
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.2f, 0.0f, -2.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_line1);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//Line2
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.2f, 0.0f, -2.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_line2);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	//Line3
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.2f, 0.0f, -2.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_line3);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(gHdc);
}



void uninitialize()
{
	if (bIsFullScreen == true) {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Log File closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}


	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
}


