#include<Windows.h>
#include<stdio.h>

#include<d3d11.h>

#pragma comment(lib,"d3d11.lib")



#define WIN_WIDTH 800
#define WIN_HEIGHT 600


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE * gpFile_alk = NULL;

char gszLogFileName_alk[] = "Log.txt";


HWND gHwnd_alk = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_alk = false;
bool bIsFullScreen_alk = false;
bool gbEscapeKeyIsPressed_alk = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain_alk = NULL;
ID3D11Device *gpID3D11Device_alk = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext_alk = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView_alk = NULL;





int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	HRESULT initialize(void);

	void display();
	void uninitialize();

	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("Direct3D11");
	bool bDone = false;
	WNDCLASSEX wndclass;

	if (fopen_s(&gpFile_alk, gszLogFileName_alk, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile_alk, "Log File is successfully opened\n");
		fclose(gpFile_alk);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Direct3D11"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	gHwnd_alk = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	HRESULT hr;
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "initialize() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "initialize() succeeded");
		fclose(gpFile_alk);
	}


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{


			display();

			if (gbActiveWindow_alk == true)
			{
				if (gbEscapeKeyIsPressed_alk == true)
					bDone = true;
			}

		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullScreen();
	void uninitialize();
	HRESULT hr;
	switch (iMsg)
	{


	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow_alk = true;
		else
			gbActiveWindow_alk = false;
		break;

	case WM_ERASEBKGND:
		return(0);
		break;



	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
			if (bIsFullScreen_alk == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow_alk = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow_alk = false;
		break;
	case WM_SIZE:
		if (gpID3D11DeviceContext_alk)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));

			if (FAILED(hr))
			{
				fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
				fprintf_s(gpFile_alk, "resize() Failed. Exitting Now..\n");
				fclose(gpFile_alk);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
				fprintf_s(gpFile_alk, "resize() Succeeded. Exitting Now..\n");
				fclose(gpFile_alk);
			}
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen_alk == false)
	{
		dwStyle = GetWindowLong(gHwnd_alk, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd_alk, &wpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd_alk, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd_alk, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd_alk, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		bIsFullScreen_alk = true;
	}

	else
	{
		SetWindowLong(gHwnd_alk, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd_alk, &wpPrev);
		SetWindowPos(gHwnd_alk,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);

		ShowCursor(true);
		bIsFullScreen_alk = false;
	}
}

HRESULT initialize()
{
	HRESULT resize(int, int);
	void uninitialize();

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType_alk;
	D3D_DRIVER_TYPE d3dDriverTypes_alk[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes_alk) / sizeof(d3dDriverTypes_alk[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = gHwnd_alk;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType_alk = d3dDriverTypes_alk[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType_alk,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain_alk,
			&gpID3D11Device_alk,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext_alk
		);

		if (SUCCEEDED(hr))
			break;

	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "INitialize() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "INitialize() Succeeded. Exitting Now..\n");
		//fclose(gpFile_alk);

		if (d3dDriverType_alk == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile_alk, "Hardware Type\n.");
		}
		else if (d3dDriverType_alk == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile_alk, "Warp Type\n.");
		}
		else if (d3dDriverType_alk == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile_alk, "Reference Type\n.");
		}
		else
		{
			fprintf_s(gpFile_alk, "Unknown Type\n.");
		}

		fprintf_s(gpFile_alk, "The Supported Highest Feature Level Is");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile_alk, "11.0\n.");
		}

		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile_alk, "10.1\n.");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile_alk, "10.0\n.");
		}
		else
		{
			fprintf_s(gpFile_alk, "Unknown\n.");

		}

		fclose(gpFile_alk);
	}

	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0f;
	gClearColor[3] = 1.0f;


	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "resize() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "resize() Succeeded. Exitting Now..\n");
		fclose(gpFile_alk);
	}


	return(S_OK);

}


HRESULT resize(int width, int height)
{
	HRESULT hr;

	if (gpID3D11RenderTargetView_alk)
	{
		gpID3D11RenderTargetView_alk->Release();
		gpID3D11RenderTargetView_alk = NULL;
	}

	gpIDXGISwapChain_alk->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain_alk->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	hr = gpID3D11Device_alk->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_alk);

	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3DDevice::CreateRenderTargetView() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3DDevice::CreateRenderTargetView() Succeeded. Exitting Now..\n");
		fclose(gpFile_alk);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	gpID3D11DeviceContext_alk->OMSetRenderTargets(1, &gpID3D11RenderTargetView_alk, NULL);

	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0.0f;
	d3dViewPort.TopLeftY = 0.0f;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_alk->RSSetViewports(1, &d3dViewPort);

	return(hr);


}

void display()
{
	gpID3D11DeviceContext_alk->ClearRenderTargetView(gpID3D11RenderTargetView_alk, gClearColor);
	gpIDXGISwapChain_alk->Present(0, 0);

}



void uninitialize()
{
	if (gpID3D11RenderTargetView_alk)
	{
		gpID3D11RenderTargetView_alk->Release();
		gpID3D11RenderTargetView_alk = NULL;
	}

	if (gpIDXGISwapChain_alk)
	{
		gpIDXGISwapChain_alk->Release();
		gpIDXGISwapChain_alk = NULL;
	}

	if (gpID3D11DeviceContext_alk)
	{
		gpID3D11DeviceContext_alk->Release();
		gpID3D11DeviceContext_alk = NULL;
	}
	if (gpID3D11Device_alk)
	{
		gpID3D11Device_alk->Release();
		gpID3D11Device_alk = NULL;
	}
	if (gpFile_alk)
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "uninitialize() Succeeded");
		fprintf_s(gpFile_alk, "Log File closed successfully");
		fclose(gpFile_alk);
		gpFile_alk = NULL;
	}

}


