#include<Windows.h>
#include<stdio.h>

#include<d3d11.h>
#include<d3dcompiler.h>

#pragma warning(disable:4838)
#include"xnamath.h"
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"D3dcompiler.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

FILE * gpFile_alk = NULL;

char gszLogFileName_alk[] = "Log.txt";


HWND gHwnd_alk = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow_alk = false;
bool bIsFullScreen_alk = false;
bool gbEscapeKeyIsPressed_alk = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain_alk = NULL;
ID3D11Device *gpID3D11Device_alk = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext_alk = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView_alk = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11HullShader *gpID3D11HullShader = NULL;
ID3D11DomainShader *gpID3D11DomainShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_vertexBuffer_position_triangle = NULL;
ID3D11Buffer *gpID3D11Buffer_vertexBuffer_position_rectangle = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_HullShader = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_DomainShader = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;

struct CBUFFER_HULL_SHADER
{
	XMVECTOR Hull_Constant_FUnction_Params;
};
struct CBUFFER_DOMAIN_SHADER
{
	XMMATRIX WorldViewProjectionMatrix;
};

struct CBUFFER_PIXEL_SHADER
{
	XMVECTOR LineColor;
};

XMMATRIX gPerspectiveProjectionMatrix;
float gAngleTriangle = 0.0f;

unsigned int guiNumberOfLineSegments = 1;


ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	HRESULT initialize(void);

	void display();
	void uninitialize();
	void Update();
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("Color Triangle");
	bool bDone = false;
	WNDCLASSEX wndclass;

	if (fopen_s(&gpFile_alk, gszLogFileName_alk, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile_alk, "Log File is successfully opened\n");
		fclose(gpFile_alk);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("2-2D Shapes(Black And White)"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	gHwnd_alk = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	HRESULT hr;
	hr = initialize();

	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "initialize() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}

	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "initialize() succeeded");
		fclose(gpFile_alk);
	}


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{

			Update();
			display();

			if (gbActiveWindow_alk == true)
			{
				if (gbEscapeKeyIsPressed_alk == true)
					bDone = true;
			}

		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void ToggleFullScreen();
	void uninitialize();
	HRESULT hr;
	switch (iMsg)
	{


	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow_alk = true;
		else
			gbActiveWindow_alk = false;
		break;

	case WM_ERASEBKGND:
		return(0);
		break;



	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
			if (bIsFullScreen_alk == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case VK_UP:
			guiNumberOfLineSegments++;
			if (guiNumberOfLineSegments >= 50)
				guiNumberOfLineSegments = 50;
			break;
		case VK_DOWN:
			guiNumberOfLineSegments--;
			if (guiNumberOfLineSegments <= 0)
				guiNumberOfLineSegments = 1;
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow_alk = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow_alk = false;
		break;
	case WM_SIZE:
		if (gpID3D11DeviceContext_alk)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));

			if (FAILED(hr))
			{
				fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
				fprintf_s(gpFile_alk, "resize() Failed. Exitting Now..\n");
				fclose(gpFile_alk);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
				fprintf_s(gpFile_alk, "resize() Succeeded. Exitting Now..\n");
				fclose(gpFile_alk);
			}
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen_alk == false)
	{
		dwStyle = GetWindowLong(gHwnd_alk, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd_alk, &wpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd_alk, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd_alk, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd_alk, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		bIsFullScreen_alk = true;
	}

	else
	{
		SetWindowLong(gHwnd_alk, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd_alk, &wpPrev);
		SetWindowPos(gHwnd_alk,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);

		ShowCursor(true);
		bIsFullScreen_alk = false;
	}
}

HRESULT initialize()
{
	HRESULT resize(int, int);
	void uninitialize();

	HRESULT hr;

	D3D_DRIVER_TYPE d3dDriverType_alk;
	D3D_DRIVER_TYPE d3dDriverTypes_alk[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	numDriverTypes = sizeof(d3dDriverTypes_alk) / sizeof(d3dDriverTypes_alk[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = gHwnd_alk;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType_alk = d3dDriverTypes_alk[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType_alk,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain_alk,
			&gpID3D11Device_alk,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext_alk
		);

		if (SUCCEEDED(hr))
			break;

	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "Initialize() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		return(hr);
	}

	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "Initialize() Succeeded. Exitting Now..\n");
		//fclose(gpFile_alk);

		if (d3dDriverType_alk == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile_alk, "Hardware Type\n.");
		}
		else if (d3dDriverType_alk == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile_alk, "Warp Type\n.");
		}
		else if (d3dDriverType_alk == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile_alk, "Reference Type\n.");
		}
		else
		{
			fprintf_s(gpFile_alk, "Unknown Type\n.");
		}

		fprintf_s(gpFile_alk, "The Supported Highest Feature Level Is");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile_alk, "11.0\n.");
		}

		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile_alk, "10.1\n.");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile_alk, "10.0\n.");
		}
		else
		{
			fprintf_s(gpFile_alk, "Unknown\n.");

		}

		fclose(gpFile_alk);
	}


	const char *vertexShaderSourceCode =
		"struct vertex_output"\
		"{"\
		"float4 position:POSITION;"\
		"};"\
		"vertex_output main(float2 pos:POSITION)"\
		"{"\
		"vertex_output output;"\
		"output.position=float4(pos,0.0f,1.0f);"\
		"return(output);"\
		"}";

	ID3DBlob *pID3Blob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3Blob_VertexShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "D3DCompile() Failed For Vertex Shader :%s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile_alk);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "D3DCompile() Succeeded For Vertex Shader .\n");
		fclose(gpFile_alk);
	}

	hr = gpID3D11Device_alk->CreateVertexShader(pID3Blob_VertexShaderCode->GetBufferPointer(),
		pID3Blob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Failed For Vertex Shader .\n");
			fclose(gpFile_alk);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Succeeded For Vertex Shader .\n");
		fclose(gpFile_alk);
	}

	gpID3D11DeviceContext_alk->VSSetShader(gpID3D11VertexShader, 0, 0);

	//
	const char *hullShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4 hull_constant_function_params;"\
		"}"\
		"struct vertex_output"\
		"{"
		"float4 position:POSITION;"\
		"};"\
		"struct hull_constant_output"\
		"{"\
		"float edges[2]:SV_TESSFACTOR;"\
		"};"\
		"hull_constant_output hull_constant_function(void)"\
		"{"\
		"hull_constant_output output;"\
		"float numberOfStrips = hull_constant_function_params[0];"\
		"float numberOfSegments=hull_constant_function_params[1];"\
		"output.edges[0]=numberOfStrips;"\
		"output.edges[1]=numberOfSegments;"\
		"return(output);"\
		"}"\
		"struct hull_output"\
		"{"\
		"float4 position:POSITION;"\
		"};"\
		"[domain(\"isoline\")]"\
		"[partitioning(\"integer\")]"\
		"[outputtopology(\"line\")]"\
		"[outputcontrolpoints(4)]"\
		"[patchconstantfunc(\"hull_constant_function\")]"\
		"hull_output main(InputPatch<vertex_output,4> input_patch,uint i:SV_OUTPUTCONTROLPOINTID)"\
		"{"\
		"hull_output output;"\
		"output.position=input_patch[i].position;"\
		"return(output);"\
		"}";


	ID3DBlob *pID3Blob_HullShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(hullShaderSourceCode,
		lstrlenA(hullShaderSourceCode) + 1,
		"HS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"hs_5_0",
		0,
		0,
		&pID3Blob_HullShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "D3DCompile() Failed For HULL Shader :%s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile_alk);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "D3DCompile() Succeeded For HULL Shader .\n");
		fclose(gpFile_alk);
	}

	hr = gpID3D11Device_alk->CreateHullShader(pID3Blob_HullShaderCode->GetBufferPointer(),
		pID3Blob_HullShaderCode->GetBufferSize(), NULL, &gpID3D11HullShader);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Failed For HULL Shader .\n");
			fclose(gpFile_alk);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Succeeded For HULL Shader .\n");
		fclose(gpFile_alk);
	}

	gpID3D11DeviceContext_alk->HSSetShader(gpID3D11HullShader, 0, 0);
	pID3Blob_HullShaderCode->Release();
	pID3Blob_HullShaderCode = NULL;

	//
	const char *domainShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4x4 worldViewProjectionMatrix;"\
		"}"\
		"struct hull_constant_output"\
		"{"\
		"float edges[2]:SV_TESSFACTOR;"\
		"};"\
		"struct hull_output"\
		"{"
		"float4 position:POSITION;"\
		"};"\
		"struct domain_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"};"\
		"[domain(\"isoline\")]"\
		"domain_output main(hull_constant_output input,OutputPatch<hull_output,4> output_patch,float2 tessCoord:SV_DOMAINLOCATION)"\
		"{"\
		"domain_output output;"\
		"float u=tessCoord.x;"\
		"float3 p0=output_patch[0].position.xyz;"\
		"float3 p1=output_patch[1].position.xyz;"\
		"float3 p2=output_patch[2].position.xyz;"\
		"float3 p3=output_patch[3].position.xyz;"\
		"float u1=(1.0f-u);"\
		"float u2=u*u;"\
		"float b3=u2*u;"\
		"float b2=3.0f*u2*u1;"\
		"float b1=3.0f*u*u1*u1;"\
		"float b0=u1*u1*u1;"\
		"float3 p=p0*b0+p1*b1+p2*b2+p3*b3;"\
		"output.position=mul(worldViewProjectionMatrix,float4(p,1.0f));"\
		"return(output);"\
		"}";



	ID3DBlob *pID3Blob_domainShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(domainShaderSourceCode,
		lstrlenA(domainShaderSourceCode) + 1,
		"DS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ds_5_0",
		0,
		0,
		&pID3Blob_domainShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "D3DCompile() Failed For Domain Shader :%s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile_alk);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "D3DCompile() Succeeded For Domain Shader .\n");
		fclose(gpFile_alk);
	}

	hr = gpID3D11Device_alk->CreateDomainShader(pID3Blob_domainShaderCode->GetBufferPointer(),
		pID3Blob_domainShaderCode->GetBufferSize(), NULL, &gpID3D11DomainShader);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Failed For Domain Shader .\n");
			fclose(gpFile_alk);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Succeeded For Domain Shader .\n");
		fclose(gpFile_alk);
	}

	gpID3D11DeviceContext_alk->DSSetShader(gpID3D11DomainShader, 0, 0);
	pID3Blob_domainShaderCode->Release();
	pID3Blob_domainShaderCode = NULL;


	//
	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer"\
		"{"\
		"float4 lineColor;"\
		"}"\
		"float4 main(void):SV_TARGET"\
		"{"\
		"float4 color;"\
		"color=lineColor;"\
		"return(color);"\
		"}";



	ID3DBlob *pID3Blob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3Blob_PixelShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "D3DCompile() Failed For Pixel Shader :%s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile_alk);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "D3DCompile() Succeeded For Pixel Shader .\n");
		fclose(gpFile_alk);
	}

	hr = gpID3D11Device_alk->CreatePixelShader(pID3Blob_PixelShaderCode->GetBufferPointer(),
		pID3Blob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
			fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Failed For Pixel Shader .\n");
			fclose(gpFile_alk);
		}
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateVertexShader() Succeeded For Pixel Shader .\n");
		fclose(gpFile_alk);
	}

	gpID3D11DeviceContext_alk->PSSetShader(gpID3D11PixelShader, 0, 0);
	pID3Blob_PixelShaderCode->Release();
	pID3Blob_PixelShaderCode=NULL;


	float vertices[] =
	{
		-1.0f,-1.0f,-0.5f,1.0f,0.5f,-1.0f,1.0f,1.0f
	};

	

	D3D11_BUFFER_DESC bufferDesc_Vertexbuffer_position_triangle;
	ZeroMemory(&bufferDesc_Vertexbuffer_position_triangle, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_Vertexbuffer_position_triangle.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_Vertexbuffer_position_triangle.ByteWidth = sizeof(float)*_ARRAYSIZE(vertices);
	bufferDesc_Vertexbuffer_position_triangle.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_Vertexbuffer_position_triangle.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device_alk->CreateBuffer(&bufferDesc_Vertexbuffer_position_triangle,
		NULL, &gpID3D11Buffer_vertexBuffer_position_triangle);

	if (FAILED(hr))
	{

		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer Position.\n");
		fclose(gpFile_alk);

	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer Position.\n");
		fclose(gpFile_alk);
	}


	

	D3D11_MAPPED_SUBRESOURCE mappedSubresource_vertex_triangle;

	ZeroMemory(&mappedSubresource_vertex_triangle, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext_alk->Map(gpID3D11Buffer_vertexBuffer_position_triangle, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource_vertex_triangle);
	memcpy(mappedSubresource_vertex_triangle.pData, vertices, sizeof(vertices));
	gpID3D11DeviceContext_alk->Unmap(gpID3D11Buffer_vertexBuffer_position_triangle, NULL);



	D3D11_INPUT_ELEMENT_DESC inputElementDesc;

	inputElementDesc.SemanticName = "POSITION";
	inputElementDesc.SemanticIndex = 0;
	inputElementDesc.Format = DXGI_FORMAT_R32G32_FLOAT;
	inputElementDesc.InputSlot = 0;
	inputElementDesc.AlignedByteOffset = 0;
	inputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc.InstanceDataStepRate = 0;



	hr = gpID3D11Device_alk->CreateInputLayout(&inputElementDesc, 1,
		pID3Blob_VertexShaderCode->GetBufferPointer(), pID3Blob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);

	if (FAILED(hr))
	{

		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateInputLayout() Failed For Vertex Buffer .\n");
		fclose(gpFile_alk);

	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateInputLayout() Succeeded For Vertex Buffer .\n");
		fclose(gpFile_alk);
	}

	gpID3D11DeviceContext_alk->IASetInputLayout(gpID3D11InputLayout);
	pID3Blob_VertexShaderCode->Release();
	pID3Blob_VertexShaderCode = NULL;
	

	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER_HULL_SHADER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device_alk->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer_HullShader);


	if (FAILED(hr))
	{

		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Failed For Constant Buffer Hull Shader.\n");
		fclose(gpFile_alk);

	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer Hull Shader.\n");
		fclose(gpFile_alk);
	}


	gpID3D11DeviceContext_alk->HSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_HullShader);

	//

	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER_DOMAIN_SHADER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device_alk->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer_DomainShader);


	if (FAILED(hr))
	{

		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Failed For Constant Buffer Domain Shader.\n");
		fclose(gpFile_alk);

	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer Domain Shader.\n");
		fclose(gpFile_alk);
	}


	gpID3D11DeviceContext_alk->DSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_DomainShader);

	//

	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER_PIXEL_SHADER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device_alk->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer_PixelShader);


	if (FAILED(hr))
	{

		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Failed For Constant Buffer Pixel Shader.\n");
		fclose(gpFile_alk);

	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer Pixel Shader.\n");
		fclose(gpFile_alk);
	}


	gpID3D11DeviceContext_alk->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer_PixelShader);


	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;


	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	hr = gpID3D11Device_alk->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{

		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateRasterizerState() Failed For Culling.\n");
		fclose(gpFile_alk);

	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3D11Device::CreateRasterizerState() Succeeded For Culling .\n");
		fclose(gpFile_alk);
	}

	gpID3D11DeviceContext_alk->RSSetState(gpID3D11RasterizerState);

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "resize() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "resize() Succeeded. Exitting Now..\n");
		fclose(gpFile_alk);
	}



	return(S_OK);

}


HRESULT resize(int width, int height)
{

	HRESULT hr;

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView_alk)
	{
		gpID3D11RenderTargetView_alk->Release();
		gpID3D11RenderTargetView_alk = NULL;
	}



	gpIDXGISwapChain_alk->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain_alk->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	hr = gpID3D11Device_alk->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_alk);

	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3DDevice::CreateRenderTargetView() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3DDevice::CreateRenderTargetView() Succeeded. Exitting Now..\n");
		fclose(gpFile_alk);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
	gpID3D11Device_alk->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);


	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	hr = gpID3D11Device_alk->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc,
		&gpID3D11DepthStencilView);


	if (FAILED(hr))
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3DDevice::CreateDepthStencilView() Failed. Exitting Now..\n");
		fclose(gpFile_alk);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "ID3DDevice::CreateDepthStencilView() Succeeded. Exitting Now..\n");
		fclose(gpFile_alk);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	gpID3D11DeviceContext_alk->OMSetRenderTargets(1, &gpID3D11RenderTargetView_alk, gpID3D11DepthStencilView);

	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_alk->RSSetViewports(1, &d3dViewPort);



	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return(hr);


}

void display()
{
	gpID3D11DeviceContext_alk->ClearRenderTargetView(gpID3D11RenderTargetView_alk, gClearColor);
	gpID3D11DeviceContext_alk->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	UINT stride = sizeof(float) * 2;
	UINT offset = 0;
	gpID3D11DeviceContext_alk->IASetVertexBuffers(0, 1, &gpID3D11Buffer_vertexBuffer_position_triangle,
		&stride, &offset);


	gpID3D11DeviceContext_alk->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);

	XMMATRIX worldMatrix = XMMatrixTranslation(0.0f,0.0f,4.0f);
	XMMATRIX viewMatrix = XMMatrixIdentity();

	XMMATRIX wvpMatrix = worldMatrix *viewMatrix*gPerspectiveProjectionMatrix;

	CBUFFER_DOMAIN_SHADER constantBuffer_domainShader;
	constantBuffer_domainShader.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext_alk->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_DomainShader, 0,
		NULL, &constantBuffer_domainShader, 0, 0);

	CBUFFER_HULL_SHADER constantBuffer_hullShader;
	constantBuffer_hullShader.Hull_Constant_FUnction_Params = XMVectorSet(1.0f,(FLOAT)guiNumberOfLineSegments,0.0f,0.0f);

	gpID3D11DeviceContext_alk->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_HullShader, 0,
		NULL, &constantBuffer_hullShader, 0, 0);

	TCHAR str[255];
	wsprintf(str, TEXT("DirectD3D11 Window[Segments =%2d]"),guiNumberOfLineSegments);
	SetWindowText(gHwnd_alk,str);


	CBUFFER_PIXEL_SHADER constantBuffer_pixelShader;
	constantBuffer_pixelShader.LineColor = XMVectorSet(1.0f, 1.0f,0.0f,1.0f);

	gpID3D11DeviceContext_alk->UpdateSubresource(gpID3D11Buffer_ConstantBuffer_PixelShader, 0,
		NULL, &constantBuffer_pixelShader, 0, 0);

	
	gpID3D11DeviceContext_alk->Draw(4, 0);


	

	gpIDXGISwapChain_alk->Present(0, 0);

}

void Update()
{
	gAngleTriangle = gAngleTriangle + 0.001f;

	if (gAngleTriangle > 360.0f)
	{
		gAngleTriangle = 0.0f;
	}
}

void uninitialize()
{

	if (gpID3D11Buffer_ConstantBuffer_DomainShader)
	{
		gpID3D11Buffer_ConstantBuffer_DomainShader->Release();
		gpID3D11Buffer_ConstantBuffer_DomainShader = NULL;
	}
	if (gpID3D11Buffer_ConstantBuffer_HullShader)
	{
		gpID3D11Buffer_ConstantBuffer_HullShader->Release();
		gpID3D11Buffer_ConstantBuffer_HullShader = NULL;
	}
	if (gpID3D11Buffer_ConstantBuffer_PixelShader)
	{
		gpID3D11Buffer_ConstantBuffer_PixelShader->Release();
		gpID3D11Buffer_ConstantBuffer_PixelShader = NULL;
	}
	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_vertexBuffer_position_rectangle)
	{
		gpID3D11Buffer_vertexBuffer_position_rectangle->Release();
		gpID3D11Buffer_vertexBuffer_position_rectangle = NULL;
	}

	if (gpID3D11Buffer_vertexBuffer_position_triangle)
	{
		gpID3D11Buffer_vertexBuffer_position_triangle->Release();
		gpID3D11Buffer_vertexBuffer_position_triangle = NULL;
	}


	if (gpID3D11RenderTargetView_alk)
	{
		gpID3D11RenderTargetView_alk->Release();
		gpID3D11RenderTargetView_alk = NULL;
	}

	if (gpIDXGISwapChain_alk)
	{
		gpIDXGISwapChain_alk->Release();
		gpIDXGISwapChain_alk = NULL;
	}

	if (gpID3D11DeviceContext_alk)
	{
		gpID3D11DeviceContext_alk->Release();
		gpID3D11DeviceContext_alk = NULL;
	}
	if (gpID3D11Device_alk)
	{
		gpID3D11Device_alk->Release();
		gpID3D11Device_alk = NULL;
	}
	if (gpFile_alk)
	{
		fopen_s(&gpFile_alk, gszLogFileName_alk, "a+");
		fprintf_s(gpFile_alk, "uninitialize() Succeeded");
		fprintf_s(gpFile_alk, "Log File closed successfully");
		fclose(gpFile_alk);
		gpFile_alk = NULL;
	}

}


