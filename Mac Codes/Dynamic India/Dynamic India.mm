#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"


enum
{
AMC_ATTRIBUTE_POSITION = 0,
AMC_ATTRIBUTE_COLOR,
AMC_ATTRIBUTE_NORMAL,
AMC_ATTRIBUTE_TEXCOORD0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *gpFile=NULL;

GLfloat transform_i1=-2.0f;
GLfloat transform_i2=-5.5f;
GLfloat transform_n=3.5f;
GLfloat transform_d=0.0f;
GLfloat transform_a=7.3f;


GLfloat planex = -4.0f;
GLfloat planey = -4.0f;
GLfloat planex1 = -4.0f;
GLfloat planey1 = 4.0f;
GLfloat planex2 = -4.0f;
GLfloat planez = -3.0f;
GLfloat planez1 = -3.0f;
GLfloat planez2 = -3.0f;
GLfloat planex3 = 0.9999999999999597f;
GLfloat planey3 = 0.0f;
GLfloat planex4 = 0.9999999999999597f;
GLfloat planey4 = 0.0f;
int flagp = 0;
int flagp3 = 0;

int flagd = 0;
GLfloat linex1 = -4.7f;
GLfloat linex2 = -5.25f;
GLfloat linex3 = -4.4f;

GLfloat linez = -2.0f;

GLfloat flag4 = 0;



@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
	
	NSApp=[NSApplication sharedApplication];
	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];
	[pPool release];
	return(0);
}

@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	

	// log file

	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	
	if(gpFile==NULL)
	{
		printf("Cannot create log file\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile,"Program is started successfully");


	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	window=[[NSWindow alloc] initWithContentRect:win_rect
					styleMask:NSWindowStyleMaskTitled |
				NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
				 NSWindowStyleMaskResizable
						backing:NSBackingStoreBuffered
						defer:NO];
	[window setTitle:@"Dynamic India"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView]; 
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile,"Program is terminated successfully");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}

	//code
}

-(void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint vao_i1;
	GLuint vao_i2;
	GLuint vao_n;
GLuint vao_d;
GLuint vao_a;
GLuint vao_line1;
GLuint vao_line2;
GLuint vao_line3;
GLuint vbo_position_i1;
GLuint vbo_position_i2;
GLuint vbo_position_n;
GLuint vbo_position_d;
GLuint vbo_position_a;
GLuint vbo_position_line1;
GLuint vbo_position_line2;
GLuint vbo_position_line3;
GLuint vbo_color_i1;
GLuint vbo_color_i2;
GLuint vbo_color_n;
GLuint vbo_color_d;
GLuint vbo_color_a;
GLuint vbo_color_line1;
GLuint vbo_color_line2;
GLuint vbo_color_line3;
    
    GLuint vao_apex;
    GLuint vbo_position_apex;
    GLuint vbo_color_apex;
    
    GLuint vao_iafquad;
    GLuint vbo_position_iafquad;
    GLuint vbo_color_iafquad;
    GLuint vao_iafbquad;
    GLuint vbo_position_iafbquad;
    GLuint vbo_color_iafbquad;
    GLuint vao_biggerwings1;
    GLuint vbo_position_biggerwings1;
    GLuint vbo_color_biggerwings1;
    GLuint vao_biggerwings2;
    GLuint vbo_position_biggerwings2;
    GLuint vbo_color_biggerwings2;
    GLuint vao_smallerwings1;
    GLuint vbo_position_smallerwings1;
    GLuint vbo_color_smallerwings1;
    GLuint vao_smallerwings2;
    GLuint vbo_position_smallerwings2;
    GLuint vbo_color_smallerwings2;
    GLuint vao_pa;
    GLuint vbo_position_pa;
    GLuint vbo_color_pa;
    
    GLuint vao_pf;
    GLuint vbo_position_pf;
    GLuint vbo_color_pf;
    
    GLuint vao_pi;
    GLuint vbo_position_pi;
    GLuint vbo_color_pi;
    
	GLuint mvpUniform;
	vmath::mat4 perspectiveProjectionMatrix;

}

-(id)initWithFrame:(NSRect)frame;
{
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};//last 0 is must
		
		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		
		if(pixelFormat==nil)
		{
			fprintf(gpFile,"\nNo Valid OpenGL Pixel Format Is Available");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			
		[self setPixelFormat:pixelFormat];
		
		[self setOpenGLContext:glContext];

	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime

{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	
	return(kCVReturnSuccess);
}


-(void)prepareOpenGL
{
	fprintf(gpFile,"OpenGL Version : %s\n",glGetString(GL_VERSION));
	fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar* vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_color=vColor;"
		"}";
		
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char *szInfolog=NULL;
	
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	szInfolog=NULL;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
	"#version 410" \
	"\n" \
	"in vec4 out_color;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
	"FragColor=out_color;" \
	"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);


	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus=0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

	const GLfloat i1[] = 
	{ -0.70f, 0.80f, 0.0f,

	- 0.72f, 0.80f, 0.0f,

	- 0.72f,-0.80f, 0.0f,

	- 0.70f, -0.80f, 0.0f,
	};
	const GLfloat i2[] = {
	0.20f, 0.80f, 0.0f,

	0.22f, 0.80f, 0.0f,

	0.22f, -0.80f, 0.0f,

	0.20f, -0.80f, 0.0f

	};
	const GLfloat n[] = {
	-0.52f, 0.80f, 0.0f,

	- 0.50f, 0.80f, 0.0f,

	- 0.50f, -0.80f, 0.0f,

	- 0.52f, -0.80f, 0.0f,


	- 0.50f, 0.80f, 0.0f,

	- 0.48f, 0.80f, 0.0f,

	- 0.36f, -0.80f, 0.0f,
		
	- 0.38f, -0.80f, 0.0f,


	- 0.38f, 0.80f, 0.0f,

	- 0.36f, 0.80f, 0.0f,

	- 0.36f, -0.80f, 0.0f,

	- 0.38f, -0.80f, 0.0f,

	};
	const GLfloat d[] = { -0.16f, 0.78f, 0.0f,

	- 0.14f, 0.78f, 0.0f,

	- 0.14f, -0.78f, 0.0f,

	- 0.16f, -0.78f, 0.0f,



	-0.16f, 0.78f, 0.0f,

	- 0.16f, 0.80f, 0.0f,

	0.0f, 0.80f, 0.0f,

	0.0f, 0.78f, 0.0f,


	- 0.16f, -0.80f, 0.0f,

	- 0.16f, -0.78f, 0.0f,

	0.0f, -0.78f, 0.0f,

	0.0f, -0.80f, 0.0f,



	-0.02f, 0.78f, 0.0f,

	0.0f, 0.78f, 0.0f,

	0.0f, -0.78f, 0.0f,

	- 0.02f, -0.78f, 0.0f
	};
	const GLfloat a[] = { 
	0.40f, -0.80f, 0.0f,

	0.42f, -0.80f, 0.0f,

	0.52f, 0.80f, 0.0f,

	0.50f, 0.80f, 0.0f,


	0.72f, -0.80f, 0.0f,

	0.74f, -0.80f, 0.0f,

	0.54f, 0.80f, 0.0f,

	0.52f, 0.80f, 0.0f
	};
	const GLfloat i1Color[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f };
	const GLfloat i2Color[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f };
	const GLfloat nColor[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f ,
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
	};
	const GLfloat dColor[] = { 
	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,



	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f
	};
	const GLfloat aColor[] = { 0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,


	0.0705882352911f, 0.5333333f, 0.0f,

	0.0705882352911f, 0.5333333f, 0.0f,

	1.0f, 0.647058823529f, 0.0f,

	1.0f, 0.647058823529f, 0.0f };
	
	const GLfloat line1Vertices[] = {
		0.50f,0.0f,0.0f,
		0.62,0.0f,0.0f
	};

	const GLfloat line2Vertices[] = {
		0.50f, -0.04f, 0.0f,
		0.63f, -0.04f, 0.0f
	};

	const GLfloat line3Vertices[] = {
		0.50f,-0.08f,0.0f,
		0.64f, -0.08f, 0.0f
	};

	const GLfloat line1Color[]={
		1.0f, 0.647058823529f, 0.0f,
		1.0f, 0.647058823529f, 0.0f
	};
	const GLfloat line2Color[] = {
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
	};
	const GLfloat line3Color[] = {
		0.0705882352911f, 0.5333333f, 0.0f,
		0.0705882352911f, 0.5333333f, 0.0f,
	};
    
    const GLfloat apexVertices[] = { 0.60f, 0.0f, 0.0f,
        0.50f, -0.20f, 0.0f,
        0.50f, 0.20f, 0.0f,
    };
    const GLfloat apexColor[] = { 0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f };
    const GLfloat biafColor[] = { 0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f };
    const GLfloat biafVertices[] = { 0.50f, -0.20f, 0.0f,
        -0.10f, -0.20f, 0.0f,
        -0.10f, 0.20f, 0.0f,
        0.50f, 0.20f, 0.0f, };
    const GLfloat iafquad[] = { -0.10f, 0.15f, 0.0f,
        -0.10f, -0.15f, 0.0f,
        -1.25f, -0.15f, 0.0f,
        -1.25f, 0.15f, 0.0f, };
    
    const GLfloat iafColor[] = {
        
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f
    };
    const GLfloat bigVertices1[] = {
        -0.25f,0.15f,0.0f,
        -0.35f,0.65f,0.0f,
        -0.55f,0.65f,0.0f,
        -0.55f,0.15f,0.0f
    };
    const GLfloat bigColor1[] = {
        
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f
    };
    
    const GLfloat bigVertices2[] = {
        -0.25f,-0.15f,0.0f,
        -0.35f,-0.65f,0.0f,
        -0.55f,-0.65f,0.0f,
        -0.55f,-0.15f,0.0f
    };
    
    const GLfloat bigColor2[] = {
        
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f
    };
    const GLfloat smallVertices1[] = {
        -1.0f, 0.15f, 0.0f,
        -1.05f, 0.45f, 0.0f,
        -1.10f, 0.45f, 0.0f,
        -1.10f, 0.15f, 0.0f,
    };
    const GLfloat smallColor1[] = {
        
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f
    };
    const GLfloat smallVertices2[] = {
        -1.0f, -0.15f, 0.0f,
        -1.05f, -0.45f, 0.0f,
        -1.10f, -0.45f, 0.0f,
        -1.10f, -0.15f, 0.0f,
    };
    
    const GLfloat smallColor2[] = {
        
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f,
        0.729411f, 0.8627f, 0.933f
    };
    
    //IAF
    
    const GLfloat piPosition[] = {
        -0.65f, 0.12f, 0.0f,
        -0.65f, -0.12f, 0.0f
    };
    
    const GLfloat piColor[] = {
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f
    };
    
    const GLfloat paPosition[] = {
        -0.50f, -0.12f, 0.0f,
        -0.40f, 0.12f, 0.0f,
        -0.40f, 0.12f, 0.0f,
        -0.30f, -0.12f, 0.0f,
        -0.45f, 0.02f, 0.0f,
        -0.35f, 0.02f, 0.0f
    };
    
    const GLfloat paColor[] = {
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f
    };
    
    const GLfloat pfPosition[] = {
        -0.15f, 0.12f, 0.0f,
        -0.15f, -0.12f, 0.0f,
        -0.15f, 0.12f, 0.0f,
        -0.04f, 0.12f, 0.0f,
        -0.15f, 0.04f, 0.0f,
        -0.06f, 0.04f, 0.0f
    };
    
    const GLfloat pfColor[] = {
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f
    };
    
    //
    glGenVertexArrays(1, &vao_pi);
    glBindVertexArray(vao_pi);
    glGenBuffers(1, &vbo_position_pi);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pi);
    glBufferData(GL_ARRAY_BUFFER, sizeof(piPosition), piPosition, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_pi);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_pi);
    glBufferData(GL_ARRAY_BUFFER, sizeof(piColor), piColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    glGenVertexArrays(1, &vao_pa);
    glBindVertexArray(vao_pa);
    glGenBuffers(1, &vbo_position_pa);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pa);
    glBufferData(GL_ARRAY_BUFFER, sizeof(paPosition), paPosition, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_pa);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_pa);
    glBufferData(GL_ARRAY_BUFFER, sizeof(paColor), paColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    
    glGenVertexArrays(1, &vao_pf);
    glBindVertexArray(vao_pf);
    glGenBuffers(1, &vbo_position_pf);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pfPosition), pfPosition, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_pf);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_pf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pfColor), pfColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    
    glGenVertexArrays(1, &vao_apex);
    glBindVertexArray(vao_apex);
    glGenBuffers(1, &vbo_position_apex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_apex);
    glBufferData(GL_ARRAY_BUFFER, sizeof(apexVertices), apexVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_apex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_apex);
    glBufferData(GL_ARRAY_BUFFER, sizeof(apexColor), apexColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    glGenVertexArrays(1, &vao_iafbquad);
    glBindVertexArray(vao_iafbquad);
    glGenBuffers(1, &vbo_position_iafbquad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_iafbquad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(biafVertices), biafVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_iafbquad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_iafbquad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(biafColor), biafColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    glGenVertexArrays(1, &vao_iafquad);
    glBindVertexArray(vao_iafquad);
    glGenBuffers(1, &vbo_position_iafquad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_iafquad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(iafquad), iafquad, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_iafquad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_iafquad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(iafColor), iafColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    glGenVertexArrays(1, &vao_iafquad);
    glBindVertexArray(vao_iafquad);
    glGenBuffers(1, &vbo_position_iafquad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_iafquad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(iafquad), iafquad, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_iafquad);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_iafquad);
    glBufferData(GL_ARRAY_BUFFER, sizeof(iafColor), iafColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    glGenVertexArrays(1, &vao_biggerwings1);
    glBindVertexArray(vao_biggerwings1);
    glGenBuffers(1, &vbo_position_biggerwings1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_biggerwings1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bigVertices1), bigVertices1, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_biggerwings1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_biggerwings1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bigColor1), bigColor1, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    glGenVertexArrays(1, &vao_biggerwings2);
    glBindVertexArray(vao_biggerwings2);
    glGenBuffers(1, &vbo_position_biggerwings2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_biggerwings2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bigVertices2), bigVertices2, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_color_biggerwings2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_biggerwings2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bigColor2), bigColor2, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    
    glGenVertexArrays(1, &vao_smallerwings1);
    glBindVertexArray(vao_smallerwings1);
    glGenBuffers(1, &vbo_position_smallerwings1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smallerwings1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(smallVertices1), smallVertices1, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_position_smallerwings1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smallerwings1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(smallColor1), smallColor1, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //
    
    glGenVertexArrays(1, &vao_smallerwings2);
    glBindVertexArray(vao_smallerwings2);
    glGenBuffers(1, &vbo_position_smallerwings2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smallerwings2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(smallVertices2), smallVertices2, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glGenBuffers(1, &vbo_position_smallerwings2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smallerwings2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(smallColor2), smallColor2, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    
	//i1
	glGenVertexArrays(1, &vao_i1);
	glBindVertexArray(vao_i1);
	glGenBuffers(1, &vbo_position_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1), i1, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_i1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i1Color), i1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//n
	glGenVertexArrays(1, &vao_n);
	glBindVertexArray(vao_n);
	glGenBuffers(1, &vbo_position_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(n), n, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_n);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_n);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//d
	glGenVertexArrays(1, &vao_d);
	glBindVertexArray(vao_d);
	glGenBuffers(1, &vbo_position_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(d), d, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_d);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_d);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//i2
	glGenVertexArrays(1, &vao_i2);
	glBindVertexArray(vao_i2);
	glGenBuffers(1, &vbo_position_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2), i2, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_i2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_i2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(i2Color), i2Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//a
	glGenVertexArrays(1, &vao_a);
	glBindVertexArray(vao_a);
	glGenBuffers(1, &vbo_position_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(a), a, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_a);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_a);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	//line1
	glGenVertexArrays(1, &vao_line1);
	glBindVertexArray(vao_line1);
	glGenBuffers(1, &vbo_position_line1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line1Vertices), line1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_line1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line1Color), line1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//line2
	glGenVertexArrays(1, &vao_line2);
	glBindVertexArray(vao_line2);
	glGenBuffers(1, &vbo_position_line2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line2Vertices), line2Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_line2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line2Color), line2Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//line3

	glGenVertexArrays(1, &vao_line3);
	glBindVertexArray(vao_line3);
	glGenBuffers(1, &vbo_position_line3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line3Vertices), line3Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_line3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_line3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line3Color), line3Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect=[self bounds];
	
	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
		perspectiveProjectionMatrix=vmath::perspective(45.0f,width/height, 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}


-(void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
     glUseProgram(gShaderProgramObject);
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    vmath::mat4  scaleMatrix;
    vmath::mat4 translationMatrix;
    //I
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(transform_i1, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_i1);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);
    
    //N
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(-0.3f, transform_n, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_n);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glBindVertexArray(0);
    
    //D
    if (flagd == 1)
    {
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_d);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
        glBindVertexArray(0);
        
        flagp = 1;
    }
    //I
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(0.3f, transform_i2, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_i2);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindVertexArray(0);
    
    //A
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::translate(transform_a, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_a);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glBindVertexArray(0);
    
    
    if(flag4==1)
    {
        //Line1
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        modelViewMatrix = vmath::translate(0.2f, 0.0f, -2.0f);
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line1);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line2
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        modelViewMatrix = vmath::translate(0.2f, 0.0f, -2.0f);
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line2);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line3
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        modelViewMatrix = vmath::translate(0.2f, 0.0f, -2.0f);
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line3);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
    }
    
    
    //flagp = 1;
    
    if (flagp == 1)
    {
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_apex);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_iafquad);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_iafbquad);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_biggerwings1);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_biggerwings2);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_smallerwings1);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_smallerwings2);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, planey, planez);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line1);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line2
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, planey, planez);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line2);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line3
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, planey, planez);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line3);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pi);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pa);
        glDrawArrays(GL_LINES, 0, 2);
        glDrawArrays(GL_LINES, 2, 4);
        glDrawArrays(GL_LINES, 4, 2);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex, planey, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pf);
        glDrawArrays(GL_LINES, 0, 2);
        glDrawArrays(GL_LINES, 2, 4);
        glDrawArrays(GL_LINES, 4, 2);
        glBindVertexArray(0);
        //====
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_apex);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_iafquad);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_iafbquad);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_biggerwings1);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_biggerwings2);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_smallerwings1);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_smallerwings2);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, planey1 + 0.02f, planez);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line1);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line2
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, planey1 + 0.02f, planez);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line2);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line3
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, planey1 + 0.02f, planez);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line3);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pi);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pa);
        glDrawArrays(GL_LINES, 0, 2);
        glDrawArrays(GL_LINES, 2, 4);
        glDrawArrays(GL_LINES, 4, 2);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex1, planey1, planez);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pf);
        glDrawArrays(GL_LINES, 0, 2);
        glDrawArrays(GL_LINES, 2, 4);
        glDrawArrays(GL_LINES, 4, 2);
        glBindVertexArray(0);
        
        //
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_apex);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_iafquad);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_iafbquad);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_biggerwings1);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_biggerwings2);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_smallerwings1);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_smallerwings2);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        glBindVertexArray(0);
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, 0.03f, -3.0f);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line1);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line2
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, 0.03f, -3.0f);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line2);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        //Line3
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(linex1, 0.03f, -3.0f);
        scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_line3);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        //
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pi);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pa);
        glDrawArrays(GL_LINES, 0, 2);
        glDrawArrays(GL_LINES, 2, 4);
        glDrawArrays(GL_LINES, 4, 2);
        glBindVertexArray(0);
        
        
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        scaleMatrix = vmath::mat4::identity();
        translationMatrix = vmath::translate(planex2, 0.0f, -3.0f);
        scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
        modelViewMatrix = translationMatrix * scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_pf);
        glDrawArrays(GL_LINES, 0, 2);
        glDrawArrays(GL_LINES, 2, 4);
        glDrawArrays(GL_LINES, 4, 2);
        glBindVertexArray(0);
        
        
        if (flagp3 == 1)
        {
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_apex);
            glDrawArrays(GL_TRIANGLES, 0, 3);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_iafquad);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_iafbquad);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_biggerwings1);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_biggerwings2);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_smallerwings1);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_smallerwings2);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(linex1, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_line1);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            //Line2
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(linex1, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_line2);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            //Line3
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(linex1, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_line3);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_pi);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_pa);
            glDrawArrays(GL_LINES, 0, 2);
            glDrawArrays(GL_LINES, 2, 2);
            glDrawArrays(GL_LINES, 4, 2);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex3, planey3, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_pf);
            glDrawArrays(GL_LINES, 0, 2);
            glDrawArrays(GL_LINES, 2, 2);
            glDrawArrays(GL_LINES, 4, 2);
            glBindVertexArray(0);
            //
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_apex);
            glDrawArrays(GL_TRIANGLES, 0, 3);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_iafquad);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_iafbquad);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_biggerwings1);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_biggerwings2);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_smallerwings1);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_smallerwings2);
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(linex1, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_line1);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            //Line2
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(linex1, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_line2);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            //Line3
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(linex1, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_line3);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_pi);
            glDrawArrays(GL_LINES, 0, 2);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_pa);
            glDrawArrays(GL_LINES, 0, 2);
            glDrawArrays(GL_LINES, 2, 2);
            glDrawArrays(GL_LINES, 4, 2);
            glBindVertexArray(0);
            
            
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            scaleMatrix = vmath::mat4::identity();
            translationMatrix = vmath::translate(planex4, planey4, -3.0f);
            scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
            modelViewMatrix = translationMatrix * scaleMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            glBindVertexArray(vao_pf);
            glDrawArrays(GL_LINES, 0, 2);
            glDrawArrays(GL_LINES, 2, 2);
            glDrawArrays(GL_LINES, 4, 2);
            glBindVertexArray(0);
            
        }
        
    }
    
	glUseProgram(0);
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
    if (transform_i1 <= -0.6f)
    {
        transform_i1 = transform_i1 + 0.01f;
    }
    if (transform_n >= 0.0f)
    {
        transform_n = transform_n - 0.01f;
    }
    if (transform_i2 <= 0.0f)
    {
        transform_i2 = transform_i2 + 0.01f;
    }
    
    
    if (transform_a >= 0.6f)
    {
        transform_a = transform_a - 0.01f;
        
    }
    else
    {
        flagd = 1;
    }
    
    
    if (flagp == 1)
    {
        
        if (planex <= 0.0)
        {
            planex = planex + 0.01f;
            
            if (planex >= -0.055108f)
            {
                planez = 5.0f;
            }
        }
        if (planey <= 0.0f)
        {
            planey = planey + 0.01f;
        }
        
        if (planex1 <= 0.0f)
        {
            planex1 = planex1 + 0.01f;
            
            //console.log(planex1);
            //fprintf(gpFile, "planex1->%f\n",planex1);
            if (planex1 >= 0.009108f)
                planez1 = 5.0f;
            
        }
        if (planey1 >= 0.0f)
        {
            planey1 = planey1 - 0.01f;
        }
        
        
        if (planex2 <= 3.0f)
        {
            
            planex2 = planex2 + 0.01f;
            linex1 = linex1 + 0.01f;
            //fprintf(gpFile, "planex2->%f\n", planex2);
            if (planex2 >= 0.971883f)
                flagp3 = 1;
            if (planex2 >= 2.040927f)
                flag4 = 1;
            
        }
        
        if (flagp3 == 1)
        {
            if (planex3 <= 3.0f)
                planex3 = planex3 + 0.01f;
            if (planey3 <= 2.0f)
                planey3 = planey3 + 0.01f;
            if (planex4 <= 3.0f)
                planex4 = planex4 + 0.01f;
            if (planey4 <= 2.0f)
                planey4 = planey4 - 0.01f;
        }
        
    }
    
    
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);

}

-(void)keyDown:(NSEvent *)theEvent
{
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
		[self release];
		[NSApp terminate:self];
		break;
		
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
			break;
	}

}

-(void)mouseDown:(NSEvent *)theEvent
{
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{

}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{
	if (vbo_position_i1)
	{
		glDeleteBuffers(1, &vbo_position_i1);
		vbo_position_i1 = 0;
	}
	if (vao_i1)
	{
		glDeleteVertexArrays(1, &vao_i1);
		vao_i1 = 0;
	}
	if (vbo_position_i2)
	{
		glDeleteBuffers(1, &vbo_position_i2);
		vbo_position_i2= 0;
	}
	if (vao_i2)
	{
		glDeleteVertexArrays(1, &vao_i2);
		vao_i2 = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
