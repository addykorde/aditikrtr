#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"

#import "Sphere.h"
enum
{
AMC_ATTRIBUTE_POSITION = 0,
AMC_ATTRIBUTE_COLOR,
AMC_ATTRIBUTE_NORMAL,
AMC_ATTRIBUTE_TEXCOORD0
};

int gbLight = 0;
Sphere *sphere=[Sphere alloc];
//sphere=
int vkey=0;
GLfloat LightAnglezero = 0.0f, LightAngleone = 0.0f, LightAngletwo = 0.0f;
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *gpFile=NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
	
	NSApp=[NSApplication sharedApplication];
	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];
	[pPool release];
	return(0);
}

@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

struct Lights {
    float ambient[4];
    float diffuse[4];
    float specular[4];
    float position[4];
};

float MaterialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
float MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
float MaterialShininess[] = { 50.0f };


Lights light[3];

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	

	// log file

	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	
	if(gpFile==NULL)
	{
		printf("Cannot create log file\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile,"Program is started successfully");


	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	window=[[NSWindow alloc] initWithContentRect:win_rect
					styleMask:NSWindowStyleMaskTitled |
				NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
				 NSWindowStyleMaskResizable
						backing:NSBackingStoreBuffered
						defer:NO];
	[window setTitle:@"Three Rotating Lights On A Sphere"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView]; 
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile,"Program is terminated successfully");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}

	//code
}

-(void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject1;
    GLuint gFragmentShaderObject1;
    GLuint gShaderProgramObject1;
    GLuint gVertexShaderObject2;
    GLuint gFragmentShaderObject2;
    GLuint gShaderProgramObject2;


	GLuint vao_pyramid;
	GLuint vao_cube;
	GLuint vbo_position_pyramid;
	GLuint vbo_position_cube;
	GLuint vbo_color_pyramid;
	GLuint vbo_color_cube;
    
    int numVertices;
    int numElements;
    
    GLuint vao_sphere;
    
    GLuint vbo_sphere_position;
    
    GLuint vbo_sphere_normal;
    
    GLuint vbo_sphere_element;
    GLuint mUniform;
    GLuint vUniform;
    GLuint pUniform;
    GLuint laUniform_red;
    GLuint laUniform_blue;
    GLuint laUniform_green;
    GLuint kaUniform;
    GLuint lsUniform_red;
    GLuint lsUniform_green;
    GLuint lsUniform_blue;
    GLuint ldUniform_red;
    GLuint ldUniform_green;
    GLuint ldUniform_blue;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint lightPositionUniform_red;
    GLuint lightPositionUniform_blue;
    GLuint lightPositionUniform_green;
    GLuint isLKeypressedUniform;
    GLuint material_shininess_uniform;
	vmath::mat4 perspectiveProjectionMatrix;
    
    
    float light_ambient[4];
    float light_diffuse[4];
    float light_specular[4];
    float light_position[4];
    
    float material_ambient[4];
    float material_diffuse[4];
    float material_specular[4];
    float material_shininess[1];
    
    
   

}

-(id)initWithFrame:(NSRect)frame;
{
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};//last 0 is musts
		
		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		
		if(pixelFormat==nil)
		{
			fprintf(gpFile,"\nNo Valid OpenGL Pixel Format Is Available");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			
		[self setPixelFormat:pixelFormat];
		
		[self setOpenGLContext:glContext];

	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime

{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	
	return(kCVReturnSuccess);
}


-(void)prepareOpenGL
{
    
    float sphere_vertices[1146];
    
    float sphere_normals[1146];
    
    float sphere_textures[764];
    
    short sphere_elemets[2280];
    
    
    [sphere getSphereVertexData:sphere_vertices andArray1:sphere_normals andArray2:sphere_textures andArray3:sphere_elemets];
    
    
    
    
    
    numVertices=[sphere getNumberOfSphereVertices];
    
    numElements=[sphere getNumberOfSphereElements];
    
	fprintf(gpFile,"OpenGL Version : %s\n",glGetString(GL_VERSION));
	fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	gVertexShaderObject1 = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar* vertexShaderSourceCode1 =
		"#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vnormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec4 u_light_position_red;" \
    "uniform vec4 u_light_position_blue;" \
    "uniform vec4 u_light_position_green;" \
    "out vec4 eye_coordinates;" \
    "out vec3 light_direction_red;" \
    "out vec3 light_direction_blue;" \
    "out vec3 light_direction_green;" \
    "out vec3 tnorm;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
    "tnorm=mat3(u_v_matrix*u_m_matrix)*vnormal;" \
    "light_direction_red=vec3(u_light_position_red)-eye_coordinates.xyz;" \
    "light_direction_blue=vec3(u_light_position_blue)-eye_coordinates.xyz;" \
    "light_direction_green=vec3(u_light_position_green)-eye_coordinates.xyz;" \
    "viewer_vector=vec3(-eye_coordinates);" \
    "}" \
    "else" \
    "{" \
    "}" \
    
    "}";

		
	glShaderSource(gVertexShaderObject1, 1, (const GLchar **)&vertexShaderSourceCode1, NULL);
	glCompileShader(gVertexShaderObject1);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char *szInfolog=NULL;
	
	glGetShaderiv(gVertexShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	szInfolog=NULL;

	gFragmentShaderObject1 = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode1 =
    "#version 410" \
    "\n" \
    "out vec4 FragColor;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec3 u_ld_red;" \
    "uniform vec3 u_ld_blue;" \
    "uniform vec3 u_ld_green;" \
    "uniform vec3 u_la_red;" \
    "uniform vec3 u_la_blue;" \
    "uniform vec3 u_la_green;" \
    "uniform vec3 u_ls_red;" \
    "uniform vec3 u_ls_blue;" \
    "uniform vec3 u_ls_green;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_ks;" \
    "uniform vec3 u_kd;" \
    "uniform float m_shininess;"
    "in vec3 light_direction_red;" \
    "in vec3 light_direction_blue;" \
    "in vec3 light_direction_green;" \
    "in vec3 tnorm;"
    "in vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec3 tdnorm=normalize(tnorm);" \
    "vec3 light_direction1_red=normalize(light_direction_red);" \
    "vec3 light_direction1_blue=normalize(light_direction_blue);" \
    "vec3 light_direction1_green=normalize(light_direction_green);" \
    "float tn_dot_ld1=max(dot(light_direction1_red,tdnorm),0.0);" \
    "float tn_dot_ld2=max(dot(light_direction1_green,tdnorm),0.0);" \
    "float tn_dot_ld3=max(dot(light_direction1_blue,tdnorm),0.0);" \
    "vec3 reflection_vector_red=reflect(-light_direction1_red,tdnorm);" \
    "vec3 reflection_vector_blue=reflect(-light_direction1_blue,tdnorm);" \
    "vec3 reflection_vector_green=reflect(-light_direction1_green,tdnorm);" \
    "vec3 viewer_vector1=normalize(viewer_vector);" \
    "vec3 ambient1 = u_la_red * u_ka;" \
    "vec3 ambient2 = u_la_green * u_ka;" \
    "vec3 ambient3 = u_la_blue * u_ka;" \
    "vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld1;" \
    "vec3 diffuse2 = u_ld_green * u_kd * tn_dot_ld2;" \
    "vec3 diffuse3 = u_ld_blue * u_kd * tn_dot_ld3;" \
    "vec3 specular1=u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector1),0.0),m_shininess);" \
    "vec3 specular2=u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector1),0.0),m_shininess);" \
    "vec3 specular3=u_ls_green * u_ks * pow(max(dot(reflection_vector_green,viewer_vector1),0.0),m_shininess);" \
    "vec3 phong_ads_ligh1=ambient1+diffuse1+specular1;" \
    "vec3 phong_ads_ligh2=ambient2+diffuse2+specular2;" \
    "vec3 phong_ads_ligh3=ambient3+diffuse3+specular3;" \
    "vec3 finalColor=phong_ads_ligh1+phong_ads_ligh2+phong_ads_ligh3;" \
    "FragColor=vec4(finalColor,1.0);" \
    "}" \
    "else"
    "{"
    "FragColor=vec4(1.0,1.0,1.0,1.0);" \
    "}"
    "}";
	glShaderSource(gFragmentShaderObject1, 1, (const GLchar **)&fragmentShaderSourceCode1, NULL);


	glCompileShader(gFragmentShaderObject1);

	glGetShaderiv(gFragmentShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	gShaderProgramObject1 = glCreateProgram();

	glAttachShader(gShaderProgramObject1, gVertexShaderObject1);

	glAttachShader(gShaderProgramObject1, gFragmentShaderObject1);

    glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_NORMAL, "vnormal");
	glLinkProgram(gShaderProgramObject1);

	GLint iProgramLinkStatus=0;
	glGetProgramiv(gShaderProgramObject1, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

    mUniform = glGetUniformLocation(gShaderProgramObject1, "u_m_matrix");
    vUniform = glGetUniformLocation(gShaderProgramObject1, "u_v_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject1, "u_p_matrix");
    laUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_la_red");
    laUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_la_green");
    laUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_la_blue");
    lsUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_ls_red");
    lsUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_ls_green");
    lsUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_ls_blue");
    ldUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_ld_red");
    ldUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_ld_green");
    ldUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_ld_blue");
    kdUniform = glGetUniformLocation(gShaderProgramObject1, "u_kd");
    kaUniform = glGetUniformLocation(gShaderProgramObject1, "u_ka");
    ksUniform = glGetUniformLocation(gShaderProgramObject1, "u_ks");
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject1, "m_shininess");
    lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_light_position_red");
    lightPositionUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_light_position_green");
    lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_light_position_blue");
    isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject1, "u_lkeyispressed");
    
    glGenVertexArrays(1, &vao_sphere);
    
    glBindVertexArray(vao_sphere);
    
    glGenBuffers(1, &vbo_sphere_position);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    gVertexShaderObject2 = glCreateShader(GL_VERTEX_SHADER);
    
    //write vertex shader code.
    const GLchar* vertexShaderSourceCode2 =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vnormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec3 u_ld_blue;" \
    "uniform vec3 u_la_blue;" \
    "uniform vec3 u_ls_blue;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_ks;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ld_red;" \
    "uniform vec3 u_la_red;" \
    "uniform vec3 u_ls_red;" \
    "uniform vec3 u_ld_green;" \
    "uniform vec3 u_la_green;" \
    "uniform vec3 u_ls_green;" \
    
    "uniform float m_shininess;"
    "uniform vec4 u_light_position_red;" \
    "uniform vec4 u_light_position_blue;" \
    "uniform vec4 u_light_position_green;" \
    "out vec3 finalColor;" \
    "vec3 light_direction_red;"\
    "vec3 light_direction_blue;"\
    "vec3 light_direction_green;"\
    "out vec3 phong_ads_light1;" \
    "out vec3 phong_ads_light2;" \
    "out vec3 phong_ads_light3;" \
    "vec3 reflection_vector_blue;"\
    "vec3 reflection_vector_green;"\
    "vec3 reflection_vector_red;"\
    "float tn_dot_ld_red;" \
    "float tn_dot_ld_blue;" \
    "float tn_dot_ld_green;" \
    "vec3 ambient1;"\
    "vec3 ambient2;"\
    "vec3 ambient3;"\
    "vec3 diffuse1;"\
    "vec3 diffuse2;"\
    "vec3 diffuse3;"\
    "vec3 specular1;"\
    "vec3 specular2;"\
    "vec3 specular3;"\
    "vec3 tnorm;" \
    "void main(void)" \
    "{" \
    "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
    "tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \
    
    "light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);" \
    
    "light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);" \
    
    "light_direction_green=normalize(vec3(u_light_position_green)-eye_coordinates.xyz);" \
    
    "tn_dot_ld_red=max(dot(light_direction_red,tnorm),0.0);" \
    
    "tn_dot_ld_blue=max(dot(light_direction_blue,tnorm),0.0);" \
    
    "tn_dot_ld_green=max(dot(light_direction_green,tnorm),0.0);" \
    
    "reflection_vector_red=reflect(-light_direction_red,tnorm);" \
    "reflection_vector_blue=reflect(-light_direction_blue,tnorm);" \
    "reflection_vector_green=reflect(-light_direction_green,tnorm);" \
    
    "vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \
    
    
    "ambient1  = u_la_red* u_ka;" \
    "ambient2= u_la_green* u_ka;" \
    "ambient3= u_la_blue* u_ka;" \
    
    "diffuse1 = u_ld_red * u_kd * tn_dot_ld_red;" \
    "diffuse2 = u_ld_green * u_kd * tn_dot_ld_green;" \
    "diffuse3 = u_ld_blue * u_kd * tn_dot_ld_blue;" \
    
    "specular1 = u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),m_shininess);" \
    "specular2 = u_ls_green * u_ks * pow(max(dot(reflection_vector_green,viewer_vector),0.0),m_shininess);" \
    "specular3 = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),m_shininess);" \
    
    "phong_ads_light1  =ambient1+diffuse1+specular1;" \
    "phong_ads_light2  =ambient2+diffuse2+specular2;" \
    "phong_ads_light3  =ambient3+diffuse3+specular3;" \
    
    "finalColor= phong_ads_light3+phong_ads_light1+phong_ads_light2;" \
    "}" \
    "else" \
    "{" \
    "finalColor=vec3(1.0,1.0,1.0);"  \
    "}" \
    
    "}";
    
    
    
    glShaderSource(gVertexShaderObject2, 1, (const GLchar **)&vertexShaderSourceCode2, NULL);
    glCompileShader(gVertexShaderObject2);
    
    glGetShaderiv(gVertexShaderObject2, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject2, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    //For Fragment Shader.
    
    gFragmentShaderObject2 = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode2 =
    "#version 410" \
    "\n" \
    "in vec3 phong_ads_light1;" \
    "in vec3 phong_ads_light2;" \
    "in vec3 finalColor;" \
    "out vec4 FragColor1;" \
    "uniform int u_lkeyispressed;" \
    "void main(void)" \
    "{" \
    
    "FragColor1=vec4(finalColor,1.0);" \
    "}";
    
    
    
    glShaderSource(gFragmentShaderObject2, 1, (const GLchar **)&fragmentShaderSourceCode2, NULL);
    glCompileShader(gFragmentShaderObject2);
    
    glGetShaderiv(gFragmentShaderObject2, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetShaderInfoLog(gFragmentShaderObject2, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    //Create shader program object
    
    
    gShaderProgramObject2 = glCreateProgram();
    
    glAttachShader(gShaderProgramObject2, gVertexShaderObject2);
    
    glAttachShader(gShaderProgramObject2, gFragmentShaderObject2);
    
    glBindAttribLocation(gShaderProgramObject2, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject2, AMC_ATTRIBUTE_NORMAL, "vnormal");
    
    glLinkProgram(gShaderProgramObject2);
    
    glGetProgramiv(gShaderProgramObject2, GL_LINK_STATUS, &iProgramLinkStatus);
    
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetProgramInfoLog(gShaderProgramObject2, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Linking of program %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    mUniform = glGetUniformLocation(gShaderProgramObject1, "u_m_matrix");
    vUniform = glGetUniformLocation(gShaderProgramObject1, "u_v_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject1, "u_p_matrix");
    laUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_la_red");
    laUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_la_green");
    laUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_la_blue");
    lsUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_ls_red");
    lsUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_ls_green");
    lsUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_ls_blue");
    ldUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_ld_red");
    ldUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_ld_green");
    ldUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_ld_blue");
    kdUniform = glGetUniformLocation(gShaderProgramObject1, "u_kd");
    kaUniform = glGetUniformLocation(gShaderProgramObject1, "u_ka");
    ksUniform = glGetUniformLocation(gShaderProgramObject1, "u_ks");
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject1, "m_shininess");
    lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject1, "u_light_position_red");
    lightPositionUniform_green = glGetUniformLocation(gShaderProgramObject1, "u_light_position_green");
    lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject1, "u_light_position_blue");
    isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject1, "u_lkeyispressed");
    
    

    
    
    glGenBuffers(1, &vbo_sphere_normal);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    
    glGenBuffers(1, &vbo_sphere_element);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elemets), sphere_elemets, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    
    
    glBindVertexArray(0);
    


    light[0].ambient[0] = 0.0f;
    light[0].ambient[1] = 0.0f;
    light[0].ambient[2] = 0.0f;
    light[0].ambient[3] = 1.0f;
    
    light[0].diffuse[0] = 1.0f;
    light[0].diffuse[1] = 0.0f;
    light[0].diffuse[2] = 0.0f;
    light[0].diffuse[3] = 1.0f;
    
    light[0].specular[0] = 1.0f;
    light[0].specular[1] = 0.0f;
    light[0].specular[2] = 0.0f;
    light[0].specular[3] = 1.0f;
    
    light[0].position[0] = 0.0f;
    light[0].position[1] = 0.0f;
    light[0].position[2] = 0.0f;
    light[0].position[3] = 1.0f;
    
    
    light[1].ambient[0] = 0.0f;
    light[1].ambient[1] = 0.0f;
    light[1].ambient[2] = 0.0f;
    light[1].ambient[3] = 1.0f;
    
    light[1].diffuse[0] = 0.0f;
    light[1].diffuse[1] = 1.0f;
    light[1].diffuse[2] = 0.0f;
    light[1].diffuse[3] = 1.0f;
    
    light[1].specular[0] = 0.0f;
    light[1].specular[1] = 1.0f;
    light[1].specular[2] = 0.0f;
    light[1].specular[3] = 1.0f;
    
    light[1].position[0] = 0.0f;
    light[1].position[1] = 0.0f;
    light[1].position[2] = 0.0f;
    light[1].position[3] = 1.0f;
    
    light[2].ambient[0] = 0.0f;
    light[2].ambient[1] = 0.0f;
    light[2].ambient[2] = 0.0f;
    light[2].ambient[3] = 1.0f;
    
    light[2].diffuse[0] = 0.0f;
    light[2].diffuse[1] = 0.0f;
    light[2].diffuse[2] = 1.0f;
    light[2].diffuse[3] = 1.0f;
    
    light[2].specular[0] = 0.0f;
    light[2].specular[1] = 0.0f;
    light[2].specular[2] = 1.0f;
    light[2].specular[3] = 1.0f;
    
    light[2].position[0] = 0.0f;
    light[2].position[1] = 0.0f;
    light[2].position[2] = 0.0f;
    light[2].position[3] = 1.0f;

    


	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect=[self bounds];
	
	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
		perspectiveProjectionMatrix=vmath::perspective(45.0f,width/height, 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}


-(void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    if(vkey==0)
    glUseProgram(gShaderProgramObject1);
    else
    glUseProgram(gShaderProgramObject2);
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 modelViewMatrix;
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    modelViewMatrix = vmath::mat4::identity();
    
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    if (gbLight == 1)
    {
        glUniform1i(isLKeypressedUniform, 1);
        
        
        
        
        glUniform3fv(laUniform_red, 1, light[0].ambient);
        glUniform3fv(ldUniform_red, 1, light[0].diffuse);
        glUniform3fv(lsUniform_red, 1, light[0].specular);
        
        glUniform4fv(lightPositionUniform_red, 1, light[0].position);
        
        
        
        glUniform3fv(laUniform_green, 1, light[1].ambient);
        glUniform3fv(ldUniform_green, 1, light[1].diffuse);
        glUniform3fv(lsUniform_green, 1, light[1].specular);
        //rotationMatrix = mat4::identity();
        //rotationMatrix = rotate(LightAngleone, 0.0f, 1.0f, 0.0f);
        //light[1].position[0] = LightAngleone;
        glUniform4fv(lightPositionUniform_green, 1, light[1].position);
        
        
        
        glUniform3fv(laUniform_blue, 1, light[2].ambient);
        glUniform3fv(ldUniform_blue, 1, light[2].diffuse);
        glUniform3fv(lsUniform_blue, 1, light[2].specular);
        
        
        glUniform4fv(lightPositionUniform_blue, 1, light[2].position);
        
        
        glUniform3fv(kaUniform, 1, MaterialAmbient);
        glUniform3fv(kdUniform, 1, MaterialDiffuse);
        glUniform3fv(ksUniform, 1, MaterialSpecular);
        glUniform1fv(material_shininess_uniform, 1, MaterialShininess);
        
        
    }
    else
    {
        glUniform1i(isLKeypressedUniform, 0);
    }
    
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);

    
    glUseProgram(0);
	
    
    LightAnglezero = LightAnglezero + 0.01f;
    LightAngleone = LightAngleone + 0.01f;
    LightAngletwo = LightAngletwo + 0.01f;
    if (LightAnglezero >= 2 * M_PI)
    {
        LightAnglezero = 0.0f;
        
    }
    light[0].position[1] = 100.0f*sin(LightAnglezero);
    light[0].position[2] = 100.0f*cos(LightAnglezero);
    
    if (LightAngleone >= 2 * M_PI)
    {
        LightAngleone = 0.0f;
    }
    light[1].position[0] = 100.0f*sin(LightAngleone);
    light[1].position[2] = 100.0f*cos(LightAngleone);
    if (LightAngletwo >= 2 * M_PI)
    {
        LightAngletwo = 0.0f;
    }
    light[2].position[0] = 100.0f*sin(LightAngleone);
    light[2].position[1] = 100.0f*cos(LightAngleone);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);

}

-(void)keyDown:(NSEvent *)theEvent
{
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
            [[self window]toggleFullScreen:self];
            break;
		break;
		
		case 'F':
		case 'f':
			vkey = 0;
            break;
        case 'V':
        case 'v':
            vkey=1;
            break;
        case 'L':
        case 'l':
            if (gbLight == 0)
            {
                gbLight = 1;
            }
            else
            {
                gbLight = 0;
            }
            break;
        case 'Q':
        case 'q':
            [self release];
            [NSApp terminate:self];
            break;
		default:
			break;
	}

}

-(void)mouseDown:(NSEvent *)theEvent
{
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{

}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{
	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}
    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    
    
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}
	

	glUseProgram(gShaderProgramObject1);
	glDetachShader(gShaderProgramObject1, gFragmentShaderObject1);
	glDetachShader(gShaderProgramObject1, gVertexShaderObject1);
	glDeleteShader(gFragmentShaderObject1);
	gFragmentShaderObject1 = 0;
	glDeleteShader(gVertexShaderObject1);
	gVertexShaderObject1 = 0;
	glDeleteProgram(gShaderProgramObject1);
	gShaderProgramObject1 = 0;
	glUseProgram(0);
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
