#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"

#import "Sphere.h"
enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXCOORD0
};

static float angleOfXRotation = 0.0f, angleOfYRotation = 0.0f, angleOfZRotation = 0.0f;

int keypress = 0;
int gbLight = 0;
Sphere *sphere=[Sphere alloc];
//sphere=
int vkey=0;
struct Materials {
    float MaterialAmbient[4];
    float MaterialDiffuse[4];
    float MaterialSpecular[4];
    float MaterialShininess[1];
};




Materials material[24];

int currentWidth = 0,currentHeight=0;
GLfloat LightAmbient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[]={1.0f,1.0f,1.0f,1.0f};
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };

//--------------------------------------------------

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *gpFile=NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
    NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
    
    NSApp=[NSApplication sharedApplication];
    [NSApp setDelegate:[[AppDelegate alloc]init]];
    
    [NSApp run];
    [pPool release];
    return(0);
}

@implementation AppDelegate
{
@private
    NSWindow *window;
    GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    
    
    // log file
    
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    gpFile=fopen(pszLogFileNameWithPath,"w");
    
    if(gpFile==NULL)
    {
        printf("Cannot create log file\n");
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(gpFile,"Program is started successfully");
    
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    
    window=[[NSWindow alloc] initWithContentRect:win_rect
                                       styleMask:NSWindowStyleMaskTitled |
            NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
            NSWindowStyleMaskResizable
                                         backing:NSBackingStoreBuffered
                                           defer:NO];
    [window setTitle:@"ADS Per Fragment Light On A Sphere"];
    [window center];
    
    glView=[[GLView alloc]initWithFrame:win_rect];
    
    [window setContentView:glView];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile,"Program is terminated successfully");
    
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
    
    //code
}

-(void)windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

-(void)dealloc
{
    [glView release];
    [window release];
    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    GLuint gVertexShaderObject1;
    GLuint gFragmentShaderObject1;
    GLuint gShaderProgramObject1;
    GLuint gVertexShaderObject2;
    GLuint gFragmentShaderObject2;
    GLuint gShaderProgramObject2;
    
    
    GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_position_pyramid;
    GLuint vbo_position_cube;
    GLuint vbo_color_pyramid;
    GLuint vbo_color_cube;
    
    int numVertices;
    int numElements;
    
    GLuint vao_sphere;
    
    GLuint vbo_sphere_position;
    
    GLuint vbo_sphere_normal;
    
    GLuint vbo_sphere_element;
    GLuint mUniform;
    GLuint vUniform;
    GLuint pUniform;
    GLuint laUniform;
    GLuint kaUniform;
    GLuint lsUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint lightPositionUniform;
    GLuint isLKeypressedUniform;
    GLuint material_shininess_uniform;
    vmath::mat4 perspectiveProjectionMatrix;
    
    
    
    
    
    
    
}

-(id)initWithFrame:(NSRect)frame;
{
    self=[super initWithFrame:frame];
    
    if(self)
    {
        [[self window]setContentView:self];
        
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };//last 0 is musts
        
        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
        
        if(pixelFormat==nil)
        {
            fprintf(gpFile,"\nNo Valid OpenGL Pixel Format Is Available");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormat];
        
        [self setOpenGLContext:glContext];
        
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime

{
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    
    return(kCVReturnSuccess);
}


-(void)prepareOpenGL
{
    
    float sphere_vertices[1146];
    
    float sphere_normals[1146];
    
    float sphere_textures[764];
    
    short sphere_elemets[2280];
    
    
    [sphere getSphereVertexData:sphere_vertices andArray1:sphere_normals andArray2:sphere_textures andArray3:sphere_elemets];
    
    
    
    
    
    numVertices=[sphere getNumberOfSphereVertices];
    
    numElements=[sphere getNumberOfSphereElements];
    
    fprintf(gpFile,"OpenGL Version : %s\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    gVertexShaderObject1 = glCreateShader(GL_VERTEX_SHADER);
    
    const GLchar* vertexShaderSourceCode1 =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vnormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec4 u_light_position;" \
    "out vec4 eye_coordinates;" \
    "out vec3 light_direction;" \
    "out vec3 tnorm;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
    "tnorm=mat3(u_v_matrix*u_m_matrix)*vnormal;" \
    "light_direction=vec3(u_light_position)-eye_coordinates.xyz;" \
    "viewer_vector=vec3(-eye_coordinates);" \
    "}" \
    "else" \
    "{" \
    "}" \
    
    "}";
    
    glShaderSource(gVertexShaderObject1, 1, (const GLchar **)&vertexShaderSourceCode1, NULL);
    glCompileShader(gVertexShaderObject1);
    
    GLint iInfoLogLength=0;
    GLint iShaderCompileStatus=0;
    char *szInfolog=NULL;
    
    glGetShaderiv(gVertexShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject1, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    iInfoLogLength=0;
    iShaderCompileStatus=0;
    szInfolog=NULL;
    
    gFragmentShaderObject1 = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode1 =
    "#version 410" \
    "\n" \
    "out vec4 FragColor;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_ks;" \
    "uniform vec3 u_kd;" \
    "uniform float m_shininess;"
    "in vec3 light_direction;" \
    "in vec3 tnorm;"
    "in vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec3 tdnorm=normalize(tnorm);" \
    "vec3 light_direction1=normalize(light_direction);" \
    "float tn_dot_ld=max(dot(light_direction1,tdnorm),0.0);" \
    "vec3 reflection_vector=reflect(-light_direction1,tdnorm);" \
    "vec3 viewer_vector1=normalize(viewer_vector);" \
    "vec3 ambient = u_la * u_ka;" \
    "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
    "vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,viewer_vector1),0.0),m_shininess);" \
    "vec3 phong_ads_light=ambient+diffuse+specular;" \
    "FragColor=vec4(phong_ads_light,1.0);" \
    "}" \
    "else"
    "{"
    "FragColor=vec4(1.0,1.0,1.0,1.0);" \
    "}"
    "}";
    glShaderSource(gFragmentShaderObject1, 1, (const GLchar **)&fragmentShaderSourceCode1, NULL);
    
    
    glCompileShader(gFragmentShaderObject1);
    
    glGetShaderiv(gFragmentShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetShaderInfoLog(gFragmentShaderObject1, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    gShaderProgramObject1 = glCreateProgram();
    
    glAttachShader(gShaderProgramObject1, gVertexShaderObject1);
    
    glAttachShader(gShaderProgramObject1, gFragmentShaderObject1);
    
    glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_NORMAL, "vnormal");
    glLinkProgram(gShaderProgramObject1);
    
    GLint iProgramLinkStatus=0;
    glGetProgramiv(gShaderProgramObject1, GL_LINK_STATUS, &iProgramLinkStatus);
    
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetProgramInfoLog(gShaderProgramObject1, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Linking of program %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    mUniform = glGetUniformLocation(gShaderProgramObject1, "u_m_matrix");
    vUniform = glGetUniformLocation(gShaderProgramObject1, "u_v_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject1, "u_p_matrix");
    laUniform = glGetUniformLocation(gShaderProgramObject1, "u_la");
    lsUniform = glGetUniformLocation(gShaderProgramObject1, "u_ls");
    ldUniform = glGetUniformLocation(gShaderProgramObject1, "u_ld");
    kdUniform = glGetUniformLocation(gShaderProgramObject1, "u_kd");
    kaUniform = glGetUniformLocation(gShaderProgramObject1, "u_ka");
    ksUniform = glGetUniformLocation(gShaderProgramObject1, "u_ks");
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject1, "m_shininess");
    lightPositionUniform = glGetUniformLocation(gShaderProgramObject1, "u_light_position");
    isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject1, "u_lkeyispressed");
    
    glGenVertexArrays(1, &vao_sphere);
    
    glBindVertexArray(vao_sphere);
    
    glGenBuffers(1, &vbo_sphere_position);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    gVertexShaderObject2 = glCreateShader(GL_VERTEX_SHADER);
    
    //write vertex shader code.
    const GLchar* vertexShaderSourceCode2 =
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vnormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_ks;" \
    "uniform vec3 u_kd;" \
    "uniform float m_shininess;"
    "uniform vec4 u_light_position;" \
    "out vec3 phong_ads_light;" \
    "void main(void)" \
    "{" \
    "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
    "vec3 tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \
    "vec3 light_direction=normalize(vec3(u_light_position)-eye_coordinates.xyz);" \
    "float tn_dot_ld=max(dot(light_direction,tnorm),0.0);" \
    "vec3 reflection_vector=reflect(-light_direction,tnorm);" \
    "vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \
    "vec3 ambient = u_la * u_ka;" \
    "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
    "vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,viewer_vector),0.0),m_shininess);" \
    "phong_ads_light=ambient+diffuse+specular;" \
    "}" \
    "else" \
    "{" \
    "phong_ads_light=vec3(1.0,1.0,1.0);" \
    "}" \
    
    "}";
    
    
    
    glShaderSource(gVertexShaderObject2, 1, (const GLchar **)&vertexShaderSourceCode2, NULL);
    glCompileShader(gVertexShaderObject2);
    
    glGetShaderiv(gVertexShaderObject2, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetShaderInfoLog(gVertexShaderObject2, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    //For Fragment Shader.
    
    gFragmentShaderObject2 = glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode2 =
    "#version 410" \
    "\n" \
    "in vec3 phong_ads_light;" \
    "out vec4 FragColor;" \
    "uniform int u_lkeyispressed;" \
    "void main(void)" \
    "{" \
    
    "FragColor=vec4(phong_ads_light,1.0);" \
    
    "}";
    
    
    
    glShaderSource(gFragmentShaderObject2, 1, (const GLchar **)&fragmentShaderSourceCode2, NULL);
    glCompileShader(gFragmentShaderObject2);
    
    glGetShaderiv(gFragmentShaderObject2, GL_COMPILE_STATUS, &iShaderCompileStatus);
    
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetShaderInfoLog(gFragmentShaderObject2, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    //Create shader program object
    
    
    gShaderProgramObject2 = glCreateProgram();
    
    glAttachShader(gShaderProgramObject2, gVertexShaderObject2);
    
    glAttachShader(gShaderProgramObject2, gFragmentShaderObject2);
    
    glBindAttribLocation(gShaderProgramObject2, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject2, AMC_ATTRIBUTE_NORMAL, "vnormal");
    
    glLinkProgram(gShaderProgramObject2);
    
    glGetProgramiv(gShaderProgramObject2, GL_LINK_STATUS, &iProgramLinkStatus);
    
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject2, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        
        if (iInfoLogLength > 0)
        {
            szInfolog = (GLchar *)malloc(iInfoLogLength);
            if (szInfolog != NULL)
            {
                GLsizei written;
                
                glGetProgramInfoLog(gShaderProgramObject2, iInfoLogLength, &written, szInfolog);
                fprintf(gpFile, "Inside Linking of program %s", szInfolog);
                free(szInfolog);
                
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    mUniform = glGetUniformLocation(gShaderProgramObject1, "u_m_matrix");
    vUniform = glGetUniformLocation(gShaderProgramObject1, "u_v_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject1, "u_p_matrix");
    laUniform = glGetUniformLocation(gShaderProgramObject1, "u_la");
    lsUniform = glGetUniformLocation(gShaderProgramObject1, "u_ls");
    ldUniform = glGetUniformLocation(gShaderProgramObject1, "u_ld");
    kdUniform = glGetUniformLocation(gShaderProgramObject1, "u_kd");
    kaUniform = glGetUniformLocation(gShaderProgramObject1, "u_ka");
    ksUniform = glGetUniformLocation(gShaderProgramObject1, "u_ks");
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject1, "m_shininess");
    lightPositionUniform = glGetUniformLocation(gShaderProgramObject1, "u_light_position");
    isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject1, "u_lkeyispressed");
    
    
    glGenBuffers(1, &vbo_sphere_normal);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    
    glGenBuffers(1, &vbo_sphere_element);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elemets), sphere_elemets, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    
    
    glBindVertexArray(0);
    
    
    
    material[0].MaterialAmbient[0] = 0.0215;
    material[0].MaterialAmbient[1] = 0.1745;
    material[0].MaterialAmbient[2] = 0.0215;
    material[0].MaterialAmbient[3] = 1.0f;
    
    material[0].MaterialDiffuse[0] = 0.07568;
    material[0].MaterialDiffuse[1] = 0.61424;
    material[0].MaterialDiffuse[2] = 0.07568;
    material[0].MaterialDiffuse[3] = 1.0f;
    
    material[0].MaterialSpecular[0] = 0.633;
    material[0].MaterialSpecular[1] = 0.727811;
    material[0].MaterialSpecular[2] = 0.633;
    material[0].MaterialSpecular[3] = 1.0f;
    
    material[0].MaterialShininess[0] = 0.6 * 128;
    
    
    
    material[1].MaterialAmbient[0] = 0.135;
    material[1].MaterialAmbient[1] = 0.2225;
    material[1].MaterialAmbient[2] = 0.1575;
    material[1].MaterialAmbient[3] = 1.0f;
    
    material[1].MaterialDiffuse[0]= 0.54;
    material[1].MaterialDiffuse[1]= 0.89;
    material[1].MaterialDiffuse[2]= 0.63;
    material[1].MaterialDiffuse[3] = 1.0f;
    
    material[1].MaterialSpecular[0]= 0.316228;
    material[1].MaterialSpecular[1]= 0.316228;
    material[1].MaterialSpecular[2]= 0.316228;
    material[1].MaterialSpecular[3] = 1.0f;
    
    material[1].MaterialShininess[0] = 0.1 * 128;
    
    
    material[2].MaterialAmbient[0] = 0.05375;
    material[2].MaterialAmbient[1] = 0.05;
    material[2].MaterialAmbient[2] = 0.06625;
    material[2].MaterialAmbient[3] = 1.0f;
    
    material[2].MaterialDiffuse[0] = 0.18275;
    material[2].MaterialDiffuse[1] = 0.17;
    material[2].MaterialDiffuse[2] = 0.22525;
    material[2].MaterialDiffuse[3] = 1.0f;
    
    material[2].MaterialSpecular[0] = 0.332741;
    material[2].MaterialSpecular[1] = 0.328634;
    material[2].MaterialSpecular[2] = 0.346435;
    material[2].MaterialSpecular[3] = 1.0f;
    
    material[2].MaterialShininess[0] = 0.3 * 128;
    
    material[3].MaterialAmbient[0] = 0.25;
    material[3].MaterialAmbient[1] = 0.20725;
    material[3].MaterialAmbient[2] = 0.20725;
    material[3].MaterialAmbient[3] = 1.0f;
    
    material[3].MaterialDiffuse[0] = 1.0;
    material[3].MaterialDiffuse[1] = 0.829;
    material[3].MaterialDiffuse[2] = 0.829;
    material[3].MaterialDiffuse[3] = 1.0f;
    
    material[3].MaterialSpecular[0] = 0.296648;
    material[3].MaterialSpecular[1] = 0.296648;
    material[3].MaterialSpecular[2] = 0.296648;
    material[3].MaterialSpecular[3] = 1.0f;
    
    material[3].MaterialShininess[0] = 0.088 * 128;
    
    material[4].MaterialAmbient[0] = 0.1745;
    material[4].MaterialAmbient[1] = 0.01175;
    material[4].MaterialAmbient[2] = 0.01175;
    material[4].MaterialAmbient[3] = 1.0f;
    
    material[4].MaterialDiffuse[0] = 0.61424;
    material[4].MaterialDiffuse[1] = 0.04136;
    material[4].MaterialDiffuse[2] = 0.04136;
    material[4].MaterialDiffuse[3] = 1.0f;
    
    material[4].MaterialSpecular[0] = 0.727811;
    material[4].MaterialSpecular[1] = 0.626959;
    material[4].MaterialSpecular[2] = 0.626959;
    material[4].MaterialSpecular[3] = 1.0f;
    
    material[4].MaterialShininess[0] = 0.6 * 128;
    
    
    material[5].MaterialAmbient[0] = 0.1;
    material[5].MaterialAmbient[1] = 0.18725;
    material[5].MaterialAmbient[2] = 0.1745;
    material[5].MaterialAmbient[3] = 1.0f;
    
    material[5].MaterialDiffuse[0] = 0.396;
    material[5].MaterialDiffuse[1] = 0.74151;
    material[5].MaterialDiffuse[2] = 0.69102;
    material[5].MaterialDiffuse[3] = 1.0f;
    
    material[5].MaterialSpecular[0] = 0.297254;
    material[5].MaterialSpecular[1] = 0.30829;
    material[5].MaterialSpecular[2] = 0.306678;
    material[5].MaterialSpecular[3] = 1.0f;
    
    material[5].MaterialShininess[0] = 0.1 * 128;
    //*************************************************************************************
    material[6].MaterialAmbient[0] = 0.329412;
    material[6].MaterialAmbient[1] = 0.223529;
    material[6].MaterialAmbient[2] = 0.027451;
    material[6].MaterialAmbient[3] = 1.0f;
    
    material[6].MaterialDiffuse[0] = 0.780392;
    material[6].MaterialDiffuse[1] = 0.568627;
    material[6].MaterialDiffuse[2] = 0.113725;
    material[6].MaterialDiffuse[3] = 1.0f;
    
    material[6].MaterialSpecular[0] = 0.992157;
    material[6].MaterialSpecular[1] = 0.941176;
    material[6].MaterialSpecular[2] = 0.807843;
    material[6].MaterialSpecular[3] = 1.0f;
    
    material[6].MaterialShininess[0] = 0.21794872 * 128;
    //---------------------------------
    material[7].MaterialAmbient[0] = 0.2125;
    material[7].MaterialAmbient[1] = 0.1275;
    material[7].MaterialAmbient[2] = 0.054;
    material[7].MaterialAmbient[3] = 1.0f;
    
    material[7].MaterialDiffuse[0] = 0.714;
    material[7].MaterialDiffuse[1] = 0.4284;
    material[7].MaterialDiffuse[2] = 0.18144;
    material[7].MaterialDiffuse[3] = 1.0f;
    
    material[7].MaterialSpecular[0] = 0.393548;
    material[7].MaterialSpecular[1] = 0.271906;
    material[7].MaterialSpecular[2] = 0.166721;
    material[7].MaterialSpecular[3] = 1.0f;
    
    material[7].MaterialShininess[0] = 0.2 * 128;
    //-------------------------------------------------------
    material[8].MaterialAmbient[0] = 0.25;
    material[8].MaterialAmbient[1] = 0.25;
    material[8].MaterialAmbient[2] = 0.25;
    material[8].MaterialAmbient[3] = 1.0f;
    
    material[8].MaterialDiffuse[0] = 0.4;
    material[8].MaterialDiffuse[1] = 0.4;
    material[8].MaterialDiffuse[2] = 0.4;
    material[8].MaterialDiffuse[3] = 1.0f;
    
    material[8].MaterialSpecular[0] = 0.774597;
    material[8].MaterialSpecular[1] = 0.774597;
    material[8].MaterialSpecular[2] = 0.774597;
    material[8].MaterialSpecular[3] = 1.0f;
    
    material[8].MaterialShininess[0] = 0.6 * 128;
    
    //---------------------------------
    
    material[9].MaterialAmbient[0] = 0.19125;
    material[9].MaterialAmbient[1] = 0.0735;
    material[9].MaterialAmbient[2] = 0.0225;
    material[9].MaterialAmbient[3] = 1.0f;
    
    material[9].MaterialDiffuse[0] = 0.7038;
    material[9].MaterialDiffuse[1] = 0.27048;
    material[9].MaterialDiffuse[2] = 0.0828;
    material[9].MaterialDiffuse[3] = 1.0f;
    
    material[9].MaterialSpecular[0] = 0.256777;
    material[9].MaterialSpecular[1] = 0.137622;
    material[9].MaterialSpecular[2] = 0.086014;
    material[9].MaterialSpecular[3] = 1.0f;
    
    material[9].MaterialShininess[0] = 0.1 * 128;
    
    //---------------------------------------
    material[10].MaterialAmbient[0] = 0.24725;
    material[10].MaterialAmbient[1] = 0.1995;
    material[10].MaterialAmbient[2] = 0.0745;
    material[10].MaterialAmbient[3] = 1.0f;
    
    material[10].MaterialDiffuse[0] = 0.75164;
    material[10].MaterialDiffuse[1] = 0.60648;
    material[10].MaterialDiffuse[2] = 0.22648;
    material[10].MaterialDiffuse[3] = 1.0f;
    
    material[10].MaterialSpecular[0] = 0.628281;
    material[10].MaterialSpecular[1] = 0.555802;
    material[10].MaterialSpecular[2] = 0.366065;
    material[10].MaterialSpecular[3] = 1.0f;
    
    material[10].MaterialShininess[0] = 0.4 * 128;
    
    //---------------------------------------
    
    material[11].MaterialAmbient[0] = 0.19225;
    material[11].MaterialAmbient[1] = 0.19225;
    material[11].MaterialAmbient[2] = 0.19225;
    material[11].MaterialAmbient[3] = 1.0f;
    
    material[11].MaterialDiffuse[0] = 0.50754;
    material[11].MaterialDiffuse[1] = 0.50754;
    material[11].MaterialDiffuse[2] = 0.50754;
    material[11].MaterialDiffuse[3] = 1.0f;
    
    material[11].MaterialSpecular[0] = 0.508273;
    material[11].MaterialSpecular[1] = 0.508273;
    material[11].MaterialSpecular[2] = 0.508273;
    material[11].MaterialSpecular[3] = 1.0f;
    
    material[11].MaterialShininess[0] = 0.4 * 128;
    
    //---------------------------------------
    material[12].MaterialAmbient[0] = 0.0;
    material[12].MaterialAmbient[1] = 0.0;
    material[12].MaterialAmbient[2] = 0.0;
    material[12].MaterialAmbient[3] = 1.0f;
    
    material[12].MaterialDiffuse[0] = 0.01;
    material[12].MaterialDiffuse[1] = 0.01;
    material[12].MaterialDiffuse[2] = 0.01;
    material[12].MaterialDiffuse[3] = 1.0f;
    
    material[12].MaterialSpecular[0] = 0.50;
    material[12].MaterialSpecular[1] = 0.50;
    material[12].MaterialSpecular[2] = 0.50;
    material[12].MaterialSpecular[3] = 1.0f;
    
    material[12].MaterialShininess[0] = 0.25 * 128;
    
    //---------------------------------------
    
    material[13].MaterialAmbient[0] = 0.0;
    material[13].MaterialAmbient[1] = 0.1;
    material[13].MaterialAmbient[2] = 0.06;
    material[13].MaterialAmbient[3] = 1.0f;
    
    material[13].MaterialDiffuse[0] = 0.0;
    material[13].MaterialDiffuse[1] = 0.50980392;
    material[13].MaterialDiffuse[2] = 0.50980392;
    material[13].MaterialDiffuse[3] = 1.0f;
    
    material[13].MaterialSpecular[0] = 0.50196078;
    material[13].MaterialSpecular[1] = 0.50196078;
    material[13].MaterialSpecular[2] = 0.50196078;
    material[13].MaterialSpecular[3] = 1.0f;
    
    material[13].MaterialShininess[0] = 0.25 * 128;
    
    //---------------------------------------
    material[14].MaterialAmbient[0] = 0.0;
    material[14].MaterialAmbient[1] = 0.0;
    material[14].MaterialAmbient[2] = 0.0;
    material[14].MaterialAmbient[3] = 1.0f;
    
    material[14].MaterialDiffuse[0] = 0.1;
    material[14].MaterialDiffuse[1] = 0.35;
    material[14].MaterialDiffuse[2] = 0.1;
    material[14].MaterialDiffuse[3] = 1.0f;
    
    material[14].MaterialSpecular[0] = 0.45;
    material[14].MaterialSpecular[1] = 0.55;
    material[14].MaterialSpecular[2] = 0.45;
    material[14].MaterialSpecular[3] = 1.0f;
    
    material[14].MaterialShininess[0] = 0.25 * 128;
    
    //---------------------------------------
    
    material[15].MaterialAmbient[0] = 0.0;
    material[15].MaterialAmbient[1] = 0.0;
    material[15].MaterialAmbient[2] = 0.0;
    material[15].MaterialAmbient[3] = 1.0f;
    
    material[15].MaterialDiffuse[0] = 0.5;
    material[15].MaterialDiffuse[1] = 0.0;
    material[15].MaterialDiffuse[2] = 0.0;
    material[15].MaterialDiffuse[3] = 1.0f;
    
    material[15].MaterialSpecular[0] = 0.7;
    material[15].MaterialSpecular[1] = 0.6;
    material[15].MaterialSpecular[2] = 0.6;
    material[15].MaterialSpecular[3] = 1.0f;
    
    material[15].MaterialShininess[0] = 0.25 * 128;
    
    //---------------------------------------
    
    material[16].MaterialAmbient[0] = 0.0;
    material[16].MaterialAmbient[1] = 0.0;
    material[16].MaterialAmbient[2] = 0.0;
    material[16].MaterialAmbient[3] = 1.0f;
    
    material[16].MaterialDiffuse[0] = 0.55;
    material[16].MaterialDiffuse[1] = 0.55;
    material[16].MaterialDiffuse[2] = 0.55;
    material[16].MaterialDiffuse[3] = 1.0f;
    
    material[16].MaterialSpecular[0] = 0.70;
    material[16].MaterialSpecular[1] = 0.70;
    material[16].MaterialSpecular[2] = 0.70;
    material[16].MaterialSpecular[3] = 1.0f;
    
    material[16].MaterialShininess[0] = 0.25 * 128;
    
    //---------------------------------------
    
    material[17].MaterialAmbient[0] = 0.0;
    material[17].MaterialAmbient[1] = 0.0;
    material[17].MaterialAmbient[2] = 0.0;
    material[17].MaterialAmbient[3] = 1.0f;
    
    material[17].MaterialDiffuse[0] = 0.5;
    material[17].MaterialDiffuse[1] = 0.5;
    material[17].MaterialDiffuse[2] = 0.0;
    material[17].MaterialDiffuse[3] = 1.0f;
    
    material[17].MaterialSpecular[0] = 0.60;
    material[17].MaterialSpecular[1] = 0.60;
    material[17].MaterialSpecular[2] = 0.50;
    material[17].MaterialSpecular[3] = 1.0f;
    
    material[17].MaterialShininess[0] = 0.25 * 128;
    
    //---------------------------------------
    
    material[18].MaterialAmbient[0] = 0.02;
    material[18].MaterialAmbient[1] = 0.02;
    material[18].MaterialAmbient[2] = 0.02;
    material[18].MaterialAmbient[3] = 1.0f;
    
    material[18].MaterialDiffuse[0] = 0.01;
    material[18].MaterialDiffuse[1] = 0.01;
    material[18].MaterialDiffuse[2] = 0.01;
    material[18].MaterialDiffuse[3] = 1.0f;
    
    material[18].MaterialSpecular[0] = 0.4;
    material[18].MaterialSpecular[1] = 0.4;
    material[18].MaterialSpecular[2] = 0.4;
    material[18].MaterialSpecular[3] = 1.0f;
    
    material[18].MaterialShininess[0] = 0.078125 * 128;
    
    //---------------------------------------
    
    material[19].MaterialAmbient[0] = 0.0;
    material[19].MaterialAmbient[1] = 0.05;
    material[19].MaterialAmbient[2] = 0.05;
    material[19].MaterialAmbient[3] = 1.0f;
    
    material[19].MaterialDiffuse[0] = 0.4;
    material[19].MaterialDiffuse[1] = 0.5;
    material[19].MaterialDiffuse[2] = 0.5;
    material[19].MaterialDiffuse[3] = 1.0f;
    
    material[19].MaterialSpecular[0] = 0.04;
    material[19].MaterialSpecular[1] = 0.7;
    material[19].MaterialSpecular[2] = 0.7;
    material[19].MaterialSpecular[3] = 1.0f;
    
    material[19].MaterialShininess[0] = 0.078125 * 128;
    
    //----------------------------------------------------
    
    material[20].MaterialAmbient[0] = 0.0;
    material[20].MaterialAmbient[1] = 0.05;
    material[20].MaterialAmbient[2] = 0.0;
    material[20].MaterialAmbient[3] = 1.0f;
    
    material[20].MaterialDiffuse[0] = 0.4;
    material[20].MaterialDiffuse[1] = 0.5;
    material[20].MaterialDiffuse[2] = 0.4;
    material[20].MaterialDiffuse[3] = 1.0f;
    
    material[20].MaterialSpecular[0] = 0.04;
    material[20].MaterialSpecular[1] = 0.7;
    material[20].MaterialSpecular[2] = 0.04;
    material[20].MaterialSpecular[3] = 1.0f;
    
    material[20].MaterialShininess[0] = 0.078125 * 128;
    
    //--------------------------------------------------
    
    material[21].MaterialAmbient[0] = 0.05;
    material[21].MaterialAmbient[1] = 0.0;
    material[21].MaterialAmbient[2] = 0.0;
    material[21].MaterialAmbient[3] = 1.0f;
    
    material[21].MaterialDiffuse[0] = 0.5;
    material[21].MaterialDiffuse[1] = 0.4;
    material[21].MaterialDiffuse[2] = 0.4;
    material[21].MaterialDiffuse[3] = 1.0f;
    
    material[21].MaterialSpecular[0] = 0.7;
    material[21].MaterialSpecular[1] = 0.04;
    material[21].MaterialSpecular[2] = 0.04;
    material[21].MaterialSpecular[3] = 1.0f;
    
    material[21].MaterialShininess[0] = 0.078125 * 128;
    //-------------------------------------------------
    material[22].MaterialAmbient[0] = 0.05;
    material[22].MaterialAmbient[1] = 0.05;
    material[22].MaterialAmbient[2] = 0.05;
    material[22].MaterialAmbient[3] = 1.0f;
    
    material[22].MaterialDiffuse[0] = 0.5;
    material[22].MaterialDiffuse[1] = 0.5;
    material[22].MaterialDiffuse[2] = 0.5;
    material[22].MaterialDiffuse[3] = 1.0f;
    
    material[22].MaterialSpecular[0] = 0.7;
    material[22].MaterialSpecular[1] = 0.7;
    material[22].MaterialSpecular[2] = 0.7;
    material[22].MaterialSpecular[3] = 1.0f;
    
    material[22].MaterialShininess[0] = 0.078125 * 128;
    
    //--------------------------------------------------
    material[23].MaterialAmbient[0] = 0.05;
    material[23].MaterialAmbient[1] = 0.05;
    material[23].MaterialAmbient[2] = 0.0;
    material[23].MaterialAmbient[3] = 1.0f;
    
    material[23].MaterialDiffuse[0] = 0.5;
    material[23].MaterialDiffuse[1] = 0.5;
    material[23].MaterialDiffuse[2] = 0.4;
    material[23].MaterialDiffuse[3] = 1.0f;
    
    material[23].MaterialSpecular[0] = 0.7;
    material[23].MaterialSpecular[1] = 0.7;
    material[23].MaterialSpecular[2] = 0.04;
    material[23].MaterialSpecular[3] = 1.0f;
    
    material[23].MaterialShininess[0] = 0.078125 * 128;

    
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearColor(0.25f,0.25f,0.25f,0.0f);
    
    perspectiveProjectionMatrix = vmath::mat4::identity();
    
    
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect=[self bounds];
    
    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;
    currentWidth=width;
    currentHeight=height;
    
    if(height==0)
    height=1;
    
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    
    perspectiveProjectionMatrix=vmath::perspective(45.0f,width/height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
    [self drawView];
}


-(void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    if(vkey==0)
    glUseProgram(gShaderProgramObject1);
    else
    glUseProgram(gShaderProgramObject2);
    vmath:: mat4 modelMatrix;
    vmath:: mat4 viewMatrix;
    vmath:: mat4 projectionMatrix;
    vmath:: mat4 rotationMatrix;
    vmath:: mat4 scaleMatrix;
    
    
    if (gbLight == 1)
    {
        glUniform1i(isLKeypressedUniform, 1);
        glUniform3fv(laUniform, 1, LightAmbient);
        glUniform3fv(lsUniform,1,LightSpecular);
        glUniform3fv(ldUniform, 1, LightDiffuse);
        //rotationMatrix = mat4::identity();
        //rotationMatrix=rotate(angleOfXRotation, 1.0f, 0.0f, 0.0f);
        
        if (keypress == 1)
        {
            LightPosition[0]=  angleOfXRotation ;
        }
        if (keypress == 2)
        {
            LightPosition[1] = angleOfXRotation ;
        }
        if (keypress == 3)
        {
            LightPosition[2] = angleOfXRotation ;
        }
        glUniform4fv(lightPositionUniform, 1, LightPosition);
        
    }
    else
    {
        glUniform1i(isLKeypressedUniform, 0);
    }
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(0, 900, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    //projectionMatrix = perspective(45.0f, (GLfloat)currentWidth / (GLfloat)currentHeight, 0.1f, 100.0f);
    //scaleMatrix = scale(1.1f, 1.1f, 0.3f);
    //modelMatrix = modelMatrix * scaleMatrix;
    
    glUniform3fv(kaUniform, 1, material[0].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[0].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[0].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[0].MaterialShininess);
    
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //********************************************************************************
    
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(0, 720, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    //modelMatrix = modelMatrix * scaleMatrix;
    
    glUniform3fv(kaUniform, 1, material[1].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[1].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[1].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[1].MaterialShininess);
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    
    
    //****************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(0, 540, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    
    
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniform3fv(kaUniform, 1, material[2].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[2].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[2].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[2].MaterialShininess);
    
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    
    //******************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(0, 360, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[3].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[3].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[3].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[3].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    
    //**************************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(0, 180, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[4].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[4].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[4].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[4].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //************************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(0, 0, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[5].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[5].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[5].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[5].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    //***************************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(500, 900, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[6].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[6].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[6].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[6].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    //**************************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(500, 720, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[7].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[7].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[7].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[7].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //**************************************************************************************************
    
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(500, 540, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[8].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[8].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[8].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[8].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //***********************************************************************************************
    
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(500, 360, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[9].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[9].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[9].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[9].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    
    //***********************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(500, 180, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[10].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[10].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[10].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[10].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //*************************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(500, 0, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[11].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[11].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[11].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[11].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //****************************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1050, 900, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[12].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[12].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[12].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[12].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //***************************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1050, 720, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[13].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[13].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[13].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[13].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //*************************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1050, 540, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[14].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[14].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[14].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[14].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //***********************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1050, 360, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[15].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[15].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[15].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[15].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //****************************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1050, 180, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[16].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[16].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[16].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[16].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //********************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1050, 0, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[17].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[17].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[17].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[17].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    //***************************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1600, 900, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[18].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[18].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[18].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[18].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //************************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1600, 720, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[19].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[19].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[19].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[19].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    //**************************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1600, 540, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[20].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[20].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[20].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[20].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    //*******************************************************************************************
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1600, 360, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[21].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[21].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[21].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[21].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //**********************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1600, 180, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[22].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[22].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[22].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[22].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //******************************************************************************************
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    glViewport(1600, 0, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
  
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    glUniform3fv(kaUniform, 1, material[23].MaterialAmbient);
    glUniform3fv(kdUniform, 1, material[23].MaterialDiffuse);
    glUniform3fv(ksUniform, 1, material[23].MaterialSpecular);
    glUniform1fv(material_shininess_uniform, 1, material[23].MaterialShininess);
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    if (angleOfXRotation >= 2 * M_PI)
    {
        angleOfXRotation = 0.0f;
    }
    if (keypress == 1)
    {
        angleOfXRotation = angleOfXRotation + 0.04f;
        LightPosition[1] = 100.0f*sin(angleOfXRotation);
        LightPosition[2] = 100.0f*cos(angleOfXRotation);
    }
    if (keypress == 2)
    {
        angleOfXRotation = angleOfXRotation + 0.04f;
        LightPosition[0] = 100.0f*sin(angleOfXRotation);
        LightPosition[2] = 100.0f*cos(angleOfXRotation);
    }
    if (keypress == 3)
    {
        angleOfXRotation = angleOfXRotation + 0.04f;
        LightPosition[0] = 100.0f*sin(angleOfXRotation);
        LightPosition[1] = 100.0f*cos(angleOfXRotation);
    }
    if (angleOfYRotation >= 360.0f)
    {
        angleOfYRotation = 0.0f;
    }
    if (angleOfZRotation >= 360.0f)
    {
        angleOfZRotation = 0.0f;
    }
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
}

-(BOOL)acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
    
}

-(void)keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
            case 27:
            [[self window]toggleFullScreen:self];
            break;
            break;
            
            case 'F':
            case 'f':
            vkey = 0;
            break;
            case 'V':
            case 'v':
            vkey=1;
            break;
            case 'L':
            case 'l':
            if (gbLight == 0)
        {
            gbLight = 1;
        }
            else
        {
            gbLight = 0;
        }
            break;
            
            case 'X':
            case 'x':
            keypress = 1;
            angleOfXRotation = 0.0f;
            break;
            
            case 'Y':
            case 'y':
            keypress = 2;
            angleOfYRotation = 0.0f;
            break;
            
            case 'Z':
            case 'z':
            keypress = 3;
            angleOfZRotation = 0.0f;
            break;

            case 'Q':
            case 'q':
            [self release];
            [NSApp terminate:self];
            break;
        default:
            break;
    }
    
}

-(void)mouseDown:(NSEvent *)theEvent
{
    
}

-(void)mouseDragged:(NSEvent *)theEvent
{
    
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
    
}

-(void)dealloc
{
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }
    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);
        vbo_sphere_element = 0;
    }
    
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    
    
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }
    
    
    glUseProgram(gShaderProgramObject1);
    glDetachShader(gShaderProgramObject1, gFragmentShaderObject1);
    glDetachShader(gShaderProgramObject1, gVertexShaderObject1);
    glDeleteShader(gFragmentShaderObject1);
    gFragmentShaderObject1 = 0;
    glDeleteShader(gVertexShaderObject1);
    gVertexShaderObject1 = 0;
    glDeleteProgram(gShaderProgramObject1);
    gShaderProgramObject1 = 0;
    glUseProgram(0);
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}
