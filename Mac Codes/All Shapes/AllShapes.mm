#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"


enum
{
AMC_ATTRIBUTE_POSITION = 0,
AMC_ATTRIBUTE_COLOR,
AMC_ATTRIBUTE_NORMAL,
AMC_ATTRIBUTE_TEXCOORD0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *gpFile=NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
	
	NSApp=[NSApplication sharedApplication];
	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];
	[pPool release];
	return(0);
}

@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	

	// log file

	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	
	if(gpFile==NULL)
	{
		printf("Cannot create log file\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile,"Program is started successfully");


	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	window=[[NSWindow alloc] initWithContentRect:win_rect
					styleMask:NSWindowStyleMaskTitled |
				NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
				 NSWindowStyleMaskResizable
						backing:NSBackingStoreBuffered
						defer:NO];
	[window setTitle:@"2-2D Shapes(Colored)"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView]; 
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile,"Program is terminated successfully");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}

	//code
}

-(void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint vao_triangle;
	GLuint vbo_triangle_position;
	GLuint vbo_triangle_color;
	GLuint vao_rectangle;
	GLuint vbo_rectangle_position;
	GLuint vbo_rectangle_color;
    
    GLuint vao_lines1;
    GLuint vbo_lines1;
    GLuint vbo_line1_color;
    GLuint vao_lines2;
    GLuint vbo_lines2;
    GLuint vbo_line2_color;
    GLuint vao_lines3;
    GLuint vbo_lines3;
    GLuint vbo_line3_color;
    GLuint vao_lines4;
    GLuint vbo_lines4;
    GLuint vbo_line4_color;
    GLuint vao_circle;
    GLuint vbo_circle_color;
    GLuint vbo_circle_position;
    
    GLuint vao_glines;
    GLuint vbo_glines;
    GLuint vbo_glines_color;
    GLuint vao_glines1;
    GLuint vbo_glines1;
    GLuint vbo_glines1_color;
    GLuint vao_square1;
    GLuint vao_square2;
    GLuint vao_square3;
    GLuint vao_square4;
    GLuint vbo_position_square1;
    GLuint vbo_position_square2;
    GLuint vbo_position_square3;
    GLuint vbo_position_square4;
    GLuint vbo_color_square1;
    GLuint vbo_color_square2;
    GLuint vbo_color_square3;
    GLuint vbo_color_square4;
    GLuint vao_circle2;
    GLuint vbo_circle2_position;
    GLuint vbo_cicle2_color;
	GLuint mvpUniform;
	vmath::mat4 perspectiveProjectionMatrix;

}

-(id)initWithFrame:(NSRect)frame;
{
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};//last 0 is must
		
		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		
		if(pixelFormat==nil)
		{
			fprintf(gpFile,"\nNo Valid OpenGL Pixel Format Is Available");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			
		[self setPixelFormat:pixelFormat];
		
		[self setOpenGLContext:glContext];

	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime

{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	
	return(kCVReturnSuccess);
}


-(void)prepareOpenGL
{
	fprintf(gpFile,"OpenGL Version : %s\n",glGetString(GL_VERSION));
	fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar* vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_color=vColor;"
		"}";
		
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char *szInfolog=NULL;
	
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	szInfolog=NULL;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
	"#version 410" \
	"\n" \
	"in vec4 out_color;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
	"FragColor=out_color;" \
	"}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);


	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus=0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

    const GLfloat lineVertices[] = {
        -0.5f,-0.5f,0.0f,
        0.0f,0.5f,0.0f };
    
    
    const GLfloat line1Vertices[] = {
        0.0f,0.5f,0.0f,
        0.5f,-0.5f,0.0f
    };
    
    const GLfloat line2Vertices[] = {
        0.5f,-0.5f,0.0f,
        -0.5f,-0.5f,0.0f
    };
    
    const GLfloat line3Vertices[] = {
        0.0f,0.5f,0.0f,
        0.0f,-0.5f,0.0f
    };
    
    const GLfloat glineColor[] = {
        1.0f,0.0f,1.0f,
        1.0f,0.0f,1.0f
    };
    
    const GLfloat glineColor1[] = {
        1.0f,0.0f,1.0f,
        1.0f,0.0f,1.0f
    };
    
    const GLfloat lineColor[] = {
        1.0f,1.0f,0.0f,
        1.0f,1.0f,0.0f,
    };
    
    const GLfloat glineVertices[] = {
        -1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f };
    
    const GLfloat gline1Vertices[] = {
        0.0f,1.0f,0.0f,
        0.0f,-1.0f,0.0f
    };
    
    const GLfloat square1Vertices[] = {
        -0.5f,-0.5f,0.0f,
        -0.5f,0.5f,0.0f
    };
    const GLfloat square2Vertices[] = {
        -0.5f,0.5f,0.0f,
        0.5f,0.5f,0.0f
    };
    const GLfloat square3Vertices[] = {
        0.5f,0.5f,0.0f,
        0.5f,-0.5f,0.0f
    };
    const GLfloat square4Vertices[] = {
        0.5f,-0.5f,0.0f,
        -0.5f,-0.5f,0.0f
    };
    
    const GLfloat square1Color[]=
    {
        1.0f,1.0f,0.0f,
        1.0f,1.0f,0.0f
    };
    
    const GLfloat square2Color[] =
    {
        1.0f,1.0f,0.0f,
        1.0f,1.0f,0.0f
    };
    
    const GLfloat square3Color[] =
    {
        1.0f,1.0f,0.0f,
        1.0f,1.0f,0.0f
    };
    
    const GLfloat square4Color[] =
    {
        1.0f,1.0f,0.0f,
        1.0f,1.0f,0.0f
    };
    
    glGenVertexArrays(1, &vao_square1);
    glBindVertexArray(vao_square1);
    glGenBuffers(1, &vbo_position_square1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square1Vertices), square1Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_color_square1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square1Color), square1Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    
    glGenVertexArrays(1, &vao_square2);
    glBindVertexArray(vao_square2);
    glGenBuffers(1, &vbo_position_square2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square2Vertices), square2Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_color_square1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square1Color), square1Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    
    glGenVertexArrays(1, &vao_square3);
    glBindVertexArray(vao_square3);
    glGenBuffers(1, &vbo_position_square3);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square3);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square3Vertices), square3Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_color_square3);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square3);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square3Color), square3Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    
    glGenVertexArrays(1, &vao_square4);
    glBindVertexArray(vao_square4);
    glGenBuffers(1, &vbo_position_square4);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square4);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square4Vertices), square4Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_color_square4);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square4);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square4Color), square4Color, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    glGenVertexArrays(1, &vao_lines1);
    glBindVertexArray(vao_lines1);
    glGenBuffers(1, &vbo_lines1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_lines1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), lineVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_line1_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_line1_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    glGenVertexArrays(1, &vao_lines2);
    glBindVertexArray(vao_lines2);
    glGenBuffers(1, &vbo_lines2);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_lines2);
    glBufferData(GL_ARRAY_BUFFER, sizeof(line1Vertices), line1Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &vbo_line2_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_line2_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    
    //Line3
    
    glGenVertexArrays(1, &vao_lines3);
    glBindVertexArray(vao_lines3);
    glGenBuffers(1, &vbo_lines3);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_lines3);
    glBufferData(GL_ARRAY_BUFFER, sizeof(line2Vertices), line2Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glGenBuffers(1, &vbo_line3_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_line3_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindVertexArray(0);
    
    //Line4
    
    
    
    //CIRCLE
    
    //Graph
    glGenBuffers(1, &vbo_circle_position);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_circle_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), NULL, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    
    glGenVertexArrays(1, &vao_glines);
    glBindVertexArray(vao_glines);
    glGenBuffers(1, &vbo_glines);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_glines);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glineVertices), glineVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_glines_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_glines_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glineColor), glineColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
    glBindVertexArray(0);
    
    
    glGenVertexArrays(1, &vao_glines1);
    glBindVertexArray(vao_glines1);
    glGenBuffers(1, &vbo_glines1);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_glines1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(gline1Vertices), gline1Vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_glines1_color);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_glines1_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glineColor1), glineColor1, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
    glBindVertexArray(0);

	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect=[self bounds];
	
	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
		perspectiveProjectionMatrix=vmath::perspective(45.0f,width/height, 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}


-(void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glUseProgram(gShaderProgramObject);
    void DrawCircle(GLfloat);
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;
    
    
    
    
    
    for (GLfloat x = -1.0f; x <= 1.05f; x = x + 0.05f)
    {
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
        modelViewMatrix = translate(0.0f, x, -3.0f);
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_glines);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
    }
    
    for (GLfloat y = -1.0f; y <= 1.05f; y = y + 0.05f)
    {
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
        modelViewMatrix = translate(y, 0.0f, -3.0f);
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
        glBindVertexArray(vao_glines1);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
    }
    
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_lines1);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_lines2);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_lines3);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_lines4);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_circle);
    DrawCircle(0.301f);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_square1);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_square2);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_square3);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_square4);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
    
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();
    modelViewMatrix = translate(0.0f, 0.2f, -3.0f);
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    glBindVertexArray(vao_circle);
    DrawCircle(0.7f);
    glBindVertexArray(0);
    
    glUseProgram(0);
	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
}

void DrawCircle(GLfloat radius) {
    GLfloat angle = 0;
    
    static float circlePos[(unsigned int)((2 * 3.1415) / 0.001) * 3];
    static float circleColor[(unsigned int)((2 * 3.1415) / 0.001) * 3];
    
    
    
    int index = 0;
    int index1 = 0;
    
    for (angle = 0.0f; angle < 2 * 3.14159265; angle = angle + 0.001f) {
        circlePos[index++] = cos(angle)*radius;
        circlePos[index++] = sin(angle)*radius-0.20;
        circlePos[index++] = 0.0f;
        //indexPos++;
    }
    for (angle = 0.0f; angle < 2 * 3.14159265; angle = angle + 0.001f) {
        circleColor[index1++] = 1.0f;
        circleColor[index1++] = 1.0f;
        circleColor[index1++] = 0.0f;
        //indexPos++;
    }
    
    
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), circlePos, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), circleColor, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_POINTS, 0, ((2 * 3.1415) / 0.001));
    
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);

}

-(void)keyDown:(NSEvent *)theEvent
{
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
		[self release];
		[NSApp terminate:self];
		break;
		
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
			break;
	}

}

-(void)mouseDown:(NSEvent *)theEvent
{
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{

}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{
	if (vbo_rectangle_position)
	{
		glDeleteBuffers(1, &vbo_rectangle_position);
		vbo_rectangle_position = 0;
	}
	if (vao_rectangle)
	{
		glDeleteVertexArrays(1, &vao_rectangle);
		vao_rectangle = 0;
	}
	if (vbo_triangle_position)
	{
		glDeleteBuffers(1, &vbo_triangle_position);
		vbo_triangle_position = 0;
	}
	if (vao_triangle)
	{
		glDeleteVertexArrays(1, &vao_triangle);
		vao_triangle = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
