#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"


enum
{
AMC_ATTRIBUTE_POSITION = 0,
AMC_ATTRIBUTE_COLOR,
AMC_ATTRIBUTE_NORMAL,
AMC_ATTRIBUTE_TEXCOORD0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *gpFile=NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
	
	NSApp=[NSApplication sharedApplication];
	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];
	[pPool release];
	return(0);
}

@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	

	// log file

	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	
	if(gpFile==NULL)
	{
		printf("Cannot create log file\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile,"Program is started successfully");


	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	window=[[NSWindow alloc] initWithContentRect:win_rect
					styleMask:NSWindowStyleMaskTitled |
				NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
				 NSWindowStyleMaskResizable
						backing:NSBackingStoreBuffered
						defer:NO];
	[window setTitle:@"Deathly Hallow"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView]; 
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile,"Program is terminated successfully");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}

	//code
}

-(void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint vao_lines1;
	GLuint vbo_lines1;
	GLuint vbo_line1_color;
	GLuint vao_lines2;
	GLuint vbo_lines2;
	GLuint vbo_line2_color;
	GLuint vao_lines3;
	GLuint vbo_lines3;
	GLuint vbo_line3_color;
	GLuint vao_lines4;
	GLuint vbo_lines4;
	GLuint vbo_line4_color;
	GLuint vao_circle;
	GLuint vbo_circle_color;
	GLuint vbo_circle_position;
	GLuint mvpUniform;
	vmath::mat4 perspectiveProjectionMatrix;

}

-(id)initWithFrame:(NSRect)frame;
{
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};//last 0 is must
		
		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		
		if(pixelFormat==nil)
		{
			fprintf(gpFile,"\nNo Valid OpenGL Pixel Format Is Available");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			
		[self setPixelFormat:pixelFormat];
		
		[self setOpenGLContext:glContext];

	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime

{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	
	return(kCVReturnSuccess);
}


-(void)prepareOpenGL
{
	fprintf(gpFile,"OpenGL Version : %s\n",glGetString(GL_VERSION));
	fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar* vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"}";
		
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char *szInfolog=NULL;
	
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	szInfolog=NULL;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
	"#version 410" \
	"\n" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
	"FragColor=vec4(1.0,1.0,0.0,1.0);" \
	"}" ;

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);


	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus=0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

	const GLfloat lineVertices[] = {
	-1.0f,-1.0f,0.0f,
	0.0f,1.0f,0.0f };


	const GLfloat line1Vertices[] = {
		0.0f,1.0f,0.0f,
		1.0f,-1.0f,0.0f
	};

	const GLfloat line2Vertices[] = {
		1.0f,-1.0f,0.0f,
		-1.0f,-1.0f,0.0f
	};

	const GLfloat line3Vertices[] = {
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0f
	};

	const GLfloat lineColor[] = {
		1.0f,1.0f,0.0f
	};


	GLfloat angle = 0;

	static float circlePos[(unsigned int)((2 * 3.1415) / 0.001) * 3];
	static float circleColor[(unsigned int)((2 * 3.1415) / 0.001) * 3];

	float radius=0.609f;

	int index = 0;


	for (angle = 0.0f; angle < 2 * 3.14159265; angle = angle + 0.001f) {
		circlePos[index++] = cos(angle)*radius;
		circlePos[index++] = sin(angle)*radius-0.38;
		circlePos[index++] = 0.0f;
		//indexPos++;
	}
	
	

	glGenVertexArrays(1, &vao_lines1);
	glBindVertexArray(vao_lines1);
	glGenBuffers(1, &vbo_lines1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), lineVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_line1_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line1_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glGenVertexArrays(1, &vao_lines2);
	glBindVertexArray(vao_lines2);
	glGenBuffers(1, &vbo_lines2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line1Vertices), line1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &vbo_line2_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line2_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	//Line3

	glGenVertexArrays(1, &vao_lines3);
	glBindVertexArray(vao_lines3);
	glGenBuffers(1, &vbo_lines3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line2Vertices), line2Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &vbo_line3_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line3_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Line4

	glGenVertexArrays(1, &vao_lines4);
	glBindVertexArray(vao_lines4);
	glGenBuffers(1, &vbo_lines4);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines4);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line3Vertices), line3Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &vbo_line4_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line4_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	//CIRCLE
	
	glGenVertexArrays(1, &vao_circle);
	glBindVertexArray(vao_circle);

	glGenBuffers(1, &vbo_circle_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(circlePos),circlePos, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	glBindVertexArray(0);

	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect=[self bounds];
	
	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
		perspectiveProjectionMatrix=vmath::perspective(45.0f,width/height, 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}


-(void)drawView
{
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);





	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines1);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines2);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines3);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines4);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_circle);
	glDrawArrays(GL_POINTS, 0, ((2 * 3.1415) / 0.001));
	glBindVertexArray(0);

	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);

}

-(void)keyDown:(NSEvent *)theEvent
{
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
		[self release];
		[NSApp terminate:self];
		break;

		
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
			break;
	}

}

-(void)mouseDown:(NSEvent *)theEvent
{
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{

}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{
	if (vbo_lines1)
	{
		glDeleteBuffers(1, &vbo_lines1);
		vbo_lines1= 0;
	}
	if (vao_lines1)
	{
		glDeleteVertexArrays(1, &vao_lines1);
		vao_lines1 = 0;
	}
	

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
