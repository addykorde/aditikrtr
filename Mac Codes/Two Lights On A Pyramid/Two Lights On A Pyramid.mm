#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"


enum
{
AMC_ATTRIBUTE_POSITION = 0,
AMC_ATTRIBUTE_COLOR,
AMC_ATTRIBUTE_NORMAL,
AMC_ATTRIBUTE_TEXCOORD0
};


struct Lights {
    float Ambient[4];
    float Diffuse[4];
    float Specular[4];
    float Position[4];
};

float MaterialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
float MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
float MaterialShininess[] = { 50.0f };

Lights lights[2];

int gbLight = 0;
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *gpFile=NULL;


@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
	
	NSApp=[NSApplication sharedApplication];
	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];
	[pPool release];
	return(0);
}

@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	

	// log file

	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	
	if(gpFile==NULL)
	{
		printf("Cannot create log file\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile,"Program is started successfully");


	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	window=[[NSWindow alloc] initWithContentRect:win_rect
					styleMask:NSWindowStyleMaskTitled |
				NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
				 NSWindowStyleMaskResizable
						backing:NSBackingStoreBuffered
						defer:NO];
	[window setTitle:@"3D Animation"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView]; 
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile,"Program is terminated successfully");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}

	//code
}

-(void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_position_pyramid;
    GLuint vbo_normal_pyramid;
    GLuint vbo_normal1_pyramid;
    GLuint mUniform;
    GLuint vUniform;
    GLuint pUniform;
    GLuint laUniform_red;
    GLuint kaUniform_red;
    GLuint lsUniform_red;
    GLuint ldUniform_red;
    GLuint kdUniform_red;
    GLuint ksUniform_red;
    GLuint lightPositionUniform_red;
    GLuint laUniform_blue;
    GLuint kaUniform_blue;
    GLuint lsUniform_blue;
    GLuint ldUniform_blue;
    GLuint kdUniform_blue;
    GLuint ksUniform_blue;
    GLuint lightPositionUniform_blue;
    GLuint isLKeypressedUniform;
    GLuint material_shininess_uniform;
	vmath::mat4 perspectiveProjectionMatrix;
    
    
    

}

-(id)initWithFrame:(NSRect)frame;
{
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};//last 0 is must
		
		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		
		if(pixelFormat==nil)
		{
			fprintf(gpFile,"\nNo Valid OpenGL Pixel Format Is Available");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			
		[self setPixelFormat:pixelFormat];
		
		[self setOpenGLContext:glContext];

	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime

{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	
	return(kCVReturnSuccess);
}


-(void)prepareOpenGL
{
	fprintf(gpFile,"OpenGL Version : %s\n",glGetString(GL_VERSION));
	fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar* vertexShaderSourceCode =
		"#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vnormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec3 u_ld_blue;" \
    "uniform vec3 u_la_blue;" \
    "uniform vec3 u_ls_blue;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_ks;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ld_red;" \
    "uniform vec3 u_la_red;" \
    "uniform vec3 u_ls_red;" \
    "uniform float m_shininess;"
    "uniform vec4 u_light_position_red;" \
    "uniform vec4 u_light_position_blue;" \
    "out vec3 finalColor;" \
    "vec3 light_direction_red;"\
    "vec3 light_direction_blue;"\
    "out vec3 phong_ads_light1;" \
    "out vec3 phong_ads_light2;" \
    "vec3 reflection_vector_blue;"\
    "vec3 reflection_vector_red;"\
    "float tn_dot_ld_red;" \
    "float tn_dot_ld_blue;" \
    "vec3 ambient1;"\
    "vec3 ambient2;"\
    "vec3 diffuse1;"\
    "vec3 diffuse2;"\
    "vec3 specular1;"\
    "vec3 specular2;"\
    "vec3 tnorm;" \
    "void main(void)" \
    "{" \
    "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
    "tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \
    
    "light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);" \
    
    "light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);" \
    
    "tn_dot_ld_red=max(dot(light_direction_red,tnorm),0.0);" \
    
    "tn_dot_ld_blue=max(dot(light_direction_blue,tnorm),0.0);" \
    
    "reflection_vector_red=reflect(-light_direction_red,tnorm);" \
    "reflection_vector_blue=reflect(-light_direction_blue,tnorm);" \
    
    "vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \
    
    
    "ambient1  = u_la_red* u_ka;" \
    "ambient2= u_la_blue* u_ka;" \
    
    "diffuse1 = u_ld_red * u_kd * tn_dot_ld_red;" \
    "diffuse2 = u_ld_blue * u_kd * tn_dot_ld_blue;" \
    
    "specular1 = u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),m_shininess);" \
    "specular2 = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),m_shininess);" \
    
    "phong_ads_light1  =ambient1+diffuse1+specular1;" \
    "phong_ads_light2  =ambient2+diffuse2+specular2;" \
    "finalColor=phong_ads_light2+phong_ads_light1;" \
    "}" \
    "else" \
    "{" \
    "finalColor=vec3(1.0,1.0,1.0);"  \
    "}" \
    
    "}";
		
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char *szInfolog=NULL;
	
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	szInfolog=NULL;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
	"#version 410" \
    "\n" \
    "in vec3 phong_ads_light1;" \
    "in vec3 phong_ads_light2;" \
    "in vec3 finalColor;" \
    "out vec4 FragColor1;" \
    "uniform int u_lkeyispressed;" \
    "void main(void)" \
    "{" \
    
    "FragColor1=vec4(finalColor,1.0);" \
    "}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);


	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vnormal");
	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus=0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

    mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
    vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
    
    laUniform_red = glGetUniformLocation(gShaderProgramObject, "u_la_red");
    lsUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
    ldUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
    
    kdUniform_red = glGetUniformLocation(gShaderProgramObject, "u_kd");
    kaUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ka");
    ksUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ks");
    
    lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");
    
    laUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
    lsUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
    ldUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
    
    lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");
    
    isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");
    
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "m_shininess");


    const GLfloat pyramidVertices[] = { 0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f ,
        1.0f, -1.0f, 1.0f ,
        
        0.0f, 1.0f, 0.0f ,
        1.0f, -1.0f, 1.0f ,
        1.0f, -1.0f, -1.0f ,
        
        0.0f, 1.0f, 0.0f ,
        -1.0f, -1.0f,-1.0f ,
        -1.0f, -1.0f, 1.0f ,
        
        0.0f, 1.0f, 0.0f ,
        1.0f, -1.0f, -1.0f ,
        -1.0f, -1.0f, -1.0f ,
    };
    
    const GLfloat pyramidNormals[] = {
        0.0f, 0.447214f, 0.894427f,
        0.0f, 0.447214f, 0.894427f,
        0.0f, 0.447214f, 0.894427f,
        
        
        
        0.894427f, 0.447214f, 0.0f,
        0.894427f, 0.447214f, 0.0f,
        0.894427f, 0.447214f, 0.0f,
        
        
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f,
        
        
        0.0f, 0.447214f, -0.89442f,
        0.0f, 0.447214f, -0.89442f,
        0.0f, 0.447214f, -0.89442f,
        
        
    };
    
    
    
    //Pyramid
    glGenVertexArrays(1, &vao_pyramid);
    glBindVertexArray(vao_pyramid);
    glGenBuffers(1, &vbo_position_pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &vbo_normal_pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormals), pyramidNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glBindVertexArray(0);
    
    
    //Values
    
    lights[0].Ambient[0] = 0.0f;
    lights[0].Ambient[1] = 0.0f;
    lights[0].Ambient[2] = 0.0f;
    lights[0].Ambient[3] = 1.0f;
    
    lights[0].Diffuse[0] = 1.0f;
    lights[0].Diffuse[1] = 0.0f;
    lights[0].Diffuse[2] = 0.0f;
    lights[0].Diffuse[3] = 1.0f;
    
    
    lights[0].Specular[0] = 1.0f;
    lights[0].Specular[1] = 0.0f;
    lights[0].Specular[2] = 0.0f;
    lights[0].Specular[3] = 1.0f;
    
    
    lights[0].Position[0] = -2.0f;
    lights[0].Position[1] = 0.0f;
    lights[0].Position[2] = 0.0f;
    lights[0].Position[3] = 1.0f;
    
    lights[1].Ambient[0] = 0.0f;
    lights[1].Ambient[1] = 0.0f;
    lights[1].Ambient[2] = 0.0f;
    lights[1].Ambient[3] = 1.0f;
    
    lights[1].Diffuse[0] = 0.0f;
    lights[1].Diffuse[1] = 0.0f;
    lights[1].Diffuse[2] = 1.0f;
    lights[1].Diffuse[3] = 1.0f;
    
    lights[1].Specular[0] = 0.0f;
    lights[1].Specular[1] = 0.0f;
    lights[1].Specular[2] = 1.0f;
    lights[1].Specular[3] = 1.0f;
    
    lights[1].Position[0] = 2.0f;
    lights[1].Position[1] = 0.0f;
    lights[1].Position[2] = 0.0f;
    lights[1].Position[3] = 1.0f;


	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	perspectiveProjectionMatrix = vmath::mat4::identity();
	
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}


-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect=[self bounds];
	
	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
		perspectiveProjectionMatrix=vmath::perspective(45.0f,width/height, 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}


-(void)drawView
{
    static float angle_pyramid=0.0f;
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;
    vmath::mat4 rotationMatrix;
    
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);
    modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    modelMatrix = modelMatrix * rotationMatrix;
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    if (gbLight == 1)
    {
        glUniform1i(isLKeypressedUniform, 1);
        
        glUniform3fv(laUniform_blue, 1, lights[1].Ambient);
        glUniform3fv(ldUniform_blue, 1, lights[1].Diffuse);
        glUniform3fv(lsUniform_blue, 1, lights[1].Specular);
        
        glUniform4fv(lightPositionUniform_blue, 1, lights[1].Position);
        
        
        glUniform3fv(laUniform_red, 1, lights[0].Ambient);
        glUniform3fv(ldUniform_red, 1, lights[0].Diffuse);
        glUniform3fv(lsUniform_red, 1, lights[0].Specular);
        glUniform4fv(lightPositionUniform_red, 1, lights[0].Position);
        
        
        glUniform3fv(kaUniform_red, 1, MaterialAmbient);
        glUniform3fv(kdUniform_red, 1, MaterialDiffuse);
        glUniform3fv(ksUniform_red, 1, MaterialSpecular);
        glUniform1fv(material_shininess_uniform, 1, MaterialShininess);
        
        
    }
    else
    {
        glUniform1i(isLKeypressedUniform, 0);
    }
    
    glBindVertexArray(vao_pyramid);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    glBindVertexArray(0);



	glUseProgram(0);

	if (angle_pyramid <= 360.0f)
	{
		angle_pyramid = angle_pyramid + 5.0f;
	}
	else
	{
		angle_pyramid = 0.0f;
	}
	

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);

}

-(void)keyDown:(NSEvent *)theEvent
{
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
		[self release];
		[NSApp terminate:self];
		break;
		
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
        case'L':
        case 'l':
            if (gbLight == 0)
        {
            gbLight = 1;
        }
            else
        {
            gbLight = 0;
        }
            break;
		default:
			break;
	}

}

-(void)mouseDown:(NSEvent *)theEvent
{
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{

}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{
	if (vbo_position_pyramid)
	{
		glDeleteBuffers(1, &vbo_position_pyramid);
		vbo_position_pyramid = 0;
	}
	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}
	

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
