#import<Foundation/Foundation.h>
#import<Cocoa/Cocoa.h>

#import<QuartzCore/CVDisplayLink.h>

#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"


enum
{
AMC_ATTRIBUTE_POSITION = 0,
AMC_ATTRIBUTE_COLOR,
AMC_ATTRIBUTE_NORMAL,
AMC_ATTRIBUTE_TEXCOORD0
};


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *,const CVTimeStamp *,CVOptionFlags,CVOptionFlags *,void *);

FILE *gpFile=NULL;
    int gbLight = 0;

@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int argc, const char * argv[])
{
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];
	
	NSApp=[NSApplication sharedApplication];
	[NSApp setDelegate:[[AppDelegate alloc]init]];

	[NSApp run];
	[pPool release];
	return(0);
}

@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	

	// log file

	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile=fopen(pszLogFileNameWithPath,"w");
	
	if(gpFile==NULL)
	{
		printf("Cannot create log file\n");
		[self release];
		[NSApp terminate:self];
	}
	
	fprintf(gpFile,"Program is started successfully");


	NSRect win_rect;
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);

	window=[[NSWindow alloc] initWithContentRect:win_rect
					styleMask:NSWindowStyleMaskTitled |
				NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable |
				 NSWindowStyleMaskResizable
						backing:NSBackingStoreBuffered
						defer:NO];
	[window setTitle:@"Interleaved"];
	[window center];

	glView=[[GLView alloc]initWithFrame:win_rect];
	
	[window setContentView:glView]; 
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	fprintf(gpFile,"Program is terminated successfully");
	
	if(gpFile)
	{
		fclose(gpFile);
		gpFile=NULL;
	}

	//code
}

-(void)windowWillClose:(NSNotification *)notification
{
	[NSApp terminate:self];
}

-(void)dealloc
{
	[glView release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

    GLuint vbo_color_cube;
    GLuint vbo_normal_cube;
    GLuint vbo_texture_cube;
    GLuint vao_cube;
    GLuint vbo_cube;
	GLuint mvpUniform;
	
    GLuint vao_sphere;
    GLuint vbo_position_sphere;
    GLuint vbo_normal_sphere;
    GLuint vbo_element_sphere;
    GLuint mUniform;
    GLuint vUniform;
    GLuint pUniform;
    GLuint laUniform;
    GLuint kaUniform;
    GLuint lsUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint lightPositionUniform;
    GLuint isLKeypressedUniform;
 
    GLuint material_shininess_uniform;
    
    GLuint texture_smiley;
    GLuint samplerUniform;
    float light_ambient[4];
    float light_diffuse[4];
    float light_specular[4];
    float light_position[4];
    
    float material_ambient[4];
    float material_diffuse[4];
    float material_specular[4];
    float material_shininess[1];
    
    

    
	GLuint pyramid_texture;
	GLuint cube_texture;
	
	GLuint texture_sampler_uniform;
	vmath::mat4 perspectiveProjectionMatrix;

}

-(id)initWithFrame:(NSRect)frame;
{
	self=[super initWithFrame:frame];
	
	if(self)
	{
		[[self window]setContentView:self];
		
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};//last 0 is must
		
		NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		
		if(pixelFormat==nil)
		{
			fprintf(gpFile,"\nNo Valid OpenGL Pixel Format Is Available");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			
		[self setPixelFormat:pixelFormat];
		
		[self setOpenGLContext:glContext];

	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime

{
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];
	[self drawView];
	[pool release];
	
	return(kCVReturnSuccess);
}


-(void)prepareOpenGL
{
	fprintf(gpFile,"OpenGL Version : %s\n",glGetString(GL_VERSION));
	fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt=1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	
	const GLchar* vertexShaderSourceCode =
		"#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vnormal;" \
    "in vec4 vColor;" \
    "out vec4 out_color;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "in vec2 vTexCoord;" \
    "out vec2 out_texcoord;" \
    "uniform int u_lkeyispressed;" \
    "uniform vec4 u_light_position;" \
    "out vec4 eye_coordinates;" \
    "out vec3 light_direction;" \
    "out vec3 tnorm;" \
    "out vec3 viewer_vector;" \
    "void main(void)" \
    "{" \
    "gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
    "tnorm=mat3(u_v_matrix*u_m_matrix)*vnormal;" \
    "light_direction=vec3(u_light_position)-eye_coordinates.xyz;" \
    "viewer_vector=vec3(-eye_coordinates);" \
    "}" \
    "else" \
    "{" \
    "}" \
    "out_texcoord=vTexCoord;" \
    "out_color=vColor;"\
    "}";

		
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	glCompileShader(gVertexShaderObject);

	GLint iInfoLogLength=0;
	GLint iShaderCompileStatus=0;
	char *szInfolog=NULL;
	
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

	
	iInfoLogLength=0;
	iShaderCompileStatus=0;
	szInfolog=NULL;

	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar* fragmentShaderSourceCode =
	"#version 410" \
    "\n" \
    "out vec4 FragColor;" \
    "uniform int u_lkeyispressed;" \
    "in vec2 out_texcoord;" \
    "in vec4 out_color;"
    "uniform sampler2D u_sampler;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_ks;" \
    "uniform vec3 u_kd;" \
    "uniform float m_shininess;"
    "in vec3 light_direction;" \
    "in vec3 tnorm;"
    "in vec3 viewer_vector;" \
    "vec3 phong_ads_light;"\
    "void main(void)" \
    "{" \
    "if(u_lkeyispressed == 1)" \
    "{" \
    "vec3 tdnorm=normalize(tnorm);" \
    "vec3 light_direction1=normalize(light_direction);" \
    "float tn_dot_ld=max(dot(light_direction1,tdnorm),0.0);" \
    "vec3 reflection_vector=reflect(-light_direction1,tdnorm);" \
    "vec3 viewer_vector1=normalize(viewer_vector);" \
    "vec3 ambient = u_la * u_ka;" \
    "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
    "vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,viewer_vector1),0.0),m_shininess);" \
    "phong_ads_light=ambient+diffuse+specular;" \
    "}" \
    "else"
    "{"
    "phong_ads_light=vec3(1.0,1.0,1.0);" \
    "}"
    "FragColor=vec4(texture(u_sampler,out_texcoord)*out_color*(phong_ads_light,1.0));" \
    "}";
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);


	glCompileShader(gFragmentShaderObject);

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

    gShaderProgramObject = glCreateProgram();
    
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vnormal");
    
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
    
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");

	glLinkProgram(gShaderProgramObject);

	GLint iProgramLinkStatus=0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Linking of program %s", szInfolog);
				free(szInfolog);

				[self release];
				[NSApp terminate:self];
			}
		}
	}

    mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
    vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
    laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
    lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
    ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
    kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
    kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
    ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
    material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "m_shininess");
    lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
    isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");
    samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	

	cube_texture=[self loadTextureFromBMPFile:"marble.bmp"];

    const GLfloat cubeVCNT[] = {
        1.0f, 1.0f, -1.0f,   0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,0.0f,
        -1.0f, 1.0f, -1.0f,     0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,0.0f,
        -1.0f, 1.0f, 1.0f,     0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,1.0f,
        1.0f, 1.0f, 1.0f,     0.0f, 1.0f, 0.0f,0.0f, 0.0f, 1.0f, 0.0f,1.0f,
        
        1.0f, -1.0f, -1.0f,     1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,0.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,1.0f,
        -1.0f, -1.0f, 1.0f,     1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,1.0f,
        1.0f, -1.0f, 1.0f,     1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,0.0f,
        
        1.0f, 1.0f, 1.0f,     1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f,1.0f,0.0f,
        -1.0f, 1.0f, 1.0f,     1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f,1.0f,1.0f,
        -1.0f, -1.0f, 1.0f,     1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f,0.0f,1.0f,
        1.0f, -1.0f, 1.0f,     1.0f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f,0.0f,0.0f,
        
        1.0f, 1.0f, -1.0f,     1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f,0.0f,0.0f,
        -1.0f, 1.0f, -1.0f,     1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f,1.0f,0.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f,1.0f,1.0f,
        1.0f, -1.0f, -1.0f,     1.0f, 1.0f, 0.0f, -1.0f, 0.0f, 0.0f,0.0f,1.0f,
        
        1.0f, 1.0f, -1.0f,     0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,1.0f,
        1.0f, 1.0f, 1.0f,     0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,0.0f,
        1.0f, -1.0f, 1.0f,     0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,0.0f,
        1.0f, -1.0f, -1.0f,     0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f,1.0f,
        
        -1.0f, 1.0f, 1.0f,     1.0f, 0.0f, 1.0f, 0.0f, -1.0f, 0.0f,1.0f,1.0f,
        -1.0f, 1.0f, -1.0f,     1.0f, 0.0f, 1.0f, 0.0f, -1.0f, 0.0f,0.0f,1.0f,
        -1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f, -1.0f, 0.0f,0.0f,0.0f,
        -1.0f, -1.0f, 1.0f , 1.0f, 0.0f, 1.0f, 0.0f, -1.0f, 0.0f,1.0f,0.0f
    };
    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);
    glGenBuffers(1, &vbo_cube);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube);
    glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), cubeVCNT, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(0 * sizeof(float)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
    glBindVertexArray(0);
    
    light_ambient[0] = 0.5f;
    light_ambient[1] = 0.5f;
    light_ambient[2] = 0.5f;
    light_ambient[3] = 0.5f;
    
    light_diffuse[0] = 1.0f;
    light_diffuse[1] = 1.0f;
    light_diffuse[2] = 1.0f;
    light_diffuse[3] = 1.0f;
    
    light_specular[0] = 1.0f;
    light_specular[1] = 1.0f;
    light_specular[2] = 1.0f;
    light_specular[3] = 1.0f;
    
    light_position[0] = 100.0f;
    light_position[1] = 100.0f;
    light_position[2] = 100.0f;
    light_position[3] = 1.0f;
    
    
    material_ambient[0] = 0.25f;
    material_ambient[1] = 0.25f;
    material_ambient[2] = 0.25f;
    material_ambient[3] = 1.0f;
    
    material_diffuse[0] = 1.0f;
    material_diffuse[1] = 1.0f;
    material_diffuse[2] = 1.0f;
    material_diffuse[3] = 1.0f;
    
    material_specular[0] = 1.0f;
    material_specular[1] = 1.0f;
    material_specular[2] = 1.0f;
    material_specular[3] = 1.0f;
    
    
    material_shininess[0] = 128.0f;
    glBindBuffer(GL_ARRAY_BUFFER, 0);



    
	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glEnable(GL_TEXTURE_2D);
	perspectiveProjectionMatrix = vmath::mat4::identity();
	
	

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
	CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBMPFile:(const char*)texFileName
{
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName=[mainBundle bundlePath];
	NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
	NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/%s",parentDirPath,texFileName];
	
	NSImage *bmpImage=[[NSImage alloc]
		initWithContentsOfFile:textureFileNameWithPath];
	if(!bmpImage)
	{
		NSLog(@"can't find %@",textureFileNameWithPath);
		return(0);
	}

	CGImageRef cgImage=[bmpImage CGImageForProposedRect:nil context:nil hints:nil];
	
	int w=(int)CGImageGetWidth(cgImage);
	int h=(int)CGImageGetHeight(cgImage);

	CFDataRef imageData=CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
	void *pixels=(void *)CFDataGetBytePtr(imageData);
	
	GLuint bmpTexture;
	glGenTextures(1,&bmpTexture);
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glBindTexture(GL_TEXTURE_2D,bmpTexture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
	glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGBA,
			w,
			h,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	CFRelease(imageData);
	return(bmpTexture);
	
}

-(void)reshape
{
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
	NSRect rect=[self bounds];
	
	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	
		perspectiveProjectionMatrix=vmath::perspective(45.0f,width/height, 0.1f, 100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

-(void)drawRect:(NSRect)dirtyRect
{
	[self drawView];
}


-(void)drawView
{
	static float angle_cube=0.0f;
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glUseProgram(gShaderProgramObject);
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 projectionMatrix;
    vmath::mat4 rotationMatrix;
    
    
    modelMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    modelMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(angle_cube, angle_cube,angle_cube);
    modelMatrix = modelMatrix * rotationMatrix;
    projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
    //modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
    glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cube_texture);
    glUniform1i(samplerUniform, 0);
    if (gbLight == 1)
     {
     glUniform1i(isLKeypressedUniform, 1);
     glUniform3fv(laUniform, 1, light_ambient);
     glUniform3fv(ldUniform, 1, light_diffuse);
     glUniform3fv(lsUniform, 1, light_specular);
     glUniform4fv(lightPositionUniform, 1, light_position);
     
     glUniform3fv(kaUniform, 1, material_ambient);
     glUniform3fv(kdUniform, 1, material_diffuse);
     glUniform3fv(ksUniform, 1, material_specular);
     glUniform1fv(material_shininess_uniform, 1, material_shininess);
     
     
     }
     else
     {
     glUniform1i(isLKeypressedUniform, 0);
     }
    
    
    glBindVertexArray(vao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glBindVertexArray(0);
    
    glUseProgram(0);

	
	if (angle_cube <= 360.0f)
	{
		angle_cube = angle_cube + 1.0f;
	}
	else
	{
		angle_cube = 0.0f;
	}

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	
}

-(BOOL)acceptsFirstResponder
{
	[[self window]makeFirstResponder:self];
	return(YES);

}

-(void)keyDown:(NSEvent *)theEvent
{
	int key=(int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27:
		[self release];
		[NSApp terminate:self];
		break;
		
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self];
			break;
		default:
        case'L':
            if (gbLight == 0)
            {
                gbLight = 1;
            }
            else
            {
                gbLight = 0;
            }
            break;
			break;
	}

}

-(void)mouseDown:(NSEvent *)theEvent
{
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{

}

-(void)rightMouseDown:(NSEvent *)theEvent
{

}

-(void)dealloc
{
	if (vbo_cube)
	{
		glDeleteBuffers(1, &vbo_cube);
		vbo_cube = 0;
	}
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}
	

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,CVOptionFlags *pFlagsOut, void *pDisplayLinkContext)
{
	CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}
