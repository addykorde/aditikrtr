#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>
#include"Sphere.h"


#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>
#include<SOIL/SOIL.h>

#include"vmath.h"

using namespace std;
using namespace vmath;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global variables.

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGLXContext;

GLfloat LightAnglezero = 0.0f, LightAngleone = 0.0f, LightAngletwo = 0.0f;
typedef GLXContext(* glXCreateContextAttribsARBProc)(Display *,GLXFBConfig,GLXContext,bool,GLint *);


glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;

//Shaders

GLint gVertexShaderObject;
GLint gFragmentShaderObject;
GLint gShaderProgramObject;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;
GLuint mUniform;
GLuint vUniform;
GLuint pUniform;
GLuint laUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;

GLuint ldUniform;
GLint keypress = 0;
GLuint lightPositionUniform;

GLuint isLKeypressedUniform;
mat4 perspectiveProjectionMatrix;
GLuint material_shininess_uniform;

int gbLight = 0;
int currentWidth = 0,currentHeight=0;

GLfloat angleOfXRotation = 0.0f, angleOfYRotation = 0.0f, angleOfZRotation = 0.0f;


struct Materials {
	float MaterialAmbient[4];
	float MaterialDiffuse[4];
	float MaterialSpecular[4];
	float MaterialShininess[1];
};

Materials material[24];

GLfloat LightAmbient[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };



float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];
int gNumVertices, gNumElements;


FILE *gpFile = NULL; 

int main(void)
{
	
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();
	
	//Opengl functions prototype declarations
	void initialize(void);
	void resize(int,int);
	void display(void);
	void Update(void);
	//Local variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	bool bdone=false;
	
	
	
	CreateWindow();
	initialize();
	

	XEvent event;
	KeySym keysym;
	
	
		while(bdone==false)
		{
		while(XPending(gpDisplay))
		{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
			break;
			case KeyPress:
			keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
			switch(keysym)
			{
				case XK_Escape:
				bdone=true;
				break;
				
				case XK_F:
				case XK_f:
				if(bFullscreen==false)
				{
					ToggleFullscreen();
					bFullscreen=true;
				}
				else{
					ToggleFullscreen();
					bFullscreen=false;
				}
				break;
				case XK_L:
				case XK_l:
				if (gbLight == 0)
				{
				gbLight = 1;
				}
				else
				{
				gbLight = 0;
				}
				break;
				case XK_X:
				case XK_x:
				keypress = 1;
				angleOfXRotation = 0.0f;
				break;

				case XK_Y:
				case XK_y:
				keypress = 2;
				angleOfYRotation = 0.0f;
				break;

				case XK_Z:
				case XK_z:
				keypress = 3;
				angleOfZRotation = 0.0f;
				break;
				
				default:
				break;
				
			}
			break;
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
				default:
				break;
			}
			break;
			case MotionNotify:
			break;
			case ConfigureNotify:
			winWidth=event.xconfigure.width;
			winHeight=event.xconfigure.height;
			resize(winWidth,winHeight);
			break;
			case Expose:
			break;
			case DestroyNotify:
			break;
			case 33:
			bdone=true;
			break;
			default:
			break;
		}
		}
			Update();
			display();
		}
		uninitialize();
		return(0);
}

void CreateWindow()
{
	
	void uninitialize(void);
	
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNoOfFBConfigs=0;

	int bestFrameBufferConfig=-1;
	int bestNoOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNoOfSamples=999;
	
	static int frameBufferAttributes[]={GLX_X_RENDERABLE,true,
					    GLX_DRAWABLE_TYPE,
					    GLX_WINDOW_BIT,
					    GLX_RENDER_TYPE,
					    GLX_RGBA_BIT,
	 				    GLX_X_VISUAL_TYPE,
	 				    GLX_TRUE_COLOR,
					    GLX_RED_SIZE,8,
					    GLX_GREEN_SIZE,8,
					    GLX_BLUE_SIZE,8,
 					    GLX_ALPHA_SIZE,8,
			 		    GLX_DEPTH_SIZE,24,
					    GLX_STENCIL_SIZE,8,
			 		    GLX_DOUBLEBUFFER,true,
					    None    }; //None=0
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&iNoOfFBConfigs);
	
	
	for(int i=0;i<iNoOfFBConfigs;i++)
	{
        pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);

        if(pTempXVisualInfo){
                int sampleBuffers,samples;
                
                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);

                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

                if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNoOfSamples){
                        bestFrameBufferConfig=i;
                        bestNoOfSamples=samples;
                }
                if(worstFrameBufferConfig<0 || !sampleBuffers && samples<worstNoOfSamples){
                        worstFrameBufferConfig=i;
                        worstNoOfSamples=samples;
                }

        }
        XFree(pTempXVisualInfo);
}


	
	bestGLXFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestGLXFBConfig;
	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	
	if(gpXVisualInfo==NULL)
	{
		printf("ERROR : Unable to Open gpXVisualInfo.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
						RootWindow(gpDisplay,gpXVisualInfo->screen),
						gpXVisualInfo->visual,
						AllocNone);
	gColormap=winAttribs.colormap;
	
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|
		StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
						  RootWindow(gpDisplay,gpXVisualInfo->screen),
						  0,
						  0,
						  giWindowWidth,
						  giWindowHeight,
						  0,
						  gpXVisualInfo->depth,
						  InputOutput,
						  gpXVisualInfo->visual,
						  styleMask,
						  &winAttribs);
	if(!gWindow)
	{
		printf("ERROR :Failed to create main window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"24-Spheres.");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void initialize(void)
{
	void uninitialize();
	void resize(int, int);
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	bool loadTexture(GLuint *,const char*);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL){
                printf("ERROR !!!Failed To Get glXCreateContextAttribsARB .\n\n");
                uninitialize();
                exit(1);
        }

        GLint Attribs[]={
            GLX_CONTEXT_MAJOR_VERSION_ARB,4,
            GLX_CONTEXT_MINOR_VERSION_ARB,5,
            GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
            None
        };    

	 gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                        gGLXFBConfig,
                                        0,
                                        true,
                                        Attribs);


        if(!gGLXContext){
                GLint Attribs[]={
                                GLX_CONTEXT_MAJOR_VERSION_ARB,1,
                                GLX_CONTEXT_MINOR_VERSION_ARB,0,
                                None
                };    

                gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                                gGLXFBConfig,
                                                0,
                                                true,
                                                Attribs);
        }

        if(!glXIsDirect(gpDisplay,gGLXContext)){
                printf("ERROR : The Obtained context is not H/W rendering context...\n");
        }else{
                printf("The Obtained context is H/W rendering context...\n");
        }
         
        glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

        GLenum result;
        result=glewInit();
        result = glewInit();
	if (result != GLEW_OK)
	{
		uninitialize();
		fprintf(gpFile, "Inside GLEW\n");
	}



	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);


	const GLchar* vertexShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vnormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ks;" \
		"uniform vec3 u_kd;" \
		"uniform float m_shininess;"
		"uniform vec4 u_light_position;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
		"vec3 tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \
		"vec3 light_direction=normalize(vec3(u_light_position)-eye_coordinates.xyz);" \
		"float tn_dot_ld=max(dot(light_direction,tnorm),0.0);" \
		"vec3 reflection_vector=reflect(-light_direction,tnorm);" \
		"vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
		"vec3 specular=u_ls * u_ks * pow(max(dot(reflection_vector,viewer_vector),0.0),m_shininess);" \
		"phong_ads_light=ambient+diffuse+specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light=vec3(1.0,1.0,1.0);" \
		"}" \

		"}";

	glShaderSource(gVertexShaderObject, 1, &vertexShaderSourceCode, NULL);

	
	glCompileShader(gVertexShaderObject);


	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	//Fragment
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	
	const GLchar* fragmentShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"uniform int u_lkeyispressed;" \
		"void main(void)" \
		"{" \

		"FragColor=vec4(phong_ads_light,1.0);" \

		"}";

	
	glShaderSource(gFragmentShaderObject, 1, &fragmentShaderSourceCode, NULL);

	
	glCompileShader(gFragmentShaderObject);

	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	
	
	
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vnormal");

	

	glLinkProgram(gShaderProgramObject);

	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Linking of program %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");


	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "m_shininess");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");



	getSphereVertexData(sphere_vertices,sphere_normals,sphere_texture,sphere_elements);
 gNumVertices = getNumberOfSphereVertices();
	 gNumElements = getNumberOfSphereElements();

	
	//Pyramid
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elements),sphere_elements,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	material[0].MaterialAmbient[0] = 0.0215;
	material[0].MaterialAmbient[1] = 0.1745;
	material[0].MaterialAmbient[2] = 0.0215;
	material[0].MaterialAmbient[3] = 1.0f;

	material[0].MaterialDiffuse[0] = 0.07568;
	material[0].MaterialDiffuse[1] = 0.61424;
	material[0].MaterialDiffuse[2] = 0.07568;
	material[0].MaterialDiffuse[3] = 1.0f;

	material[0].MaterialSpecular[0] = 0.633;
	material[0].MaterialSpecular[1] = 0.727811;
	material[0].MaterialSpecular[2] = 0.633;
	material[0].MaterialSpecular[3] = 1.0f;

	material[0].MaterialShininess[0] = 0.6 * 128;



	material[1].MaterialAmbient[0] = 0.135;
	material[1].MaterialAmbient[1] = 0.2225;
	material[1].MaterialAmbient[2] = 0.1575;
	material[1].MaterialAmbient[3] = 1.0f;

	material[1].MaterialDiffuse[0]= 0.54;
	material[1].MaterialDiffuse[1]= 0.89;
	material[1].MaterialDiffuse[2]= 0.63;
	material[1].MaterialDiffuse[3] = 1.0f;

	material[1].MaterialSpecular[0]= 0.316228;
	material[1].MaterialSpecular[1]= 0.316228;
	material[1].MaterialSpecular[2]= 0.316228;
	material[1].MaterialSpecular[3] = 1.0f;

	material[1].MaterialShininess[0] = 0.1 * 128;


	material[2].MaterialAmbient[0] = 0.05375;
	material[2].MaterialAmbient[1] = 0.05;
	material[2].MaterialAmbient[2] = 0.06625;
	material[2].MaterialAmbient[3] = 1.0f;

	material[2].MaterialDiffuse[0] = 0.18275;
	material[2].MaterialDiffuse[1] = 0.17;
	material[2].MaterialDiffuse[2] = 0.22525;
	material[2].MaterialDiffuse[3] = 1.0f;

	material[2].MaterialSpecular[0] = 0.332741;
	material[2].MaterialSpecular[1] = 0.328634;
	material[2].MaterialSpecular[2] = 0.346435;
	material[2].MaterialSpecular[3] = 1.0f;

	material[2].MaterialShininess[0] = 0.3 * 128;

	material[3].MaterialAmbient[0] = 0.25;
	material[3].MaterialAmbient[1] = 0.20725;
	material[3].MaterialAmbient[2] = 0.20725;
	material[3].MaterialAmbient[3] = 1.0f;

	material[3].MaterialDiffuse[0] = 1.0;
	material[3].MaterialDiffuse[1] = 0.829;
	material[3].MaterialDiffuse[2] = 0.829;
	material[3].MaterialDiffuse[3] = 1.0f;

	material[3].MaterialSpecular[0] = 0.296648;
	material[3].MaterialSpecular[1] = 0.296648;
	material[3].MaterialSpecular[2] = 0.296648;
	material[3].MaterialSpecular[3] = 1.0f;

	material[3].MaterialShininess[0] = 0.088 * 128;

	material[4].MaterialAmbient[0] = 0.1745;
	material[4].MaterialAmbient[1] = 0.01175;
	material[4].MaterialAmbient[2] = 0.01175;
	material[4].MaterialAmbient[3] = 1.0f;

	material[4].MaterialDiffuse[0] = 0.61424;
	material[4].MaterialDiffuse[1] = 0.04136;
	material[4].MaterialDiffuse[2] = 0.04136;
	material[4].MaterialDiffuse[3] = 1.0f;

	material[4].MaterialSpecular[0] = 0.727811;
	material[4].MaterialSpecular[1] = 0.626959;
	material[4].MaterialSpecular[2] = 0.626959;
	material[4].MaterialSpecular[3] = 1.0f;

	material[4].MaterialShininess[0] = 0.6 * 128;


	material[5].MaterialAmbient[0] = 0.1;
	material[5].MaterialAmbient[1] = 0.18725;
	material[5].MaterialAmbient[2] = 0.1745;
	material[5].MaterialAmbient[3] = 1.0f;

	material[5].MaterialDiffuse[0] = 0.396;
	material[5].MaterialDiffuse[1] = 0.74151;
	material[5].MaterialDiffuse[2] = 0.69102;
	material[5].MaterialDiffuse[3] = 1.0f;

	material[5].MaterialSpecular[0] = 0.297254;
	material[5].MaterialSpecular[1] = 0.30829;
	material[5].MaterialSpecular[2] = 0.306678;
	material[5].MaterialSpecular[3] = 1.0f;

	material[5].MaterialShininess[0] = 0.1 * 128;
	//*************************************************************************************
	material[6].MaterialAmbient[0] = 0.329412;
	material[6].MaterialAmbient[1] = 0.223529;
	material[6].MaterialAmbient[2] = 0.027451;
	material[6].MaterialAmbient[3] = 1.0f;

	material[6].MaterialDiffuse[0] = 0.780392;
	material[6].MaterialDiffuse[1] = 0.568627;
	material[6].MaterialDiffuse[2] = 0.113725;
	material[6].MaterialDiffuse[3] = 1.0f;

	material[6].MaterialSpecular[0] = 0.992157;
	material[6].MaterialSpecular[1] = 0.941176;
	material[6].MaterialSpecular[2] = 0.807843;
	material[6].MaterialSpecular[3] = 1.0f;

	material[6].MaterialShininess[0] = 0.21794872 * 128;
	//---------------------------------
	material[7].MaterialAmbient[0] = 00.2125;
	material[7].MaterialAmbient[1] = 0.1275;
	material[7].MaterialAmbient[2] = 0.054;
	material[7].MaterialAmbient[3] = 1.0f;

	material[7].MaterialDiffuse[0] = 0.714;
	material[7].MaterialDiffuse[1] = 0.4284;
	material[7].MaterialDiffuse[2] = 0.18144;
	material[7].MaterialDiffuse[3] = 1.0f;

	material[7].MaterialSpecular[0] = 0.393548;
	material[7].MaterialSpecular[1] = 0.271906;
	material[7].MaterialSpecular[2] = 0.166721;
	material[7].MaterialSpecular[3] = 1.0f;

	material[7].MaterialShininess[0] = 0.2 * 128;
	//-------------------------------------------------------
	material[8].MaterialAmbient[0] = 0.25;
	material[8].MaterialAmbient[1] = 0.25;
	material[8].MaterialAmbient[2] = 0.25;
	material[8].MaterialAmbient[3] = 1.0f;

	material[8].MaterialDiffuse[0] = 0.4;
	material[8].MaterialDiffuse[1] = 0.4;
	material[8].MaterialDiffuse[2] = 0.4;
	material[8].MaterialDiffuse[3] = 1.0f;

	material[8].MaterialSpecular[0] = 0.774597;
	material[8].MaterialSpecular[1] = 0.774597;
	material[8].MaterialSpecular[2] = 0.774597;
	material[8].MaterialSpecular[3] = 1.0f;
	
	material[8].MaterialShininess[0] = 0.6 * 128;

	//---------------------------------

	material[9].MaterialAmbient[0] = 0.19125;
	material[9].MaterialAmbient[1] = 0.0735;
	material[9].MaterialAmbient[2] = 0.0225;
	material[9].MaterialAmbient[3] = 1.0f;

	material[9].MaterialDiffuse[0] = 0.7038;
	material[9].MaterialDiffuse[1] = 0.27048;
	material[9].MaterialDiffuse[2] = 0.0828;
	material[9].MaterialDiffuse[3] = 1.0f;

	material[9].MaterialSpecular[0] = 0.256777;
	material[9].MaterialSpecular[1] = 0.137622;
	material[9].MaterialSpecular[2] = 0.086014;
	material[9].MaterialSpecular[3] = 1.0f;

	material[9].MaterialShininess[0] = 0.1 * 128;

	//---------------------------------------
	material[10].MaterialAmbient[0] = 0.24725;
	material[10].MaterialAmbient[1] = 0.1995;
	material[10].MaterialAmbient[2] = 0.0745;
	material[10].MaterialAmbient[3] = 1.0f;

	material[10].MaterialDiffuse[0] = 0.75164;
	material[10].MaterialDiffuse[1] = 0.60648;
	material[10].MaterialDiffuse[2] = 0.22648;
	material[10].MaterialDiffuse[3] = 1.0f;

	material[10].MaterialSpecular[0] = 0.628281;
	material[10].MaterialSpecular[1] = 0.555802;
	material[10].MaterialSpecular[2] = 0.366065;
	material[10].MaterialSpecular[3] = 1.0f;

	material[10].MaterialShininess[0] = 0.4 * 128;

	//---------------------------------------

	material[11].MaterialAmbient[0] = 0.19225;
	material[11].MaterialAmbient[1] = 0.19225;
	material[11].MaterialAmbient[2] = 0.19225;
	material[11].MaterialAmbient[3] = 1.0f;

	material[11].MaterialDiffuse[0] = 0.50754;
	material[11].MaterialDiffuse[1] = 0.50754;
	material[11].MaterialDiffuse[2] = 0.50754;
	material[11].MaterialDiffuse[3] = 1.0f;

	material[11].MaterialSpecular[0] = 0.508273;
	material[11].MaterialSpecular[1] = 0.508273;
	material[11].MaterialSpecular[2] = 0.508273;
	material[11].MaterialSpecular[3] = 1.0f;

	material[11].MaterialShininess[0] = 0.4 * 128;

	//---------------------------------------
	material[12].MaterialAmbient[0] = 0.0;
	material[12].MaterialAmbient[1] = 0.0;
	material[12].MaterialAmbient[2] = 0.0;
	material[12].MaterialAmbient[3] = 1.0f;

	material[12].MaterialDiffuse[0] = 0.01;
	material[12].MaterialDiffuse[1] = 0.01;
	material[12].MaterialDiffuse[2] = 0.01;
	material[12].MaterialDiffuse[3] = 1.0f;

	material[12].MaterialSpecular[0] = 0.50;
	material[12].MaterialSpecular[1] = 0.50;
	material[12].MaterialSpecular[2] = 0.50;
	material[12].MaterialSpecular[3] = 1.0f; 

	material[12].MaterialShininess[0] = 0.25 * 128;

	//---------------------------------------

	material[13].MaterialAmbient[0] = 0.0;
	material[13].MaterialAmbient[1] = 0.1;
	material[13].MaterialAmbient[2] = 0.06;
	material[13].MaterialAmbient[3] = 1.0f;

	material[13].MaterialDiffuse[0] = 0.0;
	material[13].MaterialDiffuse[1] = 0.50980392;
	material[13].MaterialDiffuse[2] = 0.50980392;
	material[13].MaterialDiffuse[3] = 1.0f;

	material[13].MaterialSpecular[0] = 0.50196078;
	material[13].MaterialSpecular[1] = 0.50196078;
	material[13].MaterialSpecular[2] = 0.50196078;
	material[13].MaterialSpecular[3] = 1.0f;

	material[13].MaterialShininess[0] = 0.25 * 128;

	//---------------------------------------
	material[14].MaterialAmbient[0] = 0.0;
	material[14].MaterialAmbient[1] = 0.0;
	material[14].MaterialAmbient[2] = 0.0;
	material[14].MaterialAmbient[3] = 1.0f;

	material[14].MaterialDiffuse[0] = 0.1;
	material[14].MaterialDiffuse[1] = 0.35;
	material[14].MaterialDiffuse[2] = 0.1;
	material[14].MaterialDiffuse[3] = 1.0f;

	material[14].MaterialSpecular[0] = 0.45;
	material[14].MaterialSpecular[1] = 0.55;
	material[14].MaterialSpecular[2] = 0.45;
	material[14].MaterialSpecular[3] = 1.0f;

	material[14].MaterialShininess[0] = 0.25 * 128;

	//---------------------------------------

	material[15].MaterialAmbient[0] = 0.0;
	material[15].MaterialAmbient[1] = 0.0;
	material[15].MaterialAmbient[2] = 0.0;
	material[15].MaterialAmbient[3] = 1.0f;

	material[15].MaterialDiffuse[0] = 0.5;
	material[15].MaterialDiffuse[1] = 0.0;
	material[15].MaterialDiffuse[2] = 0.0;
	material[15].MaterialDiffuse[3] = 1.0f;

	material[15].MaterialSpecular[0] = 0.7;
	material[15].MaterialSpecular[1] = 0.6;
	material[15].MaterialSpecular[2] = 0.6;
	material[15].MaterialSpecular[3] = 1.0f;

	material[15].MaterialShininess[0] = 0.25 * 128;

	//---------------------------------------

	material[16].MaterialAmbient[0] = 0.0;
	material[16].MaterialAmbient[1] = 0.0;
	material[16].MaterialAmbient[2] = 0.0;
	material[16].MaterialAmbient[3] = 1.0f;

	material[16].MaterialDiffuse[0] = 0.55;
	material[16].MaterialDiffuse[1] = 0.55;
	material[16].MaterialDiffuse[2] = 0.55;
	material[16].MaterialDiffuse[3] = 1.0f;

	material[16].MaterialSpecular[0] = 0.70;
	material[16].MaterialSpecular[1] = 0.70;
	material[16].MaterialSpecular[2] = 0.70;
	material[16].MaterialSpecular[3] = 1.0f;

	material[16].MaterialShininess[0] = 0.25 * 128;

	//---------------------------------------

	material[17].MaterialAmbient[0] = 0.0;
	material[17].MaterialAmbient[1] = 0.0;
	material[17].MaterialAmbient[2] = 0.0;
	material[17].MaterialAmbient[3] = 1.0f;

	material[17].MaterialDiffuse[0] = 0.5;
	material[17].MaterialDiffuse[1] = 0.5;
	material[17].MaterialDiffuse[2] = 0.0;
	material[17].MaterialDiffuse[3] = 1.0f;

	material[17].MaterialSpecular[0] = 0.60;
	material[17].MaterialSpecular[1] = 0.60;
	material[17].MaterialSpecular[2] = 0.50;
	material[17].MaterialSpecular[3] = 1.0f;

	material[17].MaterialShininess[0] = 0.25 * 128;

	//---------------------------------------

	material[18].MaterialAmbient[0] = 0.02;
	material[18].MaterialAmbient[1] = 0.02;
	material[18].MaterialAmbient[2] = 0.02;
	material[18].MaterialAmbient[3] = 1.0f;

	material[18].MaterialDiffuse[0] = 0.01;
	material[18].MaterialDiffuse[1] = 0.01;
	material[18].MaterialDiffuse[2] = 0.01;
	material[18].MaterialDiffuse[3] = 1.0f;

	material[18].MaterialSpecular[0] = 0.4;
	material[18].MaterialSpecular[1] = 0.4;
	material[18].MaterialSpecular[2] = 0.4;
	material[18].MaterialSpecular[3] = 1.0f;

	material[18].MaterialShininess[0] = 0.078125 * 128;

	//---------------------------------------

	material[19].MaterialAmbient[0] = 0.0;
	material[19].MaterialAmbient[1] = 0.05;
	material[19].MaterialAmbient[2] = 0.05;
	material[19].MaterialAmbient[3] = 1.0f;

	material[19].MaterialDiffuse[0] = 0.4;
	material[19].MaterialDiffuse[1] = 0.5;
	material[19].MaterialDiffuse[2] = 0.5;
	material[19].MaterialDiffuse[3] = 1.0f;

	material[19].MaterialSpecular[0] = 0.04;
	material[19].MaterialSpecular[1] = 0.7;
	material[19].MaterialSpecular[2] = 0.7;
	material[19].MaterialSpecular[3] = 1.0f;

	material[19].MaterialShininess[0] = 0.078125 * 128;

	//----------------------------------------------------

	material[20].MaterialAmbient[0] = 0.0;
	material[20].MaterialAmbient[1] = 0.05;
	material[20].MaterialAmbient[2] = 0.0;
	material[20].MaterialAmbient[3] = 1.0f;

	material[20].MaterialDiffuse[0] = 0.4;
	material[20].MaterialDiffuse[1] = 0.5;
	material[20].MaterialDiffuse[2] = 0.4;
	material[20].MaterialDiffuse[3] = 1.0f;

	material[20].MaterialSpecular[0] = 0.04;
	material[20].MaterialSpecular[1] = 0.7;
	material[20].MaterialSpecular[2] = 0.04;
	material[20].MaterialSpecular[3] = 1.0f;

	material[20].MaterialShininess[0] = 0.078125 * 128;

	//--------------------------------------------------

	material[21].MaterialAmbient[0] = 0.05;
	material[21].MaterialAmbient[1] = 0.0;
	material[21].MaterialAmbient[2] = 0.0;
	material[21].MaterialAmbient[3] = 1.0f;

	material[21].MaterialDiffuse[0] = 0.5;
	material[21].MaterialDiffuse[1] = 0.4;
	material[21].MaterialDiffuse[2] = 0.4;
	material[21].MaterialDiffuse[3] = 1.0f;

	material[21].MaterialSpecular[0] = 0.7;
	material[21].MaterialSpecular[1] = 0.04;
	material[21].MaterialSpecular[2] = 0.04;
	material[21].MaterialSpecular[3] = 1.0f;

	material[21].MaterialShininess[0] = 0.078125 * 128;
	//-------------------------------------------------
	material[22].MaterialAmbient[0] = 0.05;
	material[22].MaterialAmbient[1] = 0.05;
	material[22].MaterialAmbient[2] = 0.05;
	material[22].MaterialAmbient[3] = 1.0f;

	material[22].MaterialDiffuse[0] = 0.5;
	material[22].MaterialDiffuse[1] = 0.5;
	material[22].MaterialDiffuse[2] = 0.5;
	material[22].MaterialDiffuse[3] = 1.0f;

	material[22].MaterialSpecular[0] = 0.7;
	material[22].MaterialSpecular[1] = 0.7;
	material[22].MaterialSpecular[2] = 0.7;
	material[22].MaterialSpecular[3] = 1.0f;

	material[22].MaterialShininess[0] = 0.078125 * 128;

	//--------------------------------------------------
	material[23].MaterialAmbient[0] = 0.05;
	material[23].MaterialAmbient[1] = 0.05;
	material[23].MaterialAmbient[2] = 0.0;
	material[23].MaterialAmbient[3] = 1.0f;

	material[23].MaterialDiffuse[0] = 0.5;
	material[23].MaterialDiffuse[1] = 0.5;
	material[23].MaterialDiffuse[2] = 0.4;
	material[23].MaterialDiffuse[3] = 1.0f;

	material[23].MaterialSpecular[0] = 0.7;
	material[23].MaterialSpecular[1] = 0.7;
	material[23].MaterialSpecular[2] = 0.04;
	material[23].MaterialSpecular[3] = 1.0f;

	material[23].MaterialShininess[0] = 0.078125 * 128;


	//
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	perspectiveProjectionMatrix = mat4::identity();

	
}


void resize(int width, int height)
{	currentWidth = width;
	currentHeight = height;
	if (height == 0)
		height = 1;
	//glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix=perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display()
{

glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;


	if (gbLight == 1)
	{
		glUniform1i(isLKeypressedUniform, 1);
		glUniform3fv(laUniform, 1, LightAmbient);
		glUniform3fv(ldUniform, 1, LightDiffuse);
		//rotationMatrix = mat4::identity();
		//rotationMatrix=rotate(angleOfXRotation, 1.0f, 0.0f, 0.0f);

		if (keypress == 1)
		{
			LightPosition[0]= { angleOfXRotation };
		}
		if (keypress == 2)
		{
			LightPosition[1] = { angleOfXRotation };
		}
		if (keypress == 3)
		{
			LightPosition[2] = { angleOfXRotation };
		}
		glUniform4fv(lightPositionUniform, 1, LightPosition);

	}
	else
	{
		glUniform1i(isLKeypressedUniform, 0);
	}
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(0, 875, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();
	//projectionMatrix = perspective(45.0f, (GLfloat)currentWidth / (GLfloat)currentHeight, 0.1f, 100.0f);
	//scaleMatrix = scale(1.1f, 1.1f, 0.3f);
	//modelMatrix = modelMatrix * scaleMatrix;

	glUniform3fv(kaUniform, 1, material[0].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[0].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[0].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[0].MaterialShininess);


	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	
	//********************************************************************************
	
	
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(0, 700, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();
	//modelMatrix = modelMatrix * scaleMatrix;

	glUniform3fv(kaUniform, 1, material[1].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[1].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[1].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[1].MaterialShininess);

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	

	
	//****************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(0, 525, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();

	

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniform3fv(kaUniform, 1, material[2].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[2].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[2].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[2].MaterialShininess);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	//******************************************************************************************
	
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(0, 350, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();


	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[3].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[3].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[3].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[3].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	//**************************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(0, 175, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[4].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[4].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[4].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[4].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//************************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(0, 0, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[5].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[5].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[5].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[5].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	//***************************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(500, 875, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[6].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[6].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[6].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[6].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	//**************************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(500, 700, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[7].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[7].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[7].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[7].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//**************************************************************************************************


	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(500, 525, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[8].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[8].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[8].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[8].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//***********************************************************************************************


	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(500, 350, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[9].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[9].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[9].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[9].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);


	//***********************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(500, 175, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[10].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[10].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[10].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[10].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//*************************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(500, 0, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[11].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[11].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[11].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[11].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//****************************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1000, 875, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[12].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[12].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[12].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[12].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//***************************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1000, 700, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[13].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[13].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[13].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[13].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//*************************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1000, 525, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[14].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[14].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[14].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[14].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//***********************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1000, 350, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[15].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[15].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[15].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[15].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//****************************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1000, 175, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[16].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[16].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[16].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[16].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//********************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1000, 0, (GLsizei)currentWidth/6 ,(GLsizei)currentHeight/6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[17].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[17].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[17].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[17].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	//***************************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1500, 875, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[18].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[18].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[18].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[18].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//************************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1500, 700, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[19].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[19].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[19].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[19].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	//**************************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1500, 525, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[20].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[20].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[20].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[20].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	//*******************************************************************************************
	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1500, 350, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();

	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[21].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[21].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[21].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[21].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//**********************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1500, 175, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[22].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[22].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[22].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[22].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	//******************************************************************************************

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	glViewport(1500, 0, (GLsizei)currentWidth / 6, (GLsizei)currentHeight / 6);
	glLoadIdentity();
	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	glUniform3fv(kaUniform, 1, material[23].MaterialAmbient);
	glUniform3fv(kdUniform, 1, material[23].MaterialDiffuse);
	glUniform3fv(ksUniform, 1, material[23].MaterialSpecular);
	glUniform1fv(material_shininess_uniform, 1, material[23].MaterialShininess);
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	
	glUseProgram(0);
	glXSwapBuffers(gpDisplay,gWindow);
}



void Update()
{
	if (angleOfXRotation >= 2 * M_PI)
	{
		angleOfXRotation = 0.0f;
	}
	if (keypress == 1)
	{
		angleOfXRotation = angleOfXRotation + 0.04f;
		LightPosition[1] = 100.0f*sin(angleOfXRotation);
		LightPosition[2] = 100.0f*cos(angleOfXRotation);
	}
	if (keypress == 2)
	{
		angleOfXRotation = angleOfXRotation + 0.04f;
		LightPosition[0] = 100.0f*sin(angleOfXRotation);
		LightPosition[2] = 100.0f*cos(angleOfXRotation);
	}
	if (keypress == 3)
	{
		angleOfXRotation = angleOfXRotation + 0.04f;
		LightPosition[0] = 100.0f*sin(angleOfXRotation);
		LightPosition[1] = 100.0f*cos(angleOfXRotation);
	}
	if (angleOfYRotation >= 360.0f)
	{
		angleOfYRotation = 0.0f;
	}
	if (angleOfZRotation >= 360.0f)
	{
		angleOfZRotation = 0.0f;
	}
}



void uninitialize(void)
{
	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	GLXContext CurrentglxContext=glXGetCurrentContext();
	
	if(CurrentglxContext!=NULL&& CurrentglxContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
		
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}




