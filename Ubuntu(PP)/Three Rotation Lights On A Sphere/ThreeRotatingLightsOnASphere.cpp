#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>
#include"Sphere.h"


#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>
#include<SOIL/SOIL.h>

#include"vmath.h"

using namespace std;
using namespace vmath;
int vkey=0;
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global variables.

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGLXContext;

GLfloat LightAnglezero = 0.0f, LightAngleone = 0.0f, LightAngletwo = 0.0f;
typedef GLXContext(* glXCreateContextAttribsARBProc)(Display *,GLXFBConfig,GLXContext,bool,GLint *);


glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;

//Shaders

GLint gVertexShaderObject1;
GLint gFragmentShaderObject1;
GLint gShaderProgramObject1;
GLint gVertexShaderObject;
GLint gFragmentShaderObject;
GLint gShaderProgramObject;;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;
GLuint mUniform;
GLuint vUniform;
GLuint pUniform;
GLuint laUniform_red;
GLuint laUniform_blue;
GLuint laUniform_green;
GLuint kaUniform;
GLuint lsUniform_red;
GLuint lsUniform_green;
GLuint lsUniform_blue;
GLuint ldUniform_red;
GLuint ldUniform_green;
GLuint ldUniform_blue;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform_red;
GLuint lightPositionUniform_blue;
GLuint lightPositionUniform_green;
GLuint isLKeypressedUniform;
GLuint material_shininess_uniform;

GLuint mUniform1;
GLuint vUniform1;
GLuint pUniform1;
GLuint laUniform_red1;
GLuint laUniform_blue1;
GLuint laUniform_green1;
GLuint kaUniform1;
GLuint lsUniform_red1;
GLuint lsUniform_green1;
GLuint lsUniform_blue1;
GLuint ldUniform_red1;
GLuint ldUniform_green1;
GLuint ldUniform_blue1;
GLuint kdUniform1;
GLuint ksUniform1;
GLuint lightPositionUniform_red1;
GLuint lightPositionUniform_blue1;
GLuint lightPositionUniform_green1;
GLuint isLKeypressedUniform1;
GLuint material_shininess_uniform1;


int gbLight = 0;
mat4 perspectiveProjectionMatrix;

struct Lights {
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float position[4];
};

float MaterialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
float MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
float MaterialShininess[] = { 50.0f };


Lights light[3];




float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];
int gNumVertices, gNumElements;


FILE *gpFile = NULL; 

int main(void)
{
	
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();
	
	//Opengl functions prototype declarations
	void initialize(void);
	void resize(int,int);
	void display(void);
	void Update(void);
	//Local variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	bool bdone=false;
	
	
	
	CreateWindow();
	initialize();
	

	XEvent event;
	KeySym keysym;
	
	
		while(bdone==false)
		{
		while(XPending(gpDisplay))
		{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
			break;
			case KeyPress:
			keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
			switch(keysym)
			{
				case XK_Escape:
				if(bFullscreen==false)
				{
					ToggleFullscreen();
					bFullscreen=true;
				}
				else{
					ToggleFullscreen();
					bFullscreen=false;
				}
				break;
				
				case XK_F:
				case XK_f:
				vkey = 1;
				gbLight = 1;
				break;
				
				case XK_V:
				case XK_v:
				vkey = 0;
				gbLight = 1;
				break;
				
				case XK_L:
				case XK_l:
				if (gbLight == 0)
				{
				gbLight = 1;
				}
				else
				{
				gbLight = 0;
				}
				break;
				case XK_Q:
				case XK_q:
				bdone=true;
				break;
				default:
				break;
			}
			break;
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
				default:
				break;
			}
			break;
			case MotionNotify:
			break;
			case ConfigureNotify:
			winWidth=event.xconfigure.width;
			winHeight=event.xconfigure.height;
			resize(winWidth,winHeight);
			break;
			case Expose:
			break;
			case DestroyNotify:
			break;
			case 33:
			bdone=true;
			break;
			default:
			break;
		}
		}
			Update();
			display();
		}
		uninitialize();
		return(0);
}

void CreateWindow()
{
	
	void uninitialize(void);
	
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNoOfFBConfigs=0;

	int bestFrameBufferConfig=-1;
	int bestNoOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNoOfSamples=999;
	
	static int frameBufferAttributes[]={GLX_X_RENDERABLE,true,
					    GLX_DRAWABLE_TYPE,
					    GLX_WINDOW_BIT,
					    GLX_RENDER_TYPE,
					    GLX_RGBA_BIT,
	 				    GLX_X_VISUAL_TYPE,
	 				    GLX_TRUE_COLOR,
					    GLX_RED_SIZE,8,
					    GLX_GREEN_SIZE,8,
					    GLX_BLUE_SIZE,8,
 					    GLX_ALPHA_SIZE,8,
			 		    GLX_DEPTH_SIZE,24,
					    GLX_STENCIL_SIZE,8,
			 		    GLX_DOUBLEBUFFER,true,
					    None    }; //None=0
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&iNoOfFBConfigs);
	
	
	for(int i=0;i<iNoOfFBConfigs;i++)
	{
        pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);

        if(pTempXVisualInfo){
                int sampleBuffers,samples;
                
                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);

                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

                if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNoOfSamples){
                        bestFrameBufferConfig=i;
                        bestNoOfSamples=samples;
                }
                if(worstFrameBufferConfig<0 || !sampleBuffers && samples<worstNoOfSamples){
                        worstFrameBufferConfig=i;
                        worstNoOfSamples=samples;
                }

        }
        XFree(pTempXVisualInfo);
}


	
	bestGLXFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestGLXFBConfig;
	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	
	if(gpXVisualInfo==NULL)
	{
		printf("ERROR : Unable to Open gpXVisualInfo.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
						RootWindow(gpDisplay,gpXVisualInfo->screen),
						gpXVisualInfo->visual,
						AllocNone);
	gColormap=winAttribs.colormap;
	
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|
		StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
						  RootWindow(gpDisplay,gpXVisualInfo->screen),
						  0,
						  0,
						  giWindowWidth,
						  giWindowHeight,
						  0,
						  gpXVisualInfo->depth,
						  InputOutput,
						  gpXVisualInfo->visual,
						  styleMask,
						  &winAttribs);
	if(!gWindow)
	{
		printf("ERROR :Failed to create main window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"Three Rotation Lights On A Sphere Per Vertex Per Fragment.");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void initialize(void)
{
	void uninitialize();
	void resize(int, int);
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfolog = NULL;
	GLchar * szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	
	bool loadTexture(GLuint *,const char*);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL){
                printf("ERROR !!!Failed To Get glXCreateContextAttribsARB .\n\n");
                uninitialize();
                exit(1);
        }

        GLint Attribs[]={
            GLX_CONTEXT_MAJOR_VERSION_ARB,4,
            GLX_CONTEXT_MINOR_VERSION_ARB,5,
            GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
            None
        };    

	 gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                        gGLXFBConfig,
                                        0,
                                        true,
                                        Attribs);


        if(!gGLXContext){
                GLint Attribs[]={
                                GLX_CONTEXT_MAJOR_VERSION_ARB,1,
                                GLX_CONTEXT_MINOR_VERSION_ARB,0,
                                None
                };    

                gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                                gGLXFBConfig,
                                                0,
                                                true,
                                                Attribs);
        }

        if(!glXIsDirect(gpDisplay,gGLXContext)){
                printf("ERROR : The Obtained context is not H/W rendering context...\n");
        }else{
                printf("The Obtained context is H/W rendering context...\n");
        }
         
        glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

        GLenum result;
        result=glewInit();
        result = glewInit();
	if (result != GLEW_OK)
	{
		uninitialize();
		fprintf(gpFile, "Inside GLEW\n");
	}



	gVertexShaderObject1 = glCreateShader(GL_VERTEX_SHADER);


	const GLchar* vertexShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vnormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ks;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ls_green;" \

		"uniform float m_shininess;"
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec4 u_light_position_green;" \
		"out vec3 finalColor;" \
		"vec3 light_direction_red;"\
		"vec3 light_direction_blue;"\
		"vec3 light_direction_green;"\
		"out vec3 phong_ads_light1;" \
		"out vec3 phong_ads_light2;" \
		"out vec3 phong_ads_light3;" \
		"vec3 reflection_vector_blue;"\
		"vec3 reflection_vector_green;"\
		"vec3 reflection_vector_red;"\
		"float tn_dot_ld_red;" \
		"float tn_dot_ld_blue;" \
		"float tn_dot_ld_green;" \
		"vec3 ambient1;"\
		"vec3 ambient2;"\
		"vec3 ambient3;"\
		"vec3 diffuse1;"\
		"vec3 diffuse2;"\
		"vec3 diffuse3;"\
		"vec3 specular1;"\
		"vec3 specular2;"\
		"vec3 specular3;"\
		"vec3 tnorm;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
		"tnorm=normalize(mat3(u_v_matrix*u_m_matrix)*vnormal);" \

		"light_direction_red=normalize(vec3(u_light_position_red)-eye_coordinates.xyz);" \

		"light_direction_blue=normalize(vec3(u_light_position_blue)-eye_coordinates.xyz);" \

		"light_direction_green=normalize(vec3(u_light_position_green)-eye_coordinates.xyz);" \

		"tn_dot_ld_red=max(dot(light_direction_red,tnorm),0.0);" \

		"tn_dot_ld_blue=max(dot(light_direction_blue,tnorm),0.0);" \

		"tn_dot_ld_green=max(dot(light_direction_green,tnorm),0.0);" \

		"reflection_vector_red=reflect(-light_direction_red,tnorm);" \
		"reflection_vector_blue=reflect(-light_direction_blue,tnorm);" \
		"reflection_vector_green=reflect(-light_direction_green,tnorm);" \

		"vec3 viewer_vector=normalize(vec3(-eye_coordinates));" \


		"ambient1  = u_la_red* u_ka;" \
		"ambient2= u_la_green* u_ka;" \
		"ambient3= u_la_blue* u_ka;" \

		"diffuse1 = u_ld_red * u_kd * tn_dot_ld_red;" \
		"diffuse2 = u_ld_green * u_kd * tn_dot_ld_green;" \
		"diffuse3 = u_ld_blue * u_kd * tn_dot_ld_blue;" \

		"specular1 = u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector),0.0),m_shininess);" \
		"specular2 = u_ls_green * u_ks * pow(max(dot(reflection_vector_green,viewer_vector),0.0),m_shininess);" \
		"specular3 = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),m_shininess);" \

		"phong_ads_light1  =ambient1+diffuse1+specular1;" \
		"phong_ads_light2  =ambient2+diffuse2+specular2;" \
		"phong_ads_light3  =ambient3+diffuse3+specular3;" \

		"finalColor= phong_ads_light3+phong_ads_light1+phong_ads_light2;" \
		"}" \
		"else" \
		"{" \
		"finalColor=vec3(1.0,1.0,1.0);"  \
		"}" \

		"}";

	glShaderSource(gVertexShaderObject1, 1, &vertexShaderSourceCode, NULL);

	
	glCompileShader(gVertexShaderObject1);

	glGetShaderiv(gVertexShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
				exit(0);
			}
		}
	}

	//Fragment
	
	gFragmentShaderObject1 = glCreateShader(GL_FRAGMENT_SHADER);

	
	const GLchar* fragmentShaderSourceCode =
		"#version 440" \
		"\n" \
		"in vec3 phong_ads_light1;" \
		"in vec3 phong_ads_light2;" \
		"in vec3 finalColor;" \
		"out vec4 FragColor1;" \
		"uniform int u_lkeyispressed;" \
		"void main(void)" \
		"{" \

		"FragColor1=vec4(finalColor,1.0);" \
		"}";
	
	glShaderSource(gFragmentShaderObject1, 1, &fragmentShaderSourceCode, NULL);

	
	glCompileShader(gFragmentShaderObject1);

	glGetShaderiv(gFragmentShaderObject1, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfolog = (GLchar *)malloc(iInfoLogLength);
			if (szInfolog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject1, iInfoLogLength, &written, szInfolog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfolog);
				free(szInfolog);

				uninitialize();
			
				exit(0);
			}
		}
	}


	
	
	
	gShaderProgramObject1 = glCreateProgram();

		glAttachShader(gShaderProgramObject1, gVertexShaderObject1);

		glAttachShader(gShaderProgramObject1, gFragmentShaderObject1);

		glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_POSITION, "vPosition");

		glBindAttribLocation(gShaderProgramObject1, AMC_ATTRIBUTE_NORMAL, "vnormal");

		glLinkProgram(gShaderProgramObject1);



	
	glGetProgramiv(gShaderProgramObject1, GL_LINK_STATUS, &iProgramLinkStatus);

		if (iProgramLinkStatus == GL_FALSE)
		{
			glGetProgramiv(gShaderProgramObject1, GL_INFO_LOG_LENGTH, &iInfoLogLength);

			if (iInfoLogLength > 0)
			{
				szInfolog = (GLchar *)malloc(iInfoLogLength);
				if (szInfolog != NULL)
				{
					GLsizei written;

					glGetProgramInfoLog(gShaderProgramObject1, iInfoLogLength, &written, szInfolog);
					fprintf(gpFile, "Inside Linking of program %s", szInfolog);
					free(szInfolog);

					uninitialize();
					
					exit(0);
				}
			}
		}


	
	mUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_m_matrix");
	vUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_v_matrix");
	pUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_p_matrix");
	laUniform_red1 = glGetUniformLocation(gShaderProgramObject1, "u_la_red");
	laUniform_green1 = glGetUniformLocation(gShaderProgramObject1, "u_la_green");
	laUniform_blue1 = glGetUniformLocation(gShaderProgramObject1, "u_la_blue");
	lsUniform_red1 = glGetUniformLocation(gShaderProgramObject1, "u_ls_red");
	lsUniform_green1 = glGetUniformLocation(gShaderProgramObject1, "u_ls_green");
	lsUniform_blue1 = glGetUniformLocation(gShaderProgramObject1, "u_ls_blue");
	ldUniform_red1 = glGetUniformLocation(gShaderProgramObject1, "u_ld_red");
	ldUniform_green1 = glGetUniformLocation(gShaderProgramObject1, "u_ld_green");
	ldUniform_blue1 = glGetUniformLocation(gShaderProgramObject1, "u_ld_blue");
	kdUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_kd");
	kaUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_ka");
	ksUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_ks");
	material_shininess_uniform1 = glGetUniformLocation(gShaderProgramObject1, "m_shininess");
	lightPositionUniform_red1 = glGetUniformLocation(gShaderProgramObject1, "u_light_position_red");
	lightPositionUniform_green1 = glGetUniformLocation(gShaderProgramObject1, "u_light_position_green");
	lightPositionUniform_blue1 = glGetUniformLocation(gShaderProgramObject1, "u_light_position_blue");
	isLKeypressedUniform1 = glGetUniformLocation(gShaderProgramObject1, "u_lkeyispressed");

	
	
	
	//Fragment Shader
	
	
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);


	const GLchar* vertexShaderSourceCode2 =
		"#version 440" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vnormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec4 u_light_position_green;" \
		"out vec4 eye_coordinates;" \
		"out vec3 light_direction_red;" \
		"out vec3 light_direction_blue;" \
		"out vec3 light_direction_green;" \
		"out vec3 tnorm;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_p_matrix*u_v_matrix*u_m_matrix*vPosition;" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec4 eye_coordinates=u_v_matrix*u_m_matrix*vPosition;" \
		"tnorm=mat3(u_v_matrix*u_m_matrix)*vnormal;" \
		"light_direction_red=vec3(u_light_position_red)-eye_coordinates.xyz;" \
		"light_direction_blue=vec3(u_light_position_blue)-eye_coordinates.xyz;" \
		"light_direction_green=vec3(u_light_position_green)-eye_coordinates.xyz;" \
		"viewer_vector=vec3(-eye_coordinates);" \
		"}" \
		"else" \
		"{" \
		"}" \

		"}";

	glShaderSource(gVertexShaderObject, 1, &vertexShaderSourceCode2, NULL);

	
	glCompileShader(gVertexShaderObject);


	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	//Fragment
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	
	const GLchar* fragmentShaderSourceCode2 =
		"#version 440" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform int u_lkeyispressed;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_ks;" \
		"uniform vec3 u_kd;" \
		"uniform float m_shininess;"
		"in vec3 light_direction_red;" \
		"in vec3 light_direction_blue;" \
		"in vec3 light_direction_green;" \
		"in vec3 tnorm;"
		"in vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lkeyispressed == 1)" \
		"{" \
		"vec3 tdnorm=normalize(tnorm);" \
		"vec3 light_direction1_red=normalize(light_direction_red);" \
		"vec3 light_direction1_blue=normalize(light_direction_blue);" \
		"vec3 light_direction1_green=normalize(light_direction_green);" \
		"float tn_dot_ld1=max(dot(light_direction1_red,tdnorm),0.0);" \
		"float tn_dot_ld2=max(dot(light_direction1_green,tdnorm),0.0);" \
		"float tn_dot_ld3=max(dot(light_direction1_blue,tdnorm),0.0);" \
		"vec3 reflection_vector_red=reflect(-light_direction1_red,tdnorm);" \
		"vec3 reflection_vector_blue=reflect(-light_direction1_blue,tdnorm);" \
		"vec3 reflection_vector_green=reflect(-light_direction1_green,tdnorm);" \
		"vec3 viewer_vector1=normalize(viewer_vector);" \
		"vec3 ambient1 = u_la_red * u_ka;" \
		"vec3 ambient2 = u_la_green * u_ka;" \
		"vec3 ambient3 = u_la_blue * u_ka;" \
		"vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld1;" \
		"vec3 diffuse2 = u_ld_green * u_kd * tn_dot_ld2;" \
		"vec3 diffuse3 = u_ld_blue * u_kd * tn_dot_ld3;" \
		"vec3 specular1=u_ls_red * u_ks * pow(max(dot(reflection_vector_red,viewer_vector1),0.0),m_shininess);" \
		"vec3 specular2=u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue,viewer_vector1),0.0),m_shininess);" \
		"vec3 specular3=u_ls_green * u_ks * pow(max(dot(reflection_vector_green,viewer_vector1),0.0),m_shininess);" \
		"vec3 phong_ads_ligh1=ambient1+diffuse1+specular1;" \
		"vec3 phong_ads_ligh2=ambient2+diffuse2+specular2;" \
		"vec3 phong_ads_ligh3=ambient3+diffuse3+specular3;" \
		"vec3 finalColor=phong_ads_ligh1+phong_ads_ligh2+phong_ads_ligh3;" \
		"FragColor=vec4(finalColor,1.0);" \
		"}" \
		"else"
		"{"
		"FragColor=vec4(1.0,1.0,1.0,1.0);" \
		"}"
		"}";
	
	glShaderSource(gFragmentShaderObject, 1, &fragmentShaderSourceCode2, NULL);

	
	glCompileShader(gFragmentShaderObject);

	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	
	
	
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vnormal");

	

	glLinkProgram(gShaderProgramObject);

	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Linking of program %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}

	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform_red = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	laUniform_green = glGetUniformLocation(gShaderProgramObject, "u_la_green");
	laUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	lsUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	lsUniform_green = glGetUniformLocation(gShaderProgramObject, "u_ls_green");
	lsUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	ldUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
	ldUniform_green = glGetUniformLocation(gShaderProgramObject, "u_ld_green");
	ldUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "m_shininess");
	lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");
	lightPositionUniform_green = glGetUniformLocation(gShaderProgramObject, "u_light_position_green");
	lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");
	isLKeypressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lkeyispressed");
	
	
	

	getSphereVertexData(sphere_vertices,sphere_normals,sphere_texture,sphere_elements);
 gNumVertices = getNumberOfSphereVertices();
	 gNumElements = getNumberOfSphereElements();

	
	//Pyramid
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(sphere_elements),sphere_elements,GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	light[0].ambient[0] = 0.0f;
	light[0].ambient[1] = 0.0f;
	light[0].ambient[2] = 0.0f;
	light[0].ambient[3] = 1.0f;

	light[0].diffuse[0] = 1.0f;
	light[0].diffuse[1] = 0.0f;
	light[0].diffuse[2] = 0.0f;
	light[0].diffuse[3] = 1.0f;

	light[0].specular[0] = 1.0f;
	light[0].specular[1] = 0.0f;
	light[0].specular[2] = 0.0f;
	light[0].specular[3] = 1.0f;

	light[0].position[0] = 0.0f;
	light[0].position[1] = 0.0f;
	light[0].position[2] = 0.0f;
	light[0].position[3] = 1.0f;


	light[1].ambient[0] = 0.0f;
	light[1].ambient[1] = 0.0f;
	light[1].ambient[2] = 0.0f;
	light[1].ambient[3] = 1.0f;

	light[1].diffuse[0] = 0.0f;
	light[1].diffuse[1] = 1.0f;
	light[1].diffuse[2] = 0.0f;
	light[1].diffuse[3] = 1.0f;

	light[1].specular[0] = 0.0f;
	light[1].specular[1] = 1.0f;
	light[1].specular[2] = 0.0f;
	light[1].specular[3] = 1.0f;

	light[1].position[0] = 0.0f;
	light[1].position[1] = 0.0f;
	light[1].position[2] = 0.0f;
	light[1].position[3] = 1.0f;

	light[2].ambient[0] = 0.0f;
	light[2].ambient[1] = 0.0f;
	light[2].ambient[2] = 0.0f;
	light[2].ambient[3] = 1.0f;

	light[2].diffuse[0] = 0.0f;
	light[2].diffuse[1] = 0.0f;
	light[2].diffuse[2] = 1.0f;
	light[2].diffuse[3] = 1.0f;

	light[2].specular[0] = 0.0f;
	light[2].specular[1] = 0.0f;
	light[2].specular[2] = 1.0f;
	light[2].specular[3] = 1.0f;

	light[2].position[0] = 0.0f;
	light[2].position[1] = 0.0f;
	light[2].position[2] = 0.0f;
	light[2].position[3] = 1.0f;
	//
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	perspectiveProjectionMatrix = mat4::identity();

	
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix=perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display()
{

glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 rotationMatrix;
	mat4 modelViewMatrix;

	modelMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	modelViewMatrix = mat4::identity();


	projectionMatrix = projectionMatrix * perspectiveProjectionMatrix;
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	if (gbLight == 1)
	{
		glUniform1i(isLKeypressedUniform, 1);




		glUniform3fv(laUniform_red, 1, light[0].ambient);
		glUniform3fv(ldUniform_red, 1, light[0].diffuse);
		glUniform3fv(lsUniform_red, 1, light[0].specular);

		glUniform4fv(lightPositionUniform_red, 1, light[0].position);



		glUniform3fv(laUniform_green, 1, light[1].ambient);
		glUniform3fv(ldUniform_green, 1, light[1].diffuse);
		glUniform3fv(lsUniform_green, 1, light[1].specular);
		//rotationMatrix = mat4::identity();
		//rotationMatrix = rotate(LightAngleone, 0.0f, 1.0f, 0.0f);
		//light[1].position[0] = LightAngleone;
		glUniform4fv(lightPositionUniform_green, 1, light[1].position);



		glUniform3fv(laUniform_blue, 1, light[2].ambient);
		glUniform3fv(ldUniform_blue, 1, light[2].diffuse);
		glUniform3fv(lsUniform_blue, 1, light[2].specular);

		
		glUniform4fv(lightPositionUniform_blue, 1, light[2].position);


		glUniform3fv(kaUniform, 1, MaterialAmbient);
		glUniform3fv(kdUniform, 1, MaterialDiffuse);
		glUniform3fv(ksUniform, 1, MaterialSpecular);
		glUniform1fv(material_shininess_uniform, 1, MaterialShininess);


	}
	else
	{
		glUniform1i(isLKeypressedUniform, 0);
	}
	glBindVertexArray(vao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);
	glXSwapBuffers(gpDisplay,gWindow);
}



void Update()
{
	LightAnglezero = LightAnglezero + 0.01f;
	LightAngleone = LightAngleone + 0.01f;
	LightAngletwo = LightAngletwo + 0.01f;
	if (LightAnglezero >= 2 * M_PI)
	{
		LightAnglezero = 0.0f;

	}
	light[0].position[1] = 100.0f*sin(LightAnglezero);
	light[0].position[2] = 100.0f*cos(LightAnglezero);

	if (LightAngleone >= 2 * M_PI)
	{
		LightAngleone = 0.0f;
	}
	light[1].position[0] = 100.0f*sin(LightAngleone);
	light[1].position[2] = 100.0f*cos(LightAngleone);
	if (LightAngletwo >= 2 * M_PI)
	{
		LightAngletwo = 0.0f;
	}
	light[2].position[0] = 100.0f*sin(LightAngleone);
	light[2].position[1] = 100.0f*cos(LightAngleone);
}



void uninitialize(void)
{
	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	GLXContext CurrentglxContext=glXGetCurrentContext();
	
	if(CurrentglxContext!=NULL&& CurrentglxContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
		
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}




