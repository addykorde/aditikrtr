#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>


#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>
#include<SOIL/SOIL.h>

#include"vmath.h"

using namespace std;
using namespace vmath;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global variables.

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGLXContext;
static GLfloat angle_pyramid = 0.0f, angle_cube = 0.0f;

typedef GLXContext(* glXCreateContextAttribsARBProc)(Display *,GLXFBConfig,GLXContext,bool,GLint *);


glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;

//Shaders

GLint gVertexShaderObject;
GLint gFragmentShaderObject;
GLint gShaderProgramObject;

GLuint vao_pyramid;
GLuint vao_cube;
GLuint vbo_position_pyramid;
GLuint vbo_position_cube;
GLuint vbo_color_pyramid;
GLuint vbo_color_cube;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;


FILE *gpFile = NULL; 

int main(void)
{
	
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();
	
	//Opengl functions prototype declarations
	void initialize(void);
	void resize(int,int);
	void display(void);
	void Update(void);
	//Local variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	bool bdone=false;
	
	
	
	CreateWindow();
	initialize();
	

	XEvent event;
	KeySym keysym;
	
	
		while(bdone==false)
		{
		while(XPending(gpDisplay))
		{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
			break;
			case KeyPress:
			keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
			switch(keysym)
			{
				case XK_Escape:
				bdone=true;
				break;
				
				case XK_F:
				case XK_f:
				if(bFullscreen==false)
				{
					ToggleFullscreen();
					bFullscreen=true;
				}
				else{
					ToggleFullscreen();
					bFullscreen=false;
				}
				break;
				
				default:
				break;
			}
			break;
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
				default:
				break;
			}
			break;
			case MotionNotify:
			break;
			case ConfigureNotify:
			winWidth=event.xconfigure.width;
			winHeight=event.xconfigure.height;
			resize(winWidth,winHeight);
			break;
			case Expose:
			break;
			case DestroyNotify:
			break;
			case 33:
			bdone=true;
			break;
			default:
			break;
		}
		}
			Update();
			display();
		}
		uninitialize();
		return(0);
}

void CreateWindow()
{
	
	void uninitialize(void);
	
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNoOfFBConfigs=0;

	int bestFrameBufferConfig=-1;
	int bestNoOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNoOfSamples=999;
	
	static int frameBufferAttributes[]={GLX_X_RENDERABLE,true,
					    GLX_DRAWABLE_TYPE,
					    GLX_WINDOW_BIT,
					    GLX_RENDER_TYPE,
					    GLX_RGBA_BIT,
	 				    GLX_X_VISUAL_TYPE,
	 				    GLX_TRUE_COLOR,
					    GLX_RED_SIZE,8,
					    GLX_GREEN_SIZE,8,
					    GLX_BLUE_SIZE,8,
 					    GLX_ALPHA_SIZE,8,
			 		    GLX_DEPTH_SIZE,24,
					    GLX_STENCIL_SIZE,8,
			 		    GLX_DOUBLEBUFFER,true,
					    None    }; //None=0
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&iNoOfFBConfigs);
	
	
	for(int i=0;i<iNoOfFBConfigs;i++)
	{
        pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);

        if(pTempXVisualInfo){
                int sampleBuffers,samples;
                
                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);

                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

                if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNoOfSamples){
                        bestFrameBufferConfig=i;
                        bestNoOfSamples=samples;
                }
                if(worstFrameBufferConfig<0 || !sampleBuffers && samples<worstNoOfSamples){
                        worstFrameBufferConfig=i;
                        worstNoOfSamples=samples;
                }

        }
        XFree(pTempXVisualInfo);
}


	
	bestGLXFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestGLXFBConfig;
	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	
	if(gpXVisualInfo==NULL)
	{
		printf("ERROR : Unable to Open gpXVisualInfo.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
						RootWindow(gpDisplay,gpXVisualInfo->screen),
						gpXVisualInfo->visual,
						AllocNone);
	gColormap=winAttribs.colormap;
	
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|
		StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
						  RootWindow(gpDisplay,gpXVisualInfo->screen),
						  0,
						  0,
						  giWindowWidth,
						  giWindowHeight,
						  0,
						  gpXVisualInfo->depth,
						  InputOutput,
						  gpXVisualInfo->visual,
						  styleMask,
						  &winAttribs);
	if(!gWindow)
	{
		printf("ERROR :Failed to create main window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"2-2D Shapes(Animated).");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void initialize(void)
{
	void uninitialize();
	void resize(int, int);
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	bool loadTexture(GLuint *,const char*);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL){
                printf("ERROR !!!Failed To Get glXCreateContextAttribsARB .\n\n");
                uninitialize();
                exit(1);
        }

        GLint Attribs[]={
            GLX_CONTEXT_MAJOR_VERSION_ARB,4,
            GLX_CONTEXT_MINOR_VERSION_ARB,5,
            GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
            None
        };    

	 gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                        gGLXFBConfig,
                                        0,
                                        true,
                                        Attribs);


        if(!gGLXContext){
                GLint Attribs[]={
                                GLX_CONTEXT_MAJOR_VERSION_ARB,1,
                                GLX_CONTEXT_MINOR_VERSION_ARB,0,
                                None
                };    

                gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                                gGLXFBConfig,
                                                0,
                                                true,
                                                Attribs);
        }

        if(!glXIsDirect(gpDisplay,gGLXContext)){
                printf("ERROR : The Obtained context is not H/W rendering context...\n");
        }else{
                printf("The Obtained context is H/W rendering context...\n");
        }
         
        glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

        GLenum result;
        result=glewInit();
        result = glewInit();
	if (result != GLEW_OK)
	{
		uninitialize();
		fprintf(gpFile, "Inside GLEW\n");
	}



	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);


	const GLchar* vertexShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec4 vColor;" \
		"in vec4 vPosition;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_color=vColor;"\
		"}";
	glShaderSource(gVertexShaderObject, 1, &vertexShaderSourceCode, NULL);

	
	glCompileShader(gVertexShaderObject);


	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	//Fragment
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	
	const GLchar* fragmentShaderSourceCode =
	"#version 440 core" \
	"\n" \
	"in vec4 out_color;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
	"FragColor=out_color;" \
	"}" ;


	
	glShaderSource(gFragmentShaderObject, 1, &fragmentShaderSourceCode, NULL);

	
	glCompileShader(gFragmentShaderObject);

	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	
	
	
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");


	glLinkProgram(gShaderProgramObject);

	mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Linking of program %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	
	const GLfloat pyramidVertices[] = { 0.0f, 1.0f, 0.0f,
										-1.0f, -1.0f, 1.0f ,
										1.0f, -1.0f, 1.0f ,
										0.0f, 1.0f, 0.0f ,
										1.0f, -1.0f, 1.0f ,
										1.0f, -1.0f, -1.0f ,
										0.0f, 1.0f, 0.0f ,
										-1.0f, -1.0f,-1.0f ,
										-1.0f, -1.0f, 1.0f ,
										0.0f, 1.0f, 0.0f ,
										1.0f, -1.0f, -1.0f ,
										-1.0f, -1.0f, -1.0f ,
	};
	const GLfloat pyramidColor[] = { 1.0f, 0.0f, 0.0f,
0.0f, 1.0f, 0.0f ,
0.0f, 0.0f, 1.0f ,

1.0f, 0.0f, 0.0f ,
0.0f, 0.0f, 1.0f ,
0.0f, 1.0f, 0.0f ,

1.0f, 0.0f, 0.0f ,
0.0f, 0.0f, 1.0f ,
0.0f, 1.0f, 0.0f ,

1.0f, 0.0f, 0.0f ,
0.0f, 1.0f, 0.0f ,
0.0f, 0.0f, 1.0f , };
	const GLfloat cubeVertices[] = {
	1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,

	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,

	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,

	1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f};
	const GLfloat cubeColor[] = { 0.0f, 1.0f, 0.0f,
									0.0f, 1.0f, 0.0f,
									0.0f, 1.0f, 0.0f,
									10.0f, 1.0f, 0.0f,

									1.0f, 1.0f, 0.0f,
									1.0f, 1.0f, 0.0f,
									1.0f, 1.0f, 0.0f,
									1.0f, 1.0f, 0.0f,

									1.0f, 0.0f, 0.0f,
									1.0f, 0.0f, 0.0f,
									1.0f, 0.0f, 0.0f,
									1.0f, 0.0f, 0.0f,

									1.0f, 1.0f, 0.0f,
									1.0f, 1.0f, 0.0f,
									1.0f, 1.0f, 0.0f,
									1.0f, 1.0f, 0.0f,

									0.0f, 0.0f, 1.0f,
									0.0f, 0.0f, 1.0f,
									0.0f, 0.0f, 1.0f,
									0.0f, 0.0f, 1.0f,

									1.0f, 0.0f, 1.0f,
									1.0f, 0.0f, 1.0f,
									1.0f, 0.0f, 1.0f,
									1.0f, 0.0f, 1.0f ,
									};


	//Pyramid

	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);
	glGenBuffers(1, &vbo_position_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_pyramid);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidColor), pyramidColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//Cube
	glGenVertexArrays(1, &vao_cube);
	glBindVertexArray(vao_cube);
	glGenBuffers(1, &vbo_position_cube);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenBuffers(1, &vbo_color_cube);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeColor), cubeColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	perspectiveProjectionMatrix = mat4::identity();

	
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix=perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display()
{

glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 rotationMatrix;
	//Pyramid
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = translate(-1.5f, 0.0f, -6.0f);
	rotationMatrix = rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_pyramid);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glBindVertexArray(0);

	//Cube
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = translate(1.5f, 0.0f, -6.0f);
	rotationMatrix = rotate(angle_cube, angle_cube, angle_cube);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix*rotationMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glBindVertexArray(0);

	glUseProgram(0);
	glXSwapBuffers(gpDisplay,gWindow);
}



void Update()
{
	if (angle_pyramid <= 360.0f)
	{
		angle_pyramid = angle_pyramid + 5.0f;
	}
	else
	{
		angle_pyramid= 0.0f;
	}
	if (angle_cube <= 360.0f)
	{
		angle_cube = angle_cube + 5.0f;
	}
	else
	{
		angle_cube = 0.0f;
	}
}



void uninitialize(void)
{
	if (vbo_position_cube)
	{
		glDeleteBuffers(1, &vbo_position_cube);
		vbo_position_cube = 0;
	}
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	GLXContext CurrentglxContext=glXGetCurrentContext();
	
	if(CurrentglxContext!=NULL&& CurrentglxContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
		
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}




