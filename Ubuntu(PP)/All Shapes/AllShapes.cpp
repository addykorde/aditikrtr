#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>


#include<GL/glew.h>
#include<GL/glx.h>
#include<GL/gl.h>
#include<SOIL/SOIL.h>

#include"vmath.h"

using namespace std;
using namespace vmath;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global variables.

bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo =NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGLXContext;
GLfloat pangle=360.0f,cangle=0.0f;
GLuint texture_stone,texture_kundali;

typedef GLXContext(* glXCreateContextAttribsARBProc)(Display *,GLXFBConfig,GLXContext,bool,GLint *);


glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGLXFBConfig;

//Shaders

	/*static float circlePos[(unsigned int)((2 * 3.1415) / 0.001) * 3];
	static float circleColor[(unsigned int)((2 * 3.1415) / 0.001) * 3];

	GLfloat angle = 0.0f; GLfloat radius = 0.301f;

	
	int index = 0;
	int index1 = 0;*/
	
	
GLint gVertexShaderObject;
GLint gFragmentShaderObject;
GLint gShaderProgramObject;

GLuint vao_lines1;
GLuint vbo_lines1;
GLuint vbo_line1_color;
GLuint vao_lines2;
GLuint vbo_lines2;
GLuint vbo_line2_color;
GLuint vao_lines3;
GLuint vbo_lines3;
GLuint vbo_line3_color;
GLuint vao_lines4;
GLuint vbo_lines4;
GLuint vbo_line4_color;
GLuint vao_circle;
GLuint vbo_circle_color;
GLuint vbo_circle_position;

GLuint vao_glines;
GLuint vbo_glines;
GLuint vbo_glines_color;
GLuint vao_glines1;
GLuint vbo_glines1;
GLuint vbo_glines1_color;
GLuint vao_square1;
GLuint vao_square2;
GLuint vao_square3;
GLuint vao_square4;
GLuint vbo_position_square1;
GLuint vbo_position_square2;
GLuint vbo_position_square3;
GLuint vbo_position_square4;
GLuint vbo_color_square1;
GLuint vbo_color_square2;
GLuint vbo_color_square3;
GLuint vbo_color_square4;
GLuint vao_circle2;
GLuint vbo_circle2_position;
GLuint vbo_cicle2_color;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;


FILE *gpFile = NULL; 

int main(void)
{
	
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();
	
	//Opengl functions prototype declarations
	void initialize(void);
	void resize(int,int);
	void display(void);
	void Update(void);
	//Local variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	bool bdone=false;
	
	
	
	
	CreateWindow();
	initialize();
	

	XEvent event;
	KeySym keysym;
	
	
		while(bdone==false)
		{
		while(XPending(gpDisplay))
		{
		XNextEvent(gpDisplay,&event);
		switch(event.type)
		{
			case MapNotify:
			break;
			case KeyPress:
			keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
			switch(keysym)
			{
				case XK_Escape:
				bdone=true;
				break;
				
				case XK_F:
				case XK_f:
				if(bFullscreen==false)
				{
					ToggleFullscreen();
					bFullscreen=true;
				}
				else{
					ToggleFullscreen();
					bFullscreen=false;
				}
				break;
				
				default:
				break;
			}
			break;
			case ButtonPress:
			switch(event.xbutton.button)
			{
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
				default:
				break;
			}
			break;
			case MotionNotify:
			break;
			case ConfigureNotify:
			winWidth=event.xconfigure.width;
			winHeight=event.xconfigure.height;
			resize(winWidth,winHeight);
			break;
			case Expose:
			break;
			case DestroyNotify:
			break;
			case 33:
			bdone=true;
			break;
			default:
			break;
		}
		}
			//Update();
			display();
		}
		uninitialize();
		return(0);
}

void CreateWindow()
{
	
	void uninitialize(void);
	
	
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	
	GLXFBConfig *pGLXFBConfig=NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNoOfFBConfigs=0;

	int bestFrameBufferConfig=-1;
	int bestNoOfSamples=-1;
	int worstFrameBufferConfig=-1;
	int worstNoOfSamples=999;
	
	static int frameBufferAttributes[]={GLX_X_RENDERABLE,true,
					    GLX_DRAWABLE_TYPE,
					    GLX_WINDOW_BIT,
					    GLX_RENDER_TYPE,
					    GLX_RGBA_BIT,
	 				    GLX_X_VISUAL_TYPE,
	 				    GLX_TRUE_COLOR,
					    GLX_RED_SIZE,8,
					    GLX_GREEN_SIZE,8,
					    GLX_BLUE_SIZE,8,
 					    GLX_ALPHA_SIZE,8,
			 		    GLX_DEPTH_SIZE,24,
					    GLX_STENCIL_SIZE,8,
			 		    GLX_DOUBLEBUFFER,true,
					    None    }; //None=0
	
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable to Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	pGLXFBConfig=glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&iNoOfFBConfigs);
	
	
	for(int i=0;i<iNoOfFBConfigs;i++)
	{
        pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);

        if(pTempXVisualInfo){
                int sampleBuffers,samples;
                
                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);

                glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);

                if(bestFrameBufferConfig<0 || sampleBuffers && samples>bestNoOfSamples){
                        bestFrameBufferConfig=i;
                        bestNoOfSamples=samples;
                }
                if(worstFrameBufferConfig<0 || !sampleBuffers && samples<worstNoOfSamples){
                        worstFrameBufferConfig=i;
                        worstNoOfSamples=samples;
                }

        }
        XFree(pTempXVisualInfo);
}


	
	bestGLXFBConfig=pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig=bestGLXFBConfig;
	XFree(pGLXFBConfig);

	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);

	
	if(gpXVisualInfo==NULL)
	{
		printf("ERROR : Unable to Open gpXVisualInfo.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay,
						RootWindow(gpDisplay,gpXVisualInfo->screen),
						gpXVisualInfo->visual,
						AllocNone);
	gColormap=winAttribs.colormap;
	
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);
	
	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|
		StructureNotifyMask;

	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow=XCreateWindow(gpDisplay,
						  RootWindow(gpDisplay,gpXVisualInfo->screen),
						  0,
						  0,
						  giWindowWidth,
						  giWindowHeight,
						  0,
						  gpXVisualInfo->depth,
						  InputOutput,
						  gpXVisualInfo->visual,
						  styleMask,
						  &winAttribs);
	if(!gWindow)
	{
		printf("ERROR :Failed to create main window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"All Shapes");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen()
{
	
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
				RootWindow(gpDisplay,gpXVisualInfo->screen),
				False,
				StructureNotifyMask,
				&xev);
}


void initialize(void)
{
	void uninitialize();
	void resize(int, int);
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	bool loadTexture(GLuint *,const char*);
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	
	if(glXCreateContextAttribsARB==NULL){
                printf("ERROR !!!Failed To Get glXCreateContextAttribsARB .\n\n");
                uninitialize();
                exit(1);
        }

        GLint Attribs[]={
            GLX_CONTEXT_MAJOR_VERSION_ARB,4,
            GLX_CONTEXT_MINOR_VERSION_ARB,5,
            GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
            None
        };    

	 gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                        gGLXFBConfig,
                                        0,
                                        true,
                                        Attribs);


        if(!gGLXContext){
                GLint Attribs[]={
                                GLX_CONTEXT_MAJOR_VERSION_ARB,1,
                                GLX_CONTEXT_MINOR_VERSION_ARB,0,
                                None
                };    

                gGLXContext=glXCreateContextAttribsARB(gpDisplay,
                                                gGLXFBConfig,
                                                0,
                                                true,
                                                Attribs);
        }

        if(!glXIsDirect(gpDisplay,gGLXContext)){
                printf("ERROR : The Obtained context is not H/W rendering context...\n");
        }else{
                printf("The Obtained context is H/W rendering context...\n");
        }
         
        glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

        GLenum result;
        result=glewInit();
        result = glewInit();
	if (result != GLEW_OK)
	{
		uninitialize();
		fprintf(gpFile, "Inside GLEW\n");
	}



	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);


	const GLchar* vertexShaderSourceCode =
		"#version 440 core" \
		"\n" \
		"in vec4 vColor;" \
		"in vec4 vPosition;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix*vPosition;" \
		"out_color=vColor;"\
		"}";
	glShaderSource(gVertexShaderObject, 1, &vertexShaderSourceCode, NULL);

	
	glCompileShader(gVertexShaderObject);


	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Vertex Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	//Fragment
	
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	
	const GLchar* fragmentShaderSourceCode =
	"#version 440 core" \
	"\n" \
	"in vec4 out_color;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
	"FragColor=out_color;" \
	"}" ;


	
	glShaderSource(gFragmentShaderObject, 1, &fragmentShaderSourceCode, NULL);

	
	glCompileShader(gFragmentShaderObject);

	
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Fragment Shader %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	
	
	
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");


	glLinkProgram(gShaderProgramObject);

	mvpUniform = glGetUniformLocation(gShaderProgramObject,"u_mvp_matrix");

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;

				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Inside Linking of program %s", szInfoLog);
				free(szInfoLog);

				uninitialize();
				exit(0);
			}
		}
	}


	
	const GLfloat lineVertices[] = {
	-0.5f,-0.5f,0.0f,
	0.0f,0.5f,0.0f };


	const GLfloat line1Vertices[] = {
		0.0f,0.5f,0.0f,
		0.5f,-0.5f,0.0f
	};

	const GLfloat line2Vertices[] = {
		0.5f,-0.5f,0.0f,
		-0.5f,-0.5f,0.0f
	};

	const GLfloat line3Vertices[] = {
		0.0f,0.5f,0.0f,
		0.0f,-0.5f,0.0f
	};

	const GLfloat glineColor[] = {
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f
	};

	const GLfloat glineColor1[] = {
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f
	};

	const GLfloat lineColor[] = {
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
	};

	const GLfloat glineVertices[] = {
	-1.0f,0.0f,0.0f,
	1.0f,0.0f,0.0f };

	const GLfloat gline1Vertices[] = {
		0.0f,1.0f,0.0f,
		0.0f,-1.0f,0.0f
	};

	const GLfloat square1Vertices[] = {
		-0.5f,-0.5f,0.0f,
		-0.5f,0.5f,0.0f
	};
	const GLfloat square2Vertices[] = {
		-0.5f,0.5f,0.0f,
		0.5f,0.5f,0.0f
	};
	const GLfloat square3Vertices[] = {
		0.5f,0.5f,0.0f,
		0.5f,-0.5f,0.0f
	};
	const GLfloat square4Vertices[] = {
		0.5f,-0.5f,0.0f,
		-0.5f,-0.5f,0.0f
	};

	const GLfloat square1Color[]=
	{
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f
	};

	const GLfloat square2Color[] =
	{
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f
	};

	const GLfloat square3Color[] =
	{
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f
	};

	const GLfloat square4Color[] =
	{
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f
	};

	glGenVertexArrays(1, &vao_square1);
	glBindVertexArray(vao_square1);
	glGenBuffers(1, &vbo_position_square1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square1Vertices), square1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_color_square1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square1Color), square1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	glGenVertexArrays(1, &vao_square2);
	glBindVertexArray(vao_square2);
	glGenBuffers(1, &vbo_position_square2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square2Vertices), square2Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_color_square1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square1Color), square1Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	glGenVertexArrays(1, &vao_square3);
	glBindVertexArray(vao_square3);
	glGenBuffers(1, &vbo_position_square3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square3Vertices), square3Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_color_square3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square3Color), square3Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	glGenVertexArrays(1, &vao_square4);
	glBindVertexArray(vao_square4);
	glGenBuffers(1, &vbo_position_square4);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_square4);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square4Vertices), square4Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_color_square4);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_square4);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square4Color), square4Color, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glGenVertexArrays(1, &vao_lines1);
	glBindVertexArray(vao_lines1);
	glGenBuffers(1, &vbo_lines1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), lineVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_line1_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line1_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glGenVertexArrays(1, &vao_lines2);
	glBindVertexArray(vao_lines2);
	glGenBuffers(1, &vbo_lines2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line1Vertices), line1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &vbo_line2_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line2_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	//Line3

	glGenVertexArrays(1, &vao_lines3);
	glBindVertexArray(vao_lines3);
	glGenBuffers(1, &vbo_lines3);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_lines3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(line2Vertices), line2Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glGenBuffers(1, &vbo_line3_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_line3_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(lineColor), lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Line4



	//CIRCLE

	//Graph
	glGenVertexArrays(1, &vao_circle);
	glBindVertexArray(vao_circle);
	glGenBuffers(1, &vbo_circle_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_circle_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	glGenVertexArrays(1, &vao_glines);
	glBindVertexArray(vao_glines);
	glGenBuffers(1, &vbo_glines);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_glines);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glineVertices), glineVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_glines_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_glines_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glineColor), glineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glBindVertexArray(0);


	glGenVertexArrays(1, &vao_glines1);
	glBindVertexArray(vao_glines1);
	glGenBuffers(1, &vbo_glines1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_glines1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(gline1Vertices), gline1Vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_glines1_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_glines1_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glineColor1), glineColor1, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glBindVertexArray(0);
	//
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	perspectiveProjectionMatrix = mat4::identity();

	
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix=perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display()
{

glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	void DrawCircle(GLfloat);
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;





	for (GLfloat x = -1.0f; x <= 1.05f; x = x + 0.05f)
	{
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		modelViewMatrix = translate(0.0f, x, -3.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glBindVertexArray(vao_glines);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
	}

	for (GLfloat y = -1.0f; y <= 1.05f; y = y + 0.05f)
	{
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		modelViewMatrix = translate(y, 0.0f, -3.0f);
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		glBindVertexArray(vao_glines1);
		glDrawArrays(GL_LINES, 0, 2);
		glBindVertexArray(0);
	}
	

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines1);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines2);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines3);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_lines4);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_circle);
	DrawCircle(0.301f);
	glBindVertexArray(0);






	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_square1);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_square2);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_square3);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_square4);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	modelViewMatrix = translate(0.0f, 0.2f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glBindVertexArray(vao_circle);
	DrawCircle(0.7f);
	glBindVertexArray(0);
	
	glUseProgram(0);
	glXSwapBuffers(gpDisplay,gWindow);
}





void DrawCircle(GLfloat radius) {
	GLfloat angle = 0;

	static float circlePos[(unsigned int)((2 * 3.1415) / 0.001) * 3];
	static float circleColor[(unsigned int)((2 * 3.1415) / 0.001) * 3];



	int index = 0;
	int index1 = 0;

	for (angle = 0.0f; angle < 2 * 3.14159265; angle = angle + 0.001f) {
		circlePos[index++] = cos(angle)*radius;
		circlePos[index++] = sin(angle)*radius-0.20;
		circlePos[index++] = 0.0f;
		//indexPos++;
	}
	for (angle = 0.0f; angle < 2 * 3.14159265; angle = angle + 0.001f) {
		circleColor[index1++] = 1.0f;
		circleColor[index1++] = 1.0f;
		circleColor[index1++] = 0.0f;
		//indexPos++;
	}



	glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_position);
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), circlePos, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_circle_color);
	glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float)*((2 * 3.1415) / 0.001), circleColor, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_POINTS, 0, ((2 * 3.1415) / 0.001));

}



void uninitialize(void)
{
	/*if (vbo_position_triangle)
	{
		glDeleteBuffers(1, &vbo_position_triangle);
		vbo_position_triangle = 0;
	}
	if (vao_triangle)
	{
		glDeleteVertexArrays(1, &vao_triangle);
		vao_triangle = 0;
	}*/

	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	GLXContext CurrentglxContext=glXGetCurrentContext();
	
	if(CurrentglxContext!=NULL&& CurrentglxContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
		
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

