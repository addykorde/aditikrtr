#include<Windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"GLU32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define NUM_POINTS 1000000
#define RADIUS 0.6962



HDC gHdc = NULL;
HGLRC ghrc = NULL;
HWND gHwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool bIsFullScreen = false;
bool bDone = false;
int iRet;

FILE * gpFile = NULL;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
GLfloat linex = 0.0f, liney = 6.0f;
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	int initialize(void);

	void display(void);
	void Update();
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("TriangleRectangleWithLoadIdentity");
	WNDCLASSEX wndclass;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("TriangleRectangleWithLoadIdentity"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixel Format failed");
		DestroyWindow(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext failed");
		DestroyWindow(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent failed");
		DestroyWindow(0);
	}
	else
	{
		fprintf(gpFile, "Initialize Function succeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update();
			}
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen();
	void uninitialize();

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
			if (bIsFullScreen == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}

	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);

		ShowCursor(true);
		bIsFullScreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatDescriptor;


	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;

	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	gHdc = GetDC(gHwnd);
	iPixelFormatDescriptor = ChoosePixelFormat(gHdc, &pfd);

	if (iPixelFormatDescriptor == 0)
	{
		return -1;
	}
	if (SetPixelFormat(gHdc, iPixelFormatDescriptor, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(gHdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return -4;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void display()
{
	int j6 = 6.0f, i6 = 0.0f;

	static GLfloat angle = 0.0f;
	GLfloat x = 0.0f, y = 0.0f;
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//Points
	glTranslatef(-9.0f, 2.3f, -15.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glPointSize(5.0f);
	glBegin(GL_POINTS);
	for (GLfloat i = 0.0f; i < 4.0f; i++)
	{
		for (GLfloat j = 0.0f; j < 4.0f; j++)
		{
			glVertex3f(i, j, 0.0f);
		}
	}
	glEnd();

	//2nd Pattern
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-5.0f, 4.0f, -28.0f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (GLfloat j = 6.0f; j > 0.0f; j = j - 2.0f)
	{
		glVertex3f(0.0f, j, 0.0f);
		glVertex3f(6.0f, j, 0.0f);
	}

	for (GLfloat i = 0.0f; i < 6.0f; i = i + 2.0)
	{
		glVertex3f(i, 6.0f, 0.0f);
		glVertex3f(i, 0.0f, 0.0f);
	}

	glVertex3f(0.0f, 4.0f, 0.0f);
	glVertex3f(2.0f, 6.0f, 0.0f);

	glVertex3f(0.0f, 2.0f, 0.0f);
	glVertex3f(4.0f, 6.0f, 0.0f);

	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 6.0f, 0.0f);


	glVertex3f(2.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 4.0f, 0.0f);

	glVertex3f(4.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 2.0f, 0.0f);

	glEnd();

	//3rd pattern
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 4.0f, -28.0f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (GLfloat j = 6.0f; j >= 0.0f; j = j - 2.0f)
	{
		glVertex3f(0.0f, j, 0.0f);
		glVertex3f(6.0f, j, 0.0f);
	}

	for (GLfloat i = 0.0f; i <= 6.0f; i = i + 2.0)
	{
		glVertex3f(i, 6.0f, 0.0f);
		glVertex3f(i, 0.0f, 0.0f);
	}
	glEnd();

	//4th pattern

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-17.0f, -6.5f, -28.0f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (GLfloat j = 6.0f; j >= 0.0f; j = j - 2.0f)
	{
		glVertex3f(0.0f, j, 0.0f);
		glVertex3f(6.0f, j, 0.0f);
	}

	for (GLfloat i = 0.0f; i <= 6.0f; i = i + 2.0)
	{
		glVertex3f(i, 6.0f, 0.0f);
		glVertex3f(i, 0.0f, 0.0f);
	}

	glVertex3f(0.0f, 4.0f, 0.0f);
	glVertex3f(2.0f, 6.0f, 0.0f);

	glVertex3f(0.0f, 2.0f, 0.0f);
	glVertex3f(4.0f, 6.0f, 0.0f);

	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 6.0f, 0.0f);


	glVertex3f(2.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 4.0f, 0.0f);

	glVertex3f(4.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 2.0f, 0.0f);

	glEnd();

	//5th pattern
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-5.0f, -6.5f, -28.0f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 0.0f, 0.0f);

	glVertex3f(6.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 6.0f, 0.0f);

	glVertex3f(6.0f, 6.0f, 0.0f);
	glVertex3f(0.0f, 6.0f, 0.0f);

	glVertex3f(0.0f, 6.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, 6.0f, 0.0f);
	glVertex3f(2.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, 6.0f, 0.0f);
	glVertex3f(4.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, 6.0f, 0.0f);
	glVertex3f(6.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, 6.0f, 0.0f);
	glVertex3f(6.0f, 2.0f, 0.0f);

	glVertex3f(0.0f, 6.0f, 0.0f);
	glVertex3f(6.0f, 4.0f, 0.0f);

	glEnd();




	//6th pattern
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, -6.5f, -28.0f);
	glBegin(GL_LINES);
	

	for (GLfloat j = 6.0f; j >= 0.0f; j = j - 0.01f)
	{
		glVertex3f(0.0f, j, 0.0f);
		glVertex3f(6.0f, j, 0.0f);
	}

	for (GLfloat i = 0.0f; i <= 6.0f; i = i + 0.01)
	{
		if (i < 2.0f)
		{
			glColor3f(1.0f, 0.0f, 0.0f);
		}
		else if(i>2.0f && i<4.0f)
		{
			glColor3f(0.0f, 1.0f, 0.0f);
		}
		else
		{
			glColor3f(0.0f, 0.0f, 1.0f);
		}
		glVertex3f(i, 6.0f, 0.0f);
		glVertex3f(i, 0.0f, 0.0f);
	}
	
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 0.0f, 0.0f);

	glVertex3f(6.0f, 0.0f, 0.0f);
	glVertex3f(6.0f, 6.0f, 0.0f);

	glVertex3f(6.0f, 6.0f, 0.0f);
	glVertex3f(0.0f, 6.0f, 0.0f);

	glVertex3f(0.0f, 6.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, 4.0f, 0.0f);
	glVertex3f(6.0f, 4.0f, 0.0f);

	glVertex3f(0.0f, 2.0f, 0.0f);
	glVertex3f(6.0f, 2.0f, 0.0f);

	glVertex3f(2.0f, 6.0f, 0.0f);
	glVertex3f(2.0f, 0.0f, 0.0f);

	glVertex3f(4.0f, 6.0f, 0.0f);
	glVertex3f(4.0f, 0.0f, 0.0f);


	glEnd();

	SwapBuffers(gHdc);
}

void Update()
{
	if (liney >= 0.0f)
		liney = liney - 2.0f;

}

void uninitialize()
{
	if (bIsFullScreen == true) {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Log File closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}

}


