#include<windows.h>
#include<stdio.h>
#include<gl/GLU.h>
#include<gl/GL.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"GLU32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

HDC gHdc = NULL;
HGLRC ghrc = NULL;

bool gbActiveWindow = false;
FILE * gpFile = NULL;
HWND ghWnd = NULL;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool bFullScreen = false;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
bool blight = false;
GLfloat static angle = 0.0f;

struct Lights{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
};

Lights lights[3];

GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0,1.0,1.0f };
GLfloat MaterialShininess[] = { 128.0f };

GLUquadric *quadric = NULL;
GLfloat LightAnglezero = 0.0f, LightAngleone = 0.0f, LightAngletwo = 0.0f;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void Update();
	int initialize(void);
	int iRet = 0;
	bool bDone = false;
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("Three Moving Lights On A Steady Sphere");
	void display();
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("ERROR"), MB_OK);
		exit(0);
	}


	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;



	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName, TEXT("Three MOving Lights On A Steady Sphere"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);
	ghWnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixel format failed");
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "PixelFormat failed");
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "WglCreateContext failed");
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "WglMake Current  failed");
	}
	else
	{
		fprintf(gpFile, "Initialize Successful");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update();

			}
			display();
		}
	}





	return((int)msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);

	void uninitialize();
	void ToggleFullScreen();
	switch (iMsg)
	{

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
	{
		switch (wParam) {
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			if (bFullScreen == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;

		case 'l':
		case 'L':
			if (blight == false)
			{
				blight = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				blight = false;
				glDisable(GL_LIGHTING);
			}
			break;
		}


	}
	break;

	case WM_DESTROY:
		uninitialize();

		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO mi;
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghWnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}

	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);
		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}



int initialize(void)
{

	lights[0].Ambient[0] = { 0.0f };
	lights[0].Ambient[1] = { 0.0f };
	lights[0].Ambient[2] = { 0.0f };
	lights[0].Ambient[3] = { 1.0f };

	lights[0].Diffuse[0] = { 1.0f };
	lights[0].Diffuse[1] = { 0.0f };
	lights[0].Diffuse[2] = { 0.0f };
	lights[0].Diffuse[3] = { 1.0f };

	lights[0].Specular[0] = { 1.0f };
	lights[0].Specular[1] = { 0.0f };
	lights[0].Specular[2] = { 0.0f };
	lights[0].Specular[3] = { 0.0f };

	lights[0].Position[0] = { 0.0f };
	lights[0].Position[1] = { 0.0f };
	lights[0].Position[2] = { 0.0f };
	lights[0].Position[3] = { 0.0f };


	lights[1].Ambient[0] = { 0.0f };
	lights[1].Ambient[1] = { 0.0f };
	lights[1].Ambient[2] = { 0.0f };
	lights[1].Ambient[3] = { 1.0f };

	lights[1].Diffuse[0] = { 0.0f };
	lights[1].Diffuse[1] = { 1.0f };
	lights[1].Diffuse[2] = { 0.0f };
	lights[1].Diffuse[3] = { 1.0f };

	lights[1].Specular[0] = { 0.0f };
	lights[1].Specular[1] = { 1.0f };
	lights[1].Specular[2] = { 0.0f };
	lights[1].Specular[3] = { 1.0f };

	lights[1].Position[0] = { 0.0f };
	lights[1].Position[1] = { 0.0f };
	lights[1].Position[2] = { 0.0f };
	lights[1].Position[3] = { 1.0f };


	lights[2].Ambient[0] = { 0.0f };
	lights[2].Ambient[1] = { 0.0f };
	lights[2].Ambient[2] = { 0.0f };
	lights[2].Ambient[3] = { 1.0f };

	lights[2].Diffuse[0] = { 0.0f };
	lights[2].Diffuse[1] = { 0.0f };
	lights[2].Diffuse[2] = { 1.0f };
	lights[3].Diffuse[3] = { 1.0f };

	lights[2].Specular[0] = { 0.0f };
	lights[2].Specular[1] = { 0.0f };
	lights[2].Specular[2] = { 1.0f };
	lights[2].Specular[3] = { 1.0f };

	lights[2].Position[0] = { 0.0f };
	lights[2].Position[1] = { 0.0f };
	lights[2].Position[2] = { 0.0f };
	lights[2].Position[2] = { 1.0f };

	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int PixelFormatIndex;

	memset((void*)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(ghWnd);

	PixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (PixelFormatIndex == 0)
	{
		return -1;

	}
	if (SetPixelFormat(gHdc, PixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return -4;
	}
	glShadeModel(GL_SMOOTH);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lights[0].Ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lights[0].Diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lights[0].Specular);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, lights[1].Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lights[1].Diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lights[1].Specular);
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2, GL_AMBIENT, lights[2].Ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, lights[2].Diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, lights[2].Specular);
	glEnable(GL_LIGHT2);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 3.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	glPushMatrix();
	glRotatef(LightAnglezero, 1.0f, 0.0f, 0.0f);
	lights[0].Position[1] = { LightAnglezero };
	glLightfv(GL_LIGHT0, GL_POSITION, lights[0].Position);
	glPopMatrix();

	glPushMatrix();
	glRotatef(LightAngleone, 0.0f, 1.0f, 0.0f);
	lights[1].Position[0] = { LightAngleone };
	glLightfv(GL_LIGHT1, GL_POSITION, lights[1].Position);
	glPopMatrix();

	glPushMatrix();
	glRotatef(LightAngletwo, 0.0f, 0.0f, 1.0f);
	lights[1].Position[0] = { LightAngletwo };
	glLightfv(GL_LIGHT2, GL_POSITION, lights[2].Position);
	glPopMatrix();


	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.75f, 30.0f, 30.0f);

	glPopMatrix();
	SwapBuffers(gHdc);

}

void Update()
{
	LightAnglezero = LightAnglezero + 0.05f;
	LightAngleone = LightAngleone + 0.05f;
	LightAngletwo = LightAngletwo + 0.05f;
	if (LightAnglezero >= 360.0f)
	{
		LightAnglezero = 0.0f;
	}
	if (LightAngleone >= 360.0f)
	{
		LightAngleone = 0.0f;
	}
	if (LightAngletwo >= 360.0f)
	{
		LightAngletwo = 0.0f;
	}
}

void uninitialize()
{
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
		ghrc = NULL;
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Log File closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}



}


