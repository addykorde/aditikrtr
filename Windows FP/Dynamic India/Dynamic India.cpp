#include<windows.h>
#include<stdio.h>
#include<mmsystem.h>
#include "resource.h"


#define _USE_MATH_DEFINES 1
#include<math.h>

#include<gl/GLU.h>
#include<gl/GL.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"GLU32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define RADIUS 1.0f
#define RADIUS2 1.08f


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND gHwnd = NULL;
HGLRC ghrc = NULL;
HDC gHdc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };


int flagd = 1;

GLfloat iOffseti1 = 0.0f;
GLfloat iOffseta = 0.0f;
GLfloat iOffsetn = 0.0f;
GLfloat iOffseti = 0.0f;
GLfloat iColoror = 0.0f, iColorog = 0.0f, iColorob = 0.0f;
GLfloat iColorgr = 0.0f, iColorgg = 0.0f, iColorgb = 0.0f;


int flagi = 0;
int flagpp = 0, flagf = 0;
bool gbActiveWindow = false;
bool bDone = false;
bool bIsFullScreen = false;
int iRet;


GLfloat iOffsetx = 0.0f;

GLfloat originX = -0.5f, originY = 1.0f;
GLfloat angle = 0.0f, angley = 0.0f, anglez = 0.0f, anglez1 = 0.0f;

GLfloat anglel1 = 0.0f, anglel2 = 0.0f;

GLfloat iOffsetxl1 = 0.0f;
GLfloat iOffsetxl2 = 0.0f;

GLfloat iOffsetx1 = 0.0f, iOffsety1 = 0.0f;
GLfloat iOffsetx2 = 0.0f, iOffsety2 = 0.0f, iOffsetx3 = 0.0f, iOffsetx4 = 0.0f;


int flag = 1;
int flag1 = 1, flag3 = 1, flagp = 1, flagp1 = 1, flag4 = 1;
FILE * gpFile = NULL;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{

	int initialize(void);

	void display(void);
	void Update();
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("Dynamic India");
	WNDCLASSEX wndclass;

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);


	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName, TEXT("Dynamic India"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 100, 100, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetPixel Format failed");
		DestroyWindow(0);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext failed");
		DestroyWindow(0);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent failed");
		DestroyWindow(0);
	}
	else
	{
		fprintf(gpFile, "Initialize Function succeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

		}
		else
		{
			/*if (gbActiveWindow == true)
			{
				Update();
			}*/
			display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void ToggleFullScreen();
	void uninitialize();
	
	switch (iMsg)
	{
	case WM_CREATE:
		PlaySound(MAKEINTRESOURCE(MY_SONG), NULL, SND_ASYNC|SND_RESOURCE);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			/*case 0x46:
				if (bIsFullScreen == false)
				{
					ToggleFullScreen();
				}
				else
				{
					ToggleFullScreen();
				}
				break;*/
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return 0;
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bIsFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(gHwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
		bIsFullScreen = true;
	}

	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);

		ShowCursor(true);
		bIsFullScreen = false;
	}
}

int initialize()
{
	void resize(int, int);
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatDescriptor;


	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;

	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	gHdc = GetDC(gHwnd);
	iPixelFormatDescriptor = ChoosePixelFormat(gHdc, &pfd);

	if (iPixelFormatDescriptor == 0)
	{
		return -1;
	}
	if (SetPixelFormat(gHdc, iPixelFormatDescriptor, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(gHdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return -4;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	ToggleFullScreen();

	return 0;
}


void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


}

void display()
{

	static GLfloat angle1 = 270.0f;
	static GLfloat angle2 = 270.0f;
	static GLfloat angle3 = 360.0f;
	static GLfloat angle4 = 0.0f;

	static GLfloat anglel5 = 270.0f;
	static GLfloat anglel6 = 0.0f;
	static GLfloat angle6 = 270.0f;
	static GLfloat anglel7 = 360.0f;
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();




	glLineWidth(3.0f);



	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.80f + iOffseti1, 0.0f, -3.0f);



	if (iOffseti1 <= 1.2f)
	{
		iOffseti1 = iOffseti1 + 0.00025f;


	}
	//I
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.70f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.72f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.72f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.70f, -0.80f, 0.0f);

	glEnd();





	//N

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.38f, 5.6f + iOffsetn, -3.0f);

	if (iOffsetn >= -5.6f)
	{
		iOffsetn = iOffsetn - 0.00025f;

	}


	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.52f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.50f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.50f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.52f, -0.80f, 0.0f);

	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.50f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.48f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.26f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.28f, -0.80f, 0.0f);

	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.28f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(-0.26f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.26f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(-0.28f, -0.80f, 0.0f);

	glEnd();



	//D

	if (flagd == 2)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.17f, 0.0f, -3.0f);

		if (iColoror <= 1.0f&&iColorog <= 0.647058823529f)
		{
			iColoror = iColoror + 0.0005;
			iColorog = iColorog + 0.0003;
		}
		else
		{
			flagpp = 2;
		}
		if (iColorgr <= 0.0705882352911f&&iColorgg <= 0.5333333f)
		{
			iColorgr = iColorgr + 0.0001;
			iColorgg = iColorgg + 0.0015;
		}
		


		glBegin(GL_QUADS);
		glColor3f(0.0f + iColoror, 0.0f + iColorog, 0.0f);
		glVertex3f(-0.16f, 0.78f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0f + iColorog, 0.0f);
		glVertex3f(-0.14f, 0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.14f, -0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.16f, -0.78f, 0.0f);




		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(-0.16f, 0.78f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(-0.16f, 0.80f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(0.14f, 0.80f, 0.0f);
		glColor3f(0.0f + iColoror, 0.0 + iColorog, 0.0f);
		glVertex3f(0.14f, 0.78f, 0.0f);

		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.16f, -0.80f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(-0.16f, -0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(0.14f, -0.78f, 0.0f);
		glColor3f(0.10f + iColorgr, 0.0f + iColorgg, 0.0f);
		glVertex3f(0.14f, -0.80f, 0.0f);


		glColor3f(0.0 + iColoror, -0.0 + iColorog, 0.0f);
		glVertex3f(0.14f, 0.78f, 0.0f);
		glColor3f(0.0f + iColoror, -0.0 + iColorog, 0.0f);
		glVertex3f(0.12f, 0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, -0.0f + iColorgg, 0.0f);
		glVertex3f(0.12f, -0.78f, 0.0f);
		glColor3f(0.0f + iColorgr, -0.0f + iColorgg, 0.0f);
		glVertex3f(0.14f, -0.78f, 0.0f);


		glEnd();



	}


	//I

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.002f, -7.7f + iOffseti, -3.0f);

	if (iOffseti <= 7.7f)
	{
		iOffseti = iOffseti + 0.00025f;

	}
	else
	{
		flagd = 2;
	}


	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.20f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.22f, 0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.22f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.20f, -0.80f, 0.0f);

	glEnd();



	//A

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(3.8f - iOffseta, 0.0f, -3.0f);

	if (iOffseta <= 3.6f)
	{
		iOffseta = iOffseta + 0.00025f;

	}

	glBegin(GL_QUADS);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.30f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.32f, -0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.52f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.50f, 0.80f, 0.0f);

	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.82f, -0.80f, 0.0f);
	glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
	glVertex3f(0.84f, -0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.54f, 0.80f, 0.0f);
	glColor3f(1.0f, 0.647058823529f, 0.0f);
	glVertex3f(0.52f, 0.80f, 0.0f);

	glEnd();

	//Three Lines for A
	if (flagf == 3)
	{
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.647058823529f, 0.0f);
		glVertex3f(0.42f, -0.02f, 0.0f);
		glVertex3f(0.68, -0.02f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.42f, 0.0f, 0.0f);
		glVertex3f(0.68f, 0.0f, 0.0f);


		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(0.42f, 0.02f, 0.0f);
		glVertex3f(0.68f, 0.02f, 0.0f);
		glEnd();
	}

	//Plane
	//-----------------------------------------------------------------------------------------------------------------------


	if (flagpp == 2)
	{


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();


		glTranslatef(-0.5f - cos(angle), 1.0f - sin(angle), -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (angle <= M_PI_2 * RADIUS)
		{
			glColor3f(0.729411f, 0.8627f, 0.933f);
			angle = angle + 0.0007f;
		}
		else
		{
			glColor3f(0.0f, 0.0f, 0.0f);
			glTranslatef(0.0f, 0.0f, 10.0f);

		}

		glRotatef(angle1, 0.0f, 0.0f, 1.0f);

		if (angle1 <= 360.0f)
		{
			angle1 = angle1 + 0.05f;

			if (angle1 == 360.0f)
			{
				angle1 = 361.0f;

			}

		}






		//Apex

		glBegin(GL_TRIANGLES);
		glVertex3f(0.60f + iOffsetx, 0.0f, 0.0f);
		glVertex3f(0.50f + iOffsetx, -0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx, 0.20f, 0.0f);
		glEnd();

		//Before IAF

		glBegin(GL_QUADS);
		glVertex3f(0.50f + iOffsetx, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx, 0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx, 0.20f, 0.0f);
		glEnd();

		//One where I write IAF
		glBegin(GL_QUADS);
		glVertex3f(-0.10f + iOffsetx, 0.15f, 0.0f);
		glVertex3f(-0.10f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.65f, 0.12f, 0.0f);
		glVertex3f(-0.65f, -0.12f, 0.0f);

		glVertex3f(-0.50f, -0.12f, 0.0f);
		glVertex3f(-0.40f, 0.12f, 0.0f);

		glVertex3f(-0.40f, 0.12f, 0.0f);
		glVertex3f(-0.30f, -0.12f, 0.0f);

		glVertex3f(-0.45f, 0.02f, 0.0f);
		glVertex3f(-0.35f, 0.02f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.15f, -0.12f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.04f, 0.12f, 0.0f);

		glVertex3f(-0.15f, 0.04f, 0.0f);
		glVertex3f(-0.06f, 0.04f, 0.0f);

		glEnd();

		//BiggerWings
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx, 0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, 0.15f, 0.0f);
		glEnd();


		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx, -0.15f, 0.0f);
		glEnd();


		//SmallerWings

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx, 0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx, -0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx, -0.15f, 0.0f);
		glEnd();


		//Heat Furnace

		glBegin(GL_LINES);
		glVertex3f(-1.25f + iOffsetx, 0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx, 0.09f, 0.0f);

		glVertex3f(-1.25f + iOffsetx, -0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx, -0.09f, 0.0f);


		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(-1.25f, 0.06f, 0.0f);
		glVertex3f(-1.50f, 0.06f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.25f, 0.0f, 0.0f);
		glVertex3f(-1.50f, 0.0f, 0.0f);



		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(-1.25f, -0.06f, 0.0f);
		glVertex3f(-1.50f, -0.06f, 0.0f);

		glEnd();



		//-------------------------------------------------------------------------------------------------------

		//ThreeLines
		/*glLineWidth(3.0f);
		glTranslatef(-0.5f - cos(anglel6), 1.0f - sin(anglel6), -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (angle <= M_PI_2 * RADIUS)
		{
			glColor3f(0.729411f, 0.8627f, 0.933f);
			anglel6 = anglel6 + 0.0007f;
		}
		else
		{
			glColor3f(0.0f, 0.0f, 0.0f);
			glTranslatef(0.0f, 0.0f, 10.0f);

		}

		glRotatef(angle6, 0.0f, 0.0f, 1.0f);

		if (angle6 <= 360.0f)
		{
			angle6 = angle6 + 0.05f;

			if (angle6 == 360.0f)
			{
				angle6 = 361.0f;

			}

		}*/

			

		
		//----------------------------------------------------------------------------------------------------------------------


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.5f - cos(angley), -1.0f + sin(angley), -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (angley <= M_PI_2 * RADIUS)
		{
			glColor3f(0.729411f, 0.8627f, 0.933f);
			angley = angley + 0.0007f;
		}
		else
		{
			glColor3f(0.0f, 0.0f, 0.0f);
			glTranslatef(0.0f, 0.0f, 10.0f);
		}



		glRotatef(angle2, 0.0f, 0.0f, -1.0f);

		if (angle2 <= 360.0f)
		{
			angle2 = angle2 + 0.05f;

			if (angle2 == 360.0f)
			{
				angle2 = 360.1f;
			}
		}

		//Apex
		glBegin(GL_TRIANGLES);
		glVertex3f(0.60f + iOffsetx2, 0.0f, 0.0f);
		glVertex3f(0.50f + iOffsetx2, -0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx2, 0.20f, 0.0f);
		glEnd();

		//Before IAF

		glBegin(GL_QUADS);
		glVertex3f(0.50f + iOffsetx2, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx2, -0.20f, 0.0f);
		glVertex3f(-0.10f + iOffsetx2, 0.20f, 0.0f);
		glVertex3f(0.50f + iOffsetx2, 0.20f, 0.0f);
		glEnd();

		//One where I write IAF
		glBegin(GL_QUADS);
		glVertex3f(-0.10f + iOffsetx2, 0.15f, 0.0f);
		glVertex3f(-0.10f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-1.25f + iOffsetx2, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.65f, 0.12f, 0.0f);
		glVertex3f(-0.65f, -0.12f, 0.0f);

		glVertex3f(-0.50f, -0.12f, 0.0f);
		glVertex3f(-0.40f, 0.12f, 0.0f);

		glVertex3f(-0.40f, 0.12f, 0.0f);
		glVertex3f(-0.30f, -0.12f, 0.0f);

		glVertex3f(-0.45f, 0.02f, 0.0f);
		glVertex3f(-0.35f, 0.02f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.15f, -0.12f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.04f, 0.12f, 0.0f);

		glVertex3f(-0.15f, 0.04f, 0.0f);
		glVertex3f(-0.06f, 0.04f, 0.0f);

		glEnd();

		//BiggerWings
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx2, 0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx2, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, 0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, 0.15f, 0.0f);
		glEnd();


		glBegin(GL_QUADS);
		glVertex3f(-0.25f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-0.35f + iOffsetx2, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, -0.65f, 0.0f);
		glVertex3f(-0.55f + iOffsetx2, -0.15f, 0.0f);
		glEnd();


		//SmallerWings

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx2, 0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx2, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, 0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3f(-1.0f + iOffsetx2, -0.15f, 0.0f);
		glVertex3f(-1.05f + iOffsetx2, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, -0.45f, 0.0f);
		glVertex3f(-1.10f + iOffsetx2, -0.15f, 0.0f);
		glEnd();


		//Heat Furnace

		glBegin(GL_LINES);
		glVertex3f(-1.25f + iOffsetx2, 0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx2, 0.09f, 0.0f);

		glVertex3f(-1.25f + iOffsetx2, -0.05f, 0.0f);
		glVertex3f(-1.40f + iOffsetx2, -0.09f, 0.0f);

		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(-1.25f, 0.06f, 0.0f);
		glVertex3f(-1.50f, 0.06f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.25f, 0.0f, 0.0f);
		glVertex3f(-1.50f, 0.0f, 0.0f);



		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(-1.25f, -0.06f, 0.0f);
		glVertex3f(-1.50f, -0.06f, 0.0f);
		glEnd();


		//---------------------------------------------------------------------------------------------------------------------------------------


		//Straight Plane
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-1.5f - iOffsetx1, 0.0f, -2.0f);
		glScalef(0.2f, 0.2f, 0.2f);

		if (iOffsetx1 >= -2.25f)
		{
			iOffsetx1 = iOffsetx1 - 0.00045;

		}
		else
		{

			iOffsetx1 = iOffsetx1 - 0.00058;
			flagp = 3;
			flagp1 = 3;

		}
		//Apex
		glBegin(GL_TRIANGLES);
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glVertex3f(0.60f, 0.0f, 0.0f);
		glVertex3f(0.50f, -0.20f, 0.0f);
		glVertex3f(0.50f, 0.20f, 0.0f);
		glEnd();

		//Before IAF

		glBegin(GL_QUADS);
		glVertex3f(0.50f, -0.20f, 0.0f);
		glVertex3f(-0.10f, -0.20f, 0.0f);
		glVertex3f(-0.10f, 0.20f, 0.0f);
		glVertex3f(0.50f, 0.20f, 0.0f);
		glEnd();

		//One where I write IAF
		glBegin(GL_QUADS);
		glVertex3f(-0.10f, 0.15f, 0.0f);
		glVertex3f(-0.10f, -0.15f, 0.0f);
		glVertex3f(-1.25f, -0.15f, 0.0f);
		glVertex3f(-1.25f, 0.15f, 0.0f);
		glEnd();
		
		glBegin(GL_LINES);

		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-0.65f, 0.12f, 0.0f);
		glVertex3f(-0.65f, -0.12f, 0.0f);

		glVertex3f(-0.50f, -0.12f, 0.0f);
		glVertex3f(-0.40f, 0.12f, 0.0f);

		glVertex3f(-0.40f, 0.12f, 0.0f);
		glVertex3f(-0.30f, -0.12f, 0.0f);

		glVertex3f(-0.45f, 0.02f, 0.0f);
		glVertex3f(-0.35f, 0.02f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.15f, -0.12f, 0.0f);

		glVertex3f(-0.15f, 0.12f, 0.0f);
		glVertex3f(-0.04f, 0.12f, 0.0f);

		glVertex3f(-0.15f, 0.04f, 0.0f);
		glVertex3f(-0.06f, 0.04f, 0.0f);

		glEnd();
		//BiggerWings
		glColor3f(0.729411f, 0.8627f, 0.933f);
		glBegin(GL_QUADS);
		glVertex3f(-0.25f, 0.15f, 0.0f);
		glVertex3f(-0.35f, 0.65f, 0.0f);
		glVertex3f(-0.55f, 0.65f, 0.0f);
		glVertex3f(-0.55f, 0.15f, 0.0f);
		glEnd();


		glBegin(GL_QUADS);
		glVertex3f(-0.25f, -0.15f, 0.0f);
		glVertex3f(-0.35f, -0.65f, 0.0f);
		glVertex3f(-0.55f, -0.65f, 0.0f);
		glVertex3f(-0.55f, -0.15f, 0.0f);
		glEnd();


		//SmallerWings

		glBegin(GL_QUADS);
		glVertex3f(-1.0f, 0.15f, 0.0f);
		glVertex3f(-1.05f, 0.45f, 0.0f);
		glVertex3f(-1.10f, 0.45f, 0.0f);
		glVertex3f(-1.10f, 0.15f, 0.0f);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3f(-1.0f, -0.15f, 0.0f);
		glVertex3f(-1.05f, -0.45f, 0.0f);
		glVertex3f(-1.10f, -0.45f, 0.0f);
		glVertex3f(-1.10f, -0.15f, 0.0f);
		glEnd();


		//Heat Furnace

		glBegin(GL_LINES);
		glVertex3f(-1.25f, 0.05f, 0.0f);
		glVertex3f(-1.40f, 0.09f, 0.0f);

		glVertex3f(-1.25f, -0.05f, 0.0f);
		glVertex3f(-1.40f, -0.09f, 0.0f);
		glEnd();
		
		//ThreeLines

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		if (iOffsetxl1 >= -20.5f)
		{
			glTranslatef(-1.5f - iOffsetx1, 0.0f, -2.0f);
		}

		else
		{
			glTranslatef(0.0f, 0.0f, 5.0f);
			flagf = 3;
		}



		glScalef(0.2f, 0.2f, 0.2f);

		iOffsetxl1 = iOffsetxl1 - 0.003f;
		glBegin(GL_LINES);
		glLineWidth(50.0f);

		glColor3f(1.0f, 0.6f, 0.2f);
		glVertex3f(-1.25f, 0.06f, 0.0f);
		glVertex3f(-1.90f + iOffsetxl1, 0.06f, 0.0f);


		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.25f, 0.0f, 0.0f);
		glVertex3f(-1.90f + iOffsetxl1, 0.0f, 0.0f);



		glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
		glVertex3f(-1.25f, -0.05f, 0.0f);
		glVertex3f(-1.90f + iOffsetxl1, -0.05f, 0.0f);

		glEnd();

		//-------------------------------------------------------------------------------------------------------------------------


		if (flagp1 == 3)
		{
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();


			glTranslatef(1.8f - cos(anglez1), 0.0f + sin(anglez1), -2.0f);
			glScalef(0.2f, 0.2f, 0.2f);
			glColor3f(0.729411f, 0.8627f, 0.933f);
			if (anglez1 <= M_PI_2 * RADIUS2)
			{
				anglez1 = anglez1 + 0.0005f;
			}


			glRotatef(angle4, 0.0f, 0.0f, 1.0f);

			if (angle4 <= 180.0f)
			{
				angle4 = angle4 + 0.009f;

				if (angle4 == 180.0f)
				{
					angle4 = 181.0f;
					flag4 = 2;
				}
			}


			if (flag4 = 2)
			{
				iOffsetx4 = iOffsetx4 + 0.00098f;
			}
			else
			{
				iOffsetx4 = 0.0f;
			}


			//Apex
			glBegin(GL_TRIANGLES);
			glVertex3f(0.60f + iOffsetx4, 0.0f, 0.0f);
			glVertex3f(0.50f + iOffsetx4, -0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx4, 0.20f, 0.0f);
			glEnd();

			//Before IAF

			glBegin(GL_QUADS);
			glVertex3f(0.50f + iOffsetx4, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx4, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx4, 0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx4, 0.20f, 0.0f);
			glEnd();

			//One where I write IAF
			glBegin(GL_QUADS);
			glVertex3f(-0.10f + iOffsetx4, 0.15f, 0.0f);
			glVertex3f(-0.10f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx4, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_LINES);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-0.65f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.65f+iOffsetx4, -0.12f, 0.0f);

			glVertex3f(-0.50f+iOffsetx4, -0.12f, 0.0f);
			glVertex3f(-0.40f+iOffsetx4, 0.12f, 0.0f);

			glVertex3f(-0.40f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.30f+iOffsetx4, -0.12f, 0.0f);

			glVertex3f(-0.45f+iOffsetx4, 0.02f, 0.0f);
			glVertex3f(-0.35f+iOffsetx4, 0.02f, 0.0f);

			glVertex3f(-0.15f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.15f+iOffsetx4, -0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx4, 0.12f, 0.0f);
			glVertex3f(-0.04f+iOffsetx4, 0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx4, 0.04f, 0.0f);
			glVertex3f(-0.06f+iOffsetx4, 0.04f, 0.0f);

			glEnd();

			//BiggerWings
			glColor3f(0.729411f, 0.8627f, 0.933f);
			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx4, 0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx4, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, 0.15f, 0.0f);
			glEnd();


			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx4, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx4, -0.15f, 0.0f);
			glEnd();


			//SmallerWings

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx4, 0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx4, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx4, -0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx4, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx4, -0.15f, 0.0f);
			glEnd();


			//Heat Furnace

			glBegin(GL_LINES);
			glVertex3f(-1.25f + iOffsetx4, 0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx4, 0.09f, 0.0f);

			glVertex3f(-1.25f + iOffsetx4, -0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx4, -0.09f, 0.0f);
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(-1.25f+iOffsetx4, 0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx4, 0.06f, 0.0f);


			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-1.25f+iOffsetx4, 0.0f, 0.0f);
			glVertex3f(-1.50f+iOffsetx4, 0.0f, 0.0f);



			glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
			glVertex3f(-1.25f+iOffsetx4, -0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx4, -0.06f, 0.0f);
			glEnd();

		}

		//------------------------------------------------------------------------------------------------------------


		if (flagp == 3)
		{
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();


			glTranslatef(1.8f - cos(anglez), 0.0f - sin(anglez), -2.0f);
			glScalef(0.2f, 0.2f, 0.2f);
			glColor3f(0.729411f, 0.8627f, 0.933f);

			if (anglez <= M_PI_2 * RADIUS2)
			{
				anglez = anglez + 0.0005f;
			}


			glRotatef(angle3, 0.0f, 0.0f, 1.0f);

			if (angle3 >= 90.0f)
			{
				angle3 = angle3 - 0.009f;

				if (angle3 == 90.0f)
				{
					angle3 = 91.0f;
					flag3 = 2;
				}
			}


			if (flag3 = 2)
			{
				iOffsetx3 = iOffsetx3 + 0.00098f;
			}
			else
			{
				iOffsetx3 = 0.0f;
			}


			//Apex
			glBegin(GL_TRIANGLES);
			glVertex3f(0.60f + iOffsetx3, 0.0f, 0.0f);
			glVertex3f(0.50f + iOffsetx3, -0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx3, 0.20f, 0.0f);
			glEnd();

			//Before IAF

			glBegin(GL_QUADS);
			glVertex3f(0.50f + iOffsetx3, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx3, -0.20f, 0.0f);
			glVertex3f(-0.10f + iOffsetx3, 0.20f, 0.0f);
			glVertex3f(0.50f + iOffsetx3, 0.20f, 0.0f);
			glEnd();

			//One where I write IAF
			glBegin(GL_QUADS);
			glVertex3f(-0.10f + iOffsetx3, 0.15f, 0.0f);
			glVertex3f(-0.10f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-1.25f + iOffsetx3, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_LINES);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(-0.65f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.65f+iOffsetx3, -0.12f, 0.0f);

			glVertex3f(-0.50f+iOffsetx3, -0.12f, 0.0f);
			glVertex3f(-0.40f+iOffsetx3, 0.12f, 0.0f);

			glVertex3f(-0.40f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.30f+iOffsetx3, -0.12f, 0.0f);

			glVertex3f(-0.45f+iOffsetx3, 0.02f, 0.0f);
			glVertex3f(-0.35f+iOffsetx3, 0.02f, 0.0f);

			glVertex3f(-0.15f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.15f+iOffsetx3, -0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx3, 0.12f, 0.0f);
			glVertex3f(-0.04f+iOffsetx3, 0.12f, 0.0f);

			glVertex3f(-0.15f+iOffsetx3, 0.04f, 0.0f);
			glVertex3f(-0.06f+iOffsetx3, 0.04f, 0.0f);

			glEnd();

			//BiggerWings
			glColor3f(0.729411f, 0.8627f, 0.933f);
			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx3, 0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx3, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, 0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, 0.15f, 0.0f);
			glEnd();


			glBegin(GL_QUADS);
			glVertex3f(-0.25f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-0.35f + iOffsetx3, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, -0.65f, 0.0f);
			glVertex3f(-0.55f + iOffsetx3, -0.15f, 0.0f);
			glEnd();


			//SmallerWings

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx3, 0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx3, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, 0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, 0.15f, 0.0f);
			glEnd();

			glBegin(GL_QUADS);
			glVertex3f(-1.0f + iOffsetx3, -0.15f, 0.0f);
			glVertex3f(-1.05f + iOffsetx3, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, -0.45f, 0.0f);
			glVertex3f(-1.10f + iOffsetx3, -0.15f, 0.0f);
			glEnd();


			//Heat Furnace

			glBegin(GL_LINES);
			glVertex3f(-1.25f + iOffsetx3, 0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx3, 0.09f, 0.0f);

			glVertex3f(-1.25f + iOffsetx3, -0.05f, 0.0f);
			glVertex3f(-1.40f + iOffsetx3, -0.09f, 0.0f);
			glColor3f(1.0f, 0.6f, 0.2f);
			glVertex3f(-1.25f+iOffsetx3, 0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx3, 0.06f, 0.0f);


			glColor3f(1.0f, 1.0f, 1.0f);
			glVertex3f(-1.25f+iOffsetx3, 0.0f, 0.0f);
			glVertex3f(-1.50f+iOffsetx3, 0.0f, 0.0f);



			glColor3f(0.0705882352911f, 0.5333333f, 0.0f);
			glVertex3f(-1.25f+iOffsetx3, -0.06f, 0.0f);
			glVertex3f(-1.50f+iOffsetx3, -0.06f, 0.0f);
			glEnd();
		}
	}
	SwapBuffers(gHdc);
}




/*void Update()
{
	if (iOffsetx >= 0.25f)
	{
		iOffsetx = iOffsetx + 0.05f;
	}
}*/

void uninitialize()
{
	if (bIsFullScreen == true) {
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Log File closed successfully");
		fclose(gpFile);
		gpFile = NULL;
	}

}



